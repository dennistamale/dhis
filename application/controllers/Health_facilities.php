<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 17/01/2018
 * Time: 17:03||2000 & 10000
 */

class health_facilities extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('hf_model');
    }


    function get_project_regions($id = null)
    {
        $regions = $this->hf_model->get_project_regions($id);
        $option = count($regions) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';
        foreach ($regions as $sc) {

            $option .= '<option value="' . $sc->id . '">' . $sc->region . '</option>';

        }

        echo $option;

    }


    function get_district()
    {

        $result = $this->hf_model->get_hf_districts();

        $option = count($result) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';

        foreach ($result as $sc) {

            $option .= '<option value="' . $sc . '">' . $sc . '</option>';

        }

        echo $option;


    }



    function get_county($district = null)
    {

        $result = isset($district) ? $this->hf_model->get_county($district) : $this->hf_model->get_county();


        $option = count($result) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';

        foreach ($result as $sc) {

            $option .= '<option value="' . $sc->county . '">' . $sc->county . '</option>';

        }

        echo $option;


    }


    function get_sub_county($county = null)
    {

        $result = isset($county) ? $this->hf_model->get_sub_county($county) : $this->hf_model->get_sub_county();


        $option = count($result) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';

        foreach ($result as $sc) {

            $option .= '<option value="' . $sc->sub_county . '">' . $sc->sub_county . '</option>';

        }

        echo $option;


    }


    function get_parish($sub_county = null)
    {


        $result = isset($sub_county) ? $this->hf_model->get_parish($sub_county) : $this->hf_model->get_parish();

        $option = count($result) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';

        foreach ($result as $sc) {

            $option .= '<option value="' . $sc->parish . '">' . $sc->parish . '</option>';

        }

        echo $option;


    }

    function get_health_facilities($parish = null)
    {

        $result = isset($parish) ? $this->hf_model->get_health_facilities($parish) : $this->hf_model->get_health_facilities();


        $option = count($result) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';

        foreach ($result as $sc) {

            $option .= '<option value="' . $sc->id . '">' . $sc->health_unit . '-' . $sc->level . '-' . $sc->owner . '</option>';

        }

        echo $option;


    }



    function get_district_health_facilities($district = null)
    {

        $result = isset($district) ? $this->hf_model->get_district_health_facilities($district) : $this->hf_model->get_health_facilities();


        $option = count($result) > 0 ? '<option value="">Select..</option>' : '<option value="">No Results..</option>';

        foreach ($result as $sc) {

            $option .= '<option value="' . $sc->id . '">' . $sc->health_unit . '-' . $sc->level . '-' . $sc->owner . '</option>';

        }

        echo $option;


    }


    function get_single_hf($id = null)
    {
        $output = false;

        if (isset($id)) {

            $output = $this->hf_model->get_single_hf($id);
        }

        return $output;
    }



    function get_hf_single_item($id){


       $hf=$this->get_single_hf($id);

      echo $hf->level;

    }

}