<?php

class migrate extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        // load migration library
        $this->load->library('migration');

    }

    public function index()
    {
        $this->latest();
    }

    public function current()
    {
        if (!$this->migration->current()) {
            echo 'Error' . $this->migration->error_string();
        } else {
            echo 'Migrations ran successfully!';
        }
    }


    public function latest()
    {
        if (!$this->migration->latest()) {
            echo 'Error' . $this->migration->error_string();
        } else {
            echo 'Migrations ran successfully!';
        }
    }

    function find_migrations()
    {
        $migrations = $this->migration->find_migrations();
        echo '<ol>';


        foreach ($migrations as $m) {

//            echo "<li>". anchor('#',$m,'title="Click on the link to go to that Version"')  ."</li>";
            echo "<li>$m</li>";
        }


        echo '</ol>';
    }

    function version($version)
    {

        if (!$this->migration->version($version)) {
            echo 'Error' . $this->migration->error_string();
        } else {
            echo 'Migrations ran successfully! ';
        }

    }

}

?>