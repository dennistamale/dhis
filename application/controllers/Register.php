<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller
{

    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";


    function __construct()
    {
        parent::__construct();
        $this->page_level = $this->uri->slash_segment(1);
        $this->page_level2 = $this->uri->slash_segment(2);

    }

    public function index($page = null)
    {

        $this->register();


    }


    function register(){
        $data['title'] = 'register';
        $data['subtitle'] = 'register';
        $this->load->view('login/header',$data);


        $this->form_validation
            ->set_rules('first_name', 'First Name', 'trim|required|min_length[3]')
            ->set_rules('last_name', 'Last Name', 'trim|required|min_length[3]')
            ->set_rules('national_id', 'National ID', 'trim|required|min_length[15]|is_unique[users.national_id]',array('is_unique' => 'The %s is already Registered with us.'))
            ->set_rules('email', 'Email Address', 'callback_email_check|trim|valid_email|required')
            ->set_rules('username', 'Username', 'trim|is_unique[users.username]',array('is_unique' => 'The %s is already Registered with us.'))
            ->set_rules('phone1', 'Phone N01', 'trim|required|min_length[10]|callback_phone_check|max_length[13]|is_unique[users.phone]',array('is_unique' => 'The %s is already Registered with us.'))
            ->set_rules('phone2', 'Phone NO2', 'trim|min_length[10]|max_length[13]')
            ->set_rules('password', 'Password', 'trim|required|matches[repeat_password]')
            ->set_rules('repeat_password', 'Repeat Password', 'trim|required')
            ->set_rules('single_basic_checkbox', 'Accept', 'trim|required');


        if($this->form_validation->run()==true){

            $values=array(
                'username'=>$username=$this->input->post('username'),
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'national_id'=>$this->input->post('national_id'),
                'email'=>$this->input->post('email'),
                'phone'=>phone($this->input->post('phone1')),
                'phone2'=>phone($this->input->post('phone2')),
                'user_type'=>$this->input->post('account_type'),
                'password'=>hashValue($password=$this->input->post('password'))
            );

           if($this->model->register_user($values)){
               $data=array(
                   'alert'=>'success',
                   'message'=> 'Account has been added Successfully',
                   'centered'=>true
               );
               $this->load->view('alert',$data);

               $this->model->session_assign($username, $password);
               header('Refresh: 1; url=' . base_url('index.php/login/'));
           }

        }else {

            $this->load->view('register/register', $data);
        }





        $this->load->view('login/footer',$data);
    }


    // this is the email check for the user
    public function email_check($str)
    {
        $results = $this->db->select('email')->from('users')->where('email', $str)->get()->row();

        if (isset($results->email)) {
            $this->form_validation->set_message('email_check', 'The %s already Exists');
            return false;
        }
        else {
            return true;
        }
    }


 // this is the email check for the user
    public function phone_check($str)
    {
        $results = $this->db->select('phone')->from('users')->where('phone', phone($str))->get()->row();

        if (isset($results->phone)) {
            $this->form_validation->set_message('phone_check', 'The %s already Exists');
            return false;
        }
        else {
            return true;
        }
    }




}