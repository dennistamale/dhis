<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class app extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->isloggedin() == true ? '' : $this->logout();
//        $this->output->enable_profiler(TRUE);

    }


    public function index()
    {
        $this->dashboard();
    }


    public function isloggedin()
    {
        return $this->session->userdata('user_type') == 1 ? true : false;

    }

    public function dashboard()
    {
        $this->load->model('dashboard_model');
        $this->load->model('dhis2_model');

        $data = array(
            'title' => 'dashboard',
            'subtitle' => 'Welcome',
            'link_details' => 'Account overview'
        );
        $page_level = $this->page_level;

        // $data['dennis']=$this->dennis();

        $this->form_validation->set_rules('filter', 'Filter', 'trim');
        if ($this->form_validation->run() == true) {


            $data['week_no'] = $week = $this->input->post('week_no');
            $data['year'] = $year = $this->input->post('year');

            //calculating the actual week and the year incase the year has 53 weeks
            $actual_week = $week + 1 != 53 ? $week + 1 : 1;
            $actual_year = $week + 1 != 53 ? $year : $year + 1;

            $getStartAndEndDate = getStartAndEndDate($actual_week, $actual_year);

            //outputing the days to view
            $data['fdate'] = $getStartAndEndDate[0] . ' 00:00:00';
            $data['last_day'] = $getStartAndEndDate[1] . ' 23:59:59';
            $data['subtitle'] = 'Week:' . $week . ' Year:' . $year;


        } else {
            $data['subtitle'] = 'Summaries';

            $day = date('Y-m-d');
            $date = date("Y-m-d", strtotime($day . " -1 month "));
            $data['fdate'] = $date;


        }

        $data['page_view'] = $this->load->view($page_level . 'dashboard/dashboard', $data,true);


        $this->load->view('app/static/main_page', $data);

    }

    function week_dates(){
        print_r(weeks());
    }



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */