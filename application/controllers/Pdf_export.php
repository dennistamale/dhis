<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//define('HEAD', 'Head');
define('SUBHEAD', 'Print Date : ' . date('d-m-Y H:i:s'));
define('SKIP_HEADER_FOOTER', 0);
define('LOGO', 'uploads/site_logo/coart-of-arms.png');
//define('ORIENTATION',isset($orientation)?$orientation:'P');
define('SUBHEADER', 'Dennis');

//error_reporting(0);
class Pdf_export extends CI_Controller
{


    public $logo = '';
    public $orientation = "";

    public $page_level = "";
    public $page_level2 = "";
    public $page_level3 = "";
    public $sub_head = "";
    public $location_title = null;
    private $acronym = 'PMIS';
    private $site_name='';


    function __construct()
    {
        parent::__construct();
        $this->isloggedin() == true ? '' : $this->invalid();
        $this->load->library('fpdf_gen');

        $this->page_level = $this->uri->segment(1);
        $this->page_level2 = $this->uri->segment(2);
        $this->page_level3 = $this->location_title = $this->uri->segment(3);
        $this->site_name=$this->site_options->title('site_name');

//            $this->logo = base_url($this->site_options->title('site_logo'));


        $this->page_level2=='distribution_report_forms'? $this->fpdf->AddPage('L'): $this->fpdf->AddPage();
       //$this->page_level2!='distribution_report_forms'? $this->cover_page():'';

        //$this->fpdf->AliasNbPages();

        // $this->fpdf->AddPage();


    }

    public function isloggedin()
    {
        return strlen($this->session->user_type) > 0 ? true : false;

    }

    function invalid()
    {

        echo "You can't access this report Contact administrator";
        exit;
    }

    function cover_page()
    {

        $vill = $this->location_title;
        $this->fpdf->SetFont('Arial', 'B', 50);


        $this->fpdf->Cell(0, 50, $this->acronym, 0, 0, 'C');
        $this->fpdf->Ln();
        $this->fpdf->Cell(0, 5, humanize($this->page_level2 == 'distribution_report' ? 'Allocation' : $this->page_level2), 0, 0, 'C');
        $this->fpdf->Ln(25);
        $this->fpdf->Cell(0, 5, humanize($this->page_level2 == 'distribution_report' ? 'List' : 'Report'), 0, 0, 'C');
        $this->fpdf->Ln(60);
        $this->fpdf->Image(LOGO, 90, 110, 30);
        $this->fpdf->SetFont('Arial', 'B', 15);

        //is_int($vill) ? ($this->fpdf->Cell(0, 5, humanize((isset($vill) ? $this->locations->get_location_type($vill) : 'Location') . ' : ' . (isset($vill) ? $this->locations->get_location_name($vill) : 'All Data')), 0, 0, 'C')) : $this->fpdf->Cell(0, 5, humanize($this->locations->get_location_name($vill)), 0, 0, 'C');

        $this->page_level2 == 'distribution_report' ? '' : $this->fpdf->AddPage();


    }

    public function index()
    {

        $this->orientation = 'L';

        $this->fpdf->SetFont('Arial', 'B', 16);
        $this->fpdf->Cell(40, 10, 'Sorry You Did not select any pdf to Export !!!');


        //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
        echo $this->fpdf->Output('PMISPdf.pdf', 'D');
//        $this->fpdf->AddPage();
//		echo $this->fpdf->Output('hello_world.pdf','I');
    }

    public function access_permit($id=null)
    {


        $this->orientation = 'L';
        $this->fpdf->AliasNbPages();

        //This is the header of the application letter
        $this->fpdf->SetFont('Arial', 'B', 12);
        $this->fpdf->Cell(40, 10, 'S/No..................................');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('', 'BU');
        $this->fpdf->Cell('', 10, 'APPLICATION FOR AIRPORT ACCESS PERMIT (DIPLOMATIC MISSIONS)','','','C');
        $this->fpdf->Ln();

        //this

        if(isset($id)) {
            $id = str_rot13($id);

            $table_name = 'access_permit';

            $r = $this->db->select()->from($table_name)->where(array('request_code' => $id))->get()->row();
            $space_btn_line = 10;


            if(!empty($r)){
            //subheading
            $this->fpdf->Cell(50, 10, '1.   APPLICANT\'S PARTICULARS:', 0, 0, 'L');
            $this->fpdf->Ln();

            $this->fpdf->writeHTML("<b> Full Name : </b>" . $r->first_name . " " . $r->last_name);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Diplomatic Mission : </b>" . $this->model->get_mission($r->diplomatic_mission));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Nationality : </b>" . $this->model->get_nationality($r->nationality));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Designation : </b>" . $r->designation);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Passport No : </b>" . $r->passport_no);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Work Permit No : </b>" . $r->work_permit_no);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Date of issue : </b>" . trending_date($r->date_of_issue));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Date of Expiry : </b>" . trending_date($r->date_of_expiry));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Place of first entry into Uganda : </b>" . $r->first_entry_in_ug . "<b> Date : </b>" . trending_date($r->date_of_entry));
            $this->fpdf->Ln($space_btn_line);

            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, '2. DECLARATION:', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');

            $this->fpdf->writeHTML("I ...................................................... certify that the information I have provided in the application form is correct and complete to the best of my knowledge.  I understand that any deliberate false statement or omission may render me liable to disciplinary action which may include denial of facilitation at the airport.", 7);
            $this->fpdf->Ln($space_btn_line);


            $this->fpdf->Ln(12);
            $this->fpdf->writeHTML("Signature :...............................Date:.............................");
            $this->fpdf->Ln($space_btn_line);


            //subheading
            $this->fpdf->AddPage();
            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->Cell(50, 10, '3. DIPLOMATIC MISSION', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');

            $this->fpdf->writeHTML("Name of Mission : .........................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("Head of Mission : .........................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("Nationality : ...................................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("Telephone No : .............................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("Signature and stamp : ..................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("Date : .............................................................................................................................................");
            $this->fpdf->Ln($space_btn_line);


            //subheading
            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, '4.  RECOMMENDATION', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');

            $this->fpdf->writeHTML("(i)  Chief of protocol, Ministry of Foreign Affairs (MOFA) clearance:");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML(".......................................................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML(".......................................................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML(".......................................................................................................................................................");
            $this->fpdf->Ln($space_btn_line);


            $this->fpdf->writeHTML("(ii) Chairman Vetting Committee: ");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("(a) Permit type :......................................................Colour Code :...............................................");
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("(b) Signature :.................................................................Date :...........................................................");
            $this->fpdf->Ln($space_btn_line);

            //subheading
            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, '5. ISSUING AUTHORITY', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');
            $this->fpdf->writeHTML("Signature :.................................................................Date :...........................................................");
            $this->fpdf->Ln($space_btn_line);
        }else{
                $this->fpdf->SetFont('Arial','I',14);
                $this->fpdf->SetTextColor(255,0,0);
                $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
                $this->fpdf->SetTextColor(128);
            }


        }else {

            $this->fpdf->SetFont('Arial','I',14);
            $this->fpdf->SetTextColor(255,0,0);
            $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
            $this->fpdf->SetTextColor(128);
        }




        //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
        $this->fpdf->Output($this->site_name.'-'.camelize($this->page_level2).'-PDF.pdf', 'D');

    }

    public function tax_clearance($id=null)
    {


        $this->orientation = 'L';
        $this->fpdf->AliasNbPages();

        //This is the header of the application letter

        $this->fpdf->SetFont('', 'BU');
        $this->fpdf->Cell('', 10, 'APPLICATION FOR TAX CLEARANCE','','','C');
        $this->fpdf->Ln();

        //this

        if(isset($id)) {
            $id = str_rot13($id);

            $table_name = 'tax_clearance';

            $r = $this->db->select()->from($table_name)->where(array('request_code' => $id))->get()->row();
            $space_btn_line = 10;


            if(!empty($r)){
            //subheading
            $this->fpdf->Cell(50, 10, '1.   APPLICANT\'S PARTICULARS:', 0, 0, 'L');
            $this->fpdf->Ln();

            $this->fpdf->writeHTML("<b> Full Name : </b>" . $r->first_name . " " . $r->last_name);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Nationality : </b>" . $this->model->get_nationality($r->nationality));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Passport No : </b>" . $r->passport_no);
            $this->fpdf->Ln($space_btn_line);


//            These are the items table

                $this->tax_clearance_items($id);

//                this is the end of items table


                $this->fpdf->SetFont('Arial', 'B', 10);

                $this->fpdf->writeHTML("Name of Carrier : $r->name_of_carrier");
                $this->fpdf->Ln($space_btn_line);
                $this->fpdf->writeHTML("Name of consignee : $r->consignee_first_name  $r->consignee_last_name      Title : $r->title");

                $this->fpdf->Ln($space_btn_line);

            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, '2. DECLARATION:', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');

            $this->fpdf->writeHTML("I ...................................................... certify that the information I have provided in the application form is correct and complete to the best of my knowledge.  I understand that any deliberate false statement or omission may render me liable to disciplinary action which may include denial of facilitation at the airport.", 7);
            $this->fpdf->Ln($space_btn_line);


            $this->fpdf->Ln(12);
            $this->fpdf->writeHTML("Signature :...............................Date:.............................");
            $this->fpdf->Ln($space_btn_line);

        }else{
                $this->fpdf->SetFont('Arial','I',14);
                $this->fpdf->SetTextColor(255,0,0);
                $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
                $this->fpdf->SetTextColor(128);
            }


        }else {

            $this->fpdf->SetFont('Arial','I',14);
            $this->fpdf->SetTextColor(255,0,0);
            $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
            $this->fpdf->SetTextColor(128);
        }




        //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
        $this->fpdf->Output($this->site_name.'-'.camelize($this->page_level2).'-PDF.pdf', 'D');

    }


    public function tax_clearance_items($request_code){


// Line break
        $this->fpdf->Ln(1);
        $header = array('#', 'Description', 'Quantity', 'Other Details', 'Official use');

        $this->fpdf->SetFillColor(152, 152, 152);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetDrawColor(240, 240, 245);
        $this->fpdf->SetLineWidth(.3);
        $this->fpdf->SetFont('Arial', 'B', 10);

// Header
        $w = array(8, 55, 20, 50, 50);
        for ($i = 0; $i < count($header); $i++)
            $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
        $this->fpdf->Ln();

// Color and font restoration
        $this->fpdf->SetFillColor(240, 240, 245);
        $this->fpdf->SetTextColor(0);

        $this->fpdf->SetFont('Arial', '', 8);

// Data
        $fill = false;
        $no = 1;
        $id = $this->db->select()->from('tax_clearance_items')->where(array('request_code'=>$request_code))->get()->result();
        foreach ($id as $d) {

            $this->fpdf->Cell($w[0], 10, $no . '.', 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[1], 10, humanize($d->description ), 0, 0, 'L', $fill);
            $this->fpdf->Cell($w[2], 10, $d->quantity, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[3], 10, $d->other_details, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[4], 10, '', 1, 0, 'L', $fill);
            $this->fpdf->Ln();
            $fill = !$fill;
            $no++;
        }

// Closing line
        $this->fpdf->Cell(array_sum($w), 0, '', 'T');

    }

    public function diplomatic_id($id=null)
    {


        $this->orientation = 'L';
        $this->fpdf->AliasNbPages();

        //This is the header of the application letter


        $this->fpdf->SetFont('', 'BU');
        $this->fpdf->Cell('', 10, 'APPLICATION FOR DIPLOMATIC ID','','','C');
        $this->fpdf->Ln();

        //this

        if(isset($id)) {
            $id = str_rot13($id);

            $table_name = 'diplomatic_id';

            $r = $this->db->select()->from($table_name)->where(array('request_code' => $id))->get()->row();
            $space_btn_line = 9;


            if(!empty($r)){
            //subheading
            $this->fpdf->Cell(50, 10, 'APPLICANT\'S PARTICULARS:', 0, 0, 'L');
            $this->fpdf->Ln();


            $this->fpdf->writeHTML("<b> Name of Mission/Int. Organization : </b>" . $this->model->get_mission($r->diplomatic_mission));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Surname of Applicant : </b>" . $r->last_name);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> First Name : </b>" . $r->first_name );
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Place  of Birth : </b>" . $r->birth_place);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Date of Birth : </b>" . $r->birth_date);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Passport No : </b>" . $r->passport_no);
            $this->fpdf->Ln($space_btn_line);
                $this->fpdf->writeHTML("<b> Type of Passport : </b>" . $r->passport_type);
                $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Place of Issue : </b>" . $r->place_of_issue);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Date of Issue : </b>" . $r->date_of_issue);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Nationality : </b>" . $this->model->get_nationality($r->nationality));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Rank and Function in Mission : </b>" . $r->rf_mission);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Home / Physical Address in Uganda : </b>" . $r->physical_address);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Telephone No. 	(Office) : </b>" . $r->phone);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Date of arrival in Uganda : </b>" . date('d-m-Y',$r->date_of_arrival) );
            $this->fpdf->Ln($space_btn_line);

            $this->fpdf->Ln(10);
            $this->fpdf->writeHTML(".........................................                     ...................................................................");
            $this->fpdf->Ln(5);
            $this->fpdf->writeHTML("Signature of Applicant                          Signature of Authorizing Officer in Mission");
                $this->fpdf->Ln(12);
                $this->fpdf->writeHTML("Signature :......................................                Mission Stamp:");
                $this->fpdf->Ln($space_btn_line);


                $this->fpdf->writeHTML("____________________________________________________________________________");
                $this->fpdf->Ln($space_btn_line);






            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, '5. OFFICIAL USE ONLY', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');
            $this->fpdf->writeHTML("Chief of Protocol, Ministry of Foreign Affairs :..................................................................................");
            $this->fpdf->Ln($space_btn_line);

            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, '5. ISSUING OFFICER', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');
            $this->fpdf->writeHTML("Type of card to be issued :..................................................................................................................");
            $this->fpdf->Ln($space_btn_line);
                $this->fpdf->writeHTML("Signature :......................................................................Date :...........................................................");



        }else{
                $this->fpdf->SetFont('Arial','I',14);
                $this->fpdf->SetTextColor(255,0,0);
                $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
                $this->fpdf->SetTextColor(128);
            }


        }else {

            $this->fpdf->SetFont('Arial','I',14);
            $this->fpdf->SetTextColor(255,0,0);
            $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
            $this->fpdf->SetTextColor(128);
        }




        //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
        $this->fpdf->Output($this->site_name.'-'.camelize($this->page_level2).'-PDF.pdf', 'D');

    }


    public function diplomatic_plate($id=null)
    {


        $this->orientation = 'L';
        $this->fpdf->AliasNbPages();

        //This is the header of the application letter
        $this->fpdf->SetFont('Arial', 'B', 12);

        $this->fpdf->Cell('', 10, 'APPLICATION FOR DIPLOMATIC PLATE','','','C');
        $this->fpdf->Ln();

        $this->fpdf->SetFont('Arial', 'B', 8);
        $this->fpdf->Cell(80, 10, 'In any correspondence on this subject please quote no:','','','L');
        $this->fpdf->SetFont('Arial', 'B', 12);
        $this->fpdf->Cell(30, 12, str_rot13($id),'','','');
        $this->fpdf->Ln();

        //this
        $this->fpdf->SetFont('Arial', 'B', 12);
        if(isset($id)) {
            $id = str_rot13($id);

            $table_name = 'diplomatic_plate';

            $r = $this->db->select()->from($table_name)->where(array('request_code' => $id))->get()->row();
            $space_btn_line = 10;


            if(!empty($r)){
                //subheading
                $this->fpdf->Cell(50, 10,  date('d M,Y'), 0, 0, 'L');
                $this->fpdf->Ln();
                $this->fpdf->Ln();
                $this->fpdf->SetFont('', '');
                $this->fpdf->Cell(50, 10, 'The Commissioner General', 0, 0, 'L');
                $this->fpdf->Ln(5);
                $this->fpdf->Cell(50, 10, 'Uganda Revenue Authority', 0, 0, 'L');
                $this->fpdf->Ln(5);
                $this->fpdf->SetFont('', 'B');
                $this->fpdf->Cell(50, 10, 'KAMPALA', 0, 0, 'L');
                $this->fpdf->Ln();
                $this->fpdf->Ln();
                $this->fpdf->Cell(50, 10, 'REGISTRATION OF A MOTOR VEHICLE', 0, 0, 'L');

                $this->fpdf->Ln(12);

                $this->fpdf->SetFont('Arial');

                $this->fpdf->writeHTML("This is to request you to register and allocate red number plates UAA ..Z series to a motor vehicle belonging to the United Nations Development Programme (UNDP) with the following particulars :-", 7);
                $this->fpdf->Ln($space_btn_line);

                $this->fpdf->writeHTML("<b> Make                  : </b>" . $r->make);
                $this->fpdf->Ln(5);

                $this->fpdf->writeHTML("<b> Chassis No       : </b>" . $r->chassis_no);

                $this->fpdf->Ln(5);
                $this->fpdf->writeHTML("<b> Engine No         : </b>" . $r->engine_no);
                $this->fpdf->Ln(15);

                $this->fpdf->writeHTML('Allan Ndagije Mugarura');
                $this->fpdf->Ln(5);
                $this->fpdf->writeHTML("<b>FOR: PERMANENT SECRETARY</b>");
                $this->fpdf->Ln($space_btn_line);



                $this->fpdf->Ln(12);
                $this->fpdf->writeHTML("Signature :...............................Date:.............................");
                $this->fpdf->Ln($space_btn_line);



            }else{
                $this->fpdf->SetFont('Arial','I',14);
                $this->fpdf->SetTextColor(255,0,0);
                $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
                $this->fpdf->SetTextColor(128);
            }


        }else {

            $this->fpdf->SetFont('Arial','I',14);
            $this->fpdf->SetTextColor(255,0,0);
            $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
            $this->fpdf->SetTextColor(128);
        }




        //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
        $this->fpdf->Output($this->site_name.'-'.camelize($this->page_level2).'-PDF.pdf', 'D');

    }




    public function collection_form($id=null)
    {


        $this->orientation = 'L';
        $this->fpdf->AliasNbPages();
        $this->second_heading_line='Dennis';

        //This is the header of the application letter
        $this->fpdf->SetFont('Arial', 'B', 12);
//        $this->fpdf->Cell(40, 10, 'S/No..................................');
//        $this->fpdf->Ln();


        //this

        if(isset($id)) {
            $id=str_rot13($id);

           //$this->fpdf->second_heading_line('Dennis');


            $app = $this->db->select('a.*,b.title,b.request_page,c.first_name as coll_first_name,c.last_name as coll_last_name,c.date_picked,c.phone as coll_phone, c.email as coll_email')
                ->from('applications a')
                ->join('request_type b', 'a.request_type=b.id')
                ->join('collection_form c', 'a.id=c.application_id', 'left')
                ->where(array('request_code' => $id))->get()->row();


           if(!empty($app)){


            $table_name = $app->request_page;

            $r = $this->db->select()->from($table_name)->where(array('request_code' => $id))->get()->row();
            $space_btn_line = 10;


            $this->fpdf->SetFont('', 'B');
            $this->fpdf->Cell('', 10, strtoupper($app->title).' COLLECTION FORM ', '', '', 'C');
            $this->fpdf->Ln();

            //subheading
               $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, 'APPLICANT\'S PARTICULARS:', 0, 0, 'L');
            $this->fpdf->Ln();

            $this->fpdf->writeHTML("<b> Full Name : </b>" . $r->first_name . " " . $r->last_name);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Passport No : </b>" . $r->passport_no);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Application Code : </b>" . $id);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Application Type : </b>" . $app->title);
            $this->fpdf->Ln(13);

            //THIS HEADING FOR THE COLLECTION DETAILS
            $this->fpdf->SetFont('', 'BU');
            $this->fpdf->Cell(50, 10, 'COLLECTOR\'S PARTICULARS:', 0, 0, 'L');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial');


            //THIS THE THE CONTECT FOR THE COLLECTION DETAILS
            $this->fpdf->writeHTML("<b> Full Name : </b>" . $app->coll_first_name . ' ' . $app->coll_last_name);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Date Collected : </b>" . $coll_date=date('d-m-Y H:i:s',$app->date_picked));
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Contact Phone : </b>" . $app->coll_phone);
            $this->fpdf->Ln($space_btn_line);
            $this->fpdf->writeHTML("<b> Email : </b>" . $app->coll_email);
            $this->fpdf->Ln($space_btn_line);


            $this->fpdf->Ln(12);
            $this->fpdf->writeHTML("Signature :...............................Date:$coll_date");
            $this->fpdf->Ln($space_btn_line);


        }else{

               $this->fpdf->SetFont('Arial','I',14);
               $this->fpdf->SetTextColor(255,0,0);
               $this->fpdf->writeHTML('Opps <b>Collection Form could not be downloaded.</b>.');
               $this->fpdf->SetTextColor(128);

           }



        }else {

            $this->fpdf->SetFont('Arial','I',14);
            $this->fpdf->SetTextColor(255,0,0);
            $this->fpdf->writeHTML('Opps <b>Application could not be found.</b>.');
            $this->fpdf->SetTextColor(128);
        }




        //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
        $this->fpdf->Output($this->site_name.'-'.camelize($this->page_level2).'-PDF.pdf', 'D');

    }



    //download  for the users in the system
    function receive_form($batch_code=null)
    {

        $this->sub_head($this->page_level2);
        $this->fpdf->AliasNbPages();

        $this->fpdf->Ln(5);


        $id = $this->db->select('a.*,b.title')->from('departure a')->join('request_type b','a.item_id=b.id')
            ->where(array('batch_code'=> str_rot13($batch_code)))
            ->get()->result();
        $space_btn_line = 10;

        //subheading
        $this->fpdf->SetFont('Arial', 'BU', 12);
        $this->fpdf->Cell(50, 10, 'RECEIVED\'S BY:', 0, 0, 'L');
        $this->fpdf->Ln();

        $received=$this->model->get_user_details($id[0]->received_by);

        $this->fpdf->writeHTML("<b> Full Name : </b>" . $received->first_name . " " . $received->last_name);
        $this->fpdf->Ln($space_btn_line);






// Line break

        $this->fpdf->Ln(2);
        $header = array('#', 'Item', 'Reference', 'Owner');

        $this->fpdf->SetFillColor(152, 152, 152);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetDrawColor(240, 240, 245);
        $this->fpdf->SetLineWidth(.3);
        $this->fpdf->SetFont('Arial', 'B', 10);

// Header
        $w = array(8, 25, 35, 50, 45, 45);
        for ($i = 0; $i < count($header); $i++)
            $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
        $this->fpdf->Ln();

// Color and font restoration
        $this->fpdf->SetFillColor(240, 240, 245);
        $this->fpdf->SetTextColor(0);

        $this->fpdf->SetFont('Arial', '', 8);

// Data
        $fill = false;
        $no = 1;

        foreach ($id as $d) {


            $diplomat = $this->model->get_user_details($d->user_id);
            $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[1], 6, $d->title, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[2], 6, $d->reference, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[3], 6, humanize($diplomat->first_name . ' ' . $diplomat->last_name), 0, 0, 'L', $fill);

            $this->fpdf->Ln();
            $fill = !$fill;
            $no++;
        }

// Closing line
        $this->fpdf->Cell(array_sum($w), 0, '', 'T');


        //THIS HEADING FOR THE COLLECTION DETAILS
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial', 'BU', 12);
        $this->fpdf->Cell(50, 10, 'DELIVERED BY:', 0, 0, 'L');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial');


        //THIS THE THE CONTECT FOR THE COLLECTION DETAILS
        $this->fpdf->writeHTML("<b> Full Name : </b>" . $id[0]->delivery_first_name . ' ' .  $id[0]->delivery_last_name);
        $this->fpdf->Ln($space_btn_line);
        $this->fpdf->writeHTML("<b> Departure Date  : </b>" . $coll_date=date('d-m-Y H:i:s', $id[0]->departure_date));
        $this->fpdf->Ln($space_btn_line);
        $this->fpdf->writeHTML("<b> Departure Place  : </b>" . $id[0]->departure_place);
        $this->fpdf->Ln($space_btn_line);
        $this->fpdf->writeHTML("<b> Contact Phone : </b>" . $id[0]->delivery_phone);
        $this->fpdf->Ln($space_btn_line);
        $this->fpdf->writeHTML("<b> Email : </b>" . $id[0]->delivery_email);
        $this->fpdf->Ln($space_btn_line);


        $this->fpdf->Ln(12);
        $this->fpdf->writeHTML("Signature :...............................Date:$coll_date");
        $this->fpdf->Ln($space_btn_line);





        $file = $this->acronym.' ' . $this->page_level2 . ' export ' . time() . '.pdf';
        $D = 'D';
        $this->fpdf->Output($file, $D);
    }

    //download  for the users in the system
    function users()
    {

        $this->sub_head($this->page_level2 . ' Report');
        $this->fpdf->AliasNbPages();

// Line break
        $this->fpdf->Ln(1);
        $header = array('#', 'Name', 'Phone', 'Location', 'username', 'Email',);

        $this->fpdf->SetFillColor(152, 152, 152);
        $this->fpdf->SetTextColor(0);
        $this->fpdf->SetDrawColor(240, 240, 245);
        $this->fpdf->SetLineWidth(.3);
        $this->fpdf->SetFont('Arial', 'B', 10);

// Header
        $w = array(8, 40, 25, 20, 45, 45);
        for ($i = 0; $i < count($header); $i++)
            $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
        $this->fpdf->Ln();

// Color and font restoration
        $this->fpdf->SetFillColor(240, 240, 245);
        $this->fpdf->SetTextColor(0);

        $this->fpdf->SetFont('Arial', '', 8);

// Data
        $fill = false;
        $no = 1;
        $id = $this->db->select()->from('users')->get()->result();
        foreach ($id as $d) {


            $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[1], 6, humanize($d->first_name . ' ' . $d->last_name), 0, 0, 'L', $fill);
            $this->fpdf->Cell($w[2], 6, $d->phone, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[3], 6, $d->city, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[4], 6, $d->username, 1, 0, 'L', $fill);
            $this->fpdf->Cell($w[5], 6, $d->email, 1, 0, 'L', $fill);
            $this->fpdf->Ln();
            $fill = !$fill;
            $no++;
        }

// Closing line
        $this->fpdf->Cell(array_sum($w), 0, '', 'T');

        $file = $this->acronym.' ' . $this->page_level2 . ' export ' . time() . '.pdf';
        $D = 'D';
        $this->fpdf->Output($file, $D);
    }

    function sub_head($sub_head = null)
    {
        $fg = intval($this->fpdf->GetPageWidth());
        isset($sub_head) ? ($fg >= 297 ? $this->fpdf->Cell(0, 6, humanize($sub_head), 0, 1, 'C') : $this->fpdf->Cell(190, 6, humanize($sub_head), 0, 1, 'C')) : '';
    }


}
