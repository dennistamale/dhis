<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax_api extends CI_Controller
{

    public $page_level = 'app/';
    public $page_level2 = "";
    public $product_id = 2;

    function __construct()
    {
        parent::__construct();

        $user_type = $this->session->user_type;

//        $this->output->enable_profiler(TRUE);

        if ($user_type == 1) {
            $this->page_level = 'app/';
        } elseif ($user_type == 2) {
            $this->page_level = 'admin/';
        } elseif ($user_type == 3) {
            $this->page_level = 'farmer/';
        } elseif ($user_type == 4) {
            $this->page_level = 'trader/';
        } elseif ($user_type == 5) {
            $this->page_level = 'agent/';
        } else {
            $this->page_level = 'home/';
        }

    }

    function inbox_notification()
    {
        $n = $this->db->where(array('replied' => 'N'))->from('inbox')->count_all_results();

        echo $n;

    }

    function get_city($id)
    {

        $get_city = $this->db->select('title,state')->from('state')->where('country', $id)->order_by('title', 'asc')->get()->result();

        foreach ($get_city as $sc) { ?>

            <option value="<?= $sc->state ?>" <?= set_select('city', $sc->title); ?>><?= $sc->title ?> </option>

        <?php } ?>

        <?php

    }

    function dialing_code($id)
    {
        $c = $this->db->select('dialing_code')->from('country')->where('a2_iso', $id)->get()->row();
        if (isset($c)) {
            echo trim($c->dialing_code);
        }

    }

    function realtime_monitoring()
    {

        /*
         * Paging
         */

        $iTotalRecords = $this->db->count_all_results('ci_sessions');
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("timestamp" => $dir),
            array("" => $dir),
            array("" => $dir),
            array("" => $dir),
            array("" => $dir),
            array("" => $dir),
            array("" => $dir),
        );
        $o_list = $order_list[$column];


        //this is the end of the sorting

        $this->db->select()->from('ci_sessions');

        $this->db->order_by(key($o_list), current($o_list));

        $query = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();

        $no = 1;
        foreach ($query as $row):


            $session_data = $row->data;


            //  print_r($session_data);
            $return_data = array();
            $offset = 0;
            while ($offset < strlen($session_data)) {
                if (!strstr(substr($session_data, $offset), "|")) {
                    throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
                }
                $pos = strpos($session_data, "|", $offset);
                $num = $pos - $offset;
                $varname = substr($session_data, $offset, $num);
                $offset += $num + 1;
                $data = unserialize(substr($session_data, $offset));
                $return_data[$varname] = $data;
                $offset += strlen(serialize($data));
            }

//            if (!empty($return_data['username'])) {


            $records["data"][] = array(

//                    '<input type="checkbox"  name="id[]" value="' . $sb->id . '">',

                $no,
                trending_date($row->timestamp),
                isset($return_data['username']) ? $return_data['username'] : 'N/A',
                $row->ip_address,
                isset($return_data['username']) ? $return_data['first_name'] . '&nbsp;' . $return_data['last_name'] : 'N/A',

                isset($return_data['username']) ? $return_data['email'] : 'N/A',
                isset($return_data['browser']) ? $return_data['browser'] : 'N/A',
                isset($return_data['platform']) ? $return_data['platform'] : 'N/A'


            );


//            }


            $no++; endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function users($type = null)
    {


        $status = isset($type) && $type == 'blocked' ? 2 : 0;
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        $this->db->from('users a')->where(array('id !=' => $this->session->id));

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("first_name" => $dir),
            array("phone" => $dir),
            array("phone" => $dir),
            array("created_on " => $dir),
            array("user_type " => $dir),
            array("status " => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.id,a.first_name,a.last_name, a.city, c.country, a.phone,status,a.created_on,b.title,')->from('users a')
            ->join('user_type b', 'a.user_type=b.id')
            ->join('country c', 'c.a2_iso=a.country')
            ->where(array('a.id !=' => $this->session->id));

        //filtering
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }

        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 23:59:59')) : '';


        isset($_REQUEST["customRoleName"]) && strlen($_REQUEST["customRoleName"]) > 0 ? $this->db->where('a.user_type', $_REQUEST["customRoleName"]) : '';


        isset($_REQUEST["customStatusName"]) && strlen($_REQUEST["customStatusName"]) > 0 ? $this->db->where('a.status', $_REQUEST["customStatusName"]) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $users = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($users as $sb):

            $output = $this->custom_library->role_exist('create user') ? (' <li>' . anchor($this->page_level . 'users/edit/' . $sb->id * date('Y'), '<i class="fa fa-edit"></i> Edit') . '</li>') : '';


            $output .= $this->custom_library->role_exist('reset user password') ? (' <li>' . anchor($this->page_level . 'users/reset_password/' . $sb->id * date('Y'), '<i class="fa fa-edit"></i> Reset Password') . '</li>') : '';

            $output .= $this->custom_library->role_exist('delete user') ? (' <li onclick="return confirm(\'Deleting user will delete all records attached to a user, Are you sure ? \')">' . anchor($this->page_level . 'users/delete/' . $sb->id * date('Y'), '<i class="fa fa-trash"></i> Delete') . '</li>') : '';

            $output .= $this->custom_library->role_exist('unblock user') || $this->custom_library->role_exist('block user') ? '<li>' . ($sb->status == 2 ? anchor($this->page_level . $this->page_level2 . 'users/unblock/' . $sb->id * date('Y'), '  <i class="fa fa-check"></i> Unblock') : anchor($this->page_level . 'users/ban/' . $sb->id * date('Y'), '  <i class="fa fa-ban"></i> Block', 'onclick="return confirm(\'You are about to ban User from accessing the System \')"')) . '</li>' : '';

            $output .= '<ul/>';


            $status = $sb->status;
            $status_btn = '<div class="btn btn-xs btn-' . ($status == '2' ? 'danger' : 'success') . '">' . ($status == '2' ? 'Blocked' : 'Active') . '</div>';


            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',

                anchor($this->page_level . 'users/edit/' . $sb->id * date('Y'), strlen($sb->first_name) > 0 ? ucwords($sb->first_name . ' ' . $sb->last_name) : '<span style="color: red;">Registration not Completed</span>'),
                $sb->country,
                $sb->phone,


                trending_date($sb->created_on),
                $sb->title,
                $status_btn,

                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function indicators($type = null)
    {


        $this->db->from('workplan_indicator_titles a')->group_by('workplan_id');
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("first_name" => $dir),
            array("phone" => $dir),
            array("phone" => $dir),
            array("created_on " => $dir),
            array("user_type " => $dir),
            array("status " => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.indicator_code,a.title as indicator, a.id,workplan_id,a.description,a.geo_level,data_type,c.name as level_name,c.level');

        $this->db->from('workplan_indicator_titles a');
        $this->db->join('workplan b', 'a.workplan_id=b.id');
        $this->db->join('workplan_level c', 'b.level=c.level');


        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }

        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 23:59:59')) : '';


        isset($_REQUEST["customRoleName"]) && strlen($_REQUEST["customRoleName"]) > 0 ? $this->db->where('a.user_type', $_REQUEST["customRoleName"]) : '';


        isset($_REQUEST["customStatusName"]) && strlen($_REQUEST["customStatusName"]) > 0 ? $this->db->where('a.status', $_REQUEST["customStatusName"]) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->group_by('workplan_id')->order_by(key($o_list), current($o_list));

        $indicators = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($indicators as $sb):


            $gl = $this->model->get_geo_level($sb->geo_level);

            $gl_name = isset($gl) ? $gl->name : 'N/A';


            $indicator_count = $this->db->where('indicator_code', $sb->indicator_code)->from('workplan_indicators')->count_all_results();

            $output = '';
            $output .= $this->custom_library->role_exist('indicators', 'group') ? (' <li>' . anchor($this->page_level . 'indicators/view/' . $sb->indicator_code * date('Y'), '<i class="icon-eye"></i> View') . '</li>') : '';

            $output .= '<ul/>';

            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',


                humanize($sb->indicator),
                $sb->data_type,
                $sb->level_name,
                $sb->level,
                $gl_name,
                $indicator_count,
                //$sb->description,

                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function activity_report($type = null)
    {


        $this->db->from('activity_report a');
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("first_name" => $dir),
            array("phone" => $dir),
            array("phone" => $dir),
            array("created_on " => $dir),
            array("user_type " => $dir),
            array("status " => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select();

        $this->db->from('activity_report a');


        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }

        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 23:59:59')) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $indicators = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($indicators as $sb):

            $hf = $this->model->get_facility_by_id($sb->health_facility);

            $h_facility = isset($hf->health_unit) ? $hf->health_unit : '';

            $facility = $sb->hf_type == 'health_facility' ? $h_facility : $sb->other_facility;


            $output = '';

            $output .= '<li>' . anchor($this->page_level . 'activity_report/view/' . $sb->id * date('Y'), '<i class="icon-eye"></i> View') . '</li>';

            $output .= '<li>' . anchor($this->page_level . 'activity_report/edit/' . $sb->id * date('Y'), '<i class="icon-pencil7"></i> Edit') . '</li>';
            $output .= '<ul/>';

            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',


                trending_date($sb->created_on),
                $sb->workplan_ref,
                $sb->activity_type,
                $sb->district,
                $sb->sub_county,
                $sb->parish,
                $facility,

                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

//http://localhost/dhis/index.php/ajax_api/dhis2_summary/monthly

    function activity_report_summary($type = null)
    {

        $this->db->from('activity_report a');

        isset($_REQUEST['ref_code']) && strlen($_REQUEST['ref_code']) > 0 ? $this->db->where('workplan_ref', $_REQUEST['ref_code']) : '';

//        echo $type;

        $type = isset($type) && $type == 'district' || isset($_REQUEST['another_type']) ? 'district' : 'activity_type';

        $this->db->group_by(array('activity_year', $type));
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("district" => $dir),
            array("activity_year" => $dir),
            array("count(a.district)" => $dir),
            array("" => $dir),
            array("" => $dir),
            array("" => $dir),
            array("" => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting

        $this->db->select("a.district,a.activity_year,count(a.$type) as activity_count,activity_type");

        $this->db->from('activity_report a');

        isset($_REQUEST['ref_code']) && strlen($_REQUEST['ref_code']) > 0 ? $this->db->where('workplan_ref', $_REQUEST['ref_code']) : '';

        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();
            $this->db->or_like('a.district', $_REQUEST['search']['value']);
            $this->db->or_like('a.activity_year', $_REQUEST['search']['value']);

            $this->db->group_end();

        }

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'filter') {


            $this->db->group_start();

            strlen($_REQUEST["activity_type"]) > 0 ? $this->db->where('a.activity_type', $_REQUEST["activity_type"]) : '';
            strlen($_REQUEST["district"]) > 0 ? $this->db->where('a.district', $_REQUEST["district"]) : '';
//            strlen($_REQUEST["month"])>0?$this->db->where('a.activity_year',  $_REQUEST["month"]):'';
            strlen($_REQUEST["year"]) > 0 ? $this->db->where('a.activity_year', $_REQUEST["year"]) : '';


            if (strlen($_REQUEST["month"]) > 0) {

                $month = str_pad($_REQUEST["month"], 2, "0", STR_PAD_LEFT);
                $year = strlen($_REQUEST["year"]) > 0 ? $_REQUEST["year"] : date('Y');

                $this->db->where(array('a.submit_date >=' => strtotime($year . '-' . $month . '-01' . ' 00:00:01'), 'a.submit_date <=' => strtotime($year . '-' . $month . '-31' . ' 23:59:59')));

            }


            $this->db->group_end();
        }


        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 23:59:59')) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering


        $this->db->group_by(array('activity_year', $type));


        $this->db->order_by(key($o_list), current($o_list));

        $indicators = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($indicators as $sb):

            $activity_details = $this->model->activity_summary_details($sb->district, $sb->activity_year);


            $records["data"][] = array(

                $type == 'activity_type' ? humanize($sb->activity_type) : humanize($sb->district),

//                $sb->activity_year,
                $sb->activity_count,
                $activity_details->male,
                $activity_details->female,
                $activity_details->combined,
                $activity_details->total,


            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function activities($type = null)
    {


        $this->db->from('workplan_activity_status a');
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("first_name" => $dir),
            array("phone" => $dir),
            array("phone" => $dir),
            array("created_on " => $dir),
            array("user_type " => $dir),
            array("status " => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,c.name as level_name,c.level,b.title as workplan_name');

        $this->db->from('workplan_activity_status a');
        $this->db->join('workplan b', 'a.workplan_id=b.id');
        $this->db->join('workplan_level c', 'b.level=c.level');


        //filtering

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }

        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 23:59:59')) : '';


        isset($_REQUEST["customRoleName"]) && strlen($_REQUEST["customRoleName"]) > 0 ? $this->db->where('a.user_type', $_REQUEST["customRoleName"]) : '';


        isset($_REQUEST["customStatusName"]) && strlen($_REQUEST["customStatusName"]) > 0 ? $this->db->where('a.status', $_REQUEST["customStatusName"]) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $indicators = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($indicators as $sb):


            $current_status = $this->model->get_activity_updates($sb->id);

            $status = isset($current_status[0]->status) ? $this->model->get_update_status($current_status[0]->status) : array();

            $status_name = isset($status) && isset($status->name) ? $status->name : 'N/A';


            /*
            Also I need these statuses to be colour-coded
Completed - Green
On Track - Grey
Behind Schedule - Red
            */

            if ($status_name == 'Behind Schedule') {
                $btn = 'bg-danger';
                // $label = 'WIP';
            } elseif ($status_name == 'Completed') {
                $btn = 'btn-success';
                // $label = 'Closed';
            } elseif ($status_name == 'On Track') {
                $btn = 'btn-primary';
                // $label = 'Closed';
            } else {
                $btn = 'bg-grey';
                // $label = 'New';
            }


            $status_btn = '<div style="width:120px;" class="btn btn-xs ' . $btn . '">' . $status_name . '</div>';


            $gl = $this->model->get_geo_level($sb->geo_level);

            $gl_name = isset($gl)? $gl->name : 'N/A';


            $output = '';
            $output .= $this->custom_library->role_exist('activities', 'group') ? (' <li>' . anchor($this->page_level . 'activities/view/' . $sb->id * date('Y'), '<i class="icon-eye"></i> View') . '</li>') : '';
            $output .= $this->custom_library->role_exist('delete activities') ? (' <li>' . anchor($this->page_level . 'activities/delete/' . $sb->id * date('Y'), '<i class="icon-bin"></i> Delete') . '</li>') : '';

            $output .= '<ul/>';

            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $sb->id . '" />  <span></span>
                                                        </label>
                    ',


                $sb->number . '&nbsp;' . humanize($sb->item_unit),

                anchor($this->page_level . 'activities/view/' . $sb->id * date('Y'), $sb->activity_name),
                $gl_name,
                date('d-m-Y', $sb->start_date),
                date('d-m-Y', $sb->end_date),
                $sb->deliverable,
//                $sb->budget,
                $status_btn,


                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function resources($type = null)
    {


        if (strlen($type) > 0) {
            if ($type == 'unpublished') {
                $where = array('a.status' => 'pending');
            } elseif ($type == 'published') {
                $where = array('a.resource_type' => $type / date('Y'));
            } else {
                $where = array();
            }
        }


        $this->db->from('resources a');
        strlen($type) > 0 ? $this->db->where($where) : '';
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        ///$user=isset($_REQUEST["user"])?$_REQUEST["user"]:'';
        $this->db->select('a.*,d.title as resource_title')->from('resources a');
        $this->db->join('resource_types d', 'd.id=a.resource_type', 'left');

        strlen($type) > 0 ? $this->db->where($where) : '';

        //$this->db->like('a.title', $_REQUEST['search']['value']);

        //filteringpb
        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();
            $this->db->like('a.title', $_REQUEST['search']['value']);
            $this->db->group_end();

        }

        if (isset($_REQUEST["date_range"]) && strlen($_REQUEST["date_range"]) > 0) {
            $dates = split_date($_REQUEST["date_range"], '~');
            $this->db->where(array('a.created_on >=' => strtotime($dates['date1'] . ' 00:00:00'), 'a.created_on <=' => strtotime($dates['date2'] . ' 23:59:59')));
        }

        isset($_REQUEST["status"]) && strlen($_REQUEST["status"]) > 0 ? $this->db->where(array('a.status' => $_REQUEST["status"])) : '';

        isset($_REQUEST["resource_type"]) && strlen($_REQUEST["resource_type"]) > 0 ? $this->db->where(array('d.id' => $_REQUEST["resource_type"])) : '';

        //end of filtering

        $orders = $this->db->order_by('a.id', 'desc')->limit($iDisplayLength, $iDisplayStart)->get()->result();

        $no = 1;
        $view = $this->custom_library->role_exist('view resource') ? '' : 'hidden';
        $edit = $this->custom_library->role_exist('edit resource') ? '' : 'hidden';
        $delete = $this->custom_library->role_exist('delete resource') ? '' : 'hidden';
        $publish = $this->custom_library->role_exist('publish resource') ? '' : 'hidden';
        $unpublish = $this->custom_library->role_exist('unpublish resource') ? '' : 'hidden';
        foreach ($orders as $sb):

            $output = ' <li ' . $view . ' >' . anchor($this->page_level . 'resources/view/' . $sb->id * date('Y'), '<i class="fa fa-eye"></i> View') . '</li>';

            $output .= ' <li ' . $edit . ' >' . anchor($this->page_level . 'resources/edit/' . $sb->id * date('Y'), '<i class="fa fa-edit"></i> Edit') . '</li>';

            $output .= ' <li ' . $delete . ' >' . anchor($this->page_level . 'resources/delete_resource/' . $sb->id * date('Y'), '<i class="fa fa-trash"></i> Delete', 'onclick="return confirm(\'Are sure you want to delete this record ?\')"') . '</li>';

            $output .= ' <li>' . (strlen($sb->attachment) > 0 ? '<a href="' . base_url($sb->attachment) . '" target="_blank"><i class="fa fa-download"></i> Download</a>' : '') . '</li>';

            if ($sb->status == 'unpublished') {

                $status = '<div style="width:100px;" class="btn btn-sm btn-danger">' . humanize($sb->status) . '</div>';
                $output .= ' <li ' . $publish . ' >' . anchor($this->page_level . 'resources/publish/' . $sb->id * date('Y'), '<i class="fa fa-check"></i> Publish', 'onclick="return confirm(\'Are sure you want to publish this record ?\')"') . '</li>';

            } elseif ($sb->status == 'published') {

                $status = '<div style="width:100px;" class="btn btn-sm btn-success">' . humanize($sb->status) . '</div>';
                $output .= ' <li ' . $unpublish . ' >' . anchor($this->page_level . 'resources/unpublish/' . $sb->id * date('Y'), '<i class="fa fa-check"></i> Unpublish', 'onclick="return confirm(\'Are sure you want to unpublish this record ?\')"') . '</li>';

            }

            $output .= '<ul/>';

            $records["data"][] = array(

                $no,
                anchor($this->page_level . 'resources/view/' . $sb->id * date('Y'), $sb->title),
                $sb->resource_title,
                trending_date($sb->created_on),
                $status,
                strlen($output) > 0 ?
                    ' <div class="btn-group">
                        <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">' .
                    $output : ''

            );
            $no++; endforeach;

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function facilities($type = null)
    {


        $status = isset($type) && $type == 'blocked' ? 2 : 0;
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        $this->db->from('health_facilities a')->where(array('id !=' => $this->session->id));

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("health_unit" => $dir),
            array("owner" => $dir),
            array("authority" => $dir),
            array("level " => $dir),
            array("district " => $dir),
            array("sub_county " => $dir),
            array("parish " => $dir),
        );
        $o_list = $order_list[$column];
        //(key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select()->from('health_facilities a');

        //filtering
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }


        isset($_REQUEST["date_from"]) && strlen($_REQUEST["date_from"]) > 0 ? $this->db->where(array('a.date_created >=' => $_REQUEST["date_from"], 'a.date_created <=' => $_REQUEST["date_to"] . ' 23:59:59')) : '';


        isset($_REQUEST["customRoleName"]) && strlen($_REQUEST["customRoleName"]) > 0 ? $this->db->where('a.user_type', $_REQUEST["customRoleName"]) : '';


        isset($_REQUEST["customStatusName"]) && strlen($_REQUEST["customStatusName"]) > 0 ? $this->db->where('a.status', $_REQUEST["customStatusName"]) : '';


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $health_facilities = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($health_facilities as $i):

            $output = (' <li>' . anchor($this->page_level . 'facilities//view/' . $i->id * date('Y'), '<i class="icon-eye"></i> View', '') . '</li>');

            $output .= (' <li>' . anchor($this->page_level . 'facilities//edit/' . $i->id * date('Y'), '<i class="icon-pencil7"></i> Edit', '') . '</li>');


            $output .= '<ul/>';


            $status = $i->status;
            $status_btn = '<div class="btn btn-xs btn-' . ($status == '2' ? 'danger' : 'success') . '">' . ($status == '2' ? 'Blocked' : 'Active') . '</div>';


            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $i->id . '" />  <span></span>
                                                        </label>
                    ',

                strtoupper($i->health_unit),
                $i->owner,
                $i->authority,
                $i->level,
                $i->district,
                $i->sub_county,
                $i->parish,

                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function survey($type = null)
    {


        $status = isset($type) && $type == 'blocked' ? 2 : 0;
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';


        $this->db->from('survey a')->where(array('id !=' => $this->session->id));

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("title" => $dir),
            array("created_on" => $dir),
            array("created_by" => $dir),

        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,b.first_name,b.last_name')->from('survey a');
        $this->db->join('users b', 'a.created_by=b.id');

        //filtering
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $status_list = array(
            array("label-info" => "Pending"),
            array("label-success" => "Approved"),


        );


        $no = 1;
        foreach ($result as $i):

            $responses = $this->db->where('survey_id', $i->id)->from('survey_answer')->count_all_results();

            $last_response = $this->db->select()->from('survey_answer')->where('survey_id', $i->id)->order_by('id', 'desc')->limit(1)->get()->row();


            $output = (' <li>' . anchor($this->page_level . 'survey/view/' . $i->id * date('Y'), '<i class="icon-eye"></i> View', '') . '</li>');

            $output .= (' <li>' . anchor($this->page_level . 'survey/edit/' . $i->id * date('Y'), '<i class="icon-pencil7"></i> Edit', '') . '</li>');

            $output .= (' <li>' . anchor($this->page_level . 'survey/create_questions/' . $i->id * date('Y'), '<i class="icon-stack-plus"></i> Create Questions', '') . '</li>');


            $output .= (' <li>' . anchor($this->page_level . 'survey/preview_question/' . $i->id * date('Y'), '<i class="icon-image3"></i> Preview Questions', '') . '</li>');

            $output .= $responses > 0 ? (' <li>' . anchor($this->page_level . 'survey/survey_responses/' . $i->id * date('Y'), '<i class="icon-insert-template"></i> Responses', '') . '</li>') : '';


            $output .= '<ul/>';


            $status = $status_list[$i->status];


            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $i->id . '" />  <span></span>
                                                        </label>
                    ',


                anchor($this->page_level . 'survey/survey_responses/' . $i->id * date('Y'), $i->title),
                $responses,
                isset($last_response)  ? trending_date($last_response->created_on) : 'None',

//                '<span class="label  ' . (key($status)) . '">' . current($status) . '</span>',
                $i->first_name . ' ' . $i->last_name,

                trending_date($i->created_on),


                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function dhis2_summary($period = 'weekly')
    {

        $this->load->model('dhis2_model');

        $indicators = $this->dhis2_model->indicators($period);


//        geo_level: 2
//geo_level_items: VA90IqaI4Ji

        if ((isset($_REQUEST['geo_level']) && strlen($_REQUEST['geo_level']) > 0) && (isset($_REQUEST['geo_level_items']) && strlen($_REQUEST['geo_level_items']) > 0)) {


            $level = $_REQUEST['geo_level'];
            $org_unit = $_REQUEST['geo_level_items'];
            $d = $this->dhis2_model->get_malaria_data($period, $level, $org_unit);

            if ($d['alert'] == 'error') {
                $trecords = 0;

                $iTotalRecords = $trecords;
                $iDisplayLength = intval($_REQUEST['length']);
                $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
                $iDisplayStart = intval($_REQUEST['start']);
                $sEcho = intval($_REQUEST['draw']);


                $records = array();
                $records["data"] = array();


            } else {

                $period == 'weekly' ? $this->db->like('period', 'w') : $this->db->not_like('period', 'w');
                $this->db->from('report_malaria_filter a');
                $this->db->group_by('a.period');

                $trecords = $this->db->count_all_results();

                $iTotalRecords = $trecords;
                $iDisplayLength = intval($_REQUEST['length']);
                $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
                $iDisplayStart = intval($_REQUEST['start']);
                $sEcho = intval($_REQUEST['draw']);


                $records = array();
                $records["data"] = array();

                $end = $iDisplayStart + $iDisplayLength;
                $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
                $order = $_REQUEST['order'];
                $column = $order[0]['column'];
                $dir = $order[0]['dir'];

                $order_list = array(
                    array("a.period" => $dir),
                );
                $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

                //this is the end of the sorting


                $this->db->select('a.period,b.name as organisation_unit')->from('report_malaria_filter a');
                $this->db->join('dhis_organisation_unit b', 'a.orgUnit=b.id');

                //filtering

                $period == 'weekly' ? $this->db->like('a.period', 'w') : $this->db->not_like('a.period', 'w');

                if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

                    $this->db->group_start();

                    $this->db->or_like('a.period', substr($_REQUEST['search']['value'], 1));

                    $this->db->group_end();

                }

                $this->db->group_by('a.period');

                $this->db->order_by('SUBSTRING(`a`.`period`, 1,4) DESC , CONVERT(SUBSTRING(`a`.`period`, 6),UNSIGNED INTEGER) DESC');

                $this->db->order_by(key($o_list), current($o_list));

                $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


                $no = 1;
                foreach ($result as $i):

                    $data = array($i->period,
//                $i->organisation_unit
                    );

                    foreach ($indicators as $wk) {

//                    print_r($wk);
//                    exit;


                        array_push($data, $this->dhis2_model->indicator_summary($i->period, $wk->indicator, 'filtered'));
                    }

                    $records["data"][] = $data;
                    $no++; endforeach;


            }


        } else {


            $period == 'weekly' ? $this->db->like('period', 'w') : $this->db->not_like('period', 'w');

            $this->db->from('report_malaria a');
            $this->db->group_by('a.period');

            $trecords = $this->db->count_all_results();
            $iTotalRecords = $trecords;
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);


            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
            $order = $_REQUEST['order'];
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];

            $order_list = array(
                array("a.period" => $dir),
            );
            $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

            //this is the end of the sorting


            $this->db->select('a.period,b.name as organisation_unit')->from('report_malaria a');
            $this->db->join('report_org_unit b', 'a.orgUnit=b.id');


            //filtering

            $period == 'weekly' ? $this->db->like('a.period', 'w') : $this->db->not_like('a.period', 'w');

            if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

                $this->db->group_start();

                $this->db->or_like('a.period', substr($_REQUEST['search']['value'], 1));

                $this->db->group_end();

            }

            $this->db->group_by('a.period');

            $this->db->order_by('SUBSTRING(`a`.`period`, 1,4) DESC , CONVERT(SUBSTRING(`a`.`period`, 6),UNSIGNED INTEGER) DESC');

            $this->db->order_by(key($o_list), current($o_list));

            $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($result as $i):

                $data = array($i->period,
//                $i->organisation_unit
                );

                foreach ($indicators as $wk) {


                    array_push($data, $this->dhis2_model->indicator_summary($i->period, $wk->indicator));
                }

                $records["data"][] = $data;
                $no++; endforeach;

        }


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function malaria_report($period = 'weekly')
    {

        $this->load->model('dhis2_model');


        $periodic_dimensions = $this->dhis2_model->periodic_dimension($period);


        $period == 'weekly' ? $this->db->like('period', 'w') : $this->db->not_like('period', 'w');

        $this->db->from('report_malaria a');
        $this->db->group_by('a.period');

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("a.period" => $dir),
        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.period,b.name as organisation_unit')->from('report_malaria a');
        $this->db->join('report_org_unit b', 'a.orgUnit=b.id');


        //filtering

        $period == 'weekly' ? $this->db->like('a.period', 'w') : $this->db->not_like('a.period', 'w');

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $this->db->or_like('a.period', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }

        $this->db->group_by('a.period');


        $this->db->order_by('SUBSTRING(`a`.`period`, 1,4) DESC , CONVERT(SUBSTRING(`a`.`period`, 6),UNSIGNED INTEGER) DESC');

        $this->db->order_by(key($o_list), current($o_list));

        $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $no = 1;
        foreach ($result as $i):

            $data = array($i->period
//            , $i->organisation_unit
            );

            foreach ($periodic_dimensions as $wk) {


                array_push($data, $this->dhis2_model->get_weekly_report($i->period, $wk->id));
            }

            $records["data"][] = $data;
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function survey_responses($survey_id = null)
    {

        isset($survey_id) ? $survey_id = $survey_id / date('Y') : '';


        $this->db->from('survey_answer a');
        isset($survey_id) ? $this->db->where(array('a.survey_id' => $survey_id)) : '';
        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("title" => $dir),
            array("created_on" => $dir),
            array("created_by" => $dir),

        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,b.first_name,b.last_name,c.title')
            ->from('survey_answer a')
            ->join('users b', 'b.id=a.created_by', 'left');
        $this->db->join('survey c', 'a.survey_id=c.id');
        isset($survey_id) ? $this->db->where(array('a.survey_id' => $survey_id)) : '';


        //filtering


        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering


        $this->db->order_by(key($o_list), current($o_list));

        $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $status_list = array(
            array("label-info" => "Pending"),
            array("label-success" => "Approved"),


        );


        $no = 1;
        foreach ($result as $i):
            $data = array();

            $hf = $this->model->get_facility_by_id($i->health_facility);
            $health_unit = isset($hf->health_unit) ? $hf->health_unit : '';

            $output = '';
            $output .= (' <li>' . anchor($this->page_level . 'survey/answer_preview/' . $i->survey_id * date('Y') . '/' . $i->id * date('Y'), '<i class="icon-image3"></i> Preview Questions', '') . '</li>');
            $output .= '<ul/>';


            $status = $status_list[$i->status];


            array_push($data,

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $i->id . '" />  <span></span>
                                                        </label>
                    ');


            !isset($survey_id) ? array_push($data, character_limiter($i->title, 40)) : '';

            array_push($data, $i->district);

            array_push($data, $health_unit);

            array_push($data, anchor($this->page_level . 'survey/answer_preview/' . $i->survey_id * date('Y') . '/' . $i->id * date('Y'), strlen($i->level) > 0 ? $i->level : 'N/A'));

            array_push($data, $i->first_name . ' ' . $i->last_name);
            array_push($data, trending_date($i->created_on));

            array_push($data, strlen($output) > 0 ?
                ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                $output : '');

            $records["data"][] = $data;


            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function surveys($type = null)
    {


        $status = isset($type) && $type == 'blocked' ? 2 : 0;
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';


        $this->db->from('survey a')->where(array('id !=' => $this->session->id));

        $trecords = $this->db->count_all_results();
        $iTotalRecords = $trecords;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);


        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


// this is the part where sorting is made
        $order = $_REQUEST['order'];
        $column = $order[0]['column'];
        $dir = $order[0]['dir'];

        $order_list = array(
            array("id" => $dir),
            array("title" => $dir),
            array("created_on" => $dir),
            array("created_by" => $dir),

        );
        $o_list = $order_list[$column];
//            (key($status)) . '">' . (current($status))

        //this is the end of the sorting


        $this->db->select('a.*,b.first_name,b.last_name')->from('survey a');
        $this->db->join('users b', 'a.created_by=b.id');

        //filtering
        isset($type) && $type != '' ? $this->db->where(array('a.status' => $status)) : '';

        if (isset($_REQUEST['search']['value']) && strlen($_REQUEST['search']['value']) > 2) {

            $this->db->group_start();

            $name = explode(' ', strtolower($_REQUEST['search']['value']));
            count($name) == 2 ? $this->db->like(array('a.first_name' => $name[0])) : $this->db->like('a.first_name', strtolower($_REQUEST['search']['value']));

            count($name) == 2 ? $this->db->or_like(array('a.last_name' => $name[1])) : $this->db->or_like('a.last_name', strtolower($_REQUEST['search']['value']));

            $this->db->or_like('a.phone', substr($_REQUEST['search']['value'], 1));

            $this->db->group_end();

        }


        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_search") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            isset($_REQUEST["customFromName"]) && strlen($_REQUEST["customFromName"]) > 0 ? $this->db->where('a.date_created >=', $_REQUEST["customFromName"]) : '';
            isset($_REQUEST["customToName"]) && strlen($_REQUEST["customToName"]) > 0 ? $this->db->where('a.date_created <=', $_REQUEST["customToName"] . ' 23:59:59') : '';
        }

        //end of filtering

        $this->db->order_by(key($o_list), current($o_list));

        $result = $this->db->limit($iDisplayLength, $iDisplayStart)->get()->result();


        $status_list = array(
            array("label-info" => "Pending"),
            array("label-success" => "Approved"),


        );


        $no = 1;
        foreach ($result as $i):

            $questions = $this->db->where('survey_id', $i->id)->from('survey_questions')->count_all_results();


            $output = (' <li>' . anchor($this->page_level . 'surveys/view/' . $i->id * date('Y'), '<i class="icon-eye"></i> View', '') . '</li>');

            $output .= (' <li>' . anchor($this->page_level . 'surveys/edit/' . $i->id * date('Y'), '<i class="icon-pencil7"></i> Edit', '') . '</li>');

            $output .= (' <li>' . anchor($this->page_level . 'surveys/create_questions/' . $i->id * date('Y'), '<i class="icon-stack-plus"></i> Create Questions', '') . '</li>');


            $output .= (' <li>' . anchor($this->page_level . 'surveys/preview_question/' . $i->id * date('Y'), '<i class="icon-image3"></i> Preview Questions', '') . '</li>');


            $output .= '<ul/>';


            $status = $status_list[$i->status];


            $records["data"][] = array(

                '
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="group-checkable" name="id[]" value="' . $i->id . '" />  <span></span>
                                                        </label>
                    ',


                anchor($this->page_level . 'surveys/view/' . $i->id * date('Y'), $i->title),
                '<span class="label  ' . (key($status)) . '">' . current($status) . '</span>',
                $i->first_name . ' ' . $i->last_name,
                $questions,
                trending_date($i->created_on),


                strlen($output) > 0 ?
                    ' <ul class="icons-list">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu9"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                        ' .
                    $output : ''

            );
            $no++; endforeach;


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    function receive_form()
    {


//        print_r($this->input->post());

        $output = array(
            'registration' => array('statusMessage' => 'You were registered successfully')
        );

        header('Content-Type: application/json');
        echo json_encode($output);

//        echo '
//
//        {"registration" : {
//	"statusMessage" : "You were registered successfully"
//}}
//
//        ';
    }

    function save_surveyjs($count = null, $survey_code = null)
    {

        $this->load->model('survey_model');

        $output = array(
            'registration' => array(
                'statusMessage' => 'Some Parameters are missing',
                'status' => 'error'
            )
        );


        if (isset($count) && isset($survey_code)) {


            $db_survey = $this->db->where(array('survey_code' => $survey_code))->from('survey')->count_all_results();


            $data = $this->input->post('pages');
            $json_to_array = json_decode(json_decode($data, true), true);

            $json_to_array['title'];

            $values = array(
                'title' => $title = $json_to_array['title'],
                'json_form' => json_decode($data, true),


            );


            if ($db_survey == 0) {

                $values['survey_code'] = $survey_code;
                $values['created_on'] = time();
                $values['created_by'] = $this->session->id;


                $this->db->insert('survey', $values);

                $this->custom_library->add_logs('', $title, '', 'has Created Survey');


                $output = array(
                    'registration' => array(
                        'statusMessage' => 'Survey Saved Successfully',
                        'status' => 'success'
                    )
                );


            } else {

                $values['updated_on'] = time();
                $values['updated_by'] = $this->session->id;


                $this->db->where(array('survey_code' => $survey_code))->update('survey', $values);


                $output = array(
                    'registration' => array(
                        'statusMessage' => 'Survey Updated Successfully',
                        'status' => 'success'
                    )
                );
            }


        }


        header('Content-Type: application/json');
        echo json_encode($output);

    }

    function save_surveyjs_response($survey_id = null)
    {

        $this->load->model('survey_model');

        $output = array(
            'registration' => array(
                'statusMessage' => 'Some Parameters are missing',
                'status' => 'error'
            )
        );


        if (isset($survey_id)) {


            // $db_survey=$this->db->where(array('survey_code'=>$survey_code))->from('survey')->count_all_results();


            $data = $this->input->post('pages');
            //$json_to_array=json_decode(json_decode($data,true),true);

            //$json_to_array['title'];

            //ALTER TABLE survey_answer ADD auditor_designation VARCHAR(100) NULL;
//ALTER TABLE survey_answer CHANGE author_name auditor VARCHAR(100);

//            print_r($this->input->get());

            $values = array(

                'district' => $this->input->get('district'),
                'date' => strtotime($this->input->get('date')),
                'health_facility' => $this->input->get('health_facility'),
                'level' => trim($this->input->get('level')),
                'auditor' => $this->input->get('auditor'),
                'auditor_designation' => $this->input->get('auditor_designation'),
                'respondent_name1' => $this->input->get('respondent_name1'),
                'designation1' => $this->input->get('designation1'),
                'respondent_name2' => $this->input->get('respondent_name2'),
                'designation2' => $this->input->get('designation2'),
                'respondent_name3' => $this->input->get('respondent_name3'),
                'designation3' => $this->input->get('designation3'),
                'respondent_name4' => $this->input->get('respondent_name4'),
                'designation4' => $this->input->get('designation4'),


                'survey_id' => $survey_id,
                'json' => $data,
                'comment' => '',


            );


            $values['created_on'] = time();
            $values['created_by'] = $this->session->id;


            $this->db->insert('survey_answer', $values);

            $this->custom_library->add_logs('', $survey_id, '', 'answer has been created Successfully');


            $output = array(
                'registration' => array(
                    'statusMessage' => 'Answer has been created Successfully',
                    'status' => 'success'
                )
            );


        }


        header('Content-Type: application/json');
        echo json_encode($output);

    }

//http://localhost/dhis/index.php/ajax_api/dhis2_summary/monthly
    function acr()
    {

        $district = 'Luwero';
        $year = '2018';

        $o = $this->model->activity_summary_details($district, $year);

        print_r($o);
    }

    function test_week()
    {
        $this->load->model('dhis2_model');
        $peridic_dimensions = $this->dhis2_model->periodic_dimension();
        $period = 'sdfs';
        $dimension = 'Fql8Zd4Pa2d.gGhClrV5odI';
        echo $this->dhis2_model->get_weekly_report($period, $dimension);
    }

    function get_geo_level_items($geo_level)
    {
        $this->load->model('dhis2_model');


        $r = $r[0] = $this->dhis2_model->get_resource_values($geo_level, 'custom_value', 'organisationUnitLevels');

        $array = array();
        $name = $r->displayName;

        foreach ($this->dhis2_model->get_organisation_unit($geo_level, 'level') as $r) {
            $array[$r->id] = $r->name;
        }


        $output = array(
            'name' => $name,
            'items' => $array
        );


        header('Content-Type: application/json');
        echo json_encode($output, JSON_PRETTY_PRINT);

    }


    function get_geo_level_item($geo_level)
    {
        $r = $this->model->get_geo_level($geo_level);

        $array = array();


        switch ($geo_level) {
            case 1: //National

                $name = $r->name;

                break;

            case 2: //Regional

                $name = $r->name;
                $type = 'region';

                foreach ($this->model->get_project_regions() as $r) {
                    $array[$r->region] = $r->region;
                }

                break;

            case 3: //District

                $name = $r->name;
                $type = 'district';


                foreach ($this->model->get_distinct_location($type) as $r) {
                    $array[$r->location] = $r->location;
                }


                break;

            case 4: //Health Facilities


                $name = 'Health Facilities';

                foreach ($this->model->get_facilities() as $r) {
                    $array[$r->id] = $r->health_unit;
                }

                break;

        }


        $output = array(
            'name' => $name,
            'items' => $array
        );


        header('Content-Type: application/json');
        echo json_encode($output, JSON_PRETTY_PRINT);

    }

}


?>