<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->isloggedin() == true ? '' : $this->logout();

    }

    public function index($page = null)
    {
//        $this->page_level = 'app/';
        $data['title'] = 'admin';
        $data['subtitle'] = '';
        $this->load->view( 'app/static/main_page', $data);
        
    }

    public function isloggedin()
    {
        return $this->session->userdata('user_type') == 2 ? true : false;

    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */