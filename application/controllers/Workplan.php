<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class workplan extends CI_Controller
{

    public $page_level = 'app/';


    function __construct()
    {
        parent::__construct();
//        $this->load->library('Workplan_lib');
//        $this->load->model('woxrkplan_model');
    }


    function test()
    {
        $f = $this->workplan_lib->test();
        print_r($f);

        print_r($this->input->post());

        echo $this->input->get('parent');
    }

    function data_tree()
    {
        $parent = $this->input->get('parent');

//        print_r($parent);

        $data = array();

        $states = array(
            "success",
            "info",
            "danger",
            "warning"
        );


        if ($parent == "#") {


            foreach ($this->workplan_lib->get_level(1) as $r) {

                $data[] = array(
                    "id" => $r->id,
                    "text" => $r->title,
                    "icon" => "icon-folder icon-lg text-" . ($states[1]),
                    "children" => $this->workplan_lib->count_children($r->id) > 0 ? true : false,
                    "type" => "root",
                    'href' => ''
                );

            }


//            for ($i = 1; $i < rand(4, 7); $i++) {
//                $data[] = array(
//                    "id" => "node_" . time() . rand(1, 100000),
//                    "text" => "Node #" . $i,
//                    "icon" => "icon-folder icon-lg text-" . ($states[rand(0, 3)]),
//                    "children" => true,
//                    "type" => "root"
//                );
//            }


        } else {


            if ($this->workplan_lib->count_children($parent) == 0) {

                $data[] = array(
                    "id" => "node_" . time() . rand(1, 100000),
                    "icon" => "icon-file-empty2 icon-state-default",
                    "text" => "No childs $parent",
                    "state" => array("disabled" => true),
                    "children" => false
                );


            } else {


                foreach ($this->workplan_lib->get_children($parent) as $r) {

                    $data[] = array(
                        "id" => $r->id,
                        "icon" => (rand(0, 3) == 2 ? "icon-file-empty" : "icon-folder") . " text-" . ($states[rand(0, 3)]),
                        "text" => anchor('#', $r->title),
                        "children" => $this->workplan_lib->count_children($r->id) > 0 ? true : false
                    );

                }

//
//
//                for ($i = 1; $i < rand(2, 4); $i++) {
//                    $data[] = array(
//                        "id" => "node_" . time() . rand(1, 100000),
//                        "icon" => (rand(0, 3) == 2 ? " icon-file icon-lg" : "icon-folder icon-lg") . " text-" . ($states[rand(0, 3)]),
//                        "text" => "Node " . $parent,
//                        "children" => (rand(0, 3) == 2 ? false : true)
//                    );
//                }

            }


        }

        header('Content-type: application/json');
        echo json_encode($data);
    }


    function create_page($parent_id, $page_type = 'outcomes')
    {


        $data['parent'] = $parent_id / date('Y');
        $data['selected_page'] = $page_type;

//        $this->load->view('app/workplan/'.$page_type,$data);
        $this->load->view('app/workplan/outputs', $data);

    }


    function update_workplan()
    {


        //print_r($this->input->post());
        //choose if to update the or the create a new record

        $id = $this->input->post('id');
        $level = $this->input->post('workplan_level');
//        $dates = split_date($_REQUEST["date_range"], '~');

        $from = strtotime(date('Y-m-d 00:00:00',strtotime($this->input->post('start_date'))));
        $to = strtotime(date('Y-m-d 23:59:59',strtotime($this->input->post('end_date'))));


        $values = array(
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'level' => $level,
            'assumption' => $this->input->post('assumption'),
            'start_time' => $from,
            'end_time' => $to,

        );

        $values['updated_on'] = time();
        $values['updated_by'] = $this->session->id;

        //decides on either to update or create a new record

        if (strlen($id) > 0) {
            $this->db->where('id', $id)->update('workplan', $values);


            $data = array(
                'alert' => 'success',
                'message' => 'You have successfully saved data'
            );
        } else {

            $data = array(
                'alert' => 'danger',
                'message' => 'Error occurred while saving data'
            );
        }
        $this->load->view('alert', $data);


    }


    function update_indicator($indicator_id,$actual_value,$indicator_type='actual_value'){

        if($this->db->where('id',$indicator_id)->update('workplan_indicators',array(
            $indicator_type=>$actual_value,
            'updated_by'=>$this->session->id,
            'updated_on'=>time()
        ))){
            echo 'success';
        }else{
            echo 'error';
        }


    }


}