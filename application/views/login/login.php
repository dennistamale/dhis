

<body class="login-container">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">


                <!-- Simple login form -->
                <form method="post">
                    <div class="panel panel-body login-form shadow">
                        <div class="text-center">



                            <div class="icon-object border-slate-300 text-slate-300" style="border-color: #fff;">
                                <a href="<?php echo base_url(); ?>">
                                    <img class="login-logo"  src="<?php echo base_url($this->site_options->title('site_logo')) ?>" style=" max-width: 220px !important;" />
                                </a>
                            </div>



                            <h5 class="content-group"><?php echo $this->site_options->title('site_name') ?> Login<small class="display-block">Enter your credentials below</small></h5>
                        </div>

                        <?php if(isset($message)){ ?>

                            <div class="alert bg-danger">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                <?php echo $message; ?>
                            </div>

                            <?php //echo ' <p style="color: red; text-align: center;"><i class="icon-ban"></i> '. $message.'</p>'; ?>
                        <?php } ?>

                        <label class="text-danger"><?php echo form_error('username','<span class="require">','</span>') ?></label>
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control"  name="username" placeholder="Username" required="required" />
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <label class="text-danger"><?php echo form_error('password','<span class="require">','</span>') ?></label>
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" name="password" placeholder="Password" required="required" class="form-control" />
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
                        </div>

                        <div class="text-center">
                            <a href="#">Forgot password?</a>
                        </div>


                        <div hidden class="content-divider text-muted form-group"><span>or sign in with</span></div>
                        <ul hidden class="list-inline form-group list-inline-condensed text-center">
                            <li><a href="#" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="btn border-pink-300 text-pink-300 btn-flat btn-icon btn-rounded"><i class="icon-dribbble3"></i></a></li>
                            <li><a href="#" class="btn border-slate-600 text-slate-600 btn-flat btn-icon btn-rounded"><i class="icon-github"></i></a></li>
                            <li><a href="#" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-twitter"></i></a></li>
                        </ul>

                        <div hidden class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
                        <?//= anchor('register','Register','class="btn bg-purple btn-block content-group"') ?>
                        <span  class="hidden help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>

                    </div>
                </form>
                <!-- /simple login form -->

