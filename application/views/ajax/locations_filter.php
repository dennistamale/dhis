<div class="form-inline pull-left">



    <div class="input-group input-sm">


        <span class="input-group-addon">Regions</span>
        <select name="regions" class="regions form-control form-filter input-sm" style="width: 90px;">
            <option value="">Regions...</option>
            <?php foreach ($this->locations->get_children(1) as $s){ ?>
                <option value="<?php echo $s->id ?>"><?php echo $s->name ?></option>
            <?php } ?>

        </select>
        <!--District-->

        <span class="input-group-addon">District</span>
        <select name="district" class="district form-control form-filter input-sm" style="width: 90px;">
            <option value="">District...</option>
        </select>
        <!--Sub county-->
        <span class="input-group-addon">Sub county</span>
        <select name="sub_county" class="sub_county form-control form-filter input-sm" style="width: 100px;">
            <option value="">Sub county...</option>

        </select>
        <!--Parish-->
        <span class="input-group-addon">Parish</span>
        <select name="parish" class="parish form-control form-filter input-sm" style="width: 90px;">
            <option value="">Parish...</option>

        </select>
        <!--Village-->
        <span class="input-group-addon">Village</span>
        <select name="village" class="village form-control form-filter input-sm" style="width: 90px;">
            <option value="">Village...</option>

        </select>
    </div>

    <div class="hidden input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
        <span class="input-group-addon">Date From </span>
        <input type="text" class="form-control   form-filter input-sm" name="date_from">
        <span class="input-group-addon">to </span>
        <input type="text" class="form-control  form-filter input-sm" name="date_to">
    </div>



    <button class="btn  btn-sm green btn-outline filter-submit margin-bottom">
        <i class="icon-equalizer"></i> Filter</button>

    <div  class="input-group input-sm text-danger filter_alert"></div>



</div>
