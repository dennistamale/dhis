
<div class="form-group">
    <legend class="text-bold">Activity Site</legend>


    <?php $districts = $this->hf_model->get_hf_districts(); ?>

    <label class="control-label col-lg-1"><span class="pull-right">District </span> </label>
    <div class="col-lg-2">
        <select name="district" class="district form-control select"
                required <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
            <option value="" selected>District</option>
            <?= isset($id) ? '<option value="' . $fac->district . '" selected>' . $fac->district . '</option>' : ''; ?>
            <?php foreach ($districts as $d) { ?>
                <option value="<?= $d ?>" <?= set_select('district', $d) ?>><?= $d ?></option>
            <?php } ?>

        </select>
        <?php echo form_error('District', '<span class="text-danger">', '</span>') ?>
    </div>


    <label class="control-label col-lg-1"><span class="pull-right">County </span> </label>
    <div class="col-lg-2">
        <select name="county" class="form-control county select"
                required <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
            <option value="" selected>County</option>

            <?= isset($id) ? '<option value="' . $fac->county . '" selected>' . $fac->county . '</option>' : ''; ?>


        </select>
        <?php echo form_error('county', '<span class="text-danger">', '</span>') ?>
    </div>


    <label class="control-label col-lg-1"><span class="pull-right">Sub&nbsp;County </span> </label>
    <div class="col-lg-2">
        <select name="sub_county" class="form-control sub_county"
                required <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
            <option value="" selected>Sub County</option>
            <?= isset($id) ? '<option value="' . $fac->sub_county . '" selected>' . $fac->sub_county . '</option>' : ''; ?>

        </select>
        <?php echo form_error('sub_county', '<span class="text-danger">', '</span>') ?>
    </div>


    <label class="control-label col-lg-1"><span class="pull-right">Parish </span> </label>
    <div class="col-lg-2">
        <select name="parish" class="select form-control parish"
                required <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
            <option value="" selected>Parish</option>
            <?= isset($id) ? '<option value="' . $fac->parish . '" selected>' . $fac->parish . '</option>' : ''; ?>

        </select>
        <?php echo form_error('parish', '<span class="text-danger">', '</span>') ?>
    </div>


</div>


<div class="form-group">


    <label class="control-label col-lg-1"><span class="pull-right">Site Type </span> </label>
    <div class="col-lg-5">

        <!--        Health Facility, Schools, Community-->
        <select name="hf_type" class="select form-control hf_type" <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
            <option value="" selected>Facility Type</option>

            <?= isset($id) ? '<option value="' . $fac->hf_type . '">' . $fac->hf_type . '</option>' : ''; ?>
            <option value="health_facility" <?= set_select('hf_type', 'health_facility', (isset($id) ?($fac->hf_type=='health_facility'?true:''):true)) ?>>Health Facility
            </option>
            <option value="schools" <?= set_select('hf_type', 'schools', (isset($id) ?($fac->hf_type=='schools'?true:''):'')) ?>>Schools</option>
            <option value="community" <?= set_select('hf_type', 'community', (isset($id) ?($fac->hf_type=='community'?true:''):'')) ?>>Community</option>


        </select>
        <?php echo form_error('hf_type', '<span class="text-danger">', '</span>') ?>
    </div>


    <span class="health_facility_span">

    <label class="control-label col-lg-1"><span class="pull-right">Health&nbsp;Facility </span> </label>
    <div class="col-lg-5">

        <select name="health_facility" class="form-control health_facility" <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
            <option value="" selected>Select Facility</option>

            <?php

            if (isset($id)) {
                $hf = $this->model->get_facility_by_id($fac->health_facility);

                echo isset($id) ? '<option value="' . $fac->health_facility . '" selected>' . $hf->health_unit . '</option>' : '';
            }

            ?>


        </select>
        <?php echo form_error('health_facility', '<span class="text-danger">', '</span>') ?>
    </div>

</span>

    <span class="other_facility" style="display: none">


    <label class="control-label col-md-1"><span class="pull-right">Facility Name</span></label>
    <div class="col-md-5">

        <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> name="other_facility"
                                                             value="<?= set_value('other_facility', isset($id) ? $fac->other_facility : '') ?>"
                                                             type="text" class="form-control">

        <?php echo form_error('other_facility', '<span class="text-danger">', '</span>') ?>
    </div>


    </span>


</div>

<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->

<script>


    $(document).ready(function () {

        $('.filter-submit').click(function () {


            <?php if($title == 'distribution_reports'){ ?>
            var r = $('.district').val();

            <?php }else{ ?>

            var r = $('.sub_county').val();

            <?php } ?>


            if (r == '') {
                $('.filter_alert').html('At least select District and Sub county');
            } else {
                $('.filter_alert').html('')
            }
        });

        //getting districts in regions
        // $('body').delegate('.edit', 'click', function (e) {
        $('.regions').change(function () {
            var r = $('.regions').val();
            if (r != '') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_regions")?>/' + r,
                    beforeSend: function () {
                        $(".district").html('<option value="">Loading...</option>');
                    },
                    success: function (d) {

                        $(".district").html(d);


                    }


                });
            }
        });

        //getting subcounty in districts

        $('.district').change(function () {

            var r = $('.district').val();

            console.log(r);

            if (r != '') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_county")?>/' + r,
                    beforeSend: function () {
                        $(".county").html('<option value="">Loading...</option>');
                    },
                    success: function (d) {

                        $(".county").html(d);


                    }


                });
            } else {
                $(".county").html('<option value="">Sub county...</option>');
            }
        });


        //getting subcounty in districts

        $('.county').change(function () {
            var r = $('.county').val();
            if (r != '') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_sub_county")?>/' + r,
                    beforeSend: function () {
                        $(".sub_county").html('<option value="">Loading...</option>');
                    },
                    success: function (d) {

                        $(".sub_county").html(d);


                    }


                });
            } else {
                $(".sub_county").html('<option value="">Sub county...</option>');
            }
        });


        //getting parish in subcounty


        $('.sub_county').change(function () {
            var r = $('.sub_county').val();
            if (r != '') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_parish")?>/' + r,
                    beforeSend: function () {
                        $(".parish").html('<option value="">Loading...</option>');
                    },
                    success: function (d) {

                        $(".parish").html(d);


                    }

                });
            } else {
                $(".parish").html('<option value="">Parish...</option>');
            }
        });


        //getting Village in Parish


        $('.parish').change(function () {
            var r = $('.parish').val();
            var hf_type = $('.hf_type').val();
            if (r != ''&&hf_type=='health_facility') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_health_facilities")?>/' + r,
                    beforeSend: function () {
                        $(".health_facility").html('<option value="">Loading...</option>');
                    },
                    success: function (d) {

                        $(".health_facility").html(d);


                    }


                });
            } else {
                $(".health_facility").html('<option value="">Health Facility...</option>');
            }
        });

        $('.hf_type').on('change',function () {

            var r = $('.hf_type').val();
            

            if (r != '') {

                // alert(r);

                if (r == 'health_facility') {
                    $('.health_facility_span').show();
                    $('.other_facility').hide();
                } else {
                    $('.health_facility_span').hide();
                    $('.other_facility').show();
                }

            }
        });

        $('.hf_type').ready(function () {

            var r = $('.hf_type').val();


            if (r != '') {

                // alert(r);

                if (r == 'health_facility') {
                    $('.health_facility_span').show();
                    $('.other_facility').hide();
                } else {
                    $('.health_facility_span').hide();
                    $('.other_facility').show();
                }

            }
        });

    });


</script>