
    <span class="<?php echo (isset($hide)?'':'alerts'); ?>">


        <?php if(isset($transparent)){ ?>

    <div class="alert alert-<?php echo isset($alert)?$alert:'info'; ?> alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <?php echo $message; ?>
    </div>

<?php }else{ ?>

    <div class="alert bg-<?php echo isset($alert)?$alert:'info'; ?> <?= isset($centered)?'col-md-offset-3 col-md-6':''; ?>" <?= isset($centered)?' style="margin-top: 100px"':''; ?>>
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <?php echo $message; ?>
    </div>


        <?php } ?>

        </span>

