<ul class="nav navbar-nav">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-phone-outgoing"></i> Make a Request <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">

        <?php foreach ($this->db->select()->from('request_type')->get()->result() as $rt): ?>
            <li class="<?php echo $title=='requests'&&$subtitle==$rt->request_page?'active':''; ?>">
                <?php echo anchor($this->page_level.'requests/'.$rt->request_page,'<i class="'.$rt->icon.'"></i> '.humanize($rt->title)) ?>
            </li>
        <?php endforeach; ?>

        </ul>
    </li>

    <?php if(isset($this->session->id)){?>

        <li class="<?= $title=='my_applications'?'active':''; ?>">

            <?php echo anchor($this->page_level.'my_applications','<i class="icon-stack-text position-left"></i> My Applications') ?>
        </li>

    <?php }else{ ?>
    <li class="<?= $title=='register'?'active':''; ?>">

        <?php echo anchor($this->page_level.'register','<i class="icon-pencil7 position-left"></i> Register') ?>
    </li>
    <?php } ?>

</ul>