<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Dennis Tamale(tamaledns@gmail.com)" name="author"/>
    <title><?php echo isset($title)? humanize($title):'' ?> | <?php echo $this->site_options->title('site_name') ?></title>

    <!-- Global stylesheets -->
<!--    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">-->
    <link href="<?= base_url() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/css/colors.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="<?= base_url($this->site_options->title('site_logo')) ?>" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/loaders/blockui.min.js"></script>

<!--    these are assets for datatables-->

    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/selects/select2.min.js"></script>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/datatables_extension_buttons_init.js"></script>


    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/jasny_bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
<!--    <script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/plugins/ui/moment/moment.min.js"></script>-->
<!--    <script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>-->

<!--These are the assets for Date pickers-->
<!--    <script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/plugins/notifications/jgrowl.min.js"></script>-->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/anytime.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/picker.time.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/pickers/pickadate/legacy.js"></script>
    
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/ui/prism.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/core/app.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/form_select2.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/picker_date.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/pages/datatables_basic.js"></script>



<!--    <script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/pages/form_checkboxes_radios.js"></script>-->

    <!-- /theme JS files -->

    <style>
        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi)
        /*components.css:1157*/
            .layout-boxed {
                background-color: #FFFFFF;
            }
            /*components.css:1125*/
            .layout-boxed {
                background-color: #FFFFFF;
            }
    </style>


</head>

<body class="layout-boxed has-detached-left">

<!-- Main navbar -->
<div class="navbar navbar-inverse bg-primary-800 hidden">
    <div class="navbar-header">

        <a class="navbar-brand" href="<?= base_url()?>"><img src="<?= base_url($this->site_options->title('site_logo')) ?>" alt="<?=  $this->site_options->title('site_name') ?>"></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">


        <?php $this->load->view('home/static/nav_bar_menu_left'); ?>

        <?php $this->load->view('home/static/nav_bar_menu_right'); ?>




    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content 0px 50px 145px;-->
    <div class="page-content" style="shadow: 0px 50px 145px; box-shadow: 0px 50px 145px; -moz-box-shadow: 0px 50px 145px; -webkit-box-shadow: 0px 50px 145px;">

        <!-- Main content -->
        <div class="content-wrapper" >

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4 style="text-align: center"><i class=" icon-arrow-left15"></i>
<!--                            new_arrival_departure-->
                            <?= isset($subtitle)?($subtitle=='new_arrival'?'NEW ARRIVAL FORM':(strtoupper(humanize($subtitle)).' APPLICATION')):'' ?>

                             <i class="icon-arrow-right15"></i></h4>
                    </div>

                    <div class="heading-elements hidden">
                        <div class="heading-btn-group">
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                            <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                        </div>
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-component">
                    <ul class="breadcrumb">
                        <li>
                            <?php echo anchor($this->uri->segment(1),'<i class="icon-home2 position-left"></i>Home') ?>
                        </li>
<!--                        <li>--><?php //echo isset($title)?( strlen($title)>0? anchor($this->page_level.$title,humanize($title)):''):''; ?><!--</li>-->
                        <li class="active"> <?php  echo isset($subtitle)?( strlen($subtitle)>0?humanize($subtitle):''):''; ?></li>

                    </ul>

                    <ul class="breadcrumb-elements ">
                        <?php if(isset($this->session->id)){?>
                            <li class="dropdown dropdown-user <?= !isset($this->session->id)?'hidden':'' ?>">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    <span><?php echo ucwords($this->session->userdata('first_name')); ?> </span>
                                    <i class="caret"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <?php echo anchor($this->page_level.'profile','<i class="icon-user-plus"></i> Account settings'); ?>
                                    </li>
                                    <li class="hidden"><a href="#"><i class="icon-coins"></i> My balance</a></li>
                                    <li class="hidden"><a href="#"><span class="badge bg-blue pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                                    <li class="divider"></li>

                                    <li>
                                        <?php echo anchor($this->page_level.'logout','<i class="icon-switch2"></i> Logout'); ?>
                                    </li>
                                </ul>
                            </li>

                            <?php }else{ ?>
                        <li>

                            <?php echo anchor('login','<i class="icon-switch2"></i> Login'); ?>
                        </li>
                        <? } ?>

                        <li class="hidden"><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        <li class="dropdown hidden">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear position-left"></i>
                                Settings
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                                <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                                <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">



                <?=   isset($alert_page)?$alert_page: $this->session->flashdata('alert');  ?>

                <?php  echo isset($page_view)?$page_view:'No Page'; ?>

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; <?= date('Y') ?>. <a href="<?= base_url() ?>"><?php echo $this->site_options->title('site_name') ?></a> by <a href="http://www.deronltd.com/" target="_blank">DERON LTD</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
