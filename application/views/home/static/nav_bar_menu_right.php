
<ul class="nav navbar-nav navbar-right">

    <?php if(isset($this->session->id)){?>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-people"></i>
            <span class="visible-xs-inline-block position-right">Users</span>
        </a>

        <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-heading">
                Users online
                <ul class="icons-list">
                    <li><a href="#"><i class="icon-gear"></i></a></li>
                </ul>
            </div>

            <ul class="media-list dropdown-content-body width-300">
                <li class="media">
                    <div class="media-left"><img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                    <div class="media-body">
                        <a href="#" class="media-heading text-semibold">Jordana Ansley</a>
                        <span class="display-block text-muted text-size-small">Lead web developer</span>
                    </div>
                    <div class="media-right media-middle"><span class="status-mark border-success"></span></div>
                </li>

                <li class="media">
                    <div class="media-left"><img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                    <div class="media-body">
                        <a href="#" class="media-heading text-semibold">Will Brason</a>
                        <span class="display-block text-muted text-size-small">Marketing manager</span>
                    </div>
                    <div class="media-right media-middle"><span class="status-mark border-danger"></span></div>
                </li>


                <li class="media">
                    <div class="media-left"><img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                    <div class="media-body">
                        <a href="#" class="media-heading text-semibold">Vanessa Aurelius</a>
                        <span class="display-block text-muted text-size-small">UX expert</span>
                    </div>
                    <div class="media-right media-middle"><span class="status-mark border-grey-400"></span></div>
                </li>
            </ul>

            <div class="dropdown-content-footer">
                <a href="#" data-popup="tooltip" title="All users"><i class="icon-menu display-block"></i></a>
            </div>
        </div>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-bubbles4"></i>
            <span class="visible-xs-inline-block position-right">Messages</span>
            <span class="badge bg-warning-400">2</span>
        </a>

        <div class="dropdown-menu dropdown-content width-350">
            <div class="dropdown-content-heading">
                Messages
                <ul class="icons-list">
                    <li><a href="#"><i class="icon-compose"></i></a></li>
                </ul>
            </div>

            <ul class="media-list dropdown-content-body">
                <li class="media">
                    <div class="media-left">
                        <img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                        <span class="badge bg-danger-400 media-badge">5</span>
                    </div>

                    <div class="media-body">
                        <a href="#" class="media-heading">
                            <span class="text-semibold">James Alexander</span>
                            <span class="media-annotation pull-right">04:58</span>
                        </a>

                        <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                    </div>
                </li>

                <li class="media">
                    <div class="media-left">
                        <img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                        <span class="badge bg-danger-400 media-badge">4</span>
                    </div>

                    <div class="media-body">
                        <a href="#" class="media-heading">
                            <span class="text-semibold">Margo Baker</span>
                            <span class="media-annotation pull-right">12:16</span>
                        </a>

                        <span class="text-muted">That was something he was unable to do because...</span>
                    </div>
                </li>

                <li class="media">
                    <div class="media-left"><img src="<?= base_url() ?>assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
                    <div class="media-body">
                        <a href="#" class="media-heading">
                            <span class="text-semibold">Jeremy Victorino</span>
                            <span class="media-annotation pull-right">22:48</span>
                        </a>

                        <span class="text-muted">But that would be extremely strained and suspicious...</span>
                    </div>
                </li>


            </ul>

            <div class="dropdown-content-footer">
                <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
            </div>
        </div>
    </li>


    <?php }else{ ?>

        <li>

            <?php echo anchor('login','<i class="icon-switch2"></i> Login'); ?>
        </li>

   <?php } ?>


    <li class="dropdown dropdown-user <?= !isset($this->session->id)?'hidden':'' ?>">
        <a class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url() ?>assets/images/placeholder.jpg<?//= base_url($this->session->userdata('photo')) ?>" alt="">
            <span><?php echo ucwords($this->session->userdata('first_name')); ?> </span>
            <i class="caret"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <?php echo anchor($this->page_level.'profile','<i class="icon-user-plus"></i> Account settings'); ?>
            </li>
            <li class="hidden"><a href="#"><i class="icon-coins"></i> My balance</a></li>
            <li class="hidden"><a href="#"><span class="badge bg-blue pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
            <li class="divider"></li>

            <li>
                <?php echo anchor($this->page_level.'logout','<i class="icon-switch2"></i> Logout'); ?>
            </li>
        </ul>
    </li>
</ul>

<!-- Left aligned search form -->
<form class="navbar-form navbar-right hidden" action="#">
    <div class="form-group has-feedback">
        <input type="search" class="form-control input-xs bg-primary" placeholder="Search">
        <div class="form-control-feedback">
            <i class="icon-search4 text-normal"></i>
        </div>
    </div>
</form>
<!-- /left aligned search form -->