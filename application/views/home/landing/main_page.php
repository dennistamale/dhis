<!DOCTYPE html>
<!--[if lt IE 7]> <html dir="ltr" lang="en-US" class="ie6"> <![endif]-->
<!--[if IE 7]>    <html dir="ltr" lang="en-US" class="ie7"> <![endif]-->
<!--[if IE 8]>    <html dir="ltr" lang="en-US" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html dir="ltr" lang="en-US"> <!--<![endif]-->

<!-- BEGIN head -->
<head>

    <!--Meta Tags-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- Title -->
    <title><?php echo isset($title)? humanize($title):'' ?> | <?php echo $this->site_options->title('site_name') ?></title>

    <!-- JavaScript (must go in header) -->
<!--    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
    <script type="text/javascript" src="<?= base_url() ?>public_assets/js/fontawesome-markers.min.js"></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?= base_url() ?>public_assets/css/style.css" type="text/css"  media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>public_assets/css/color-yellow.css" type="text/css"  media="all" />
<!--    <link rel="stylesheet" href="--><?//= base_url() ?><!--public_assets/css/color-red.css" type="text/css"  media="all" />-->
    <link rel="stylesheet" href="<?= base_url() ?>public_assets/css/responsive.css" type="text/css"  media="all" />
    <link href="<?= base_url() ?>public_assets/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public_assets/rs-plugin/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public_assets/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public_assets/rs-plugin/css/navigation.css">
<!--    <link rel="stylesheet" type="text/css" href="--><?//= base_url() ?><!--public_assets/css/owl.carousel.css">-->
<!--    <link rel="stylesheet" type="text/css" href="--><?//= base_url() ?><!--public_assets/css/prettyPhoto.css">-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public_assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public_assets/css/font2.css">
<!--    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>-->
<!--    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url($this->site_options->title('site_logo')) ?>" type="image/x-icon" />

    <!-- END head -->
</head>

<!-- BEGIN body -->
<body>

<!-- BEGIN .outer-wrapper -->
<div class="outer-wrapper">

    <!-- BEGIN .header-area-1 -->
    <div class="header-area-1 h" style="box-shadow: 0px 0px 10px; -moz-box-shadow: 0px 0px 10px; -webkit-box-shadow: 0px 0px 10px;">

        <!-- BEGIN .top-bar-wrapper -->
        <div hidden class="top-bar-wrapper">

            <!-- BEGIN .top-bar -->
            <div class="top-bar clearfix">

                <!-- BEGIN .top-bar-left -->
                <div class="top-bar-left">
                    <p>Luxury Transport Services In New York City</p>
                    <!-- END .top-bar-left -->
                </div>

                <!-- BEGIN .top-bar-right -->
                <div class="top-bar-right">
                    <ul>
                        <li><a href="about-us.html">About Us</a></li>
                        <li><a href="our-fleet-3-col.html">Our Fleet</a></li>
                        <li><a href="service-rates.html">Service Rates</a></li>
                    </ul>
                    <!-- END .top-bar-right -->
                </div>

                <!-- END .top-bar -->
            </div>

            <!-- END .top-bar-wrapper -->
        </div>

        <!-- BEGIN .header-content -->




        <div class="header-content" >

            <!-- BEGIN .logo -->
            <div class="logo" style="margin: -10px 0 15px 0 !important;">
                <a href="<?= base_url() ?>">
                <?php
                $image_properties = array(
                    'src'   => 'assets/images/logo-long.png',
                    'alt'   => $this->site_options->title('site_name'),
//                    'class' => 'post_images',
//                    'width' => '50',
                    'height'=> '70',
                    'title' => $this->site_options->title('site_name'),
//                    'rel'   => 'lightbox'
                );

                echo img($image_properties);


                ?>

                </a>


                <!-- END .logo -->
            </div>


            <!-- BEGIN .header-icons-wrapper -->
            <div class="header-icons-wrapper clearfix">



                <!-- BEGIN .header-icons-inner -->
                <div class="header-icons-inner clearfix">



                    <!-- BEGIN .header-icon -->
                    <div class="header-icon">
                        <?= anchor('#','<i class="fa fa-arrow-right" aria-hidden="true"></i><strong>About</strong>') ?>
                        <!-- END .header-icon -->
                    </div>

                    <!-- BEGIN .header-icon -->
                    <div class="header-icon">

                        <?= anchor('login','<i class="fa fa-arrow-right" aria-hidden="true"></i><strong>Login</strong>') ?>

                        <!-- END .header-icon -->
                    </div>

                    <!-- BEGIN .header-icon -->
                    <div class="header-icon">


                        <?= anchor('#','<i class="fa fa-arrow-right" aria-hidden="true"></i><strong>Help</strong>') ?>

                        <!-- END .header-icon -->
                    </div>

                    <!-- END .header-icons-inner -->
                </div>

                <!-- END .header-icons-wrapper -->
            </div>
            <!-- END .header-content -->
        </div>



        <!-- END .header-area-1 -->
    </div>

    <div hidden id="page-header">
        <h1>About Us</h1>
        <div class="title-block3"></div>
        <p><a href="#">Home</a><i class="fa fa-angle-right"></i>About Us</p>
    </div>



    <?=   isset($alert_page)?$alert_page: $this->session->flashdata('alert');  ?>

    <?php  echo isset($page_view)?$page_view:'No Page'; ?>




    <!-- BEGIN .footer -->
<footer class="footer">

    <!-- BEGIN .footer-inner -->
    <div class="footer-inner clearfix">

        <!-- BEGIN .one-half -->
        <div class="one-half">

            <h5>About Us</h5>
            <div class="title-block6"></div>
            <p><?php
                $about=$this->db->select()->from('pages')->where(array('page'=>'about'))->get()->row();

                echo word_limiter($about->content,100);

                ?></p>



            <!-- END .one-half -->
        </div>

        <!-- BEGIN .one-fourth -->
        <div class="one-fourth">

            <h5>Links</h5>
            <div class="title-block6"></div>
            <ul>
                <li><a href="#"></a>
                    <?= anchor('#','About Protocol MIS') ?>
                </li>
                <li>
                    <?= anchor('home/contact','Contact Details') ?>
                </li>


            </ul>
            <!-- END .one-fourth -->
        </div>

        <!-- BEGIN .one-fourth -->
        <div class="one-fourth">

            <h5>Contact Details</h5>
            <div class="title-block6"></div>
            <ul class="contact-widget">

                <li class="cw-address"> Ministry Of Foreign Affairs Street Address: Plot No.2A-B Apollo Kaggwa Road</li>
                <li class="cw-address">1st Floor Room 1.5 Town/City<span>Kampala</span></li>
                <li class="cw-cell">P. O. Box:<span>7048</span></li>
<!--                <li class="cw-phone">Telephone<span>256-041-230911</span></li>-->
<!--                <li class="cw-phone">Facsimile number<span>256-041-230911</span></li>-->
            </ul>


<!--            Attention: Ministry Of Foreign Affairs Street Address: Plot No.2A-B Apollo Kaggwa Road-->
<!--            Floor/Room number: 1st Floor Room 1.5 Town/City: Kampala-->
<!--            P. O. Box: 7048-->
<!--            Country: Uganda-->
<!--            Telephone: 256-041-230911-->
<!--            Facsimile number: 256-041-230911-->


            <!-- END .one-fourth -->
        </div>

        <div class="clearboth"></div>

        <!-- END .footer-bottom -->
    </div>

    <!-- BEGIN .footer-bottom -->
    <div class="footer-bottom">

        <p class="footer-message">&copy; <?= date('Y') ?>. <a href="<?= base_url() ?>"><?php echo $this->site_options->title('site_name') ?></a>. All Rights Reserved</p>

        <!-- END .footer-inner -->
    </div>

    <!-- END .footer -->
</footer>

<!-- END .outer-wrapper -->
</div>

<!-- JavaScript -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script type="text/javascript" src="<?= base_url() ?>assets/js/core/libraries/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/rs-plugin/js/jquery.themepunch.tools.min.js?rev=5.0"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/js/owl.carousel.min.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/js/jquery.prettyPhoto.js"></script>-->

<!-- Only required for local server -->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--public_assets/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>-->
<!-- Only required for local server -->

<script type="text/javascript" src="<?= base_url() ?>public_assets/js/scripts.js"></script>

<!-- END body -->
</body>
</html>