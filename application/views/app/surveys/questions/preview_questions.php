<?php
if(isset($id)){
    $qn=$this->survey_model->get_surveys($id);

}
?>
<style>
    .control-label {
        text-align: right !important;
    }
</style>
<!-- Horizontal form options -->
<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level . $this->page_level2 . 'new_' . singular($title) . '/' . (isset($id) ? $id * date('Y') : ''), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title">Preview : <?= isset($id) ? humanize($qn->title):'' ?></h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $author_id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

                <?php $this->load->view('ajax/location_filter_js') ?>


                <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> name="id" hidden value="<?= isset($id) ? $id : '' ?>">
                <?= $myForm; ?>

            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->

    </div>
</div>
<!-- /vertical form options -->
