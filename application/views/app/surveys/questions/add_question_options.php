<?php //$this->load->view($this->page_level.'stories/story_version'); ?>

<!-- Theme JS files -->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--ckeditor/ckeditor.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/pages/editor_ckeditor.js"></script>-->

<!-- /theme JS files -->

<style>
    .select2-container--disabled .select2-selection--single:not([class*=bg-]) {
        background-color: #fafafa;
        color: #999999;
        border: none;
    }
</style>


<?php
if (isset($id)) {

    $r = $this->db->select('a.*')->from('survey_question_options a')->where(array('a.id' => $id))->get()->row();

    if (!empty($r)) {
        $id = $r->id;
    }
}
?>

<?php

echo validation_errors();

?>


<style>
    .control-label {
        text-align: right !important;
    }
</style>


<div class="row">
    <div class="col-md-6">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open($this->page_level . $this->page_level2 . 'add_question_options/' . $id * date('Y') , array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>

                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">


                <input name="id" hidden value="<?= isset($id) ? $id : '' ?>">


                <div class="row">

                    <div class="col-md-12">


                        <table class="table table-xs">
                            <thead>

                            <tr>

                                <th>Option</th>
                                <th>Label</th>
                            </tr>
                            </thead>

                            <tbody id="forms">

                            <?php foreach ($this->survey_model->get_question_options($id) as  $qp){ ?>

                            <tr>
                                <td>

                                    <input name="option_id[]" hidden value="<?= isset($id) ? $qp->id : '' ?>">

                                    <input  <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                           name="question_option[]"
                                           value="<?= set_value('', isset($id) ? $qp->question_option : '') ?>"
                                           class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                           placeholder="Question Option">
                                    <?php echo form_error('question_option[]', '<span class="text-danger">', '</span>') ?>


                                </td>
                                <td>



                                    <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                         name="option_label[]"
                                                                                         value="<?= set_value('option_label[]', isset($id) ? $qp->option_label : '') ?>"
                                                                                         class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                         placeholder="Expected Value">
                                    <?php echo form_error('option_label[]', '<span class="text-danger">', '</span>') ?>


                                </td>
                            </tr>

                            <?php } ?>


                            <tr>
                                <td>

                                    <input name="option_id[]" hidden value="">

                                    <input <?= $subtitle == 'view' ? 'readonly' : ''; ?>  type="text"
                                                                                         name="question_option[]"
                                                                                         value="<?= set_value('question_option[]') ?>"
                                                                                         class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                         placeholder="Question Option">
                                    <?php echo form_error('question_option[]', '<span class="text-danger">', '</span>') ?>


                                </td>
                                <td>



                                    <input <?= $subtitle == 'view' ? 'readonly' : ''; ?>  type="text"
                                                                                         name="option_label[]"
                                                                                         value="<?= set_value('option_label[]') ?>"
                                                                                         class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                         placeholder="Option Label">
                                    <?php echo form_error('option_label[]', '<span class="text-danger">', '</span>') ?>


                                </td>
                            </tr>

                            </tbody>
                            <tfoot>
                            <tr class="success">
                                <td colspan="2">
                                    <a href="#" class="label label-success pull-right" id="add_row" title="Add row" data-popup="tooltip" ><i class="icon-plus2"></i> Add row</a>
                                </td>
                            </tr>
                            </tfoot>
                        </table>



                    </div>
                </div>


                <div class="text-right <?= $subtitle == 'view' ? 'hidden' : ''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i>
                    </button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>

<script>



    $(document).ready(function () {

        var question_option ='<input name="option_id[]" hidden><input required type="text" name="question_option[]" class="form-control" placeholder="Question Option">';
        var option_label ='<input required type="text" name="option_label[]" class="form-control" placeholder="Option Label Value">';

        var row ='<tr><td>'+question_option+'</td><td>'+option_label+'</td></tr>';


        $("body").delegate('#add_row','click',function (e) {
            e.preventDefault();
            $("#forms").append(row);


        });



    });
</script>


