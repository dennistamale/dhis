
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->

<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">

    <!--    --><?php //print_r($this->input->post()); ?>
    <?= form_open(); ?>

    <div class="panel-body">
        <div class="table-container">
            <div class="table-actions-wrapper">
                <span> </span>

                <?php echo $this->custom_library->role_exist('create survey')? anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New Survey','class="btn btn-sm btn-primary"'):''; ?>



            </div>
            <table class="table datatable-ajax" id="<?= $title ?>">
                <thead>
                <tr>
                    <th style="width: 8px;">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                            <span></span>
                        </label>
                    </th>
                    <!--                    List Applications: Date, Type, Applicant, Passport, Country, Status, Actions-->
                    <th> Title </th>
                    <th> Status </th>
                    <th> Created by </th>
                    <th> Qns </th>
                    <th> Created On </th>
                    <th style="width: 8px;">Action</th>

                </tr>

                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<?php $this->load->view('ajax/'.$title);  ?>
