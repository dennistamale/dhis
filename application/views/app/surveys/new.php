<?php
if(isset($id)){
    $r=$this->db->select('*')->from('survey')->where(array('id'=>$id))->get()->row();
}
?>

<style>
    .control-label{
        text-align: right !important;
    }
</style>
<!-- Horizontal form options -->
<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level.$this->page_level2.'new/'.( isset($id)?$id*date('Y'):'' ), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">

            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>  <?= isset($id)?'<b>'.$r->title.'</b>':'' ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3)=='view'? anchor($this->page_level.$this->page_level2.'edit/'.$id*date('Y'),'<i class="icon-pencil6"></i>','title="Edit&nbsp;Details" data-popup="tooltip"'):''; ?>
                        </li>


                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">




                <input <?= $subtitle=='view'?'readonly':''; ?> name="id" hidden value="<?= isset($id)?$id:'' ?>">




                <div class="form-group">
                    <label class="col-lg-2 control-label">Title:</label>
                    <div class="col-lg-10">
                        <input <?= $subtitle=='view'?'readonly':''; ?> required type="text" name="title"  value="<?= set_value('title',isset($id)?$r->title:'') ?>"  class="form-control" placeholder="Title">
                        <?php echo form_error('title','<span class="text-danger">','</span>') ?>

                    </div>

                </div>



                <div class="form-group">
                    <label class="col-lg-2 control-label">Description:</label>
                    <div class="col-lg-10">

                        <textarea <?= $subtitle=='vie
                        w'?'readonly':''; ?> name="description"  class="form-control" placeholder="Description"><?= set_value('description',isset($id)?$r->title:'') ?></textarea>

                        <?php echo form_error('description','<span class="text-danger">','</span>') ?>

                    </div>

                </div>



                <div class="text-right <?= $subtitle=='view'?'hidden':''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->

    </div>
</div>
<!-- /vertical form options -->
