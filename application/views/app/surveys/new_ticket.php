<?php
if (isset($author_id)) {

    $r = $this->db->select('a.*')->from('author a')->where(array('a.id' => $author_id))->get()->row();

    if (!empty($r)) {
        $id = $r->id;
    }
}
?>
<style>
    .control-label {
        text-align: right !important;
    }
</style>
<!-- Horizontal form options -->
<div class="row">
    <div class="col-md-6">

        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level . $this->page_level2 . 'new_' . singular($title) . '/' . (isset($id) ? $author_id * date('Y') : ''), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?></h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $author_id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

                <?php

                //echo $f=json_encode($myForm,JSON_PRETTY_PRINT);

                //echo '<br/><br/><br/>';
                //print_r($d=json_decode($f,true));

                //print_r($this->input->post())

                ?>

                <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> name="id" hidden
                                                                     value="<?= isset($id) ? $id : '' ?>">
                <?= CI_form_builder($myForm, 'vertical'); ?>

            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->

    </div>
</div>
<!-- /vertical form options -->
