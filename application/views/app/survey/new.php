<?php $this->load->view($this->page_level . $this->page_level2.'surveyjs_header'); ?>

<?php
if(isset($id)){
    $r=$this->db->select('*')->from('survey')->where(array('id'=>$id))->get()->row();

}
?>

<style>
    .control-label{
        text-align: right !important;
    }
</style>
<!-- Horizontal form options -->

<div class="row">

    <div class="col-md-12">
        <input <?= $subtitle=='view'?'readonly':''; ?> name="id" hidden value="<?= isset($id)?$id:'' ?>">
        <div id="surveyContainer">
            <div id="editorElement"></div>
        </div>

    </div>




</div>


<script>
    Survey.Survey.cssType = "bootstrap";
    Survey.defaultBootstrapCss.navigationButton = "btn btn-green";
    // SurveyEditor.editorLocalization.currentLocale = "es";
    var editorOptions = {};
    var editor = new SurveyEditor.SurveyEditor("editorElement", editorOptions);



    //Setting this callback will make visible the "Save" button

    editor.saveSurveyFunc = function (saveNo, callback) {
        // alert('ok');
        var json_form=editor.text;
        var survey_code="<?= isset($id)?$r->survey_code:$this->survey_model->survey_code(); ?>";
        // console.log(json_form);
        callback(saveNo, true);
        console.log(saveNo);
        // alert(json_form);

        $.ajax({
            type: 'POST',
            url: '<?= base_url('ajax_api/save_surveyjs') ?>/'+saveNo+'/'+survey_code,
            data: { pages: JSON.stringify(json_form) },
            // contentType: "application/json",
            dataType: 'text',
            success: function(data) {
                //alert('data: ' + data);

                },
            error: function( xhr, status, error ) {
               console.log(status+':'+error);
            }

        });


    };

    editor.showOptions = true;
    editor.showState = true;


    <?php
    if(isset($id)){

    ?>

    var  json ='<?= substr(substr(json_encode($r->json_form),1),0,-1) ?>';


    <?php }else{  ?>

    var json = '{\n' +
        ' "title": "Survey Title",\n' +
        ' "pages": [\n' +
        '  {\n' +
        '   "name": "page1"\n' +
        '  }\n' +
        ' ]\n' +
        '}';




    <? } ?>

    editor.text = json;
</script>