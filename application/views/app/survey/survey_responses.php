<?php
$survey_id=$this->uri->segment(4);

?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->

<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">

    <!--    --><?php //print_r($this->input->post()); ?>
    <?= form_open(); ?>

    <div class="panel-body">
        <div class="table-container">
            <div class="table-actions-wrapper">
                <span> </span>

                <?php echo $this->custom_library->role_exist('create survey')? anchor($this->page_level.$this->page_level2.'preview_question/'.$this->uri->segment(4),' <i class="fa fa-eye"></i> Preview Question','class="btn btn-sm btn-primary"'):''; ?>



                <div class="btn-group">
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">Assessments <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right">

                        <?php

                        $this->db
                            ->select()
                            ->from('survey');
                        $this->db->limit(10)->order_by('id','desc');
                        $surveys=$this->db->get()->result();


                        foreach ($surveys as $as): ?>

                        <li>
                        <?= anchor('app/survey/survey_responses/'.$as->id*date('Y'),'<i class="icon-arrow-right15"></i> '.$as->title); ?>
                        </li>

                        <?php endforeach; ?>

                    </ul>
                </div>


            </div>

            <table class="table datatable-ajax" id="<?= $subtitle ?>">
                <thead>
                <tr>
                    <th style="width: 8px;">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                            <span></span>
                        </label>
                    </th>
<?php
echo strlen($survey_id)==0?'<th>Assessment</th>':'';
?>
                    <th> District </th>
                    <th> Institution </th>
                    <th> Level </th>
                    <th> Created by </th>
                    <th> Created On </th>
                    <th style="width: 8px;">Action</th>

                </tr>

                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<?php $this->load->view('ajax/'.$subtitle);  ?>
