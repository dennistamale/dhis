
<?php

if(isset($id)){
    $qn=$this->survey_model->get_surveys($id);

}
$no_rows=2;

$survey_options=$this->survey_model->get_survey_options();
?>


<style>
    .table > tbody > tr > td {
        padding: 0px 0px;
        line-height: 1.5384616;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .add_row{
        cursor: copy ;
        margin-top: 5px;
    }
</style>


<?= validation_errors(); ?>



<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-bold"><?= humanize($subtitle); ?> Facility</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li style="color: white;"><?= $subtitle == 'view' ? anchor($this->page_level.$this->page_level2.'preview_question/'.$id*date('Y'),'<i class="icon-image3"></i> Preview','class="label bg-purple"'):'' ?></li>

                <li><?= $subtitle == 'view' ? anchor($this->page_level.$this->page_level2.'edit/'.$id*date('Y'),'<i class="icon-pencil7"></i> Edit','class="label label-info"'):'' ?></li>
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">


        <?= form_open($this->page_level.$this->page_level2.'new/'.( isset($id)?$id*date('Y'):'' ), array('class' => 'form-horizontal')) ?>

        <input name="survey_id" hidden value="<?= isset($id)?$id:'' ?>">




        <div class="content-group">




            <div class="form-group">
                <label class="col-lg-2 control-label"><span class="pull-right">Survey Title:</span></label>
                <div class="col-lg-10">
                    <input <?= $subtitle=='view'?'readonly':''; ?> required type="text" name="title"  value="<?= set_value('title',isset($id)?$qn->title:'') ?>"  class="form-control" placeholder="Survey Title">
                    <?php echo form_error('title','<span class="text-danger">','</span>') ?>

                </div>

            </div>



            <div class="form-group">
                <label class="col-lg-2 control-label"><span class="pull-right">Description:</span></label>
                <div class="col-lg-10">

                    <textarea <?= $subtitle=='view'?'readonly':''; ?> name="description"  class="form-control" placeholder="Description"><?= set_value('description',isset($id)?$qn->description:'') ?></textarea>

                    <?php echo form_error('description','<span class="text-danger">','</span>') ?>

                </div>

            </div>





            <div class="form-group">

                <legend  class="text-bold">Questions</legend>

                <div class="table-responsive">
                    <table class="table  table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 8px;">#</th>
                            <th width="70%">Question</th>
                            <th  width="10%">Q-type</th>
                            <th  width="10%">Required?</th>
                            <th width="10%" colspan="2">Placeholder</th>
                        </tr>
                        </thead>
                        <tbody id="question_forms">

                        <?php if(isset($id)&&$f= $survey_qns=$this->survey_model->get_survey_questions($id)!=false){
                            $i=1;



                            foreach ( $survey_qns=$this->survey_model->get_survey_questions($id) as $r){
                                ?>


                                <tr data-row="<?= $i ?>" class="question_rows">


                                    <td>
                                        <input hidden value="<?= $r->id ?>"  name="qn[id][]">
                                        <span style="padding-left: 20px;"><?= $i ?></span></td>
                                    <td><input  <?= $subtitle=='view'?'readonly':''; ?>  data-indicator_id=""
                                                                                         class="no-background no-border question_rows form-control"
                                                                                         placeholder="Question" name="qn[question][]"  value="<?= $r->question ?>" ></td>



                                    <td>



                                        <select  <?= $subtitle=='view'?'readonly':''; ?>  class="no-background no-border question_rows form-control"  name="qn[question_type][]">

                                            <option value="">Question Type</option>
                                            <?php foreach ($survey_options as $op){ ?>
                                                <option value="<?= $op->id ?>" <?= set_select('qn[question_type][]',$op->id,$op->id==$r->question_type?true:'') ?> > <?= $op->title ?></option>
                                            <?php } ?>

                                        </select>
                                    </td>

                                    <td style="padding-left: 10px;">

                                        <label class="radio-inline">
                                            <input type="radio" name="qn[required][<?= $i ?>]" value="0"   <?= set_radio('qn[required]['.$i.']', 0,$r->required==0?true:'' ); ?>>No</label>

                                        <label class="radio-inline">
                                            <input type="radio" name="qn[required][<?= $i ?>]" value="1"  <?= set_radio('qn[required]['.$i.']', 1,$r->required==1?true:''); ?>>Yes</label>


                                    </td>




                                    <td>

                                        <input  <?= $subtitle=='view'?'readonly':''; ?>  data-indicator_id=""
                                                                                         class="no-background no-border question_rows form-control"
                                                                                         placeholder="Placeholder" name="qn[placeholder][]"  value="<?= $r->placeholder ?>" >
                                    </td>
                                    <td>



                                        <?= $subtitle=='edit'? anchor($this->page_level.'surveys/add_question_options/'.$r->id*date('Y'), '<i class="text-warning   icon-menu2"></i>',' data-popup="tooltip" data-placement ="left" title="Add Options"'):''; ?>


                                    </td>
                                </tr>

                                <?php $i++;
                            }
                        }else { ?>

                            <?php for ($i = 1; $i <= $no_rows; $i++) { ?>

                                <tr data-row="<?= $i ?>" class="question_rows">


                                    <td>
                                        <input hidden value=""  name="qn[id][]">
                                        <span style="padding-left: 20px;"><?= $i ?></span></td>
                                    <td>

                                        <input  class="no-background no-border question_rows form-control" placeholder="Question" name="qn[question][]"

                                                value="<?= set_value('qn[question][]') ?>"
                                        >

                                    </td>

                                    <td>


                                        <select class="no-background no-border question_rows form-control"  name="qn[question_type][]">

                                            <option value="">Question Type</option>
                                            <?php foreach ($survey_options as $r){ ?>
                                                <option value="<?= $r->id ?>" <?= set_select('qn[question_type][]',$r->id) ?>> <?= $r->title ?></option>
                                            <?php } ?>

                                        </select>
                                    </td>

                                    <td style="padding-left: 10px;">

                                        <label class="radio-inline">
                                            <input type="radio" name="qn[required][<?= $i ?>]" value="0"   <?= set_radio('qn[required]['.$i.']', '0',TRUE ); ?>>No</label>

                                        <label class="radio-inline">
                                            <input type="radio" name="qn[required][<?= $i ?>]" value="1"  <?= set_radio('qn[required]['.$i.']', '1'); ?>>Yes</label>


                                    </td>

                                    <td colspan="2">
                                        <input   class="no-background no-border question_rows form-control" placeholder="Placeholder" name="qn[placeholder][]"   value="<?= set_value('qn[placeholder][]') ?>">
                                    </td>



                                </tr>

                            <?php }
                        }
                        ?>


                        </tbody>
                    </table>

                </div>
                <div  data-q_row="<?= $i ?>" id="add_qn"  class="<?= $subtitle=='view'?'hidden':''; ?>    label label-success label-icon label-rounded pull-right add_row" data-popup="tooltip" data-placement ="left" title="Add row" ><b><i class=" icon-plus2"></i></b></div>



            </div>


        </div>




        <?php if($subtitle!='view'){ ?>

            <div class="text-right">
                <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>

        <?php } ?>

        <?= form_close() ?>

    </div>
</div>



<script>



    $(document).ready(function () {




        $("body").delegate('#add_qn','click',function (e) {


            var current_row = $( this ).data();
            var current_count=current_row.q_row;
            var new_count=current_count+1;


            var id ='<input hidden value="" name="qn[id][]">';

            var question ='<input class="no-background no-border indicator_estimate form-control" placeholder="Question" name="qn[question][]">';

            var question_type ='<select class="no-background no-border question_rows form-control"  name="qn[question_type][]"> <option value="">Question Type</option><?php foreach ($survey_options as $r){ ?><option value="<?= $r->id ?>"> <?= $r->title ?></option><?php } ?></select>';

            var required =' <label class="radio-inline"><input type="radio" name="qn[required]['+new_count+']" value="0"   <?= set_radio('qn[required]['.$i.']', '0',TRUE ); ?>>No</label><label class="radio-inline"><input type="radio" name="qn[required]['+new_count+']" value="1"  <?= set_radio('qn[required]['.$i.']', '1'); ?>>Yes</label>';

            var placeholder ='<input class="no-background no-border placeholder form-control" placeholder="Placeholder" name="qn[placeholder][]" >';

            var row ='<tr data-row="'+(current_count)+'"   class="question_rows"> <td>'+id+'<span style="padding-left: 20px;">'+current_count+'</span></td><td>'+question+'</td><td>'+question_type+'</td><td  style="padding-left: 10px;">'+required+'</td><td >'+placeholder+'</td><td></td></tr>';
            e.preventDefault();

            $("#question_forms").append(row);
            current_row.q_row=new_count;
        });





    });
</script>



