<script src="<?= base_url() ?>assets/surveyjs/survey.jquery.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>assets/surveyjs/survey.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/surveyjs/index.css">


<?php
if(isset($id)){
    $qn=$this->db->select('*')->from('survey')->where(array('id'=>$id))->get()->row();

}
?>

<?php if(isset($answer_id)){
        $answer= $this->survey_model->get_survey_answer($answer_id);
}
?>

<style>
    .control-label {
        text-align: right !important;
    }
</style>
<!-- Horizontal form options -->
<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->

        <div class="panel panel-flat">
            <div class="panel-heading">

                <h5 class="panel-title">Preview : <?= isset($id)&&!empty($qn->title) ? humanize($qn->title):'' ?></h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $author_id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

                <?= form_open_multipart($this->page_level . $this->page_level2 . 'new_' . singular($title) . '/' . (isset($id) ? $id * date('Y') : ''), array('class' => 'form-horizontal location_form')) ?>


                <?php


                if((isset($answer_id))) {
                    $lc = array(

                        'district' => $answer->district,
                        'date' => $answer->date,
                        'health_facility' => $answer->health_facility,
                        'level' => $answer->level,
                        'auditor' => $answer->auditor,
                        'auditor_designation' => $answer->auditor_designation,
                        'respondent_name1' =>$answer->respondent_name1,
                        'designation1' => $answer->designation1,
                        'respondent_name2' => $answer->respondent_name2,
                        'designation2' => $answer->designation2,
                        'respondent_name3' => $answer->respondent_name3,
                        'designation3' => $answer->designation3,
                        'respondent_name4' => $answer->respondent_name4,
                        'designation4' => $answer->designation4,

                    );
                    $location['id']=true;
                    $location['fac']=json_decode(json_encode($lc));


                }else{
                    $location['id']=null;
                }



                $this->load->view('app/survey/questions/respondents_field',$location);

//                $this->load->view('ajax/location_filter_js',$location);


                ?>

                <?= form_close() ?>
                <div id="surveyElement"></div>
                <div id="surveyResult"></div>


            </div>
        </div>

        <!-- /basic layout -->

    </div>
</div>
<!-- /vertical form options -->

<?php
$json=!empty($qn->json_form)?$qn->json_form:json_encode(array());

?>


<script>
    // Survey.StylesManager.applyTheme("orange");
    // Survey.StylesManager.applyTheme("darkblue");




    Survey.defaultBootstrapCss.navigationButton = "btn btn-xs btn-primary";

    Survey.Survey.cssType = "bootstrap";


    var json = <?= $json ?>;

    var model = new Survey.Model(json);

    model
        .onComplete
        .add(function (result) {
            //document
                //.querySelector('#surveyResult')
                // .innerHTML = "result: " + JSON.stringify(result.data);


            var survey_id =<?=$id?>;
            var location =$('form').serialize();

            $.ajax({
                type: 'POST',
                url: '<?= base_url('ajax_api/save_surveyjs_response') ?>/'+survey_id+'/?'+location,
                data: { pages: JSON.stringify(result.data) },
                // contentType: "application/json",
                dataType: 'text',
                success: function(data) {

                    $('.location_form').hide();
                    //alert('data: ' + data);

                },
                error: function( xhr, status, error ) {
                    console.log(status+':'+error);
                }

            });


        });






    model
        .onUpdateQuestionCssClasses
        .add(function (survey, options) {
            var classes = options.cssClasses

            // classes.root = "sq-root";
            // classes.title = "sq-title";
            classes.lable = "sv_qcbc";
            classes.item = "sq-item";
            // classes.label = "sq-label";
            //
            // if (options.question.isRequired) {
            //     classes.title = "sq-title sq-title-required";
            //     classes.root = "sq-root sq-root-required";
            // }
            //
            // if (options.question.getType() === "checkbox") {
            //     classes.root = "sq-root sq-root-cb";
            // }

        });




    <?php if(isset($answer_id)){
    ?>

    model.data = <?= $answer->json ?>;
    model.mode ='display';

    <?php } ?>





    $("#surveyElement").Survey({model:model});




</script>

<script>



    $( document ).ready(function() {

        $('#surveyElement').ready(function(){

            $( "h3" ).remove();

            // $('.card-block').prepend($('.location_form').html())
        });

        $(body).delegate(function(){

            $('.sv_next_btn').click(function{

                alert('');

            });


        });



    });


</script>
