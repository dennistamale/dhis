<?php  $no_rows=4; ?>
<?php $districts=$this->hf_model->get_hf_districts(); ?>

<style>
    .table > tbody > tr > td{
        padding: 0px 0px;
        line-height: 1.5384616;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .add_row{
        cursor: copy ;
        margin-top: 5px;
    }

    .label_field{
        padding: 8px !important;
        width: 150px;
        /*line-height: 1.5384616;*/
        /*vertical-align: top;*/
        /*border-top: 1px solid #ddd;*/
    }





</style>


<div class="form-group">

    <legend  class="text-bold">Facilitators</legend>

    <div class="table-responsive">
        <table class="table table-sm  table-bordered">

            <tbody id="fac_forms">

            <tr class="facilitator_rows">

                <td  class="label_field">  District </td>

                <td>
                    <div class="form-group">
                    <select name="district" class="district form-control select" required  <?= $subtitle=='view'?'disabled':''; ?> >
                        <option value="" selected>District</option>
                        <?= isset($id)?'<option value="'.$fac->district.'" selected>'.$fac->district.'</option>':''; ?>
                        <?php foreach ($districts as $d){ ?>
                            <option value="<?= $d ?>" <?= set_select('district', $d) ?>><?= $d ?></option>
                        <?php } ?>

                    </select>
                    <?php echo form_error('District','<span class="text-danger">','</span>') ?>

                        </div>
                </td>

                <td  class="label_field">Date</td>

                <td><div class="form-group">
                    <input  <?= $subtitle=='view'?'readonly':''; ?>  required name="date" type="text" value="<?= set_value('date',isset($id)&&$subtitle!='add_another'?date('d F Y',$fac->date):date('d F Y')) ?>" class="form-control pickadate-selectors" placeholder="Date"   data-popup="tooltip" title="" data-placement="top" data-original-title="Date">
                    </div>



            </tr>

            <tr class="facilitator_rows">

                <td  class="label_field">    Health Facility    </td>

                <td>
                    <div class="form-group">
                    <select name="health_facility" class="form-control health_facility select" required  <?= $subtitle=='view'?'disabled':''; ?> >
                        <option value="" selected>Health Facility</option>

                        <?php

                        if(isset($id)){
                            $hf=$this->model->get_facility_by_id($fac->health_facility);

                            echo isset($id)?'<option value="'.$fac->health_facility.'" selected>'.$hf->health_unit.'</option>':'';
                        }

                        ?>


                    </select>
                    <?php echo form_error('health_facility','<span class="text-danger">','</span>') ?>
                        </div>
                </td>

                <td  class="label_field">Level</td>

                <td>

                    <input   <?= $subtitle=='view'?'readonly':'readonly'; ?>   class="no-background no-border form-control level" placeholder="Level" name="level" value="<?= set_value('level',isset($id)?$fac->level:'') ?>">


                </td>

            </tr>


            <tr class="facilitator_rows">

                <td  class="label_field">    Name of Auditor     </td>

                <td>
                    <div class="form-group">
                        <input   <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Auditor" name="auditor" value="<?= set_value('auditor',isset($id)?$fac->auditor:'') ?>">
                    <?php echo form_error('auditor','<span class="text-danger">','</span>') ?>
                        </div>
                </td>

                <td  class="label_field">Designation of Auditor</td>

                <td>

                    <input   <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Designation" name="auditor_designation" value="<?= set_value('auditor_designation',isset($id)?$fac->auditor_designation:'') ?>">


                </td>

            </tr>


            <tr class="facilitator_rows">
                <td></td>
                <td  class="label_field">  Name Of Respondents </td>
                <td></td>
                <td  class="label_field">  Designation Of Respondents </td>


            </tr>




                    <tr  class="facilitator_rows">

                        <td  class="label_field">  Respondent 1 :   </td>

                        <td>



                            <input  <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Respondent" name="respondent_name1" value="<?= set_value('respondent_name1',isset($id)?$fac->respondent_name1:'') ?>"></td>

                        <td  class="label_field">Designation</td>

                        <td>
                            <input  class="no-background no-border form-control" placeholder="Designation" name="designation1"
                                    value="<?= set_value('designation1',isset($id)?$fac->designation1:'') ?>"
                            ></td>

                    </tr>




                    <tr  class="facilitator_rows">

                        <td  class="label_field">  Respondent 2 :   </td>

                        <td><input  <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Respondent" name="respondent_name2"
                                   value="<?= set_value('respondent_name2',isset($id)?$fac->respondent_name2:'') ?>"
                            ></td>

                        <td  class="label_field">Designation</td>

                        <td><input  class="no-background no-border form-control" placeholder="Designation" name="designation2"
                                    value="<?= set_value('designation2',isset($id)?$fac->designation2:'') ?>"
                            ></td>

                    </tr>




                    <tr  class="facilitator_rows">

                        <td  class="label_field">  Respondent 3 :   </td>

                        <td><input  <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Respondent" name="respondent_name3"  value="<?= set_value('respondent_name3',isset($id)?$fac->designation3:'') ?>"></td>

                        <td  class="label_field">Designation</td>

                        <td><input  class="no-background no-border form-control" placeholder="Designation" name="designation3"  value="<?= set_value('designation3',isset($id)?$fac->designation3:'') ?>"></td>

                    </tr>




                    <tr  class="facilitator_rows">

                        <td  class="label_field">  Respondent 4:   </td>

                        <td><input  <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Respondent" name="respondent_name4" value="<?= set_value('respondent_name4',isset($id)?$fac->respondent_name4:'') ?>"></td>

                        <td  class="label_field">Designation</td>

                        <td><input  <?= $subtitle=='view'?'readonly':''; ?>   class="no-background no-border form-control" placeholder="Designation" name="designation4" value="<?= set_value('designation4',isset($id)?$fac->designation4:'') ?>"></td>

                    </tr>




            </tbody>
        </table>

    </div>




</div>


<script>


    $(document).ready(function () {


        $('.district').change(function () {

            var r = $('.district').val();

            console.log(r);

            if (r != '') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_district_health_facilities")?>/' + r,
                    beforeSend: function () {
                        $(".health_facility").html('<option value="">Loading...</option>');
                    },
                    success: function (d) {

                        $(".health_facility").html(d);


                    }


                });
            } else {
                $(".health_facility").html('<option value="">Health Facility...</option>');
            }
        });

        $('.health_facility').change(function () {

            var r = $('.health_facility').val();

            console.log(r);

            if (r != '') {
                $.ajax({
                    type: 'GET',

                    url: '<?php echo base_url("index.php/health_facilities/get_hf_single_item")?>/' + r,
                    beforeSend: function () {
                        $(".level").val('Please wait..');
                    },
                    success: function (d) {

                        $(".level").val(d);
                    }


                });
            } else {
                $(".level").html('');
            }
        });

    });

</script>
