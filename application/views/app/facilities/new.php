<?php

if(isset($id)){
    $fac=$this->model->get_facility_by_id($id);
}

?>


<?php
//$ddate = date('Y-m-d');
//$date = new DateTime($ddate);
//$week = $date->format("W");
//echo "Date: $ddate";
//echo '<br/>';
//echo "Weeknummer: $week";
 ?>

<?= validation_errors(); ?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= humanize($subtitle); ?> Facility</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><?= $subtitle == 'view' ? anchor($this->page_level.$this->page_level2.'edit/'.$id*date('Y'),'<i class="icon-pencil7"></i> Edit','class="label label-info"'):'' ?></li>
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">


        <?= form_open('', array('class' => 'form-horizontal')) ?>


        <fieldset class="content-group">



            <!--            <legend class="text-bold">Basic inputs</legend>-->

            <input name="id" hidden value="<?= isset($id)?$id:'' ?>">
            <input name="country" hidden value="UG">



            <div class="form-group">
                <label class="control-label col-lg-2">Facility Name</label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="facility_name" value="<?= set_value('facility_name',isset($id)?$fac->health_unit:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('facility_name','<span class="text-danger">','</span>') ?>
                </div>



                <label class="control-label col-lg-2"><span class="pull-right">Owner</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="owner" value="<?= set_value('owner',isset($id)?$fac->owner:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('owner','<span class="text-danger">','</span>') ?>
                </div>




            </div>


            <div class="form-group">
                <label class="control-label col-lg-2">Authority</label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="authority" value="<?= set_value('authority',isset($id)?$fac->authority:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('authority','<span class="text-danger">','</span>') ?>
                </div>



                <label class="control-label col-lg-2"><span class="pull-right">Level</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="level" value="<?= set_value('level',isset($id)?$fac->level:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('level','<span class="text-danger">','</span>') ?>
                </div>




            </div>

            <div class="form-group">


                <?php $districts=$this->model->get_district() ?>

                <label class="control-label col-lg-2">Select District  </label>
                <div class="col-lg-4">
                    <select class="select" name="district" class="form-control" required  <?= $subtitle=='view'?'disabled':''; ?> >
                        <option value="" selected>District</option>
                        <?php foreach ($districts as $d){ ?>
                            <option value="<?= $d->title ?>" <?= set_select('state', $d->title, isset($id)?(strtolower($d->title)==strtolower($fac->district)?true:''):'') ?>><?= $d->title ?></option>
                        <?php } ?>

                    </select>
                    <?php echo form_error('village','<span class="text-danger">','</span>') ?>
                </div>



                <label class="control-label col-lg-2"><span class="pull-right">County</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="county" value="<?= set_value('county',isset($id)?$fac->county:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('county','<span class="text-danger">','</span>') ?>
                </div>



            </div>


            <div class="form-group">

                <label class="control-label col-lg-2"><span class="">Sub County</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="sub_county" value="<?= set_value('sub_county',isset($id)?$fac->sub_county:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('sub_county','<span class="text-danger">','</span>') ?>
                </div>


                <label class="control-label col-lg-2"><span class="pull-right">Parish</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle=='view'?'readonly':''; ?> name="parish" value="<?= set_value('parish',isset($id)?$fac->parish:'') ?>" type="text" class="form-control" required>

                    <?php echo form_error('parish','<span class="text-danger">','</span>') ?>
                </div>


            </div>




        </fieldset>

        <?php if($subtitle!='view'){ ?>

            <div class="text-right">
                <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>

        <?php } ?>

        <?= form_close() ?>

    </div>
</div>
