<?php

if(isset($id)){
    $fac_cont=$this->db->select()->from('facility_contact')->where(array('facility_id'=>$id))->get()->result();


}

?>

<?php

if(isset($id)){
    $f=$this->db->select('name')->from('health_facility')->where(array('id'=>$id))->get()->row();
}

?>

<?php

//print_r($this->input->post())

?>



        <?= $subtitle =='edit'||$subtitle =='view'?'':'<div class="row">' ?>
<div class=" <?= $subtitle =='edit'||$subtitle =='view'?'col-md-7':'col-md-12' ?>" >

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= humanize($subtitle); ?> to <?= humanize($f->name); ?> Facility </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">


        <?= form_open($this->page_level.$this->page_level2.'add_contacts', array('class' => 'form-horizontal')) ?>


        <fieldset class="content-group">

<!--            <legend class="text-bold">Basic inputs</legend>-->

            <input name="id" hidden value="<?= isset($id)?$id:'' ?>">

            <?php for($i=0;$i<3;$i++){ ?>

            <div class="form-group">
                <label class="control-label col-lg-2">Contact Details <?= $i+1; ?></label>
                <div class="col-lg-3">

                    <input <?= $subtitle=='view_contact'?'readonly':''; ?> name="first_names[][first_name]" value="<?= set_value('first_names[][first_name]',isset($fac_cont[$i])?$fac_cont[$i]->first_name:'') ?>" type="text" class="form-control" placeholder="First Name" <?= $i==0?'required':'' ?>>

                    <?php echo form_error('first_names[][first_name]','<span class="text-danger">','</span>') ?>
                </div>
                <div class="col-lg-3">

                    <input <?= $subtitle=='view_contact'?'readonly':''; ?> name="last_names[][last_name]" value="<?= set_value('last_names[][last_name]',isset($fac_cont[$i])?$fac_cont[$i]->last_name:'') ?>" type="text" class="form-control" placeholder="Last Name" <?= $i==0?'required':'' ?>>

                    <?php echo form_error('last_names[][last_name]','<span class="text-danger">','</span>') ?>
                </div>
                <div class="col-lg-3">

                    <input maxlength="12" <?= $subtitle=='view_contact'?'readonly':''; ?> <?= isset($fac_cont[$i])?'readonly':'' ?> name="contacts[][contact]" value="<?= set_value('contacts[][contact]',isset($fac_cont[$i])?$fac_cont[$i]->contact:'') ?>" type="text" class="form-control" placeholder="Phone No" <?= $i==0?'required':'' ?>>

                    <?php echo form_error('contacts[][contact]','<span class="text-danger">','</span>') ?>
                </div>
                <div class="col-lg-1">

                    <?= isset($fac_cont[$i]->id)?anchor($this->page_level.$this->page_level2.'remove_contact/'.$fac_cont[$i]->id*date('Y').'/'.$id*date('Y'),' <i class="icon-phone-minus"></i>','class="text-danger"'):''; ?>
                </div>
            </div>

            <?php } ?>

        </fieldset>


        <?php if($subtitle!='view_contact'){ ?>

        <div class="text-right">
            <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
            <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
        </div>
        <?php } ?>

        <?= form_close() ?>

    </div>
</div>

</div>


<?= $subtitle =='edit'||$subtitle =='view'?'':'</div>' ?>

