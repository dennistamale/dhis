<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="panel panel-white">
            <div class="panel-heading">
            <div class="panel-title">
                <h5>
                    <i class="icon-stairs-up"></i>
                    <span class="caption-subject bold uppercase">Workplan Levels&nbsp;</span>

                    <span class="pull-right">
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn btn-success btn-xs"'); ?>
                        </span>
                </h5>

            </div>
        </div>
            <div class="panel-body">
                <table class="table  datatable-basic" id="sample_">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Name </th>
                        <th> Level </th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select()->from('workplan_level')->order_by('level','asc')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->name ?> </td>
                            <td> <?php echo $c->level ?> </td>

                            <td>
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">

                                            <li>

                                                <?php echo anchor($this->page_level.$this->page_level2.'view/'.$c->id*date('Y'),'  <i class="icon-eye"></i> View') ?>
                                            </li>
                                            <li>

                                                <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$c->id*date('Y'),'  <i class="icon-pencil6"></i> Edit') ?>
                                            </li>
                                            <li>

                                                <?php echo anchor($this->page_level.$this->page_level2.'delete/'.$c->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>


                            </td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->