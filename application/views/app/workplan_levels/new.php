
<?php
if (isset($id)) {

    $r = $this->db->select('a.*')->from('workplan_level a')->where(array('a.id' => $id))->get()->row();

    if (!empty($r)) {
        $id = $r->id;
    }

    $data['id']=$id;

}
?>




<div class="row">
    <div class="col-md-12">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level . $this->page_level2 . 'new/' . (isset($id) ? '/' . $id * date('Y') : ''), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>


                        <li><a data-action="collapse"></a></li>
                        <!--                        <li><a data-action="reload"></a></li>-->
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">


                <input name="id" hidden value="<?= isset($id) ? $id : '' ?>">

                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">


                            <div class="col-lg-6">
                                <label>Name:</label>

                                <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                                                              name="name"
                                                                                                                              value="<?= set_value('name', isset($id) ? $r->name : '') ?>"
                                                                                                                              class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                                                              placeholder="Name">
                                <?php echo form_error('name', '<span class="text-danger">', '</span>') ?>

                            </div>



                            <div class="col-lg-6">
                                <label>Level:</label>

                                <input <?= $subtitle == 'view'||isset($id) ? 'readonly' : ''; ?> required type="number"
                                                                                                                             name="level"
                                                                                                                             value="<?= set_value('level', isset($id) ? $r->level : '') ?>"
                                                                                                                             class="form-control <?= $subtitle == 'view'? 'no-border' : ''; ?> "
                                                                                                                             placeholder="Level">
                                <?php echo form_error('level', '<span class="text-danger">', '</span>') ?>

                            </div>



                        </div>




                    </div>
                </div>


                <div class="text-right <?= $subtitle == 'view'? 'hidden' : ''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i>
                    </button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>

