
<?php
if(isset($author_id)){

    $r=$this->db->select('a.*')->from('author a')->where(array('a.id'=>$author_id))->get()->row();

    if(!empty($r)){
        $id=$r->id;
    }
}


?>

<?php $country=$this->db->select('a.a2_iso,a.a3_iso,a.country,a.nationality,a.dialing_code')->from('country a')->join('selected_countries b','a.a2_iso=b.a2_iso')->get()->result() ?>

<?php // print_r($this->input->post()) ?>
<style>
    .control-label{
        text-align: right !important;
    }
</style>
<!-- Horizontal form options -->
<div class="row">
    <div class="col-md-12">

        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level.$this->page_level2.'new_author/'.( isset($id)?$author_id*date('Y'):'' ), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>  <?= isset($id)?'<b>'.$r->first_name.'</b>':'' ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3)=='view'? anchor($this->page_level.$this->page_level2.'edit/'.$author_id*date('Y'),'<i class="icon-pencil6"></i>','title="Edit&nbsp;Details" data-popup="tooltip"'):''; ?>
                        </li>


                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">




                <input <?= $subtitle=='view'?'readonly':''; ?> name="id" hidden value="<?= isset($id)?$id:'' ?>">




                <div class="form-group">


                    <label class="control-label col-lg-2 ">Country</label>
                    <div class="col-lg-4">
                        <select class="select form-control country" name="country"  required  <?= $subtitle=='view'?'disabled':''; ?> >
                            <option value="" selected>Select Country</option>
                            <?php foreach ($country as $d){ ?>

                                <!--this determins if this a dependant or not-->


                                <option value="<?= $d->a2_iso ?>" <?= set_select('country', $d->a2_iso, isset($id)?($d->a2_iso==$r->country?true:''):'') ?>><?= $d->country ?></option>



                            <?php } ?>

                        </select>
                        <?php echo form_error('country','<span class="text-danger">','</span>') ?>
                    </div>


                    <label class="col-lg-2 control-label">District:</label>

                    <div class="col-lg-4">

                        <?php $state=$this->db->select('state,title')->from('state')->where(array('country'=>'UG'))->get()->result(); ?>

                        <select style="font-size: xx-small !important; width: 100px;" class="select form-control district" name="district"  required  <?= $subtitle=='view'?'disabled':''; ?> >
                            <option value="" selected>Select District</option>

                            <?php foreach ($state as $d){ ?>

                                <option value="<?= $d->state ?>" <?= set_select('state', $d->state, isset($id)?($d->state==$r->state?true:''):'') ?>><?= $d->title ?></option>

                            <?php } ?>

                            <option value="0"  <?= set_select('state', 0, isset($id)?(0==$r->state?true:''):'') ?>>Not Available</option>
                        </select>

                    </div>

                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">First Name:</label>
                    <div class="col-lg-4">
                        <input <?= $subtitle=='view'?'readonly':''; ?> required type="text" name="first_name"  value="<?= set_value('first_name',isset($id)?$r->first_name:'') ?>"  class="form-control" placeholder="First Name">
                        <?php echo form_error('first_name','<span class="text-danger">','</span>') ?>

                    </div>

                    <label class="col-lg-2 control-label">Last Name:</label>
                    <div class="col-lg-4">
                        <input <?= $subtitle=='view'?'readonly':''; ?> required type="text" name="last_name"  value="<?= set_value('last_name',isset($id)?$r->last_name:'') ?>"  class="form-control" placeholder="Last Name">
                        <?php echo form_error('last_name','<span class="text-danger">','</span>') ?>

                    </div>
                </div>

                <div class="form-group">

                    <label class="col-lg-2 control-label">Gender:</label>
                    <div class="col-lg-4">
                        <label class="radio-inline">
                            <input <?= $subtitle=='view'?'disabled':''; ?> required type="radio" class="styled" name="gender" value="M" checked="checked" <?php echo set_radio('gender','M'); ?>>
                            Male
                        </label>

                        <label class="radio-inline">
                            <input <?= $subtitle=='view'?'disabled':''; ?> required type="radio" class="styled" value="F" name="gender" <?php echo set_radio('gender','F'); ?>>
                            Female
                        </label>
                        <?php echo form_error('gender','<span class="text-danger">','</span>') ?>
                    </div>




                    <label class="col-lg-2 control-label">Email:</label>
                    <div class="col-lg-4">
                        <input <?= $subtitle=='view'?'readonly':''; ?> required type="email" name="email"  value="<?= set_value('email',isset($id)?$r->email:'') ?>"  class="form-control" placeholder="Email">
                        <?php echo form_error('email','<span class="text-danger">','</span>') ?>

                    </div>



                </div>


                <div class="form-group">


                    <label class="col-lg-2 control-label">Contact Phone:</label>

                    <div class="col-lg-1">


                        <input <?= $subtitle=='view'?'readonly':''; ?> required readonly type="text"  minlength="1" maxlength="12"  name="dialing_code"  value="<?= set_value('dialing_code',isset($id)?substr($r->phone, 0,-9):'') ?>"  class="form-control dialing_code" placeholder="Dialing Code">


                        <?php echo form_error('dialing_code','<span class="text-danger">','</span>') ?>
                    </div>
                    <div class="col-lg-3">
                        <?//= substr($r->phone, 0,-9).' '.substr($r->phone, -9); ?>
                        <input <?= $subtitle=='view'?'readonly':''; ?> required type="phone"  minlength="9" maxlength="12"  name="phone"  value="<?= set_value('phone',isset($id)?substr($r->phone, -9):'') ?>"  class="form-control" placeholder="Contact Phone">
                        <?php echo form_error('phone','<span class="text-danger">','</span>') ?>

                    </div>



                    <label class="control-label col-lg-2">School Name</label>
                    <div class="col-md-4">
                        <select class="select form-control" id="school_name" name="school_id"
                                placeholder="School Name"><?php echo form_error('school_id', '<label class="text-danger">', '<label>') ?>

                            <option value="<?php echo $this->input->post('school_id') ?>" <?php echo set_select('school_id', $this->input->post('school_name'), true); ?>>
                                <?php echo $this->input->post('school_id')?$this->input->post('school_id'):' Select School...'; ?>
                            </option>


                        </select>
                        <?php echo form_error('school_id', '<label class="text-danger">', '<label>') ?>
                    </div>

                </div>




                <div class="form-group">


                    <label class="col-lg-2 control-label">Class:</label>

                    <div class="col-lg-4">


                        <select style="font-size: xx-small !important; width: 100px;" class="select form-control class" name="class"  required  <?= $subtitle=='view'?'disabled':''; ?> >
                            <option value="" selected>Select Class</option>

                            <option value="0"  <?= set_select('class', 0, isset($id)?(0==$r->class?true:''):'') ?>>Not Available</option>
                        </select>

                    </div>


                </div>



                <div class="text-right <?= $subtitle=='view'?'hidden':''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->

    </div>
</div>
<!-- /vertical form options -->


<script>
    $(".country").change(function(){


        var country = $(".country").val()

        if(country!='') {

            $.ajax({
                type: 'POST',
                url: '<?php echo base_url( "ajax_api/get_city/")?>'+country,
                beforeSend: function () {
                    $(".district").html('<option value="">Please wait...</option>');
                },
                success: function (d) {


                    if (d != '') {

                        //this is clear the message box
                        $("#info_message").html('');

                        //prefilling the info_id field
                        $(".district").html(d);

                    } else {

                        $("#info_message").html('<div class="alert alert-danger"><i class="icon-question"></i> An error as occurred, Check your data....</div>');
                    }
                }
            });



            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/dialing_code")?>/' + country,
                beforeSend:function(){
                    $(".dialing_code").val('Please Wait...');
                },
                success: function (d) {

                    $(".dialing_code").val(d);


                }


            });




            $(".class").html('<option value="">Select Class...</option>')
            if(country=='UG'){

                <?php for($i=1;$i<=7;$i++){ ?>

                $(".class").append('<option value="Primary <?php echo $i ?>" <?php echo set_select("class", 'Primary'); ?>>Primary <?php echo $i ?></option>');

                <?php } ?>

            }else if(country=='KE'){

                <?php for($i=1;$i<=8;$i++){ ?>

                $(".class").append('<option value="Standard <?php echo $i ?>" <?php echo set_select("class", 'Standard'); ?>>Standard <?php echo $i ?></option>');

                <?php } ?>
            }

        }else{
            alert('nothing Selected!!!!');
        }

    });

    $(".district").change(function(){
        var pc= $(".district").val();

        if(pc !='') {

            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("index.php/ajax_api/school")?>/' + pc,
                beforeSend:function(){
                    $("#school_name").html('<option value=""><i class="fa fa-spin fa-spinner"><i> Please Wait...</option>');
                },
                success: function (d) {

                    $("#school_name").html(d);


                }


            });




        }




    });

</script>