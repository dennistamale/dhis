<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="panel panel-white">
            <div class="panel-heading">
            <div class="panel-title">
                <div class="caption font-dark">
                    <i class="icon-globe font-dark"></i>
                    <span class="caption-subject bold uppercase">Regions</span>
                </div>
                <div class="actions pull-left">

            <?php //echo anchor($this->page_level.$this->page_level2.'new_country',' <i class="fa fa-plus"></i> New Country','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
        </div>
            <div class="panel-body">
                <table class="table datatable-basic" id="sample_">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Code </th>
                        <th> Title</th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select()->from('regions')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->region ?> </td>
                            <td> <?php echo $c->title ?> </td>

                            <td>


                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>



                                            </li>
                                        </ul>
                                    </li>
                                </ul>




                            </td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->