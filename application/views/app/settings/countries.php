<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="panel panel-white">
            <div class="panel-heading">
            <div class="panel-title">
                <h5>
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Selected Countries&nbsp;</span>

                    <span class="pull-right">
                    <?php echo anchor($this->page_level.$this->page_level2.'new_country',' <i class="fa fa-plus"></i> New Country','class="btn btn-success btn-xs"'); ?>
                        </span>
                </h5>

            </div>
        </div>
            <div class="panel-body">
                <table class="table  datatable-basic" id="sample_">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Country </th>
                        <th> Country Code </th>
                        <th> Zip Code </th>
                        <th width="70"> Status </th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select('country, a2_iso, a3_un, num_un, dialing_code,status')->from('selected_countries')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->country ?> </td>
                            <td> <?php echo $c->a2_iso ?> </td>
                            <td> <?php echo $c->dialing_code ?> </td>
                            <td><?php echo $c->status==1?'<div class="btn btn-sm green-jungle" style="width: 70px;">active</div>':'<div class="btn btn-sm btn-danger" style="width: 70px;">Blocked</div>' ?></td>
                            <td>

                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>

                                                <?php echo anchor($this->page_level.$this->page_level2.'delete_country/'.$c->a2_iso,'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                            </li>
                                            <li>

                                                <?php echo $c->status==0? anchor($this->page_level.$this->page_level2.'unblock_country/'.$c->a2_iso,'  <i class="fa fa-check"></i> Unblock'): anchor($this->page_level.$this->page_level2.'ban_country/'.$c->a2_iso,'  <i class="fa fa-ban"></i> Block' ,'onclick="return confirm(\'You are about to ban Country from accessing the System \')"') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>


                            </td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->