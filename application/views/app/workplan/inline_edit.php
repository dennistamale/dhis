<style>
    .select2-container--disabled .select2-selection--single:not([class*=bg-]) {
        background-color: #fafafa;
        color: #999999;
        border: none;
    }


</style>

<?php

if (isset($id)) {

    $r = $this->workplan_lib->get_single_item($id);

    if (!empty($r)) {
        $id = $r->id;
    }
}
?>

<style>
    .control-label {
        text-align: right !important;
    }
</style>


<div class="row" style="margin-bottom: 10px;">
    <div class="col-md-12">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open('#', array('class' => 'form-horizontal inline-form')) ?>

        <div class="panel panel-white">

            <div class="panel-body" id="form-<?= $r->id ?>">

                <div id="response_<?= $r->id ?>"></div>

                <input required hidden name="id" id="id-<?= $r->id ?>"  value="<?= isset($id) ? $id : '' ?>">
                <input required hidden name="parent_id" id="parent_id-<?= $r->id ?>"   value="<?= isset($id) ? $r->parent_id : '' ?>">

                <fieldset>

                    <div class="form-group">


                        <div hidden class="col-lg-3">
                            <label class="control-label">Workplan Type</label>
                            <?php $workplan_level = $this->db->select()->from('workplan_level')->get()->result(); ?>
                            <select id="workplan_level-<?= $r->id ?>" class="select-border-color border-warning form-control" name="workplan_level"
                                    required  >
                                <option value="" selected>Select type</option>


                                <?php foreach ($workplan_level as $d) { ?>

                                    <option value="<?= $d->level ?>" <?= set_select('workplan_level', $d->id, isset($id) ? ($d->level == $r->level ? true : '') : '') ?>><?= humanize($d->name) ?></option>

                                <?php } ?>

                            </select>
                            <?php echo form_error('workplan_level', '<span class="text-danger">', '</span>') ?>
                        </div>


                        <div class="col-lg-6">
                            <label class="col-lg-2">Title:</label>

                            <input id="title-<?= $r->id ?>"  required type="text" name="title"
                                                                                 value="<?= set_value('title', isset($id)? $r->title : '') ?>"
                                                                                 class="form-control no-border"
                                                                                 placeholder="Title" readonly="true">
                            <?php echo form_error('title', '<span class="text-danger">', '</span>') ?>

                        </div>



                        <div class="col-md-2">

                            <label>Start Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <input   id="start_date-<?= $r->id ?>"   required name="start_date" type="text" value="<?= date('d F Y',$r->start_time) ?>"  class="form-control pickadate-selectors" placeholder="Date"   data-popup="tooltip" title="" data-placement="top" data-original-title="Start Date">
                            </div>
                        </div>


                        <div class="col-md-2">

                            <label>End Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <input  id="end_date-<?= $r->id ?>"  required name="end_date" type="text" value="<?= date('d F Y',$r->end_time) ?>" class="form-control pickadate-selectors" placeholder="Date"   data-popup="tooltip" title="" data-placement="top" data-original-title="End Date">
                            </div>
                        </div>
                        <div class="col-md-2">

                            <ul class="icons-list pull-right" style="margin-top: 38px;">

                                <li>
                            <span class="text-primary edit" data-content_id="<?= $r->id ?>" title="Edit&nbsp;Details"
                                  data-popup="tooltip" style="cursor: pointer;"><i class="icon-pencil6"></i> Edit</span>
                                </li>

                                <li>
                                    <?= anchor('app/workplan/delete/' . $r->id * date('Y'), '<i class="icon-trash"></i> Delete ', 'title="Delete" data-popup="tooltip" class="text-danger"'); ?>
                                </li>


                            </ul>

                        </div>



                    </div>


                </fieldset>

                <fieldset>
                    <legend>Description</legend>

                    <div class="content-group">


                        <?= form_error('description', '<span class="text-danger">', '</span>') ?>
                        <textarea id="description-<?= $r->id ?>" required readonly name="description" class="form-control no-border"
                                  contenteditable="tru" rows="4"
                                  ><?= set_value('description', isset($id) ? clean($r->description) : '') ?></textarea>


                        <!--                        <div id="editor-inline" contenteditable="true">-->
                        <!--                            --><? //= $r->description ?>
                        <!--                        </div>-->


                    </div>

                </fieldset>

                <fieldset>

                    <div class="form-group">


                        <div class="col-lg-12">
                            <label class="col-lg-2">Assumption:</label>

                            <textarea id="assumption-<?= $r->id ?>" required readonly name="assumption"
                                      class="form-control no-border "><?= set_value('title', isset($id)? $r->assumption : '') ?></textarea>
                            <?php echo form_error('assumption', '<span class="text-danger">', '</span>') ?>

                        </div>

                    </div>

                </fieldset>


                <div class="text-right hidden action-buttons-<?= $r->id ?>">

                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i>
                    </button>
                    <button type="button" class="btn btn-success save-date" data-button_id="<?= $r->id ?>">Submit <i
                                class="icon-floppy-disk position-right"></i></button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>

