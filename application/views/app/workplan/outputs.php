<div class="panel-group panel-group-control content-group-lg remove-padding">

    <?php $no = 1;

    if ($this->workplan_lib->get_children($parent) == false) {

        $alert = array('alert' => 'warning', 'message' => 'No ' . $selected_page);
        $this->load->view('alert', $alert);

    } else {


        $status_list = array(
            array("default" => "N/A"),
            array("google-blue" => "Goals"),
            array("google-red" => "Impacts"),
            array("google-yellow" => "Outcomes"),
            array("google-green" => "Outputs"),
            array("indigo-300" => "Activities"),
            array("info" => "Indicators")
        );


        foreach ($this->workplan_lib->get_children($parent) as $r) {


            $status = $status_list[$r->level];


            $activity = $r->id * date('Y');

            $level = $this->workplan_lib->get_workplan_level($r->level + 1);

            $page_type = isset($level->name) ? strtolower($level->name) : '';

            ?>


            <div class="panel border-<?= key($status) ?>">
                <div class="panel-heading bg-<?= key($status) ?>">
                    <h6 class="panel-title" <?php if ($r->level < 5) { ?> data-parent="<?= $activity ?>" <?php } ?>
                        data-page_type="<?= $page_type ?>">
                        <a data-toggle="collapse" href="#collapsible-control-output<?= $activity ?>">
                            <?= singular($r->name) . " : " . $r->title ?>
                        </a>
                    </h6>

                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li>
                                <?= anchor('app/workplan/add_another/' . $r->id * date('Y'), '<i class="icon-plus2"></i> Add ' . $level->name , 'title="Add&nbsp;' . $level->name.' to ' .$r->title. '" data-popup="tooltip"'); ?>
                            </li>


                            <!--                                <li><a data-action="collapse"></a></li>-->
                            <!--                                <li><a data-action="reload"></a></li>-->
                            <!--                                <li><a data-action="close"></a></li>-->
                        </ul>
                    </div>

                </div>
                <div id="collapsible-control-output<?= $activity ?>"
                     class="panel-collapse collapse  <? //= $no == 1 ? 'in' : '' ?>">
                    <div class="panel-body">




                        <?php


                        $records['r'] = $r;
                        $records['id'] = $r->id;
                        $records['level'] = $level;
                        $r->level==5?'':$this->load->view('app/workplan/inline_edit', $records);

                        $r->level==5?'':$this->load->view('app/workplan/indicators', $records);

                        $r->level==5?$this->load->view('app/activities/activities_inline', $records):'';

                        ?>

                        <div id="<?= $page_type . '_' . $activity ?>"></div>

                    </div>
                </div>
            </div>


            <?php $no++;
        } ?>

    <?php } ?>

</div>
<!-- /collapsible with left control button -->

