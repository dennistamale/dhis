
<?php $indicator_titles=$this->model->get_indicators_title($id); ?>

<!-- Striped rows -->
<div class="panel panel-flat border-top-teal-400" style="margin-bottom: 10px;">
    <div class="panel-heading">
        <h5 class="panel-title">Indicators</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <?= anchor('app/workplan/add_indicator/' . $r->id * date('Y'), ' <i class="icon-plus2"></i>   Add Indicator', 'title="Add Indicator" data-popup="tooltip" class="label border-teal-400 text-teal-400 "'); ?>
                </li>


                <li><a data-action="collapse"></a></li>
<!--                <li><a data-action="reload"></a></li>-->
<!--                <li><a data-action="close"></a></li>-->
            </ul>
        </div>
    </div>



        <?php if(count($indicator_titles)>0){ ?>


            <?php foreach ($indicator_titles as $it){ ?>

            <?php $indicators=$this->model->get_indicators($it->indicator_code); ?>



            <div class="table-responsive" style="margin-bottom: 5px;">






                <table class=" table table-striped table-bordered table-striped indicators">


                    <?php $no=1; ?>


                    <thead>



                    <tr class="bg-teal-400">

                        <th style="width: 250px;">
                            <span title="<?= $it->title ?>"> <?= character_limiter(humanize($it->title), 20);  ?> </span>
                            <?= anchor('app/workplan/delete_indicator/' . $it->indicator_code * date('Y'), ' <i class="icon-bin"></i>', 'title="Delete Indicator" data-popup="tooltip" class="label  pull-right" onclick="return confirm(\'You are about to delete indicator, Are you sure ? \')"'); ?>
                        </th>
                        <th style=" width: 100px;">Interval</th>
                        <?php foreach($indicators as $r){ ?>

                            <th>
                                <?= $r->interval_title ?>
                                <!--                        <input   class="form-contro no-background" value="--><?//= $r->indicator ?><!--">-->
                            </th>

                        <?php } ?>

                    </tr>



                    </thead>
                    <tbody>



                    <tr class="success">

                        <td rowspan="2" style="background: #26A69A !important; color: #fff!important;" > <?= $it->description ?></td>
                        <th>Expected</th>

                        <?php foreach($indicators as $r){ ?>
                            <td style="padding: 0px 20px;">

                                <input readonly data-indicator_id="<?= $r->id ?>" class="no-background indicator_estimate" id="form-ind-est-<?= $r->id ?>" value="<?= $r->estimated_value ?>">

                            </td>
                        <?php } ?>


                    </tr>

                    <tr class="info">

                        <th>Actual</th>

                        <?php foreach($indicators as $r){ ?>
                            <td style="padding: 0px 20px;">
                                <input readonly data-indicator_id="<?= $r->id ?>" class="no-background indicator_actual" id="form-ind-<?= $r->id ?>" value="<?= $r->actual_value ?>">
                            </td>
                        <?php } ?>

                    </tr>

                    </tbody>
                </table>



            </div>


        <?php } ?>
        <?php }else { ?>
            <div class="text-teal-400 text-center">No indicators yet</div>
        <?php  } ?>





</div>
<!-- /striped rows -->
