
<!-- Detached content -->

    <div class="row">

        <?php foreach ($this->workplan_lib->get_level(1) as $r) { ?>


            <?php $user=$this->model->get_user_details($r->created_by); ?>

        <div class="col-lg-6">
            <div class="panel panel-flat blog-horizontal blog-horizontal-2">
                <div class="panel-body">


                    <div class="blog-preview">
                        <div class="content-group-sm media blog-title stack-media-on-mobile text-left">
                            <div class="media-body">
                                <h5 class="text-semibold no-margin">

                                    <?= anchor($this->page_level . $this->page_level2 . 'view_goal/'.$r->id*date('Y'), '<i class=" icon-arrow"></i>'.$r->title) ?>

                                </h5>

                                <ul class="list-inline list-inline-separate no-margin text-muted">
                                    <li>by


                                        <?= isset($user->id)>0? anchor($this->page_level.'profile/'.$user->id*date('Y'),$user->first_name.' '.$user->last_name):'User N/A' ?>

                                    </li>
                                    <li><?= trending_date($r->created_on) ?></li>
                                </ul>
                            </div>

                            <h5 class="text-success media-right no-margin-bottom text-semibold"> <?=  anchor($this->page_level.$this->page_level2.'edit/'.$r->id*date('Y'),'<i class="icon-pencil6"></i>','title="Edit&nbsp;Goal" data-popup="tooltip"'); ?></h5>
                        </div>

                        <?= word_limiter($r->description,40,'<a href="#">[...]</a>') ?>

                    </div>
                </div>

                <div class="panel-footer panel-footer-condensed">
                    <div class="heading-elements">
                        <ul class="list-inline list-inline-separate heading-text">
                            <li><i class="icon-users position-left"></i> 382</li>
                            <li><i class="icon-alarm position-left"></i>
                                <?= isset($r->created_on)>0?time_elapsed_string(date('Y:m:d H:i:s',$r->created_on)):''; ?>
                            </li>

                        </ul>


                        <?= anchor($this->page_level . $this->page_level2 . 'view_goal/'.$r->id*date('Y'), 'View<i class="icon-arrow-right14 position-right"></i>','class="heading-text pull-right"') ?>
                    </div>
                </div>
            </div>
        </div>

        <?php } ?>

    </div>

<!-- /detached content -->
