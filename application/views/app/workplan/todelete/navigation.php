<!-- Detached sidebar -->
<div class="sidebar-detached">
    <div class="sidebar sidebar-default">
        <div class="sidebar-content">

            <!-- Sidebar search -->
            <div class="sidebar-category">
                <div class="category-title">
                    <span>Search</span>
                    <ul class="icons-list">
                        <li><a href="#" data-action="collapse"></a></li>
                    </ul>
                </div>

                <div class="category-content">
                    <form action="#">
                        <div class="has-feedback has-feedback-left">
                            <input type="search" class="form-control" placeholder="Search">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-size-base text-muted"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /sidebar search -->


            <div hidden class="sidebar-category">
                <div class="category-content no-paddin">
                <div id="tree_4" class="tree-demo"></div>
                </div>
            </div>


            <!-- Sub navigation -->
            <div class="sidebar-category ">

                <div class="category-content no-padding">
                    <ul class="navigation navigation-alt navigation-accordion ">


                        <li class="navigation-divider"></li>
                        <li class="navigation-header">Goals</li>
                        <li class="navigation-divider"></li>
                        <li><?= anchor($this->page_level . $this->page_level2 . 'new_workplan/1', '<i class=" icon-add-to-list"></i>New Goal') ?></li>


                        <?php foreach ($this->workplan_lib->get_level(1) as $r) { ?>

                            <li class="<?= $subtitle=='view_goal'&&$this->uri->segment(4)/date('Y')==$r->id?'active':''; ?>"  ><?= anchor($this->page_level . $this->page_level2 . 'view_goal/'.$r->id*date('Y'), '<i class=" icon-target"></i>'.$r->title) ?></li>

                        <?php } ?>

                        <li>
                            <a href="#"><i class="icon-target"></i> Goal One</a>
                            <ul>
                                <li><?= anchor($this->page_level . $this->page_level2 . 'new_workplan/2/goal_id', '<i class=" icon-add-to-list"></i>New Outcome') ?></li>

                                <li>
                                    <a href="#"><i class="icon-primitive-dot"></i> Outcome1</a>
                                    <ul>
                                        <li><?= anchor($this->page_level . $this->page_level2 . 'new_workplan/3/outcome_id', '<i class=" icon-add-to-list"></i>New Output') ?></li>
                                        <li>
                                            <a href="#"><i class="iicon-primitive-dot"></i> Output1</a>
                                            <ul>
                                                <li><?= anchor($this->page_level . $this->page_level2 . 'new_workplan/3/output_id', '<i class=" icon-add-to-list"></i>New Activity') ?></li>
                                                <li>
                                                    <a href="#"><i class="icon-primitive-dot"></i> Activity1</a>
                                                    <ul>
                                                        <li><?= anchor($this->page_level . $this->page_level2 . 'new_workplan/3/output_id', '<i class=" icon-add-to-list"></i>New Indicator') ?></li>
                                                        <li><a href="#"><i class="icon-primitive-dot"></i> Indicator 1</a></li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>

                                    </ul>
                                </li>

                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
            <!-- /sub navigation -->


        </div>
    </div>
</div>
<!-- /detached sidebar -->

<?php

$this->load->view('app/'.$this->page_level2.'tree_js');

?>

