<link href="<?= base_url() ?>assets/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css"/>

<script src="<?= base_url() ?>assets/jstree/dist/jstree.min.js" type="text/javascript"></script>

<script>
    $("#tree_4").jstree({
        "core" : {
            "themes" : {
                "responsive": false
            },
            // so that create works
            "check_callback" : true,
            'data' : {
                'url' : function (node) {
                    return '<?= base_url('workplan/data_tree/') ?>';
                }
                ,
                'data' : function (node) {
                    return { 'parent' : node.id };
                }
            }
        },
        "types" : {
            "default" : {
                "icon" : "fa fa-folder icon-state-warning icon-lg"
            },
            "file" : {
                "icon" : "fa fa-file icon-state-warning icon-lg"
            }
        },
        "state" : { "key" : "demo3" },
        "plugins" : [ "dnd", "state", "types" ]
    });
</script>