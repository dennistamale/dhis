<?php //$this->load->view($this->page_level.'stories/story_version'); ?>

<!-- Theme JS files -->
<script type="text/javascript" src="<?= base_url() ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/editor_ckeditor.js"></script>

<!-- /theme JS files -->

<style>
    .select2-container--disabled .select2-selection--single:not([class*=bg-]) {
        background-color: #fafafa;
        color: #999999;
        border: none;
    }
</style>


<?php
if(isset($id)){

    $r=$this->db->select('a.*')->from('workplan a')->where(array('a.id'=>$id))->get()->row();

    if(!empty($r)){
        $id=$r->id;
    }
}
?>

<?php

echo validation_errors();

?>


<style>
    .control-label{
        text-align: right !important;
    }
</style>



<div class="row">
    <div class="col-md-12">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level.$this->page_level2.'new_workplan/'.( isset($id)?$id*date('Y'):'' ), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3)=='view'? anchor($this->page_level.$this->page_level2.'edit/'.$id*date('Y'),'<i class="icon-pencil6"></i>','title="Edit&nbsp;Details" data-popup="tooltip"'):''; ?>
                        </li>


                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">




                <input name="id" hidden value="<?= isset($id)?$id:'' ?>">
                <input name="parent_id" hidden value="<?= isset($id)?$r->parent_id:'' ?>">
                <input name="subtitle" hidden value="<?= isset($subtitle)?$subtitle:'' ?>">

                <fieldset>

                    <div class="form-group">



                        <div hidden class="col-lg-3">
                            <label class="control-label">Workplan Type</label>
                            <?php $workplan_level=$this->db->select()->from('workplan_level')->get()->result(); ?>
                            <select class="select-border-color border-warning form-control" name="workplan_level"  required  <?= $subtitle=='view'?'disabled':''; ?> >
                                <option value="" selected>Select type</option>


                                <?php foreach ($workplan_level as $d){ ?>

                                    <option value="<?= $d->level ?>" <?= set_select('workplan_level', $d->id, isset($id)?($d->level==$r->level?true:''):($subtitle=='new_workplan'&&$d->level==1?true:'')) ?>><?= humanize($d->name) ?></option>

                                <?php } ?>

                            </select>
                            <?php echo form_error('workplan_level','<span class="text-danger">','</span>') ?>
                        </div>





                        <div class="col-lg-6">
                            <label class="col-lg-2">Title:</label>

                            <input  <?= $subtitle=='view'?'readonly':''; ?> required type="text" name="title"  value="<?= set_value('title',isset($id)&&$subtitle!='add_another'?$r->title:'') ?>"  class="form-control <?= $subtitle=='view'?'no-border':''; ?> " placeholder="Title">
                            <?php echo form_error('title','<span class="text-danger">','</span>') ?>

                        </div>


                        <div class="col-md-3">

                            <label>Start Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <input  <?= $subtitle=='view'?'readonly':''; ?>  required name="start_date" type="text" value="<?= set_value('start_date',isset($id)&&$subtitle!='add_another'?$r->start_time:date('d F Y')) ?>" class="form-control pickadate-selectors" placeholder="Date"   data-popup="tooltip" title="" data-placement="top" data-original-title="Start Date">
                            </div>
                        </div>


                        <div class="col-md-3">

                            <label>End Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <input  <?= $subtitle=='view'?'readonly':''; ?>  required name="end_date" type="text" value="<?= set_value('end_date',isset($id)&&$subtitle!='add_another'?$r->end_time:date('d F Y')) ?>" class="form-control pickadate-selectors" placeholder="Date"   data-popup="tooltip" title="" data-placement="top" data-original-title="End Date">
                            </div>
                        </div>


                    </div>



                </fieldset>

                <fieldset>
                    <legend>Description</legend>

                    <div class="content-group">
                        <?php if($subtitle=='view'){ ?>

                            <?= isset($id)?(count($edits)>0?$edits[0]->story:$r->story_content):''; ?>

                            <span class="pull-right"><?= $this->uri->segment(3)=='view'? anchor($this->page_level.$this->page_level2.'edit/'.$story_id*date('Y'),'<i class="icon-pencil6"></i> Edit','title="Edit&nbsp;Story" data-popup="tooltip"'):''; ?></span>
                        <?php }else{ ?>

                            <?php if($subtitle=='edit'){ ?>

                                <textarea name="description" class="form-control" id="editor-full" rows="2" <?= $subtitle=='view'?'readonly':''; ?> cols="2"><?= set_value('description',$r->description) ?></textarea>

<!--                                <textarea hidden name="original_story">--><?//= isset($id)?(count($edits)>0?$edits[0]->story:$r->story_content):''; ?><!--</textarea>-->
                            <?php }
                            elseif($subtitle=='add_another'){  ?>

                                <textarea name="description" class="form-control" id="editor-full" rows="4"  cols="4">
                                    <?= set_value('description',$this->input->post()?$this->input->post('description'):'') ?>
                                </textarea>


                          <?php  }else{ ?>

                            <?= form_error('description','<span class="text-danger">','</span>') ?>
                            <textarea name="description" class="form-control" id="editor-full" rows="4" <?= $subtitle=='view'?'readonly':''; ?> cols="4"><?= set_value('description',isset($id)?$r->description:'') ?></textarea>
                                
                                <?php } ?>

                        <?php } ?>
                    </div>

                </fieldset>

                <fieldset>

                    <div class="form-group">



                        <div class="col-lg-12">
                            <label class="col-lg-2">Assumption:</label>

                            <textarea name="assumption" class="form-control"><?= set_value('title',isset($id)&&$subtitle!='assumption'?$r->assumption:'') ?></textarea>
                            <?php echo form_error('assumption','<span class="text-danger">','</span>') ?>

                        </div>

                    </div>

                </fieldset>




                <div class="text-right <?= $subtitle=='view'?'hidden':''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>

