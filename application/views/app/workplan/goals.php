<script src="<?= base_url() ?>assets/js/jquery.validate.js"></script>

<script type="text/javascript"
        src="<?= base_url() ?>assets/js/plugins/tables/handsontable/handsontable.min.js"></script>

<!--<script type="text/javascript" src="assets/js/core/app.js"></script>-->

<style>
    .remove-padding {

        margin-left: -15px !important;
        margin-right: -20px !important;
        margin-bottom: -15px !important;
    }
</style>
<style>
    .no-background {
        width: 100%;
        height: 40px;
        background: none;
        border: none;
        font-size: inherit;
        cursor: pointer;
    }
</style>


<link href="<?= base_url() ?>assets/css/google.css" rel="stylesheet" type="text/css">


<div class="row">


    <div class="col-md-12">

        <div class="panel-group panel-group-control content-group-lg" id="accordion-control">


            <?php $no = 1;

            $ch = $this->workplan_lib->get_children(1);

            if (!empty($ch)) {

                foreach ($ch as $r) {

                    $activity = $r->id * date('Y');
                    $level = $this->workplan_lib->get_workplan_level($r->level + 1);
                    $page_type = strtolower($level->name);

                    ?>

                    <div class="panel border-google-blue">
                        <div class="panel-heading bg-google-blue">
                            <h6 class="panel-title" data-parent="<?= $activity ?>" data-page_type="<?= $page_type ?>">
                                <a class="<?= $no == 1 ? 'collapsed' : 'collapsed' ?>" data-toggle="collapse"
                                   data-parent="#accordion-control" href="#accordion-control-outcome<?= $activity ?>"

                                ><?= singular($r->name) . " : " . $r->title ?>
                                </a>
                            </h6>

                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li>
                                        <?= anchor('app/workplan/add_another/' . $r->id * date('Y'), '<i class="icon-plus2"></i> Add ' . $level->name, 'title="Add&nbsp;' . $level->name . ' to ' . $r->title . '" data-popup="tooltip"'); ?>
                                    </li>

                                </ul>
                            </div>


                        </div>

                        <div id="accordion-control-outcome<?= $activity ?>"
                             class="panel-collapse collapse <?//= $no == 1 ? 'in' : '' ?>">
                            <div class="panel-body">

                                <?php
                                $records['r'] = $r;
                                $records['id'] = $r->id;
                                $records['level'] = $level;
                                $this->load->view('app/workplan/inline_edit', $records);
                                $this->load->view('app/workplan/indicators', $records);

                                ?>

                                <div id="<?= $page_type . '_' . $activity ?>"></div>

                            </div>
                        </div>

                    </div>

                    <?php $no++;
                }

            } else {


                $alert = array(
                    'alert' => 'info',
                    'transparent' => 1,
                    'message' => 'No impacts found <br/>' . anchor($this->page_level . 'workplan/add_another/' . $parent * date('Y'), 'Click Here to Add New')
                );
                $this->load->view('alert', $alert);

                //$this->load->view('app/workplan/goals_default');
            }

            ?>
        </div>
        <!-- /accordion with left control button -->


    </div>
</div>

<script>


    $(document).ready(function () {


        $('body').delegate('.pickadate-selectors', 'click', function (e) {

            // Dropdown selectors
            $('.pickadate-selectors').pickadate({

                weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                showMonthsShort: true,
                selectYears: true,
                selectMonths: true,
                min: [2015, 1, 01],
                selectYears: 30
            });

        });


        $('body').delegate('.indicator_actual', 'click', function (e) {
            e.preventDefault();
            var t = $(this);
            var indicator_id = t.data("indicator_id");

            $("#form-ind-" + indicator_id + ":input").prop("readonly", false);
            $("#form-ind-" + indicator_id + ":text").prop("readonly", false);
            $("#form-ind-" + indicator_id + ":input").removeClass("no-background");

            t.mouseleave(function (ev) {

                $("#form-ind-" + indicator_id + ":input").prop("readonly", true);
                $("#form-ind-" + indicator_id + ":text").prop("readonly", true);
                $("#form-ind-" + indicator_id + ":input").addClass("no-background");

                var actual_value = t.val();


                $.ajax({
                    type: 'GET',
                    url: '<?php echo base_url("index.php/workplan/update_indicator")?>/' + indicator_id + '/' + actual_value+'/actual_value',

                    beforeSend: function () {
                    },
                    success: function (d) {

                        console.log(d);
                        //$("#" + page_type + '_' + parent).html(d);


                    }


                });


                //alert(t.val());
            });


        });

        $('body').delegate('.indicator_estimate', 'click', function (e) {
            e.preventDefault();
            var t = $(this);
            var indicator_id = t.data("indicator_id");

            $("#form-ind-est-" + indicator_id + ":input").prop("readonly", false);
            $("#form-ind-est-" + indicator_id + ":text").prop("readonly", false);
            $("#form-ind-est-" + indicator_id + ":input").removeClass("no-background");

            t.mouseleave(function (ev) {

                $("#form-ind-est-" + indicator_id + ":input").prop("readonly", true);
                $("#form-ind-" + indicator_id + ":text").prop("readonly", true);
                $("#form-ind-est-" + indicator_id + ":input").addClass("no-background");

                var actual_value = t.val();


                $.ajax({
                    type: 'GET',
                    url: '<?php echo base_url("index.php/workplan/update_indicator")?>/' + indicator_id + '/' + actual_value+'/estimated_value',

                    beforeSend: function () {
                    },
                    success: function (d) {

                        console.log(d);
                        //$("#" + page_type + '_' + parent).html(d);


                    }


                });


                //alert(t.val());
            });


        });

        $('body').delegate('.panel-title', 'click', function (e) {

            var t = $(this);
            var parent = t.data("parent");
            var page_type = t.data("page_type");


            if (parent != '' && page_type != '') {


                $.ajax({
                    type: 'GET',
                    url: '<?php echo base_url("index.php/workplan/create_page")?>/' + parent + '/' + page_type,

                    beforeSend: function () {

                        var alert_type = 'warning';
                        var head = '<div class="alert alert-' + alert_type + ' alert-styled-left"> <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>';
                        var footer = '</div>';

                        $("#" + page_type + '_' + parent).html(head + 'Please Wait' + footer);


//                        $("#"+page_type+'_'+parent).html('<span value=""><i class="spinner icon-spinner2"><i/> Please Wait...</span>');
                    },
                    success: function (d) {

                        $("#" + page_type + '_' + parent).html(d);


                    }


                });

            }

        });


        $('body').delegate('.edit', 'click', function (e) {

            var t = $(this);
            var content_id = t.data("content_id");

            //alert(content_id);
            $("#form-" + content_id + " :input").prop("readonly", false);
            $("#form-" + content_id + " :text").prop("readonly", false);
            $("#form-" + content_id + " :input").removeClass("no-border");
            $("#form-" + content_id + " :text").removeClass("no-border");
            $(".action-buttons-" + content_id).removeClass("hidden");
        });


        $("body").delegate(".save-date", "click", function (event) {
            event.preventDefault();

            var t = $(this);
            var button_id = t.data("button_id");

            var id = $("#id-" + button_id).valid();
            var parent_id = $("#parent_id-" + button_id).valid();
            var start_date = $("#start_date-" + button_id).valid();
            var end_date = $("#end_date-" + button_id).valid();
            var title = $("#title-" + button_id).valid();
            var workplan_level = $("#workplan_level-" + button_id).valid();
            var assumption = $("#assumption-" + button_id).valid();
            var description = $("#description-" + button_id).valid();


            //if the first forms are filled then other fields are activated

            if (id == true && parent_id == true && start_date == true && end_date == true && title == true && workplan_level == true && assumption == true && description == true) {


                var id_val = $("#id-" + button_id).val();
                var parent_id_val = $("#parent_id-" + button_id).val();
                var start_date_val = $("#start_date-" + button_id).val();
                var end_date_val = $("#end_date-" + button_id).val();
                var title_val = $("#title-" + button_id).val();
                var workplan_level_val = $("#workplan_level-" + button_id).val();
                var desc = $("#description-" + button_id).val();
                var assump = $("#assumption-" + button_id).val();

                var form_data = {
                    'id': id_val,
                    'parent_id': parent_id_val,
                    'start_date': start_date_val,
                    'end_date': end_date_val,
                    'title': title_val,
                    'description': desc,
                    'workplan_level': workplan_level_val,
                    'assumption': assump
                }


                // var form_data = $('#form-' + button_id + ' input,select,textarea,text').serializeArray();


                // console.log(form_data);
                $.ajax({
                    type: 'POST',
                    data: form_data,
                    url: '<?= base_url("workplan/update_workplan")?>',
                    beforeSend: function () {
                        $("#response_" + button_id).html('<div class="alert alert-danger"><i class="icon-question"></i> Please wait while we are processing Data....</div>');
                    },
                    success: function (d) {

                        if (d != '') {

                            //this is clear the message box
                            $("#response_" + button_id).html(d);

                            $("#form-" + button_id + " :input").prop("readonly", true);
                            $("#form-" + button_id + " :text").prop("readonly", true);
                            $("#form-" + button_id + " :input").addClass("no-border");
                            $("#form-" + button_id + " :text").addClass("no-border");
                            $(".action-buttons-" + button_id).addClass("hidden");

                        } else {

                            $("#response_" + button_id).html('<div class="alert alert-danger"><i class="icon-question"></i> An error as occurred, Check your data....</div>');
                        }
                    }
                });
            } else {

                $("#response_" + button_id).html('<div class="alert alert-danger"><i class="icon-question"></i> Some fields are missing....</div>');
            }

        });


    });

</script>
