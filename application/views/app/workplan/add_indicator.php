<?php //$this->load->view($this->page_level.'stories/story_version'); ?>

<!-- Theme JS files -->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--ckeditor/ckeditor.js"></script>-->
<!--<script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/pages/editor_ckeditor.js"></script>-->

<!-- /theme JS files -->

<style>
    .select2-container--disabled .select2-selection--single:not([class*=bg-]) {
        background-color: #fafafa;
        color: #999999;
        border: none;
    }
</style>


<?php
if (isset($id)) {

    $r = $this->db->select('a.*')->from('workplan_indicators a')->where(array('a.id' => $id))->get()->row();

    if (!empty($r)) {
        $id = $r->id;
    }
}
?>

<?php

echo validation_errors();

//print_r($r);

?>


<style>
    .control-label {
        text-align: right !important;
    }
</style>


<div class="row">
    <div class="col-md-12">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level . $this->page_level2 . 'add_indicator/' . $workplan_id * date('Y') . (isset($id) ? '/' . $id * date('Y') : ''), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>

                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">


                <input name="id" hidden value="<?= isset($id) ? $id : '' ?>">
                <input name="workplan_id" hidden value="<?= $workplan_id ?>">


                <div class="row">
                    <div class="col-md-6">



                        <div class="form-group">


                            <div class="col-lg-12">
                                <label>Indicator Name:</label>

                                <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                     name="indicator"
                                                                                     value="<?= set_value('indicator', isset($id) ? $r->name : '') ?>"
                                                                                     class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                     placeholder="Indicator">
                                <?php echo form_error('indicator', '<span class="text-danger">', '</span>') ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-lg-6">
                                <label>Data Source:</label>

                                <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                     name="data_type"
                                                                                     value="<?= set_value('data_type', isset($id) ? $r->data_type : '') ?>"
                                                                                     class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                     placeholder="Data Source">
                                <?php echo form_error('data_type', '<span class="text-danger">', '</span>') ?>

                            </div>

                            <div class="col-lg-6">
                                <label>Geo Level:</label>



                                <select class="select"  name="geo_level" <?= $subtitle == 'view' ? 'disabled' : ''; ?> required >
                                    <option value="" <?= set_select('geo_level','',true) ?>>Geo Level</option>


                                    <?php foreach ($this->model->get_geo_level() as $gl){ ?>
                                    <option value="<?= $gl->id ?>" <?= set_select('geo_level',$gl->id) ?>><?= $gl->name ?></option>

                                    <?php } ?>

                                </select>

                            </div>
                        </div>


                        <div class="content-group">
                            <label>Description</label>
                            <?= form_error('description', '<span class="text-danger">', '</span>') ?>
                            <textarea name="description" class="form-control" id="editor-ful"
                                      rows="4" <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                                      cols="4"><?= set_value('description', isset($id) ? $r->description : '') ?></textarea>

                        </div>




                    </div>
                    <div class="col-md-6">


                        <table class="table table-xs">
                            <thead>

                            <tr>

                                <th>Interval</th>
                                <th>Expected Value</th>
                            </tr>
                            </thead>

                            <tbody id="indicator_forms">



                            <tr>

                                <td>

<!--                                    <input name="indicator_id[]" value="--><?//= $subtitle == 'edit_indicator' ? 'readonly' : ''; ?><!--">-->



                                        <input readonly <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                             name="interval_title[]"
                                                                                             value="<?= set_value('', isset($id) ? $r->interval_title : 'Baseline') ?>"
                                                                                             class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                             placeholder="Interval">
                                        <?php echo form_error('interval_title[]', '<span class="text-danger">', '</span>') ?>


                                </td>
                                <td>



                                        <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                             name="estimated_value[]"
                                                                                             value="<?= set_value('', isset($id) ? $r->estimated_value : '') ?>"
                                                                                             class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                             placeholder="Expected Value">
                                        <?php echo form_error('estimated_value', '<span class="text-danger">', '</span>') ?>


                                </td>
                            </tr>

                            <tr>
                                <td>



                                        <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                             name="interval_title[]"
                                                                                             value="<?= set_value('', isset($id) ? $r->interval_title : '') ?>"
                                                                                             class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                             placeholder="Interval">
                                        <?php echo form_error('interval_title[]', '<span class="text-danger">', '</span>') ?>


                                </td>
                                <td>



                                        <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> required type="text"
                                                                                             name="estimated_value[]"
                                                                                             value="<?= set_value('', isset($id) ? $r->estimated_value : '') ?>"
                                                                                             class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                                                                             placeholder="Estimated Value">
                                        <?php echo form_error('estimated_value', '<span class="text-danger">', '</span>') ?>


                                </td>
                            </tr>

                            </tbody>
                            <tfoot>
                            <tr class="success">
                                <td colspan="2">
                                    <a href="#" class="label label-success pull-right" id="add_indicator" title="Add Indicator" data-popup="tooltip" ><i class="icon-plus2"></i> Add Interval</a>
                                </td>
                            </tr>
                            </tfoot>
                        </table>



                    </div>
                </div>


                <div class="text-right <?= $subtitle == 'view' ? 'hidden' : ''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i>
                    </button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>


<!--<input required type="text" name="indicator_title[]" class="form-control" placeholder="Indicator Title">-->
<!--<input required type="text" name="estimated_value[]" class="form-control" placeholder="Estimated Value">-->



<script>



    $(document).ready(function () {

        var title ='<input required type="text" name="interval_title[]" class="form-control" placeholder="Interval">';
        var estimated_value ='<input required type="text" name="estimated_value[]" class="form-control" placeholder="Estimated Value">';
        var row ='<tr><td>'+title+'</td><td>'+estimated_value+'</td></tr>';


        $("body").delegate('#add_indicator','click',function (e) {
            e.preventDefault();
            $("#indicator_forms").append(row);


        });



    });
</script>


