<?php

$t=$this->db->select()->from('stored_json')->limit(100)->order_by('id','desc')->get()->result();

?>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE panel-->
        <div class="panel panel-white bordered">
            <div class="panel-heading">
            <div class="panel-title">


                <div class="caption font-dark hidden-print">
                 <h6><?= humanize($subtitle) ?></h6>


                </div>
                <div class="heading-elements">
                <div class="tools">


                    <?php  echo anchor($this->page_level.$this->page_level2.'realtime_monitoring','Realtime Monitoring','class="btn btn-default"') ?>
                    <?php  echo anchor($this->page_level.$this->page_level2.'stored_json','Stored Json','class="btn btn-default"') ?>
                </div>
                </div>
            </div>
        </div>
            <div class="panel-body">
                <table class="table  datatable-button-init-basic" id="sample_1">

                    <thead>
                    <tr>
                        <th width="2">#</th>
                        <th>Json</th>
                        <th>Json Type</th>
                        <th>Date</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $no=1;
                    foreach($t as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td>  <?php echo ($c->json) ?>    </td>
                            <td>  <?php echo humanize($c->json_type) ?>    </td>
                            <td style="white-space: nowrap;">  <?php echo trending_date($c->date_sent) ?>    </td>


                        </tr>
                        <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE panel-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

