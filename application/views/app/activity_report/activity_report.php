

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />


<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->

<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">

    <!--    --><?php //print_r($this->input->post()); ?>
    <?= form_open(); ?>

    <div class="panel-body">
        <div class="table-container">

            <div class="table-actions-wrapper">
                <span> </span>
                <a  href="<?= base_url($this->page_level . $this->page_level2 . 'new') ?>"
                                                                                                        class="btn btn-xs btn-primary table-group-action-submi" type="submit">
                    <i class="fa fa-plus"></i> Add New </a>
                <?php // humanize($title) ?>
            </div>

            <table class="table datatable-ajax" id="<?= $title ?>">
                <thead>

                <tr>
                    <th style="width: 8px;">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                            <span></span>
                        </label>
                    </th>
                    <th style="width: 115px;"> Date </th>
                    <th> Workplan Ref </th>
                    <th> Activity Type </th>
                    <th> District </th>
                    <th> Subcounty </th>
                    <th> Parish </th>
                    <th> Institution </th>
                    <th>Action</th>

                </tr>

                </thead>
                <tbody> </tbody>
            </table>

        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<?php
$this->load->view('app/'.$this->page_level2.'ajax_'.$title);
?>
