<?php

if (isset($id)) {
    $fac = $this->model->get_activity_report_id($id);
}

$no_rows = 2;


?>


<style>
    .table > tbody > tr > td {
        padding: 0px 0px;
        line-height: 1.5384616;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .add_row {
        cursor: copy;
        margin-top: 5px;
        margin-right: 5px;
    }

    .remove_row {
        cursor: pointer;
        margin-top: 3px;
        margin-left: 5px;
    }
</style>


<?= validation_errors(); ?>


<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-bold"><?= humanize($subtitle); ?> Facility</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><?= $subtitle == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $id * date('Y'), '<i class="icon-pencil7"></i> Edit', 'class="label label-info"') : '' ?></li>
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">


        <?= form_open($this->page_level . $this->page_level2 . 'new/' . (isset($id) ? $id * date('Y') : ''), array('class' => 'form-horizontal')) ?>

        <input name="id" hidden value="<?= isset($id) ? $id : '' ?>">


        <div class="content-group">

            <div class="form-group">


                <label class="control-label col-md-1"><span class="pull-right">Activity Name</span></label>
                <div class="col-md-6">

                    <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> name="activity_name"
                                                                         value="<?= set_value('activity_name', isset($id) ? $fac->activity_name : '') ?>"
                                                                         type="text" class="form-control" required>

                    <?php echo form_error('activity_name', '<span class="text-danger">', '</span>') ?>
                </div>


                <label class="control-label col-md-2"><span class="pull-right">Reporting Date</span></label>
                <div class="col-md-3">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                required name="start_date" type="text"
                                value="<?= set_value('start_date', isset($id) ? date('d F Y', $fac->start_date) : date('d F Y')) ?>"
                                class="form-control pickadate-selectors" placeholder="Date" data-popup="tooltip"
                                title="" data-placement="top" data-original-title="Reporting Date">
                    </div>
                </div>

            </div>


            <?php

            $location['fac'] = isset($fac) ? $fac : array();

            $this->load->view('ajax/location_filter_js', $location) ?>


            <div class="form-group">


                <legend class="text-bold">Activity</legend>


                <label class="control-label col-lg-1"><span class="pull-right">Activity&nbsp;Type</span></label>
                <div class="col-lg-3">

                    <select class="select" name="activity_type" class="form-control"
                            required <?= $subtitle == 'view' ? 'disabled' : ''; ?> >
                        <option value="" selected>Activity Type</option>


                        <?php foreach ($this->model->get_activity_type() as $d) { ?>

                            <option value="<?= $d->type ?>" <?= set_select('activity_type', $d->type, isset($id) ? ($d->type == $fac->activity_type ? true : '') : '') ?> >
                                <?= $d->name ?>
                            </option>

                        <?php } ?>

                    </select>


                    <?php echo form_error('activity_type', '<span class="text-danger">', '</span>') ?>
                </div>


                <label class="control-label col-lg-1"><span class="pull-right">Start</span></label>
                <div class="col-lg-3">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                required name="start_date" type="text"
                                value="<?= set_value('start_date', isset($id) ? date('d F Y', $fac->start_date) : date('d F Y')) ?>"
                                class="form-control pickadate-selectors" placeholder="Date" data-popup="tooltip"
                                title="" data-placement="top" data-original-title="Start Date">
                    </div>
                </div>


                <label class="control-label col-lg-1"><span class="pull-right">Stop</span></label>
                <div class="col-lg-3">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                required name="end_date" type="text"
                                value="<?= set_value('end_date', isset($id) ? date('d F Y', $fac->end_date) : date('d F Y')) ?>"
                                class="form-control pickadate-selectors" placeholder="End Date" data-popup="tooltip"
                                title="" data-placement="top" data-original-title="End Date">
                    </div>

                    <?php echo form_error('level', '<span class="text-danger">', '</span>') ?>
                </div>


            </div>


            <div class="form-group">


                <label class="control-label col-lg-1"><span class="pull-right">Workplan&nbsp;Ref#</span></label>


                <div class="col-lg-11">

                    <select name="workplan_ref" class="workplan_ref select"
                            required <?= $subtitle == 'view' ? 'disabled' : ''; ?> >


                        <option value="" selected>Select Worplan Ref</option>

                        <?php foreach ($this->db->select('workplan_id')->from('workplan_activity_status')->group_by('workplan_id')->get()->result() as $d) {
                            $path = $this->workplan_lib->get_path($d->workplan_id);

                            ?>

                            <optgroup label="<?= $path[1]['name'] ?>">

                                <?php foreach ($this->db->select()->from('workplan_activity_status')->where('workplan_id', $d->workplan_id)->get()->result() as $was) { ?>

                                    <option value="<?= $was->code ?>" <?= set_select('workplan_ref', $was->code, isset($id) ? ($was->code == $fac->workplan_ref ? true : '') : '') ?>><?= $was->code . ':' . $was->activity_name ?></option>

                                <?php } ?>

                            </optgroup>

                        <?php } ?>
                    </select>
                    <?php echo form_error('workplan_ref', '<span class="text-danger">', '</span>') ?>
                </div>

            </div>

            <div class="form-group">

                <!--Objectives-->
                <label class="control-label col-lg-1"><span class="pull-right">Objectives</span></label>
                <div class="col-lg-11">

                    <textarea class="form-control" rows="4"
                              name="objectives" <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                              required><?= set_value('objectives', isset($id) ? $fac->objectives : '') ?></textarea>

                    <?php echo form_error('objectives', '<span class="text-danger">', '</span>') ?>
                </div>


            </div>


            <!--Objectives-->


            <div class="form-group">

                <legend class="text-bold">Facilitators</legend>

                <div class="table-responsive">
                    <table class="table  table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Group</th>
                            <th>Designation</th>
                            <th>Note</th>
                            <th style="width:8px;"></th>
                        </tr>
                        </thead>
                        <tbody id="fac_forms">

                        <?php if (isset($id) && $f = $this->model->activity_report_components('activity_report_facilitators', $fac->id) != false) {
                            $i = 1;

                            foreach ($this->model->activity_report_components('activity_report_facilitators', $fac->id) as $r) {
                                ?>


                                <tr data-row="<?= $i ?>" class="facilitator_rows">
                                    <input hidden value="<?= $r->id ?>" name="fac[id][]">
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="name"
                                                                                             name="fac[name][]"
                                                                                             value="<?= $r->name ?>">
                                    </td>
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="group"
                                                                                             name="fac[group][]"
                                                                                             value="<?= $r->group ?>">
                                    </td>
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="designation"
                                                                                             name="fac[designation][]"
                                                                                             value="<?= $r->designation ?>">
                                    </td>
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="note"
                                                                                             name="fac[note][]"
                                                                                             value="<?= $r->note ?>">
                                    </td>

                                    <td>

                                        <div data-fac_row="<?= $i ?>"
                                             class="<?= $subtitle == 'view' ? 'hidden' : ''; ?> remove_fac   label label-danger label-icon label-rounded remove_row"
                                             data-popup="tooltip" data-placement="left" title="Remove row"><b><i
                                                        class="icon-cross2"></i></b>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;
                            }
                        } else { ?>

                            <?php for ($i = 1; $i <= $no_rows; $i++) { ?>

                                <tr data-row="<?= $i ?>" class="facilitator_rows  fac_row_<?= $i ?>"">
                                <input hidden value="" name="fac[id][]">
                                <td><input data-indicator_id=""
                                           class="no-background no-border indicator_estimate form-control"
                                           placeholder="name" name="fac[name][]"></td>
                                <td><input data-indicator_id=""
                                           class="no-background no-border indicator_estimate form-control"
                                           placeholder="group" name="fac[group][]"></td>
                                <td><input data-indicator_id=""
                                           class="no-background no-border indicator_estimate form-control"
                                           placeholder="designation" name="fac[designation][]"></td>
                                <td><input data-indicator_id=""
                                           class="no-background no-border indicator_estimate form-control"
                                           placeholder="note" name="fac[note][]">
                                </td>

                                <td>

                                    <div data-fac_row="<?= $i ?>"
                                         class="<?= $subtitle == 'view' ? 'hidden' : ''; ?> remove_fac   label label-danger label-icon label-rounded remove_row"
                                         data-popup="tooltip" data-placement="left" title="Remove row"><b><i
                                                    class="icon-cross2"></i></b>
                                    </div>

                                </td>

                                </tr>

                            <?php }
                        }
                        ?>


                        </tbody>
                    </table>

                </div>
                <div data-fac_row="<?= $i ?>" id="add_fac"
                     class="<?= $subtitle == 'view' ? 'hidden' : ''; ?>    label label-success label-icon label-rounded pull-right add_row"
                     data-popup="tooltip" data-placement="left" title="Add row"><b><i class=" icon-plus2"></i></b></div>


            </div>


            <div class="form-group">


                <div class="row">

                    <div class="col-md-8">
                        <legend class="text-bold">Participants/Beneficiaries</legend>
                        <div class="table-responsive">
                            <table class="table  table-bordered">
                                <thead>
                                <tr>
                                    <th>Ben.Type</th>
                                    <th>Age group</th>
                                    <th>Male</th>
                                    <th>Female</th>
                                    <th>Combined</th>
                                </tr>
                                </thead>
                                <tbody id="ben_forms">
                                <?php if (isset($id) && $f = $this->model->activity_report_components('activity_report_beneficiaries', $fac->id) != false) {
                                    $i = 1;

                                    foreach ($this->model->activity_report_components('activity_report_beneficiaries', $fac->id) as $r) {
                                        ?>

                                        <tr data-row="<?= $i ?>" class="beneficiaries_rows ow_<?= $i ?>">

                                            <input hidden value="<?= $r->id ?>" name="ben[id][]">

                                            <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                                                        data-indicator_id=""
                                                        class="no-background no-border indicator_estimate form-control"
                                                        placeholder="Beneficiary Type" name="ben[ben_type][]"
                                                        value="<?= $r->ben_type ?>"></td>
                                            <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                                                        data-indicator_id=""
                                                        class="no-background no-border indicator_estimate form-control"
                                                        placeholder="Age Group" name="ben[age_group][]"
                                                        value="<?= $r->age_group ?>"></td>
                                            <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                                                        data-indicator_id=""
                                                        class="no-background no-border indicator_estimate form-control"
                                                        placeholder="Male" name="ben[male][]" value="<?= $r->male ?>">
                                            </td>

                                            <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                                                        data-indicator_id=""
                                                        class="no-background no-border indicator_estimate form-control"
                                                        placeholder="Female" name="ben[female][]"
                                                        value="<?= $r->female ?>"></td>

                                            <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?>
                                                        data-indicator_id=""
                                                        class="no-background no-border indicator_estimate form-control"
                                                        placeholder="Combined" name="ben[combined][]"
                                                        value="<?= $r->combined ?>"></td>

                                        </tr>

                                        <?php
                                        $i++;
                                    }


                                } else { ?>

                                    <?php for ($i = 1; $i <= $no_rows; $i++) { ?>

                                        <tr data-row="<?= $i ?>" class="beneficiaries_rows row_<?= $i ?>">

                                            <input hidden value="" name="ben[id][]">

                                            <td><input data-indicator_id=""
                                                       class="no-background no-border indicator_estimate form-control"
                                                       placeholder="Beneficiary Type" name="ben[ben_type][]"></td>

                                            <td><input data-indicator_id=""
                                                       class="no-background no-border indicator_estimate form-control"
                                                       placeholder="Age Group" name="ben[age_group][]"></td>
                                            <td><input data-indicator_id=""
                                                       class="no-background no-border indicator_estimate form-control"
                                                       placeholder="Male" name="ben[male][]"></td>

                                            <td><input data-indicator_id=""
                                                       class="no-background no-border indicator_estimate form-control"
                                                       placeholder="Female" name="ben[female][]"></td>

                                            <td><input data-indicator_id=""
                                                       class="no-background no-border indicator_estimate form-control"
                                                       placeholder="Combined" name="ben[combined][]"></td>

                                        </tr>

                                    <?php }
                                } ?>

                            </table>
                        </div>
                        <div id="add_ben" data-ben_row="<?= $i ?>"
                             class="hidden <?= $subtitle == 'view' ? 'hidden' : ''; ?>    label label-success label-icon label-rounded pull-right add_row"
                             data-popup="tooltip" data-placement="left" title="Add row"><b><i
                                        class=" icon-plus2"></i></b></div>
                    </div>
                    <div class="col-md-4">
                        <legend class="text-bold">Distributions</legend>
                        <table class="table  table-bordered">
                            <thead>
                            <tr>
                                <th>Commodity</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody id="dist_forms">


                            <?php if (isset($id) && $f = $this->model->activity_report_components('activity_report_distributions', $fac->id) != false) {
                                $i = 1;

                                foreach ($this->model->activity_report_components('activity_report_distributions', $fac->id) as $r) {
                                    ?>

                                    <tr data-row="<?= $i ?>" class="distributions_rows ow_<?= $i ?>">
                                        <input hidden value="<?= $r->id ?>" name="dist[id][]">
                                        <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                                 class="no-background no-border indicator_estimate form-control"
                                                                                                 placeholder="Commodity"
                                                                                                 name="dist[commodity][]"
                                                                                                 value="<?= $r->commodity ?>">
                                        </td>
                                        <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                                 class="no-background no-border indicator_estimate form-control"
                                                                                                 placeholder="Quantity"
                                                                                                 name="dist[quantity][]"
                                                                                                 value="<?= $r->quantity ?>">
                                        </td>
                                        <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                                 class="no-background no-border indicator_estimate form-control"
                                                                                                 placeholder="Unit"
                                                                                                 name="dist[unit][]"
                                                                                                 value="<?= $r->unit ?>">
                                        </td>
                                        <td>

                                            <div data-dist_row="<?= $i ?>"
                                                 class="<?= $subtitle == 'view' ? 'hidden' : ''; ?> remove_dist   label label-danger label-icon label-rounded remove_row"
                                                 data-popup="tooltip" data-placement="left" title="Remove row"><b><i
                                                            class="icon-cross2"></i></b>
                                            </div>

                                        </td>

                                    </tr>
                                    <?php $i++;
                                }
                            } else { ?>


                                <?php for ($i = 1; $i <= $no_rows; $i++) { ?>


                                    <tr data-row="<?= $i ?>" class="distributions_rows row_<?= $i ?>">
                                        <input hidden value="" name="dist[id][]">
                                        <td><input data-indicator_id=""
                                                   class="no-background no-border indicator_estimate form-control"
                                                   placeholder="Commodity" name="dist[commodity][]"></td>
                                        <td><input data-indicator_id=""
                                                   class="no-background no-border indicator_estimate form-control"
                                                   placeholder="Quantity" name="dist[quantity][]"></td>
                                        <td><input data-indicator_id=""
                                                   class="no-background no-border indicator_estimate form-control"
                                                   placeholder="Unit" name="dist[unit][]"></td>
                                        <td>

                                            <div data-dist_row="<?= $i ?>"
                                                 class="<?= $subtitle == 'view' ? 'hidden' : ''; ?>   remove_dist label label-danger label-icon label-rounded remove_row"
                                                 data-popup="tooltip" data-placement="left" title="Remove row"><b><i
                                                            class="icon-cross2"></i></b>
                                            </div>

                                        </td>

                                    </tr>


                                <?php }
                            }
                            ?>

                            </tbody>

                        </table>


                        <div id="add_dist" data-dist_row="<?= $i ?>"
                             class="<?= $subtitle == 'view' ? 'hidden' : ''; ?>    label label-success label-icon label-rounded add_row pull-right"
                             data-popup="tooltip" data-placement="left" title="Add row"><b><i
                                        class="icon-plus2"></i></b>
                        </div>


                    </div>
                </div>


            </div>

            <div class="form-group">
                <legend class="text-bold">Description of the Activity Result</legend>
                <textarea <?= $subtitle == 'view' ? 'readonly' : ''; ?> rows="4" name="description"
                                                                        class="form-control"><?= set_value('description', isset($id) ? $fac->description : '') ?></textarea>

                <?php echo form_error('remarks', '<span class="text-danger">', '</span>') ?>
            </div>


            <div class="form-group">

                <legend class="text-bold">Challenges</legend>

                <div class="table-responsive">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Challenges</th>
                            <th>How Solved</th>
                            <th>Referral</th>
                            <th style="width: 8px;"></th>

                        </tr>
                        </thead>
                        <tbody id="chal_forms">


                        <?php if (isset($id) && $f = $this->model->activity_report_components('activity_report_challenges', $fac->id) != false) {
                            $i = 1;

                            foreach ($this->model->activity_report_components('activity_report_challenges', $fac->id) as $r) {
                                ?>

                                <tr data-row="<?= $i ?>" class="challenges_rows chal_row_<?= $i ?>">
                                    <input hidden value="<?= $r->id ?>" name="chal[id][]">
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="Challenges"
                                                                                             name="chal[challenges][]"
                                                                                             value="<?= $r->challenges ?>">
                                    </td>
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="How Solved"
                                                                                             name="chal[how_solved][]"
                                                                                             value="<?= $r->how_solved ?>">
                                    </td>
                                    <td><input <?= $subtitle == 'view' ? 'readonly' : ''; ?> data-indicator_id=""
                                                                                             class="no-background no-border indicator_estimate form-control"
                                                                                             placeholder="Referral"
                                                                                             name="chal[referral][]"
                                                                                             value="<?= $r->referral ?>">
                                    </td>

                                    <td>

                                        <div data-chal_row="<?= $i ?>"
                                             class="<?= $subtitle == 'view' ? 'hidden' : ''; ?> remove_chal   label label-danger label-icon label-rounded remove_row"
                                             data-popup="tooltip" data-placement="left" title="Remove row"><b><i
                                                        class="icon-cross2"></i></b>
                                        </div>

                                    </td>

                                </tr>


                                <?php $i++;
                            }
                        } else { ?>


                            <?php for ($i = 1; $i <= $no_rows; $i++) { ?>

                                <tr data-row="<?= $i ?>" class="challenges_rows  chal_row_<?= $i ?>">
                                    <input hidden value="" name="chal[id][]">
                                    <td><input data-indicator_id=""
                                               class="no-background no-border indicator_estimate form-control"
                                               placeholder="Challenges" name="chal[challenges][]"></td>
                                    <td><input data-indicator_id=""
                                               class="no-background no-border indicator_estimate form-control"
                                               placeholder="How Solved" name="chal[how_solved][]"></td>
                                    <td><input data-indicator_id=""
                                               class="no-background no-border indicator_estimate form-control"
                                               placeholder="Referral" name="chal[referral][]"></td>


                                    <td>

                                        <div data-chal_row="<?= $i ?>"
                                             class="<?= $subtitle == 'view' ? 'hidden' : ''; ?> remove_chal   label label-danger label-icon label-rounded remove_row"
                                             data-popup="tooltip" data-placement="left" title="Remove row"><b><i
                                                        class="icon-cross2"></i></b>
                                        </div>

                                    </td>


                                </tr>

                            <?php }
                        }
                        ?>

                        </tbody>

                    </table>
                </div>

                <div id="add_chal" data-chal_row="<?= $i ?>"
                     class="<?= $subtitle == 'view' ? 'hidden' : ''; ?>    label label-success label-icon label-rounded pull-right add_row"
                     data-popup="tooltip" data-placement="left" title="Add row"><b><i class=" icon-plus2"></i></b></div>

            </div>


            <div class="form-group">
                <legend class="text-bold">Notes / Remarks</legend>
                <textarea <?= $subtitle == 'view' ? 'readonly' : ''; ?> rows="4" name="remarks"
                                                                        class="form-control"><?= set_value('remarks', isset($id) ? $fac->remarks : '') ?></textarea>
                <?php echo form_error('remarks', '<span class="text-danger">', '</span>') ?>

            </div>


            <div class="form-group">
                <legend class="text-bold"></legend>
                <label class="control-label col-lg-2"><span class="pull-right">Submitted by</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> name="submitted_by"
                                                                         value="<?= set_value('submitted_by', isset($id) ? $fac->submitted_by : '') ?>"
                                                                         type="text" class="form-control" required>

                    <?php echo form_error('submitted_by', '<span class="text-danger">', '</span>') ?>
                </div>


                <label class="control-label col-lg-2"><span class="pull-right">Date</span></label>
                <div class="col-lg-4">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                required name="clear_date" type="text"
                                value="<?= set_value('clear_date', isset($id) ? date('d F Y', $fac->clear_date) : date('d F Y')) ?>"
                                class="form-control pickadate-selectors" placeholder="Date" data-popup="tooltip"
                                title="" data-placement="top" data-original-title="Clear Date">
                    </div>

                    <?php echo form_error('clear_date', '<span class="text-danger">', '</span>') ?>
                </div>


            </div>


            <div class="form-group">
                <label class="control-label col-lg-2"><span class="pull-right">Cleared by</span></label>
                <div class="col-lg-4">

                    <input <?= $subtitle == 'view' ? 'readonly' : ''; ?> name="cleared_by"
                                                                         value="<?= set_value('cleared_by', isset($id) ? $fac->cleared_by : '') ?>"
                                                                         type="text" class="form-control">

                    <?php echo form_error('cleared_by', '<span class="text-danger">', '</span>') ?>
                </div>


                <label class="control-label col-lg-2"><span class="pull-right">Date</span></label>
                <div class="col-lg-4">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                        <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                name="submit_date" type="text"
                                value="<?= set_value('submit_date', isset($id) ? date('d F Y', $fac->submit_date) : date('d F Y')) ?>"
                                class="form-control pickadate-selectors" placeholder="Date" data-popup="tooltip"
                                title="" data-placement="top" data-original-title="Submit Date">
                    </div>

                    <?php echo form_error('submit_date', '<span class="text-danger">', '</span>') ?>
                </div>


            </div>


        </div>


        <?php if ($subtitle != 'view') { ?>

            <div class="text-right">
                <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i></button>
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i>
                </button>
            </div>

        <?php } ?>

        <?= form_close() ?>

    </div>
</div>


<script>


    $(document).ready(function () {


        $("body").delegate('#add_fac', 'click', function (e) {

            var fac_id = '<input hidden value="" name="fac[id][]">';
            var fac_name = '<input class="no-background no-border indicator_estimate form-control" placeholder="name" name="fac[name][]">';
            var fac_group = '<input class="no-background no-border indicator_estimate form-control" placeholder="group" name="fac[group][]">';
            var fac_designation = '<input class="no-background no-border indicator_estimate form-control" placeholder="designation" name="fac[designation][]">';
            var fac_note = '<input class="no-background no-border indicator_estimate form-control" placeholder="note" name="fac[note][]">';

            var current_row = $(this).data();
            var current_count = current_row.fac_row;
            var new_count = current_count + 1;


            var remove_button = '<div data-fac_row="' + (current_count) + '" class="remove_fac label label-danger label-icon label-rounded remove_row" data-popup="tooltip" data-placement="left" title="Remove row"><b><i  class="icon-cross2"></i></b>  </div>';


            var row = '<tr data-row="' + (current_count) + '"   class="facilitator_rows fac_row_' + current_count + '">' + fac_id + '<td>' + fac_name + '</td><td>' + fac_group + '</td><td>' + fac_designation + '</td><td>' + fac_note + '</td><td>' + remove_button + '</td></tr>';
            e.preventDefault();

            $("#fac_forms").append(row);
            current_row.fac_row = new_count;
        });


        $("body").delegate('.remove_fac', 'click', function (e) {

            var current_row = $(this).data();
            var current_count = current_row.fac_row;
            $('.fac_row_' + current_count).remove();
        });


        function add_ben(current_row) {

            //this adds beneficiallies which was initial #add_ben

            var ben_id = '<input hidden value="" name="ben[id][]">';
            var ben_type = '<input class="no-background no-border indicator_estimate form-control" placeholder="Beneficiary Type" name="ben[ben_type][]">';
            var ben_age_group = '<input class="no-background no-border indicator_estimate form-control" placeholder="Age Group"  name="ben[age_group][]">';
            var ben_male = '<input class="no-background no-border indicator_estimate form-control" placeholder="Male"  name="ben[male][]">';
            var ben_female = '<input class="no-background no-border indicator_estimate form-control" placeholder="Female"  name="ben[female][]">';
            var ben_combined = '<input class="no-background no-border indicator_estimate form-control" placeholder="Combined"  name="ben[combined][]">';

            //var current_row = $(this).data();
            var current_count = current_row.dist_row;
            var new_count = current_count + 1;


            var row = '<tr  data-row="' + (current_count) + '"   class="beneficiaries_rows row_' + current_count + '">' + ben_id + '<td>' + ben_type + '</td><td>' + ben_age_group + '</td><td>' + ben_male + '</td><td>' + ben_female + '</td><td>' + ben_combined + '</td></tr>';
            //e.preventDefault();

            $("#ben_forms").append(row);
            current_row.ben_row = new_count;
        }

        function add_dist(current_row) {
            //adding distributions
            var dist_id = '<input hidden value="" name="dist[id][]">';

            var dist_commodity = '<input class="no-background no-border indicator_estimate form-control" placeholder="Commodity" name="dist[commodity][]">';
            var dist_quantity = '<input class="no-background no-border indicator_estimate form-control" placeholder="Quantity" name="dist[quantity][]">';
            var dist_unit = '<input class="no-background no-border indicator_estimate form-control" placeholder="Unit" name="dist[unit][]">';


            //var current_row = $(this).data();
            var current_count = current_row.dist_row;
            var new_count = current_count + 1;

            var remove_button = '<div data-dist_row="' + (current_count) + '" class="remove_dist label label-danger label-icon label-rounded remove_row" data-popup="tooltip" data-placement="left" title="Remove row"><b><i  class="icon-cross2"></i></b>  </div>';

            var row = '<tr data-row="' + (current_count) + '"   class="distributions_rows row_' + current_count + '">' + dist_id + '<td>' + dist_commodity + '</td><td>' + dist_quantity + '</td><td>' + dist_unit + '</td><td>' + remove_button + '</td></tr>';
            // e.preventDefault();

            $("#dist_forms").append(row);
            current_row.dist_row = new_count;

        }


        $("body").delegate('#add_dist', 'click', function (e) {
            var current_row = $(this).data();
            add_ben(current_row);
            add_dist(current_row);
        });


        $("body").delegate('.remove_dist', 'click', function (e) {

            var current_row = $(this).data();
            var current_count = current_row.dist_row;
            $('.row_' + current_count).remove();
        });


        $("body").delegate('#add_chal', 'click', function (e) {

            var chal_id = '<input hidden value="" name="chal[id][]">';

            var chal_challenges = '<input class="no-background no-border indicator_estimate form-control select" placeholder="Challenges" name="chal[challenges][]">';
            var chal_how_solved = '<input class="no-background no-border indicator_estimate form-control" placeholder="How Solved" name="chal[how_solved][]">';
            var chal_referral = '<input class="no-background no-border indicator_estimate form-control" placeholder="Referral" name="chal[referral][]">';


            var current_row = $(this).data();
            var current_count = current_row.chal_row;
            var new_count = current_count + 1;


            var remove_button = '<div data-chal_row="' + (current_count) + '" class="remove_chal label label-danger label-icon label-rounded remove_row" data-popup="tooltip" data-placement="left" title="Remove row"><b><i  class="icon-cross2"></i></b>  </div>';


            var row = '<tr data-row="' + (current_count) + '"   class="challenges_rows chal_row_' + current_count + '">' + chal_id + '<td>' + chal_challenges + '</td><td>' + chal_how_solved + '</td><td>' + chal_referral + '</td><td>' + remove_button + '</td></tr>';
            e.preventDefault();

            $("#chal_forms").append(row);
            current_row.chal_row = new_count;
        });


        $("body").delegate('.remove_chal', 'click', function (e) {

            var current_row = $(this).data();
            var current_count = current_row.chal_row;

            $('.chal_row_' + current_count).remove();
        });


    });
</script>



