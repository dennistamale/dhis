<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css"/>
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>


<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->

<!-- Begin: Demo Datatable 1 -->
<div class="panel panel-flat">

    <!--        --><?php //print_r($_REQUEST); ?>
    <?= form_open(); ?>

    <div class="panel-body">
        <div class="table-container">

            <div class="table-actions-wrapper">
                <span> </span>

                <!--                --><? //= anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add New','class="btn btn-sm btn-primary"'); ?>

                <div class="input-group" style="width: 150px;">
                    <span class="input-group-addon bg-primary"><i class="icon-lis text-white text-bold">Sort by</i></span>

                    <select class="table-group-status-input form-control  input-inline input-small input-sm" name="sort"


                            onchange="window.location.href='<?= base_url($this->page_level . $this->page_level2 . $this->page_level3) ?>'+(this.value)"

                    >

                        <option value="" <?php echo set_select('sort', '') ?>>Sort by</option>
                        <option value="district" <?php echo set_select('sort', 'district') ?>>District</option>
                        <option value="activity_type" <?php echo set_select('sort', 'activity_type',true) ?>>Activity Type
                        </option>


                    </select>

                </div>
                <button class="btn hidden btn-xs btn-success table-group-action-submi" type="submit">
                    <i class="fa fa-check"></i> Submit
                </button>


            </div>

            <table class="table datatable-ajax" id="<?= $subtitle ?>">
                <thead>
                <tr>
                    <td colspan="6">
                        <div class="row form-inline">

                            <div class="form-group">
                                <div class="input-group" style="width: 150px;">
                                    <span class="input-group-addon"><i class="icon-city"></i></span>
                                    <select name="activity_type" class="form-control form-filter">
                                        <option value="" <?php echo set_select('activity_type', '', true) ?>>Activity
                                            Type
                                        </option>
                                        <?php foreach ($this->model->get_activity_type() as $d) { ?>
                                            <option value="<?= $d->type ?>" <?= set_select('activity_type', $d->type) ?>><?= $d->name ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group" style="width: 150px;">
                                    <span class="input-group-addon"><i class="icon-city"></i></span>
                                    <select name="district" class="select form-filter">
                                        <option value="" <?php echo set_select('district', '', true) ?>>District
                                        </option>
                                        <?php foreach ($this->hf_model->get_hf_districts() as $d) { ?>

                                            <option value="<?= $d ?>" <?= set_select('district', $d) ?>><?= $d ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="input-group" style="width: 150px;">
                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    <select name="month" class="select form-filter">
                                        <option value="" <?php echo set_select('month', '', true) ?>>Month</option>
                                        <?php for ($i = 1; $i <= 12; $i++) { ?>

                                            <option value="<?= $i ?>" <?= set_select('month', $i) ?>><?= monthName($i - 1) ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="input-group" style="width: 150px;">
                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    <select name="year" class="select form-filter">
                                        <option value="" <?php echo set_select('year', '', true) ?>>Year</option>
                                        <?php

                                        $current_year = date('Y');

                                        for ($i = $current_year; $i >= ($current_year - 90); $i--) { ?>

                                            <option value="<?= $i ?>" <?= set_select('year', $i) ?>><?= $i ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn bg-orange-300 filter-submit"><i
                                            class="icon-equalizer2 position-left"></i>Filter
                                </button>
                                <span hidden class="pull-right">
                                        <button type="submit" name="export" value="excel"
                                                class="btn btn-success filter-submit"><i
                                                    class="fa fa-file-excel-o position-left"></i>Export Excel</button>
                                    </span>
                            </div>
                        </div>
                    </td>
                </tr>


                <tr>

                    <th> <?= strlen($this->uri->segment(4)) > 0 ? humanize($this->uri->segment(4)) : 'Activity Type' ?></th>
                    <th> Activity Count</th>
                    <th colspan="4" class="text-center"> Beneficiaries</th>


                </tr>
                <tr class="text-primary">
                    <th></th>
                    <th></th>
                    <th> Females</th>
                    <th> Males</th>
                    <th> Both</th>
                    <th>Total</th>

                </tr>

                </thead>
                <tbody></tbody>
            </table>

        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<?php


$others['others'] = isset($ref_code) ? array(
    'ref_code' => $ref_code,
    'activity_status_id' => $activity_status_id,
) : null;

$this->load->view('app/activity_report/ajax_' . $subtitle, $others)


?>
