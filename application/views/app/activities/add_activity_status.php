<?php //$this->load->view($this->page_level.'stories/story_version'); ?>

<!-- Theme JS files -->
<!--<script type="text/javascript" src="--><? //= base_url() ?><!--ckeditor/ckeditor.js"></script>-->
<!--<script type="text/javascript" src="--><? //= base_url() ?><!--assets/js/pages/editor_ckeditor.js"></script>-->

<!-- /theme JS files -->

<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>


<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/form_multiselect.js"></script>

<style>
    .select2-container--disabled .select2-selection--single:not([class*=bg-]) {
        background-color: #fafafa;
        color: #999999;
        border: none;
    }
</style>


<?php
if (isset($id)) {

    $r = $this->db->select('a.*')->from('workplan_activity_status a')->where(array('a.id' => $id))->get()->row();

    if (!empty($r)) {
        $id = $r->id;

        $workplan_id = $r->workplan_id;
    }

    $data['id'] = $id;

}
?>

<?php

//echo validation_errors();


?>


<style>
    .control-label {
        text-align: right !important;
    }
</style>


<div class="row">
    <div class="col-md-12">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open($this->page_level . $this->page_level2 . 'add_activity_status/' . (isset($id) ? '/' . $id * date('Y') : ''), array('class' => 'form-horizonta')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view' ? anchor($this->page_level . $this->page_level2 . 'edit/' . $id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>


                        <li><a data-action="collapse"></a></li>
                        <!--                        <li><a data-action="reload"></a></li>-->
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">


                <input name="id" hidden value="<?= isset($id) ? $id : '' ?>">
                <input name="workplan_id" hidden value="<?= $workplan_id ?>">


                <div class="row">


                    <div class="col-md-12">

                        <div class="form-group">


                            <div class="col-lg-6">
                                <label>Activity Name:</label>

                                <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                        required type="text"
                                        name="activity_name"
                                        value="<?= set_value('activity_name', isset($id) ? $r->activity_name : '') ?>"
                                        class="form-control <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'no-border' : ''; ?> "
                                        placeholder="Activity Name">
                                <?php echo form_error('activity_name', '<span class="text-danger">', '</span>') ?>

                            </div>


                            <div class="col-lg-2">
                                <label>Item Qty:</label>
                                <!--                                this is named as number in the database-->

                                <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                        required type="text"
                                        name="number"
                                        value="<?= set_value('number', isset($id) ? $r->number : '') ?>"
                                        class="form-control <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'no-border' : ''; ?> "
                                        placeholder="Number">
                                <?php echo form_error('number', '<span class="text-danger">', '</span>') ?>

                            </div>


                            <div class="col-lg-2">
                                <label>Item Unit:</label>

                                <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                        required type="text"
                                        name="item_unit"
                                        value="<?= set_value('item_unit', isset($id) ? $r->item_unit : '') ?>"
                                        class="form-control <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'no-border' : ''; ?> "
                                        placeholder="Item Unit">
                                <?php echo form_error('item_unit', '<span class="text-danger">', '</span>') ?>

                            </div>


                            <div class="col-lg-2">
                                <label>Geo Level:</label>


                                <select class="select geo_level"
                                        name="geo_level" <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'disabled' : ''; ?>
                                        required>
                                    <option value="" <?= set_select('geo_level', '', true) ?>>Geo Level</option>


                                    <?php foreach ($this->model->get_geo_level() as $gl) { ?>
                                        <option value="<?= $gl->id ?>" <?= set_select('geo_level', $gl->id, (isset($id) ? ($r->geo_level == $gl->id ? true : '') : '')) ?>><?= $gl->name ?></option>

                                    <?php } ?>

                                </select>

                            </div>
                        </div>

                        <div class="form-group block geo_level_items_group" hidden>
                            <div class="col-lg-12">
                                <label class="geo_level_label"></label>
                                <div class="multi-select-full">

                                    <select class="multiselec form-control geo_level_items" name="geo_level_items"
                                            multiple="multiple" required>

                                    </select>

                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <div class="col-lg-12">
                                <label>Description</label>
                                <?= form_error('description', '<span class="text-danger">', '</span>') ?>
                                <textarea name="description" class="form-control" id="editor-ful"
                                          rows="4" <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                          cols="4"><?= set_value('description', isset($id) ? $r->description : '') ?></textarea>
                            </div>

                        </div>


                        <div class="form-group">

                            <div class="col-md-6">

                                <label>Start Date:</label>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                    <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                            required name="start_date" type="text"
                                            value="<?= set_value('start_date', isset($id) ? date('d F Y', $r->start_date) : date('d F Y')) ?>"
                                            class="form-control pickadate-selectors" placeholder="Date"
                                            data-popup="tooltip" title="" data-placement="top"
                                            data-original-title="Start Date">
                                </div>
                            </div>


                            <div class="col-md-6">

                                <label>End Date:</label>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                    <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                            required name="end_date" type="text"
                                            value="<?= set_value('end_date', isset($id) ? date('d F Y', $r->end_date) : date('d F Y')) ?>"
                                            class="form-control pickadate-selectors" placeholder="Date"
                                            data-popup="tooltip" title="" data-placement="top"
                                            data-original-title="End Date">
                                </div>
                            </div>


                        </div>


                        <div class="form-group">

                            <div class="col-lg-6">
                                <label>Deliverable:</label>

                                <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                        required type="text"
                                        name="deliverable"
                                        value="<?= set_value('deliverable', isset($id) ? $r->deliverable : '') ?>"
                                        class="form-control <?= $subtitle == 'view' ? 'no-border' : ''; ?> "
                                        placeholder="Deliverable">
                                <?php echo form_error('deliverable', '<span class="text-danger">', '</span>') ?>

                            </div>

                            <div class="col-lg-6">
                                <label>Budget:</label>

                                <input <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'readonly' : ''; ?>
                                        required type="text"
                                        name="budget"
                                        value="<?= set_value('budget', isset($id) ? $r->budget : '') ?>"
                                        class="form-control <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'no-border' : ''; ?> "
                                        placeholder="Budget">
                                <?php echo form_error('budget', '<span class="text-danger">', '</span>') ?>

                            </div>

                        </div>


                    </div>
                </div>


                <div class="text-right <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'hidden' : ''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i>
                    </button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>

<?php $subtitle == 'view' || $subtitle == 'update_activity_status' ? $this->load->view('app/activities/updates', $data) : ''; ?>

<?php

if ($subtitle == 'view' || $subtitle == 'update_activity_status') {

    $ars['subtitle'] = 'activity_report_summary';
    $ars['ref_code'] = $r->code;
    $ars['extra_sub_url'] = 'activity_type';
    $ars['activity_status_id'] = $r->id * date('Y');

    $this->load->view('app/activity_report/activity_type_summary', $ars);
}
?>

<script>


    $(document).ready(function () {

        // multiselect



            $('.geo_level').change(function () {

                var geo_level = $(this).val();

                // alert(geo_level);

                if (geo_level != '' && geo_level > 1) {

                    $('.geo_level_items_group').show();

                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url('ajax_api/get_geo_level_items/') ?>' + geo_level,
                        success: function (data) {
                            //console.log('data: ' + data);


                            var label = data.name;
                            $('.geo_level_label').html(label);

                            // $('.geo_level_items').html(data);
                            //
                            var selectValues = data.items;


                            $('.geo_level_items').html($("<option></option>").attr("value", '').text(' Select ' + label+'s'));

                            $.each(selectValues, function (key, value) {

                                $('.geo_level_items')
                                    .append($("<option></option>")
                                        .attr("value", key)
                                        .text(value));
                            });

                        },
                        error: function (xhr, status, error) {
                            console.log(status + ':' + error);
                        }

                    });
                } else {
                    $('.geo_level_items_group').hide();
                }


            });




    });
</script>
