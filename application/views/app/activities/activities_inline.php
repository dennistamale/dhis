
<?php $get_activity_status=$this->model->get_activity_status($id); ?>

<!-- Striped rows -->
<div class="panel panel-flat border-top-indigo" style="margin-bottom: 10px;">
    <div class="panel-heading">
        <h5 class="panel-title">Activity</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <?= strlen($id)? anchor('app/activities/add_activity_status/' . $id * date('Y'), ' <i class="icon-plus2"></i>   Add Activity', 'title="Add Indicator" data-popup="tooltip" class="label border-indigo text-indigo "'):''; ?>
                </li>


                <li><a data-action="collapse"></a></li>
<!--                <li><a data-action="reload"></a></li>-->
<!--                <li><a data-action="close"></a></li>-->
            </ul>
        </div>
    </div>





        <?php if($get_activity_status!=false && isset($get_activity_status)){ ?>






                <table class="table table-striped" style="margin-bottom: 5px;">
                <thead>
                <tr>
                    <th> Output </th>
                    <th> Name </th>
                    <th> Level </th>
                    <th> Start </th>
                    <th> End </th>
                    <th> Deliverable </th>
                    <th> Budget </th>
                    <th> Status </th>
                    <th>Action</th>

                </tr>

                </thead>
                <tbody>

                <?php

                foreach ($get_activity_status as $ac) {

                $gl=$this->model->get_geo_level($ac->geo_level);

                $gl_name=isset($gl)?$gl->name:'N/A';




                $current_status=$this->model->get_activity_updates($ac->id);

                $status=isset($current_status[0]->status)?$this->model->get_update_status( $current_status[0]->status):array();
                $status_name=isset($status,$status->name)?$status->name:'N/A';

                if ($status_name == 'Behind Schedule') {
                    $btn = 'bg-danger';
                    // $label = 'WIP';
                } elseif ($status_name == 'Completed') {
                    $btn = 'btn-success';
                    // $label = 'Closed';
                } elseif ($status_name == 'On Track') {
                    $btn = 'btn-primary';
                    // $label = 'Closed';
                }
                 else {
                    $btn = 'bg-grey';
                    // $label = 'New';
                }
    
    
                $status_btn = '<div style="width:120px;" class="btn btn-xs ' . $btn . '">' . $status_name . '</div>';
    

                ?>



                <tr>

                <td> <?= $ac->number.'&nbsp;'.humanize($ac->item_unit) ?> </td>
                <td>  <?= $ac->activity_name ?> </td>
                <td> <?= $gl_name ?>  </td>
                    <td><?= date('d-m-Y',$ac->start_date) ?> </td>
                    <td><?= date('d-m-Y',$ac->end_date) ?> </td>
                <td> <?= $ac->deliverable ?> </td>
                <td> <?= $ac->budget ?>  </td>
                <td> <?= $status_btn ?>  </td>
                <td>



                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <?php echo anchor('app/activities/view/'.$ac->id*date('Y'),'  <i class="fa fa-eye"></i> View') ?>
                                </li>

                            </ul>
                        </li>
                    </ul>




                </td>

                </tr>

                <?php } ?>

                </tbody>
                </table>





        <?php }else { ?>
            <div class="text-indigo text-center">No Statuses yet..</div>
        <?php  } ?>





</div>
<!-- /striped rows -->
