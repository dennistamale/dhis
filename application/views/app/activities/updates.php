<!-- Small table -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Updates</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!--                <li><a data-action="reload"></a></li>-->
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table table-sm">
            <thead>
            <tr>
                <th style="width: 8px;">#</th>
                <th style="width: 150px;">Status</th>
                <th>Update</th>
                <th style="width: 130px">Updated by</th>
                <th style="width: 220px;"><i class=" icon-attachment"></i>Attachment</th>
                <th style="width: 200px;">Date</th>
            </tr>
            </thead>
            <tbody>


            <?= form_open('app/activities/update_activity_status/' . $id * date('Y')) ?>
            <tr>
                <td colspan="2">


                    <input name="update_id" hidden value="<?= isset($update_id) ? $update_id : '' ?>">


                    <select class="select" name="status" required>
                        <option value="" <?= set_select('status', '', true) ?>>Status</option>


                        <?php foreach ($this->model->get_update_status() as $gl) { ?>
                            <option value="<?= $gl->id ?>" <?= set_select('status', $gl->id) ?>><?= $gl->name ?></option>

                        <?php } ?>

                    </select>

                </td>
                <td colspan="2">

                    <?= form_error('update', '<span class="text-danger">', '</span>') ?>

                    <input class="form-control input-xlg" name="update" placeholder="Update" <?= set_value('update') ?>/>

                </td>


                <td>

                    <input type="file" minlength="10" maxlength="12" name="image" value="<?= set_value('image') ?>"
                           class="form-control file-input" placeholder="Specimen"

                           data-browse-class="btn btn-primary  col-md-8" data-show-caption="false"
                           data-show-upload="false" <?= isset($id) ? '' : 'required'; ?> />


                </td>
                <td>
                    <button name="update_activity_status_btn" class="btn btn-xs btn-success" type="submit"><i class="icon-floppy-disk"></i> Update</button>
                    <button class="btn btn-xs btn-default" type="reset"><i class=" icon-cross2" title="Cancel"></i></button>
                </td>
            </tr>
            <?= form_close() ?>

            <tr>
                <td colspan="6"></td>
            </tr>




            <?php $updates = $this->model->get_activity_updates($id); ?>

            <?php if ($updates != false && count($updates) > 0) { ?>


                <?php
                $no = 1;

                foreach ($updates as $up) {


                    $status = $this->model->get_update_status($up->status);

                    $status_name = isset($status) ? $status->name : 'N/A';


                    $user = $this->model->get_user_details($up->created_by);

                    ?>


                    <tr>
                        <td><?= $no ?></td>
                        <td><?= $status_name ?></td>
                        <td><?= $up->update ?></td>
                        <td><?= $user->first_name . ' ' . $user->last_name ?></td>
                        <td><i class=" icon-attachment"></i><?= $up->attachment ?></td>
                        <td><?= trending_date($up->created_on) ?></td>
                    </tr>

                    <?php $no++;
                } ?>

            <?php } else { ?>
                <tr>
                    <td colspan="6" class="text-info text-center">No updates yet</td>
                </tr>

            <?php } ?>

            </tbody>
        </table>
    </div>
</div>
<!-- /small table -->

