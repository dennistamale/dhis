<?php $message=$this->db->select()->from($table)->where(array('id'=>$id))->get()->row();

?>


<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/mail_list_read.js"></script>

<!-- Single mail -->
<div class="panel panel-white">

    <!-- Mail toolbar -->
    <div class="panel-toolbar panel-toolbar-inbox">
        <div class="navbar navbar-default">
            <ul class="nav navbar-nav visible-xs-block no-border">
                <li>
                    <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                        <i class="icon-circle-down2"></i>
                    </a>
                </li>
            </ul>

            <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
                <div class="btn-group navbar-btn">
                    <a href="#" class="btn btn-default"><i class="icon-reply"></i> <span class="hidden-xs position-right">Reply</span></a>
                    <a href="#" class="btn btn-default"><i class="icon-reply-all"></i> <span class="hidden-xs position-right">Reply to all</span></a>
                    <a href="#" class="btn btn-default"><i class="icon-forward"></i> <span class="hidden-xs position-right">Forward</span></a>
                    <a href="#" class="btn btn-default"><i class="icon-bin"></i> <span class="hidden-xs position-right">Delete</span></a>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-menu7"></i>
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">One more line</a></li>
                        </ul>
                    </div>
                </div>

                <div class="pull-right-lg">
                    <p class="navbar-text"><?= trending_date_time($message->created_on) ?></p>
                    <div class="btn-group navbar-btn">
                        <a href="#" class="btn btn-default"><i class="icon-printer"></i> <span class="hidden-xs position-right">Print</span></a>
                        <a href="#" class="btn btn-default"><i class="icon-new-tab2"></i> <span class="hidden-xs position-right">Share</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /mail toolbar -->


    <!-- Mail details -->
    <div class="media stack-media-on-mobile mail-details-read">
        <a href="#" class="media-left">
										<span class="btn bg-teal-400 btn-rounded btn-icon btn-xlg">
											<span class="letter-icon"></span>
										</span>
        </a>

        <div class="media-body">
            <h6 class="media-heading"><?= humanize($message->subject) ?></h6>
            <div class="letter-icon-title text-semibold"><?= $table=='inbox'?$message->phone: $message->to_user ?><a href="#" hidden>&lt;jira@diakultd.atlassian.net&gt;</a></div>
        </div>
    </div>
    <!-- /mail details -->


    <!-- Mail container -->
    <div class="mail-container-read">

       <?= $message->message ?>

    </div>
    <!-- /mail container -->


    <!-- Attachments -->
    <div class="mail-attachments-container hidden">
        <h6 class="mail-attachments-heading">2 Attachments</h6>

        <ul class="mail-attachments">
            <li>
											<span class="mail-attachments-preview">
												<i class="icon-file-pdf icon-2x"></i>
											</span>

                <div class="mail-attachments-content">
                    <span class="text-semibold">new_december_offers.pdf</span>

                    <ul class="list-inline list-inline-condensed no-margin">
                        <li class="text-muted">174 KB</li>
                        <li><a href="#">View</a></li>
                        <li><a href="#">Download</a></li>
                    </ul>
                </div>
            </li>

            <li>
											<span class="mail-attachments-preview">
												<i class="icon-file-pdf icon-2x"></i>
											</span>

                <div class="mail-attachments-content">
                    <span class="text-semibold">assignment_letter.pdf</span>

                    <ul class="list-inline list-inline-condensed no-margin">
                        <li class="text-muted">736 KB</li>
                        <li><a href="#">View</a></li>
                        <li><a href="#">Download</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <!-- /attachments -->

</div>
<!-- /single mail -->