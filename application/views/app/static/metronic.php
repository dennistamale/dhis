<!-- BEGIN PAGE LEVEL PLUGINS -->
 <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->



<!-- Begin: Demo Datatable 1 -->
<div class="portlet light portlet-fit portlet-datatable bordered">

    <div class="portlet-body">
        <div class="table-container">
            <div class="table-actions-wrapper">
                <span> </span>
                <select class="table-group-action-input form-control input-inline input-small input-sm">
                    <option value="">Select...</option>
                    <option value="Cancel">Cancel</option>
                    <option value="Cancel">Hold</option>
                    <option value="Cancel">On Hold</option>
                    <option value="Close">Close</option>
                </select>
                <button class="btn btn-sm green table-group-action-submit">
                    <i class="fa fa-check"></i> Submit</button>
            </div>
            <table class="table datatable-ajax" id="datatable_ajax">
                <thead>
                <tr>
                    <th width="2%">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                            <span></span>
                        </label>
                    </th>
                    <th width="5%"> Record&nbsp;# </th>
                    <th width="15%"> Date </th>
                    <th width="200"> Customer </th>
                    <th width="10%"> Ship&nbsp;To </th>
                    <th width="10%"> Price </th>
                    <th width="10%"> Amount </th>
                    <th width="10%"> Status </th>
                    <th width="1%"> Actions </th>
                </tr>
                <tr>
                    <td> </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="order_id"> </td>
                    <td>
                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_from" placeholder="From">
                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="icon-calendar"></i>
                                                                </button>
                                                            </span>
                        </div>
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control form-filter input-sm" readonly name="order_date_to" placeholder="To">
                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="icon-calendar"></i>
                                                                </button>
                                                            </span>
                        </div>
                    </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="order_customer_name"> </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="order_ship_to"> </td>
                    <td>
                        <div class="margin-bottom-5">
                            <input type="text" class="form-control form-filter input-sm" name="order_price_from" placeholder="From" /> </div>
                        <input type="text" class="form-control form-filter input-sm" name="order_price_to" placeholder="To" /> </td>
                    <td>
                        <div class="margin-bottom-5">
                            <input type="text" class="form-control form-filter input-sm margin-bottom-5 clearfix" name="order_quantity_from" placeholder="From" /> </div>
                        <input type="text" class="form-control form-filter input-sm" name="order_quantity_to" placeholder="To" /> </td>
                    <td>
                        <select name="order_status" class="form-control form-filter input-sm">
                            <option value="">Select...</option>
                            <option value="pending">Pending</option>
                            <option value="closed">Closed</option>
                            <option value="hold">On Hold</option>
                            <option value="fraud">Fraud</option>
                        </select>
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                <i class="fa fa-search"></i> Search</button>
                        </div>
                        <button class="btn btn-sm red btn-outline filter-cancel">
                            <i class="fa fa-times"></i> Reset</button>
                    </td>
                </tr>
                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div>
</div>
<!-- End: Demo Datatable 1 -->




<script src="<?= base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php $this->load->view('ajax/ajax_table'); ?>
<!-- END PAGE LEVEL SCRIPTS -->
