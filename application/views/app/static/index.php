<!--<script type="text/javascript" src="--><?//= base_url() ?><!--assets/js/pages/datatables_data_sources.js"></script>-->

<?php $this->load->view($this->page_level.'static/datatables_data_source'); ?>


               <!-- Ajax sourced data -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Ajax sourced data</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        DataTables has the ability to read data from virtually any <code>JSON</code> data source that can be obtained by <code>Ajax</code>. This can be done, in its most simple form, by setting the <code>ajax</code> option to the address of the <code>JSON</code> data source. The example below shows DataTables loading data for a table from <code>arrays</code> as the data source (object parameters can also be used through the <code>columns.data</code> option).
                    </div>

                    <table class="table datatable-ajax">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Location</th>
                            <th>Extn.</th>
                            <th>Start date</th>
                            <th>Salary</th>
                            <th>option</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /ajax sourced data -->



<?php   if (rwmb_meta('charity_banner') == 'hbanner') {
    $medias = rwmb_meta( 'bg_img', 'type=image&size=Facebookvertical');
    foreach ( $medias as $media )
    { ?>
        <section class="hero home-style-four">
            <div class="hero-inner" style="background: url(<?php echo balanceTags($media['url']) ?>) center center/cover no-repeat local;">
                <div class="container">
                    <div class="hero-title">
                        <h1><?php echo rwmb_meta( 'charity_bn_text');?></h1>
                        <p><?php echo rwmb_meta( 'charity_bn_content');?></p>
                        <a href="<?php echo rwmb_meta( 'charity_bn_link');?>" class="btn theme-btn"><?php echo rwmb_meta( 'charity_bn_btn');?></a>
                    </div>
                </div>
            </div>
        </section>
    <?php } } elseif (rwmb_meta('charity_banner') == 'hbanner1') {
    $medias = rwmb_meta( 'bg_img', 'type=image&size=Facebookvertical');
    foreach ( $medias as $media )
    { ?>
        <section class="main-banar home-style-five" style="background: url(<?php echo balanceTags($media['url']) ?>) center center/cover no-repeat local;">
            <div class="banar-inner">
                <div class="container">
                    <div class="banar-title active-banar-title">
                        <span><?php echo rwmb_meta( 'charity_bn_content');?></span>
                        <h1><?php echo rwmb_meta( 'charity_bn_text');?></h1>
                        <a href="<?php echo rwmb_meta( 'charity_bn_link');?>" class="btn theme-btn"><?php echo rwmb_meta( 'charity_bn_btn');?></a>
                    </div>
                </div>
            </div>
        </section>
    <?php } } else {
    $medias = rwmb_meta( 'bg_img', 'type=image&size=Facebookvertical');
    foreach ( $medias as $media )
    { ?>

    <?php   } }?>

