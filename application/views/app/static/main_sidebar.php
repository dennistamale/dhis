<!-- Main sidebar -->
<div class="sidebar sidebar-main" style="background: <?= $this->site_options->title('site_color_code') ?>;">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user hidden">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="<?= base_url($this->session->userdata('photo')) ?>"
                                                        class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?= ucwords($this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name')); ?></span>
                        <div class="text-size-mini text-muted ">
                            <i class="icon-key text-size-small"></i> &nbsp;<?= $this->session->userdata('username'); ?>
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="#"><i class="icon-circle"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!--

         Dashboard
Workplan
Activity Report
Assessments
Indicators

         -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>


                    <!-- Dashboard -->
                    <li <?= $this->custom_library->role_exist('dashboard', 'group') ? '' : 'hidden'; ?>
                            class="<?= $title == 'dashboard' ? 'active' : ''; ?>">

                        <?= anchor('', '<i class="icon-meter2"></i> <span>Dashboard</span>') ?>

                    </li>




                    <!-- Workplan -->

                    <li <?= $this->custom_library->role_exist('workplan', 'group') ? '' : 'hidden'; ?>>
                        <a href="#"><i class="icon-tree7"></i> <span>Workplan</span></a>
                        <ul>

                            <li class="<?= $subtitle == 'new_workplan' ? 'active' : ''; ?>"><?= anchor($this->page_level . 'workplan/new_workplan', 'New Goal') ?></li>


                            <li class="<?=  $subtitle == 'goals'||$subtitle == 'add_another' ? 'active' : ''; ?>">

                                <?= anchor($this->page_level . 'workplan/goals',  'Goals') ?>

                            </li>



                        </ul>
                    </li>


                    <!--  Activity Report-->

                    <li <?= $this->custom_library->role_exist('activity report', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-books"></i> <span>Activity Report</span></a>
                        <ul>


                            <li <?= $this->custom_library->role_exist('activity report summary') ? '' : 'hidden'; ?> class="<?= $title == 'activity_report'&&($subtitle == 'activity_report_summary')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activity_report/activity_report_summary', 'Summary') ?>
                            </li>

<!---->
<!--                            <li --><?//= $this->custom_library->role_exist('activity report summary') ? '' : 'hidden'; ?><!-- class="--><?//= $title == 'activity_report'&&($subtitle == 'activity_type_summary')? 'active' : ''; ?><!--">-->
<!--                                --><?//= anchor($this->page_level . 'activity_report/activity_type_summary/activity_type', 'Activity Type Summary') ?>
<!--                            </li>-->


                            <li <?= $this->custom_library->role_exist('activity report summary') ? '' : 'hidden'; ?> class="<?= $title == 'activity_report'&&($subtitle == 'status_report')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activity_report/status_report', 'Status Report') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('list activity report') ? '' : 'hidden'; ?> class="<?= $title == 'activity_report'&&$subtitle == ''? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activity_report', 'List all') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('new activity report') ? '' : 'hidden'; ?> class="<?= $title == 'activity_report'&&($subtitle == 'new'||$subtitle == 'view'||$subtitle == 'edit')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activity_report/new', 'New') ?>
                            </li>


                        </ul>
                    </li>




                    <!--  Activity Report-->

                    <li <?= $this->custom_library->role_exist('dhis report', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-books"></i> <span>DHIS2 Report</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('list malaria weekly') ? '' : 'hidden'; ?> class="<?= $title == 'dhis2_reports'&&$subtitle == 'weekly_summaries'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'dhis2_reports/weekly_summaries', 'Weekly Indicators') ?>
                            </li>


                            <li <?= $this->custom_library->role_exist('list malaria weekly') ? '' : 'hidden'; ?> class="<?= $title == 'dhis2_reports'&&$subtitle == 'malaria_weekly'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'dhis2_reports/malaria_weekly', 'Weekly Data') ?>
                            </li>




                            <li <?= $this->custom_library->role_exist('list malaria monthly') ? '' : 'hidden'; ?> class="<?= $title == 'dhis2_reports'&&$subtitle == 'monthly_summaries'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'dhis2_reports/monthly_summaries', 'Monthly Indicators') ?>
                            </li>


                            <li <?= $this->custom_library->role_exist('list malaria monthly') ? '' : 'hidden'; ?> class="<?= $title == 'dhis2_reports'&&$subtitle == 'malaria_monthly'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'dhis2_reports/malaria_monthly', 'Monthly') ?>
                            </li>

                        </ul>
                    </li>





                    <!-- Assessments -->



                    <li <?= $this->custom_library->role_exist('surveys', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-question3"></i> <span>Assessment</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('list surveys') ? '' : 'hidden'; ?> class="<?= ($title=='surveys'||$title=='survey')&&($subtitle == ''||$subtitle == 'edit'||$subtitle == 'view'||$subtitle == 'preview_question'||$subtitle == 'answer_preview')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'survey', 'Assessments') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('create survey') ? '' : 'hidden'; ?> class="<?= $title == 'survey' && $subtitle == 'new'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'survey/new', 'Create Assessment') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('list surveys') ? '' : 'hidden'; ?> class="<?= $subtitle == 'survey_responses'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'survey/survey_responses', 'Responses') ?>
                            </li>

                        </ul>
                    </li>





                    <!-- Indicators -->

                    <li <?= $this->custom_library->role_exist('indicators', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-droplet"></i> <span>Indicators</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('list indicators') ? '' : 'hidden'; ?> class="<?= $title == 'indicators'&&$subtitle == ''? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'indicators', 'List all') ?>
                            </li>

                        </ul>
                    </li>




                <!--Routine Activities-->
                    <li <?= $this->custom_library->role_exist('activities', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-stackoverflow"></i> <span>Routine Activities</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('list activites') ? '' : 'hidden'; ?> class="<?= $title=='activities'&&($subtitle == ''||$subtitle == 'edit'||$subtitle == 'view')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activities', 'Activities') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('list training') ? '' : 'hidden'; ?> class="<?= $subtitle == 'training'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activities/training', 'Training') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('list research') ? '' : 'hidden'; ?>  class="<?= $subtitle == 'research' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'activities/research', 'Research') ?>
                            </li>


                        </ul>
                    </li>


                    <li <?= $this->custom_library->role_exist('resources', 'group') ? '' : 'hidden'; ?>>
                        <a href="#"><i class="icon-notebook"></i><span>Resources</span></a>
                        <ul>
                            <li <?= $this->custom_library->role_exist('create resource') ? '' : 'hidden'; ?> class="nav-item <?php echo $title == 'resources' && $subtitle=='new'?'active':''; ?> ">

                                <?php echo anchor($this->page_level.'resources/new','<i class="fa fa-plus" style="font-size: smaller;"></i> <span class="title">Add Resource</span>','class="nav-link "') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('list resources') ? '' : 'hidden'; ?>
                                    class="<?php echo $title=='resources'&& $subtitle=='' ? 'active' : ''; ?> ">

                                <?php echo anchor($this->page_level.'resources/','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">All Resources</span>','class="nav-link "') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('preview resources') ? '' : 'hidden'; ?>
                                    class="<?php echo $title=='resources'&& $subtitle=='preview_resources' ? 'active' : ''; ?> ">

                                <?php echo anchor($this->page_level.'resources/preview_resources','<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">Preview Resources</span>','class="nav-link "') ?>
                            </li>

                            <?php //foreach($this->db->select('id,title')->from('resource_types')->get()->result() as $pub_menu): ?>
                                <li class="nav-item <?php //echo $subtitle==$pub_menu->id*date('Y')?'active':''; ?> ">


                                    <?php //echo anchor($this->page_level.'resources/'.$pub_menu->id*date('Y'),'<i class="fa fa-list" style="font-size: smaller;"></i> <span class="title">'.$pub_menu->title.'</span>','class="nav-link "') ?>

                                </li>
                            <?php //endforeach; ?>
                        </ul>
                    </li>


                    <!-- Surveys with s-->
                    <li <?= $this->custom_library->role_exist('surveys', 'group') ? '' : 'hidden'; ?>  hidden>
                        <a href="#"><i class="icon-question3"></i> <span>Surveys</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('list surveys') ? '' : 'hidden'; ?> class="<?= $title=='surveys'&&($subtitle == ''||$subtitle == 'edit'||$subtitle == 'view'||$subtitle == 'preview_question')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'surveys', 'Surveys') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('create survey') ? '' : 'hidden'; ?> class="<?= $subtitle == 'new'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'surveys/new', 'Create Survey') ?>
                            </li>

                        </ul>
                    </li>

                    <!--REVIEW OF PATIENTS RECORDS-->
                    <li hidden <?= $this->custom_library->role_exist('surveys', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class=" icon-paste2"></i> <span>Review of Patients Records</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('list surveys') ? '' : 'hidden'; ?> class="<?= $title=='forms'&&($subtitle == ''||$subtitle == 'form_d')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'forms/form_d', 'Form D') ?>
                            </li>



                        </ul>
                    </li>

                    <!--   Messaging-->
                    <li <?= $this->custom_library->role_exist('messaging', 'group') ? '' : 'hidden'; ?>
                            class="<?= $title == 'messaging' ? 'active' : ''; ?>">

                        <?= anchor($this->page_level . 'messaging', '<i class="icon-bubbles9"></i> <span>Messaging</span>') ?>

                    </li>

                    <li hidden class="navigation-header"><span>System Utilities</span> <i class="icon-menu" title="Main pages"></i></li>

                    <!--   User Management-->
                    <li <?= $this->custom_library->role_exist('user management', 'group') ? '' : 'hidden'; ?>
                            class=" <?= $title == 'profile' || $title == 'permissions' ? 'active' : ''; ?>">
                        <a href="#"><i class="icon-user"></i> <span>User Management</span></a>
                        <ul>


                            <li class="<?= $subtitle == 'profile' ? 'active' : ''; ?>"><?= anchor($this->page_level . 'profile', 'Basic Info') ?></li>


                            <li <?= $this->custom_library->role_exist('new user') ? '' : 'hidden'; ?>
                                    class="<?= $title == 'users' && ($subtitle == 'new' || $subtitle == 'edit') ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'users/new', 'Add User') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('View User List') ? '' : 'hidden'; ?>
                                    class="<?= $title == 'users' && ($subtitle == '' || $subtitle == 'view_users') ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'users', 'View Users') ?>
                            </li>


                            <li <?= $this->custom_library->role_exist('View User List') ? '' : 'hidden'; ?>
                                    class="<?= $title == 'users' && $subtitle == 'filter' && $this->uri->segment(4) == 'active' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'users/filter/active', 'active') ?>
                            </li>


                            <li <?= $this->custom_library->role_exist('View User List') ? '' : 'hidden'; ?>
                                    class="<?= $title == 'users' && $subtitle == 'filter' && $this->uri->segment(4) == 'blocked' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'users/filter/blocked', 'Inactive') ?>
                            </li>


                            <li class="  <?= $this->custom_library->role_exist('permissions') && $this->session->user_type == '1' ? '' : 'hidden'; ?>  <?= ($title == 'permissions' && $subtitle == '') || $subtitle == 'perm_group' || ($title == 'permissions' && $subtitle == 'edit') || ($title == 'permissions' && $subtitle == 'new') ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'permissions', 'Permissions') ?></li>

                            <li class="  <?= $this->custom_library->role_exist('roles') && $this->session->user_type == '1' ? '' : 'hidden'; ?>  <?= $title == 'permissions' && $subtitle == 'roles' || $subtitle == 'remove_permission' ? 'active' : ''; ?>"><?= anchor($this->page_level . 'permissions/roles', 'Roles') ?></li>


                        </ul>
                    </li>

                    <!--Audit Trail-->
                    <li <?= $this->custom_library->role_exist('audit logs', 'group') ? '' : 'hidden'; ?>
                            class="<?= $title == 'logs' ? 'active' : ''; ?>">

                        <?= anchor($this->page_level . 'logs', '<i class="icon-list"></i> <span> Audit Trail</span>') ?>

                    </li>

                    <!--Health Facilities-->
                    <li class="<?= $title == 'facilities' ? 'active' : ''; ?>">

                        <?= anchor($this->page_level . 'facilities', '<i class="icon-home7"></i> <span>Healthy Facilities</span>') ?>

                    </li>

                    <li <?= $this->custom_library->role_exist('administrative units', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-earth"></i> <span>Administrative units</span></a>
                        <ul>

                            <li <?= $this->custom_library->role_exist('regions') ? '' : 'hidden'; ?> class="<?= $title == 'administrative_units'&&($subtitle == 'regions'||$subtitle == '')? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'administrative_units', 'Regions') ?>
                            </li>

                            <li <?= $this->custom_library->role_exist('new region') ? '' : 'hidden'; ?> class="<?= $title == 'administrative_units'&&$subtitle == 'new_region'? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'administrative_units/new_region', 'New Region') ?>
                            </li>

                        </ul>
                    </li>

                    <li <?= $this->custom_library->role_exist('settings', 'group') ? '' : 'hidden'; ?> >
                        <a href="#"><i class="icon-cogs"></i> <span>Settings</span></a>
                        <ul>
                            <li class="<?php
                            echo $title == 'settings' && ($subtitle == 'countries' || $subtitle == '' || $subtitle == 'new_country' || $subtitle == 'ban_country' || $subtitle == 'delete_country' || $subtitle == 'unblock_country') ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'settings/countries', 'Countries') ?>
                            </li>

                            <li class="<?= $title == 'settings' && $subtitle == 'regions' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'settings/regions', 'Regions') ?>
                            </li>

                            <li class="<?= $title == 'settings' && $subtitle == 'resource_types' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'settings/resource_types', 'Resource Types') ?>
                            </li>

                            <li class="<?= $title == 'site_options' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . 'site_options', 'Site Options') ?>
                            </li>

                            <li class="<?= $title == 'workplan_levels' ? 'active' : ''; ?>">
                                <?= anchor($this->page_level . '/workplan_levels', 'Workplan Levels') ?>
                            </li>


                        </ul>
                    </li>

                    <li hidden>
                        <a href="#"><i class="icon-bubble"></i> <span>Color system</span></a>
                        <ul>
                            <li class="active"><a href="colors_primary.html">Primary palette</a></li>
                            <li><a href="colors_danger.html">Danger palette</a></li>

                        </ul>
                    </li>

                    <!-- /main -->


                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->


<?php if ($this->uri->segment(2) == 'messaging') { ?>


    <!-- Secondary sidebar -->
    <div class="sidebar sidebar-secondary sidebar-default">
        <div class="sidebar-content">

            <!-- Actions -->
            <div class="sidebar-category">
                <div class="category-title">
                    <span>Actions</span>
                    <ul class="icons-list">
                        <li><a href="#" data-action="collapse"></a></li>
                    </ul>
                </div>

                <div class="category-content">
                    <?php //echo anchor($this->page_level . 'messaging/compose_email', 'Compose Email', 'class="btn bg-indigo-400 btn-block"') ?>

                    <?= anchor($this->page_level . 'messaging/compose_sms', 'Compose SMS', 'class="btn bg-indigo-400 btn-block"') ?>
                </div>
            </div>
            <!-- /actions -->


            <!-- Sub navigation -->
            <div class="sidebar-category">
                <div class="category-title hidden">
                    <span>Navigation</span>
                    <ul class="icons-list">
                        <li><a href="#" data-action="collapse"></a></li>
                    </ul>
                </div>

                <div class="category-content no-padding">
                    <ul class="navigation navigation-alt navigation-accordion no-padding-bottom">
                        <li class="navigation-header">Folders</li>
                        <li class="<?= $this->uri->segment(3) == '' ? 'active' : '' ?>"><a
                                    href="<?= base_url($this->page_level . 'messaging') ?>"><i
                                        class="icon-drawer-in"></i> Inbox <span
                                        class="badge badge-success hidden">32</span></a></li>
                        <li class=" <?= $this->uri->segment(3) == 'sent' ? 'active' : '' ?>"> <?= anchor($this->page_level . 'messaging/sent', '<i class="icon-drawer-out"></i> Sent') ?></li>
                        <li class="<?= $this->uri->segment(3) == 'drafts' ? 'active' : '' ?>"><a href="#"><i
                                        class="icon-drawer3"></i> Drafts</a></li>


                    </ul>
                </div>
            </div>
            <!-- /sub navigation -->


        </div>
    </div>
    <!-- /secondary sidebar -->

<?php } ?>