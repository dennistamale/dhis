<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" /><!---->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->


<?php $pub=$this->db->select()->from('blog')->where('id',$id)->get()->row(); ?>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Edit Resource Type</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open_multipart('',array('class'=>'form-horizontal')) ?>
                <!--                id, full_name, city, password, username, region, country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by, id, id-->
                <div class="form-body">

                    <div class="row">


                        <div class="form-group col-md-12">

                            <label class="col-md-2 control-label" for="title">Title</label>
                            <div class="col-md-10">


                                <input type="text" name="title" class="form-control" value="<?php echo $pub->topic ?>">
                                <label for="form_control_1"> <?php echo form_error('title','<span style=" color:red;">','</span>') ?></label>

                            </div>

                        </div>


                        <div class="form-group col-md-6">
                            <label class="col-md-4 control-label"> Featured Image<br/><b hidden style="white-space: nowrap;">Dimensions : 140px * 140px </b></label>

                            <div class="col-md-8">


                                    <?php if( isset($error)){?>
                                        <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                    <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 400px; height: 250px;">
                                            <img src="<?php echo strlen($pub->image)>0?base_url($pub->image):base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                        </div>
                                        <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image">
																</span>
                                            <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                        </div>
                                    </div>



                                <input name="current" value="<?php echo $pub->image ?>" hidden>


                            </div>

                        </div>




                        <!--    <div class="form-group col-md-12">-->
                        <!---->
                        <!--                        <label class="col-md-2 control-label" for="form_control_1">Description</label>-->
                        <!--                        <div class="col-md-10">-->
                        <!---->
                        <!--                            <textarea class="form-control" rows="5" maxlength="500" required   name="notes">--><?php //echo set_value('notes') ?><!--</textarea>-->
                        <!--                            <label for="form_control_1"> --><?php //echo form_error('notes','<span style=" color:red;">','</span>') ?><!--</label>-->
                        <!---->
                        <!--                        </div>-->
                        <!---->
                        <!--                    </div>-->

                        <div class="form-group col-md-6">
                            <label class="col-md-4 control-label" for="form_control_1">News Type</label>
                            <div class="col-md-8">
                                <select class="form-control" name="news_type" required>
                                    <option value="" <?php echo set_select('news_type', ''); ?> >News Type</option>
                                    <option value="blog" <?php echo set_select('news_type', 'blog',$pub->blog_type=='blog'?true:''); ?> >News</option>
                                    <option value="event" <?php echo set_select('news_type', 'event',$pub->blog_type=='event'?true:''); ?> >Event</option>



                                </select>
                                <label for="form_control_1"> <?php echo form_error('news_type','<span style=" color:red;">','</span>') ?></label>

                            </div>

                        </div>




                        <div class="form-group col-md-12">

                            <label class="col-md-2 control-label" for="form_control_1">Description</label>
                            <div class="col-md-10">

                                <?php  ?>
                                <?php echo $this->ckeditor->editor('notes',@$default_value=$pub->content);?>
                                <?php echo form_error('notes','<span style=" color:red;">','</span>'); ?>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>-->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>-->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>


<!-- END PAGE LEVEL PLUGINS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->
