<div class="col-md-12">
    <div class="panel panel-flat">

        <div class="panel-body">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#basic-tab1" data-toggle="tab">Graphical View </a></li>
                    <li><a href="#basic-tab2" data-toggle="tab">Tabular View</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="basic-tab1">



                        <?php $this->load->view('app/dashboard/dashboard_js') ?>

                        <?= form_open(); ?>
                        <?php $this->load->view('app/dhis2_reports/geo_level_filter'); ?>
                        <?= form_close();  ?>
                        <br/>
                        <?php $this->load->view('app/dashboard/monthly_indicator_graphs'); ?>



                    </div>

                    <div class="tab-pane" id="basic-tab2">

                        <?php $this->load->view('app/dhis2_reports/monthly_summaries_table'); ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('app/dhis2_reports/geo_level_filter_bottom'); ?>


