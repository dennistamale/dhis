
<script>


    $(document).ready(function () {

        // multiselect


        $('.geo_level').change(function () {

            var geo_level = $(this).val();
            // var periods = $('.periods').val();



            if (geo_level != '' && geo_level.length > 0) {

                // alert(periods);

                $('.geo_level_items_group').show();

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('ajax_api/get_geo_level_items/') ?>' + geo_level,
                    success: function (data) {
                        //console.log('data: ' + data);


                        var label = data.name;
                        $('.geo_level_label').html(label);

                        // $('.geo_level_items').html(data);
                        //
                        var selectValues = data.items;


                        $('.geo_level_items').html($("<option></option>").attr("value", '').text(' Select ' + label + 's'));

                        $.each(selectValues, function (key, value) {

                            $('.geo_level_items')
                                .append($("<option></option>")
                                    .attr("value", key)
                                    .text(value));
                        });

                    },
                    error: function (xhr, status, error) {
                        console.log(status + ':' + error);
                    }

                });
            } else {
                $('.geo_level_items_group').hide();
            }


        });


    });
</script>
