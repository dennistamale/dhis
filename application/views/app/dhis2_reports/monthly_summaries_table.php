



<?php $monthly_dimensions=$this->dhis2_model->indicators('monthly'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->

<!-- Begin: Demo Datatable 1 -->
<!--                        <div class="panel panel-flat">-->

<!--    --><?php //print_r($this->input->post()); ?>
<?= form_open(); ?>

<div class="row">
    <div class="table-container">
        <div  class="table-actions-wrapper">
            <span> </span>

            <?php //echo $this->custom_library->role_exist('create survey')? anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New Survey','class="btn btn-sm btn-primary"'):''; ?>
            <h4 class="bold"><?= humanize($subtitle) ?></h4>


        </div>
        <div class="table-responsive">


            <?php
            $cols=0;
            foreach($monthly_dimensions as $f){$cols++;}
            ?>


            <table class="table datatable-ajax display  table-striped nowrap" id="<?= $subtitle ?>">
                <thead>
                <tr>
                    <td colspan="<?php echo $cols+1; ?>">

                        <?php $this->load->view('app/dhis2_reports/geo_level_filter'); ?>

                    </td>
                </tr>
                <tr>

                    <th> Period </th>
<!--                    <th> OrgUnit </th>-->

                    <?php foreach ($monthly_dimensions as $wd){ ?>

                        <th> <?= $wd->name ?> </th>
                    <?php } ?>


                </tr>

                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div>
</div>
<?= form_close() ?>
<!--                        </div>-->
<!-- End: Demo Datatable 1 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<?php $this->load->view('ajax/'.$subtitle);  ?>


