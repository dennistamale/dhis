<div class="row">


    <div class="col-lg-1 col-md-1  col-sm-1 hidden">
        <!--        <label>Geo Level:</label>-->



<!--        LAST_12_MONTH-->
<!--        LAST_52_WEEKS-->

        <?php $default_select=$this->uri->segment(3)=='monthly_summaries'?'LAST_12_MONTH':'LAST_52_WEEKS'?>

        <select class="select periods"
                name="periods" <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'disabled' : ''; ?>
                required>
            <option value="" <?= set_select('periods', '', true) ?>>Period</option>


            <?php foreach ($this->dhis2_model->get_resource_values('', '', 'periods') as $gl) { ?>
                <option value="<?= $gl->custom_value ?>" <?= set_select('periods', $gl->custom_value, (isset($id) ? ($r->geo_level == $gl->id ? true : '') : ($gl->custom_value == $default_select ? true : ''))) ?>><?= $gl->displayName ?></option>

            <?php } ?>

        </select>

    </div>


    <div class="col-lg-2  col-md-2  col-sm-2 ">
        <!--        <label>Geo Level:</label>-->


        <select class="select geo_level form-filter"
                name="geo_level" <?= $subtitle == 'view' || $subtitle == 'update_activity_status' ? 'disabled' : ''; ?>
                required>
            <option value="" <?= set_select('geo_level', '', true) ?>>Geo Level</option>


            <?php foreach ($this->dhis2_model->get_resource_values('', '', 'organisationUnitLevels') as $gl) { ?>
                <option value="<?= $gl->custom_value ?>" <?= set_select('geo_level', $gl->custom_value, (isset($id) ? ($r->geo_level == $gl->id ? true : '') : ($gl->id == 1 ? true : ''))) ?>><?= $gl->displayName ?></option>

            <?php } ?>

        </select>

    </div>


    <div class="col-lg-2 col-md-2  col-sm-2 ">
        <div class="multi-select-full">

            <select class="multiselec form-control geo_level_items form-filter" name="geo_level_items" required>
                <option value="">Details</option>

            </select>

        </div>
    </div>


    <div class="col-md-2  col-md-2  col-sm-2 ">
        <button type="submit" class="btn bg-orange-300 filter-submit"><i
                    class="icon-equalizer2 position-left"></i>Filter
        </button>

        <span hidden class="pull-right">
                    <button type="submit" name="export" value="excel" class="btn btn-success filter-submit"><i
                                class="fa fa-file-excel-o position-left"></i>Export Excel</button>

                </span>
    </div>

</div>

