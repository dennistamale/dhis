
<!--Weekly-->
<?php


$json_weekly='[{
  "columns": [
    {
      "dimension": "dx",
      "items": [
        {
          "id": "fclvwNhzu7d.gGhClrV5odI",
          "name": "Malaria Cases - WEP"
        },
        {
          "id": "YXIu5CW9LPR.gGhClrV5odI",
          "name": "Malaria Deaths - WEP"
        },
        {
          "id": "KPmTI3TGwZw.urYpFjLrrYa",
          "name": "Malaria tests - WEP Microscopy Positive Cases"
        },
        {
          "id": "KPmTI3TGwZw.t8Jv78lnSbU",
          "name": "Malaria tests - WEP Microscopy Tested Cases"
        },
        {
          "id": "KPmTI3TGwZw.Kp9EiDyxPNp",
          "name": "Malaria tests - WEP Positive Cases 5+ Years"
        },
        {
          "id": "KPmTI3TGwZw.t4m0M8uKbwF",
          "name": "Malaria tests - WEP Positve Cases Under 5 Years"
        },
        {
          "id": "KPmTI3TGwZw.PfdPCxOoiBj",
          "name": "Malaria tests - WEP RDT Positve Cases"
        },
        {
          "id": "KPmTI3TGwZw.kcXY5cWAOsB",
          "name": "Malaria tests - WEP RDT Tested Cases"
        },
        {
          "id": "KPmTI3TGwZw.uW3d2AqT4Aq",
          "name": "Malaria tests - WEP Suspected Malaria (fever)"
        },
        {
          "id": "ZgPtploqq0L.RVczviqd1Vv",
          "name": "Malaria treated - WEP 12+ Years"
        },
        {
          "id": "ZgPtploqq0L.Mswd99t7UbA",
          "name": "Malaria treated - WEP 3+ to 7 Years"
        },
        {
          "id": "ZgPtploqq0L.spSdFB7CVOu",
          "name": "Malaria treated - WEP 4 Months to 3 Years"
        },
        {
          "id": "ZgPtploqq0L.nNWIFTErQVY",
          "name": "Malaria treated - WEP 7+ to 12 Years"
        },
        {
          "id": "ZgPtploqq0L.TTwU8Bv4fKm",
          "name": "Malaria treated - WEP Microscopy Negative Cases Treated"
        },
        {
          "id": "ZgPtploqq0L.vp37EZVg3xG",
          "name": "Malaria treated - WEP Microscopy Positive Cases Treated"
        },
        {
          "id": "ZgPtploqq0L.w16OBdsg4b7",
          "name": "Malaria treated - WEP Not Tested Cases Treated"
        },
        {
          "id": "ZgPtploqq0L.NfJyyb1eBaw",
          "name": "Malaria treated - WEP RDT Negative Cases Treated"
        },
        {
          "id": "ZgPtploqq0L.QOWAmZ7jBno",
          "name": "Malaria treated - WEP RDT Positive Cases Treated"
        }
      ]
    }
  ],
  "rows": [
    {
      "dimension": "pe",
      "items": [
        {
          "id": "LAST_52_WEEKS"
        }
      ]
    },
    {
      "dimension": "ou",
      "items": [
        {
          "id": "jKFy8N9yJrQ",
          "name": "St. Jude Domiciliary Clinic HCII"
        },
        {
          "id": "k09iQ8VnzuV",
          "name": "Kaab Medi Care HC II"
        },
        {
          "id": "xJeLMas9wey",
          "name": "Kiboga Health Care Clinic HC II PHP"
        },
        {
          "id": "udyIvAUWRfi",
          "name": "Kyanamuyonjo HC III"
        },
        {
          "id": "adH912aeJav",
          "name": "Kyoomya HC II"
        },
        {
          "id": "qQsReSuKUsB",
          "name": "Masiliba Church Of Uganda HC II"
        },
        {
          "id": "DtmNrJzXRHm",
          "name": "Mwezi HC II"
        },
        {
          "id": "QJMOVcaM8HV",
          "name": "New Qaulity Clinic HC II"
        },
        {
          "id": "H7lh0G6yB3U",
          "name": "Bukomero Clinic HC II"
        },
        {
          "id": "aVPeLDNBucL",
          "name": "Bukomero HC IV"
        },
        {
          "id": "GLHy3J2KXBU",
          "name": "Bugabo Clinic HC II"
        },
        {
          "id": "Ozcyxmbe0t2",
          "name": "Dwaniro Clinic HC II"
        },
        {
          "id": "kiwXaS8ccpN",
          "name": "Elian Health Clinic HC II"
        },
        {
          "id": "bQIMnhjAURl",
          "name": "Katalama HC II"
        },
        {
          "id": "FHtJEqFHwS4",
          "name": "Kiboga Katwe HC III GOVT"
        },
        {
          "id": "LCLV69y7s8Z",
          "name": "Muyenje HC II"
        },
        {
          "id": "lDPaRCAB2hR",
          "name": "Kachwangozi  HC II"
        },
        {
          "id": "abVoisfXO5q",
          "name": "Kansanga General Clinic HC II"
        },
        {
          "id": "ArbUlowwE1y",
          "name": "Kyayimba Epi Centre HC II"
        },
        {
          "id": "yMj0s6Ozoyo",
          "name": "Life Care Clinic HC II"
        },
        {
          "id": "aasaRQtJ3KZ",
          "name": "Lirinda Clinic HC II"
        },
        {
          "id": "ev1JnEmvQe0",
          "name": "Namiringa HC II"
        },
        {
          "id": "SUsqWDu5p84",
          "name": "Kajjere Clinc HC II"
        },
        {
          "id": "CMNFahpvV4I",
          "name": "Kiboga Kambugu HC II GOVT"
        },
        {
          "id": "zfjwLaS3lZe",
          "name": "Kikwatambogo HC II"
        },
        {
          "id": "llNeiWHW1oW",
          "name": "Seeta HC II"
        },
        {
          "id": "Jap4sosMZZN",
          "name": "Agape Nursing Home CLINIC"
        },
        {
          "id": "YuEmzNR0L9A",
          "name": "Bamusuuta/Bamusuta Project HC II"
        },
        {
          "id": "a6h04I37hOu",
          "name": "Family Health Clinic HC II"
        },
        {
          "id": "itDFttFpJRC",
          "name": "Kasumba\'s Clinic HC II"
        },
        {
          "id": "acJji9Ruj5T",
          "name": "Kiboga HOSPITAL"
        },
        {
          "id": "aQi3Qb160NY",
          "name": "Kiboga Prisons Clinic"
        },
        {
          "id": "RK75MaOWsiE",
          "name": "Kiboga police clinic HC II"
        },
        {
          "id": "I5FLxQY5uQG",
          "name": "Mukisa Clinic HC II"
        },
        {
          "id": "WuyEZ5vAK0Z",
          "name": "St Mary\'s Clinic Kiboga"
        },
        {
          "id": "ancPUqPujhL",
          "name": "St. Peter\'S Clinic HC III"
        },
        {
          "id": "wozLwv6Fb7u",
          "name": "Bulaga HC II"
        },
        {
          "id": "EcW1QZ55AVQ",
          "name": "Buninga HC II"
        },
        {
          "id": "qGMx7Aqs7aa",
          "name": "Byakika Clinic HC II"
        },
        {
          "id": "B6qYJ8IW15f",
          "name": "Kasejjere HC II"
        },
        {
          "id": "jwMeXYaVDne",
          "name": "Kisweeka HC II"
        },
        {
          "id": "yw4DRWUv1oK",
          "name": "Kyekumbya HC II"
        },
        {
          "id": "mNMqG0YEffI",
          "name": "Lwamata HC III"
        },
        {
          "id": "AMxVAqEBIgs",
          "name": "Ndibula Clinic HC II"
        },
        {
          "id": "XKZZLEtQDeC",
          "name": "Ndibula Maternity Home HC II"
        },
        {
          "id": "k2T4dghrYMm",
          "name": "Nsala HC II"
        },
        {
          "id": "XJ5agEJ9SwG",
          "name": "People\'s clinic HC II"
        },
        {
          "id": "N3GhLCBwXQo",
          "name": "St Maria Goretti Domicilliary Clinic"
        },
        {
          "id": "kKuqQelGUnq",
          "name": "Muwanga HC III"
        },
        {
          "id": "J64jaeaqOc6",
          "name": "Nabwendo HC III"
        },
        {
          "id": "rgWsOKPrUaL",
          "name": "Nakasengere General Clinic HC II"
        },
        {
          "id": "cf80tiiMBsh",
          "name": "Nakasozi HC II"
        },
        {
          "id": "GUlztXJ4MRg",
          "name": "Makuukulu HC III"
        },
        {
          "id": "aULXp2H2LGI",
          "name": "Kitanda HC III"
        },
        {
          "id": "UjBIv3oF9PN",
          "name": "Mwebaza Domiciliary Clinic HCII"
        },
        {
          "id": "RWEo3tF0K1h",
          "name": "Mirambi HC III"
        },
        {
          "id": "JWY3IEK3xHs",
          "name": "Legacy Medical Center HCII"
        },
        {
          "id": "ogD8wXFjIUD",
          "name": "Kisojjo HC II GOVT"
        },
        {
          "id": "HZqbQQwaQhP",
          "name": "Kambi Clinic HC II"
        },
        {
          "id": "hkoHNZAApbg",
          "name": "Kagoggo HC II"
        },
        {
          "id": "RlulmyGd2XY",
          "name": "Buyoga HC III"
        },
        {
          "id": "NCu3t93AIns",
          "name": "Luyitayita HC II"
        },
        {
          "id": "A7BAVMsywaQ",
          "name": "Kawoko Muslim HC III"
        },
        {
          "id": "ajaZHwTza6B",
          "name": "Kabigi Muslim HC II"
        },
        {
          "id": "VOB2uTrIMvs",
          "name": "Eva Domiciliary Clinic HC II"
        },
        {
          "id": "nVTvYNeCRkf",
          "name": "Buwenda (Butenga) HC II"
        },
        {
          "id": "wHboGxWIhq0",
          "name": "Butenga Medical Center HC III"
        },
        {
          "id": "gBVBGJ2p9b3",
          "name": "Butenga HC IV"
        },
        {
          "id": "agQsp6hFdZa",
          "name": "St. Mechtilda Kitaasa HC III"
        },
        {
          "id": "VCIAE5Bta3H",
          "name": "St. Mary\'S Maternity Home HC III"
        },
        {
          "id": "obXcd9wL3Pf",
          "name": "Bukomansimbi Medical Center HC III"
        },
        {
          "id": "R9ujLreag0E",
          "name": "Buke Medical Centre HC III"
        },
        {
          "id": "dRVlZtrIfcM",
          "name": "Kingangazzi HC II"
        },
        {
          "id": "zxCz0mvkspt",
          "name": "Busagula HC II"
        },
        {
          "id": "HW6fE9RYBjI",
          "name": "Bigasa HC III"
        },
        {
          "id": "oUk4orT6V2h",
          "name": "Dr. Nabala\'S Clinic"
        },
        {
          "id": "VCZvyMMtuNQ",
          "name": "KKonko HC II"
        },
        {
          "id": "fznvuJ4HgYn",
          "name": "Kalagala HC II GOVT"
        },
        {
          "id": "KbedMCCLOcZ",
          "name": "Kidron Health Center HCII"
        },
        {
          "id": "HXGBuRUKJP9",
          "name": "Kirimutu Clinic"
        },
        {
          "id": "ORS7Injeacl",
          "name": "M/S Mbeyiza Maternity CLINIC"
        },
        {
          "id": "RLxqwqtSrRv",
          "name": "Naminya Community Outreach Home CLINIC"
        },
        {
          "id": "joubGoymaJC",
          "name": "Naminya HC II"
        },
        {
          "id": "Cl3rE7BPg2L",
          "name": "Naminya Maternity Home And Clinic"
        },
        {
          "id": "hQWqHwozRFe",
          "name": "St Eliza Health Center HCII"
        },
        {
          "id": "an1CZjQ65h7",
          "name": "Wakisi HC III"
        },
        {
          "id": "Ijowd7CA6q6",
          "name": "KBMK Medical Clinic HCII"
        },
        {
          "id": "bWOR5fD8vdv",
          "name": "Kavule HC II"
        },
        {
          "id": "iJCZZ9rUZYm",
          "name": "Ssenyi HC II"
        },
        {
          "id": "TYC4xgc3ieF",
          "name": "Ssi HC III"
        },
        {
          "id": "zhFVYJiWUOS",
          "name": "Tongolo HC II"
        },
        {
          "id": "Mlh3jcaJw9n",
          "name": "St. Francis Nyenga HOSPITAL"
        },
        {
          "id": "HtDqqOuCfA3",
          "name": "Kabizzi HC  II"
        },
        {
          "id": "Dil7ITnaKlq",
          "name": "Kabizzi -Bugoba HC II"
        },
        {
          "id": "L1xyzHN8G9z",
          "name": "Christ The King HC II"
        },
        {
          "id": "WfEOwsEOMbc",
          "name": "Buziika HC II"
        },
        {
          "id": "imFgSY3OUvZ",
          "name": "Buwagajjo HC III"
        },
        {
          "id": "aRZTDpzTYep",
          "name": "Nkokonjeru Gvt HC II"
        },
        {
          "id": "aFxXtxwMU7f",
          "name": "Nkokonjeru  HOSPITAL"
        },
        {
          "id": "hYLNzVryIZJ",
          "name": "Ebenezer HC II"
        },
        {
          "id": "tDnc44nKr26",
          "name": "St. Francis Health Care Services HC III"
        },
        {
          "id": "GTpX3fXgyQK",
          "name": "Nytil Southern Range Nyanza Clinic"
        },
        {
          "id": "OiuyBahpmGI",
          "name": "Njeru T.C HC III"
        },
        {
          "id": "DQ0Pj4O6Bkq",
          "name": "Nile Breweries Company Clinic  HC III"
        },
        {
          "id": "yM6f1QC8fPA",
          "name": "Lugazi II HC II"
        },
        {
          "id": "XQwQJoJ40dG",
          "name": "Canan HC II"
        },
        {
          "id": "RVSKVTxRcnF",
          "name": "Bukaya HC II"
        },
        {
          "id": "ksuEOzTzI0q",
          "name": "Bujagali HC II"
        },
        {
          "id": "zfecZ4rmn1B",
          "name": "Bugungu YP Prison HC II"
        },
        {
          "id": "Jk4Ws9uWKqb",
          "name": "Bugungu YO Prison HC II"
        },
        {
          "id": "aX1tLCw6glR",
          "name": "Bugungu HC II"
        },
        {
          "id": "rK2BWivCFxl",
          "name": "Ngoma Medical Clinic HC II"
        },
        {
          "id": "SYUCRivGRom",
          "name": "Al-hijira HC III"
        },
        {
          "id": "qdW8M8091gP",
          "name": "Ngogwe HC III"
        },
        {
          "id": "z7Cf9DgHZWX",
          "name": "Namulesa HC II"
        },
        {
          "id": "Eu7L6ZmhA4R",
          "name": "Kikwayi HC II"
        },
        {
          "id": "k7sazN1EJ0o",
          "name": "DDungi HC II"
        },
        {
          "id": "maCVupfrN6c",
          "name": "Bubiro HC II"
        },
        {
          "id": "PwgfpQbQXTQ",
          "name": "Najjembe HC III"
        },
        {
          "id": "H6KWp7l4pOe",
          "name": "Kizigo HC II"
        },
        {
          "id": "raKmghARRwv",
          "name": "Buwundo HC II"
        },
        {
          "id": "BIiLhy7eYLl",
          "name": "Makonge HC III"
        },
        {
          "id": "CqBb08p28JJ",
          "name": "Makindu HC III"
        },
        {
          "id": "HkriBSaojwZ",
          "name": "Kiyindi Medical Clinic"
        },
        {
          "id": "cxWQMqqkkAF",
          "name": "Kisimba Muslim HC II"
        },
        {
          "id": "qADX1ITocto",
          "name": "Kisimba Mission CLINIC"
        },
        {
          "id": "yfEXqa4COUS",
          "name": "Kingdom Life Health Centre CLINIC"
        },
        {
          "id": "ao01qm8XEGE",
          "name": "Kakukunyu Clinic"
        },
        {
          "id": "saURJVbWEtz",
          "name": "Top Care Maternity Home CLINIC HCII"
        },
        {
          "id": "XD2FMtVJgtK",
          "name": "St. Anthony Clinic"
        },
        {
          "id": "AZHrWAlaWuD",
          "name": "St. Agness Domiciliary Cente CLINIC"
        },
        {
          "id": "aagIwPHSkZ7",
          "name": "Moscow Clinic"
        },
        {
          "id": "ssN8ZVpgGEx",
          "name": "Medact Clinic"
        },
        {
          "id": "aOmZnNXPlN2",
          "name": "Marina Medical Centre CLINIC"
        },
        {
          "id": "achC82olIo9",
          "name": "Lugazi Scoul HOSPITAL"
        },
        {
          "id": "myAq6S25Sib",
          "name": "Lugazi Rural Clinic"
        },
        {
          "id": "qYZPZV35BRO",
          "name": "Lugazi Police "
        },
        {
          "id": "J8lqAR46pse",
          "name": "Lugazi Muslim HC II"
        },
        {
          "id": "HTBCQKqmG9S",
          "name": "Lugazi Mission HC II"
        },
        {
          "id": "skaKlRXEgCS",
          "name": "Living Water Community Medical Centre CLINIC"
        },
        {
          "id": "Zp3RAVoxP42",
          "name": "Kisozi Dental & Medical Clinic"
        },
        {
          "id": "nKKX3gA72zS",
          "name": "Kawolo HOSPITAL"
        },
        {
          "id": "mrSGcdJTAGf",
          "name": "Joska Medical Clinic"
        },
        {
          "id": "GiEDg9V9JjR",
          "name": "Kasaku HC II"
        },
        {
          "id": "VWQL8uhCRH4",
          "name": "ENG BD Military Lugazi HC III"
        },
        {
          "id": "analluLubfV",
          "name": "Busabaga HC III"
        },
        {
          "id": "D2O6Qzf4hl8",
          "name": "Health Initiative for Africa - Uganda"
        },
        {
          "id": "U4PSDjSLsi2",
          "name": "Health Initiative Association Uganda"
        },
        {
          "id": "aClaerhXk59",
          "name": "Buikwe St. Charles Lwanga HOSPITAL"
        },
        {
          "id": "sHwAlmYg5Hs",
          "name": "Kasubi HC III"
        },
        {
          "id": "cK5zkZIUFsN",
          "name": "Buikwe HC III"
        }
      ]
    }
  ],
  "filters": null,
  "showHierarchy": true,
  "legendDisplayStyle": "FILL",
  "url": "https://hmis1.health.go.ug/hmis2",
  "el": "reportTable1"
}]';


?>

<?php


$d=$this->dhis2_model->update_dimension($json_weekly);

print_r($d);


?>
