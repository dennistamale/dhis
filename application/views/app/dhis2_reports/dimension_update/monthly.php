
<!--Monthly-->
<?php

$json_monthly='[{
  "columns": [
    {
      "dimension": "dx",
      "items": [
        {
          "id": "Q62QyMDzm3h.ZIk9srZ7KTh",
          "name": "105-1.3 OPD Malaria (Total) 0-28 Days, Female"
        },
        {
          "id": "Q62QyMDzm3h.N6sVfgBHDhx",
          "name": "105-1.3 OPD Malaria (Total) 0-28 Days, Male"
        },
        {
          "id": "Q62QyMDzm3h.z5Yltji9KtS",
          "name": "105-1.3 OPD Malaria (Total) 29 Days-4 Years, Female"
        },
        {
          "id": "Q62QyMDzm3h.gaJzDinonuC",
          "name": "105-1.3 OPD Malaria (Total) 29 Days-4 Years, Male"
        },
        {
          "id": "Q62QyMDzm3h.jft3ntNlnPJ",
          "name": "105-1.3 OPD Malaria (Total) 5-59 Years, Female"
        },
        {
          "id": "Q62QyMDzm3h.c5w8FybAf6t",
          "name": "105-1.3 OPD Malaria (Total) 5-59 Years, Male"
        },
        {
          "id": "Q62QyMDzm3h.PS4qYEgqQmx",
          "name": "105-1.3 OPD Malaria (Total) 60andAbove Years, Female"
        },
        {
          "id": "Q62QyMDzm3h.dHjWd4es7e4",
          "name": "105-1.3 OPD Malaria (Total) 60andAbove Years, Male"
        },
        {
          "id": "J4wj2B8rV6P.ZIk9srZ7KTh",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 0-28 Days, Female"
        },
        {
          "id": "J4wj2B8rV6P.N6sVfgBHDhx",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 0-28 Days, Male"
        },
        {
          "id": "J4wj2B8rV6P.z5Yltji9KtS",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 29 Days-4 Years, Female"
        },
        {
          "id": "J4wj2B8rV6P.gaJzDinonuC",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 29 Days-4 Years, Male"
        },
        {
          "id": "J4wj2B8rV6P.jft3ntNlnPJ",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 5-59 Years, Female"
        },
        {
          "id": "J4wj2B8rV6P.c5w8FybAf6t",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 5-59 Years, Male"
        },
        {
          "id": "J4wj2B8rV6P.PS4qYEgqQmx",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 60andAbove Years, Female"
        },
        {
          "id": "J4wj2B8rV6P.dHjWd4es7e4",
          "name": "105-1.3 OPD Malaria Confirmed (Microscopic & RDT) 60andAbove Years, Male"
        },
        {
          "id": "adF5g61P6YH.ZIk9srZ7KTh",
          "name": "105-1.3 OPD Malaria In Pregnancy  0-28 Days, Female"
        },
        {
          "id": "adF5g61P6YH.N6sVfgBHDhx",
          "name": "105-1.3 OPD Malaria In Pregnancy  0-28 Days, Male"
        },
        {
          "id": "adF5g61P6YH.z5Yltji9KtS",
          "name": "105-1.3 OPD Malaria In Pregnancy  29 Days-4 Years, Female"
        },
        {
          "id": "adF5g61P6YH.gaJzDinonuC",
          "name": "105-1.3 OPD Malaria In Pregnancy  29 Days-4 Years, Male"
        },
        {
          "id": "adF5g61P6YH.jft3ntNlnPJ",
          "name": "105-1.3 OPD Malaria In Pregnancy  5-59 Years, Female"
        },
        {
          "id": "adF5g61P6YH.c5w8FybAf6t",
          "name": "105-1.3 OPD Malaria In Pregnancy  5-59 Years, Male"
        },
        {
          "id": "adF5g61P6YH.PS4qYEgqQmx",
          "name": "105-1.3 OPD Malaria In Pregnancy  60andAbove Years, Female"
        },
        {
          "id": "adF5g61P6YH.dHjWd4es7e4",
          "name": "105-1.3 OPD Malaria In Pregnancy  60andAbove Years, Male"
        },
        {
          "id": "M3l4H6S7nLR.Avvv8JaSwGR",
          "name": "105-6  Malaria Rapid Diagnostic tests Days out of stock"
        },
        {
          "id": "M3l4H6S7nLR.BhwinOOCTmR",
          "name": "105-6  Malaria Rapid Diagnostic tests Quantity Utilized"
        },
        {
          "id": "M3l4H6S7nLR.JhQChFHlaNM",
          "name": "105-6  Malaria Rapid Diagnostic tests Stock at Hand"
        },
        {
          "id": "STF7P24RAqC.snPZPBW4ZW0",
          "name": "105-7.3 Lab Malaria Microscopy  Number Done, 5 years and above"
        },
        {
          "id": "STF7P24RAqC.bSUziAZFlqN",
          "name": "105-7.3 Lab Malaria Microscopy  Number Done, Under 5 years"
        },
        {
          "id": "STF7P24RAqC.Li89EZS6Jss",
          "name": "105-7.3 Lab Malaria Microscopy  Number Positive, 5 years and above"
        },
        {
          "id": "STF7P24RAqC.PT9vhPUpxc4",
          "name": "105-7.3 Lab Malaria Microscopy  Number Positive, Under 5 years"
        },
        {
          "id": "fOZ2QUjRSB7.snPZPBW4ZW0",
          "name": "105-7.3 Lab Malaria RDTs Number Done, 5 years and above"
        },
        {
          "id": "fOZ2QUjRSB7.bSUziAZFlqN",
          "name": "105-7.3 Lab Malaria RDTs Number Done, Under 5 years"
        },
        {
          "id": "fOZ2QUjRSB7.Li89EZS6Jss",
          "name": "105-7.3 Lab Malaria RDTs Number Positive, 5 years and above"
        },
        {
          "id": "fOZ2QUjRSB7.PT9vhPUpxc4",
          "name": "105-7.3 Lab Malaria RDTs Number Positive, Under 5 years"
        },
        {
          "id": "KLvfkPOHRin.gGhClrV5odI",
          "name": "107 4.1d Functional Laboratory For Malaria Diagnosis"
        },
        {
          "id": "Ug4xX8noNlY.gGhClrV5odI",
          "name": "107 4.1e Treatment of Severe Malaria "
        },
        {
          "id": "ps7Cj6JKM64.z1I5NwTvSkt",
          "name": "108-5b Severe malaria  Packed Cells, 0-4 Years"
        },
        {
          "id": "ps7Cj6JKM64.Bh0JlVT2QRL",
          "name": "108-5b Severe malaria  Packed Cells, 5 years and above"
        },
        {
          "id": "ps7Cj6JKM64.eqTLHZ8pEIY",
          "name": "108-5b Severe malaria  Plasma, 0-4 Years"
        },
        {
          "id": "ps7Cj6JKM64.nZpDoUKfYRQ",
          "name": "108-5b Severe malaria  Plasma, 5 years and above"
        },
        {
          "id": "ps7Cj6JKM64.FwsObyc5Dt2",
          "name": "108-5b Severe malaria  Platelets, 0-4 Years"
        },
        {
          "id": "ps7Cj6JKM64.bGq9J9mHODP",
          "name": "108-5b Severe malaria  Platelets, 5 years and above"
        },
        {
          "id": "ps7Cj6JKM64.OWku4zsdztD",
          "name": "108-5b Severe malaria  Whole Blood, 0-4 Years"
        },
        {
          "id": "ps7Cj6JKM64.eA3YLuCoJgJ",
          "name": "108-5b Severe malaria  Whole Blood, 5 years and above"
        },
        {
          "id": "vlmLOxXIs6Z.SQR19VM3MBj",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) 5 years and above, Case, Female"
        },
        {
          "id": "vlmLOxXIs6Z.wkhzwzTXO5R",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) 5 years and above, Case, Male"
        },
        {
          "id": "vlmLOxXIs6Z.QuELIu2jDD5",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) 5 years and above, Death, Female"
        },
        {
          "id": "vlmLOxXIs6Z.RADt2gczsA8",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) 5 years and above, Death, Male"
        },
        {
          "id": "vlmLOxXIs6Z.BNx5feudBi7",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) Under 5 years, Case, Female"
        },
        {
          "id": "vlmLOxXIs6Z.Gqu7GLDWYxg",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) Under 5 years, Case, Male"
        },
        {
          "id": "vlmLOxXIs6Z.dEwYU3a6mDX",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) Under 5 years, Death, Female"
        },
        {
          "id": "vlmLOxXIs6Z.CK2t9ijMAkq",
          "name": "108-6 Malaria Confirmed (Microscopic &RDT) Under 5 years, Death, Male"
        },
        {
          "id": "Gg4H29c3VpF.rwzuUppxDIL",
          "name": "108-6 Malaria In Pregnancy 10-19 Years, Case"
        },
        {
          "id": "Gg4H29c3VpF.XYPwtzoviZg",
          "name": "108-6 Malaria In Pregnancy 10-19 Years, Death"
        },
        {
          "id": "Gg4H29c3VpF.Sr9aztpFnbt",
          "name": "108-6 Malaria In Pregnancy 20-24 Years, Case"
        },
        {
          "id": "Gg4H29c3VpF.OZjzX4ovkwG",
          "name": "108-6 Malaria In Pregnancy 20-24 Years, Death"
        },
        {
          "id": "Gg4H29c3VpF.Q7cGJXMcHrq",
          "name": "108-6 Malaria In Pregnancy >=25 Years, Case"
        },
        {
          "id": "Gg4H29c3VpF.S3qBSFUv2OZ",
          "name": "108-6 Malaria In Pregnancy >=25 Years, Death"
        },
        {
          "id": "l5Z1EvnZY3X.SQR19VM3MBj",
          "name": "108-6 Malaria total 5 years and above, Case, Female"
        },
        {
          "id": "l5Z1EvnZY3X.wkhzwzTXO5R",
          "name": "108-6 Malaria total 5 years and above, Case, Male"
        },
        {
          "id": "l5Z1EvnZY3X.QuELIu2jDD5",
          "name": "108-6 Malaria total 5 years and above, Death, Female"
        },
        {
          "id": "l5Z1EvnZY3X.RADt2gczsA8",
          "name": "108-6 Malaria total 5 years and above, Death, Male"
        },
        {
          "id": "l5Z1EvnZY3X.BNx5feudBi7",
          "name": "108-6 Malaria total Under 5 years, Case, Female"
        },
        {
          "id": "l5Z1EvnZY3X.Gqu7GLDWYxg",
          "name": "108-6 Malaria total Under 5 years, Case, Male"
        },
        {
          "id": "l5Z1EvnZY3X.dEwYU3a6mDX",
          "name": "108-6 Malaria total Under 5 years, Death, Female"
        },
        {
          "id": "l5Z1EvnZY3X.CK2t9ijMAkq",
          "name": "108-6 Malaria total Under 5 years, Death, Male"
        }
      ]
    }
  ],
  "rows": [
    {
      "dimension": "ou",
      "items": [
        {
          "id": "LEVEL-5"
        },
        {
          "id": "cK5zkZIUFsN",
          "name": "Buikwe HC III"
        },
        {
          "id": "sHwAlmYg5Hs",
          "name": "Kasubi HC III"
        },
        {
          "id": "aClaerhXk59",
          "name": "Buikwe St. Charles Lwanga HOSPITAL"
        },
        {
          "id": "U4PSDjSLsi2",
          "name": "Health Initiative Association Uganda"
        },
        {
          "id": "D2O6Qzf4hl8",
          "name": "Health Initiative for Africa - Uganda"
        },
        {
          "id": "analluLubfV",
          "name": "Busabaga HC III"
        },
        {
          "id": "VWQL8uhCRH4",
          "name": "ENG BD Military Lugazi HC III"
        },
        {
          "id": "GiEDg9V9JjR",
          "name": "Kasaku HC II"
        },
        {
          "id": "mrSGcdJTAGf",
          "name": "Joska Medical Clinic"
        },
        {
          "id": "nKKX3gA72zS",
          "name": "Kawolo HOSPITAL"
        },
        {
          "id": "Zp3RAVoxP42",
          "name": "Kisozi Dental & Medical Clinic"
        },
        {
          "id": "skaKlRXEgCS",
          "name": "Living Water Community Medical Centre CLINIC"
        },
        {
          "id": "HTBCQKqmG9S",
          "name": "Lugazi Mission HC II"
        },
        {
          "id": "J8lqAR46pse",
          "name": "Lugazi Muslim HC II"
        },
        {
          "id": "qYZPZV35BRO",
          "name": "Lugazi Police "
        },
        {
          "id": "myAq6S25Sib",
          "name": "Lugazi Rural Clinic"
        },
        {
          "id": "achC82olIo9",
          "name": "Lugazi Scoul HOSPITAL"
        },
        {
          "id": "aOmZnNXPlN2",
          "name": "Marina Medical Centre CLINIC"
        },
        {
          "id": "ssN8ZVpgGEx",
          "name": "Medact Clinic"
        },
        {
          "id": "aagIwPHSkZ7",
          "name": "Moscow Clinic"
        },
        {
          "id": "AZHrWAlaWuD",
          "name": "St. Agness Domiciliary Cente CLINIC"
        },
        {
          "id": "XD2FMtVJgtK",
          "name": "St. Anthony Clinic"
        },
        {
          "id": "saURJVbWEtz",
          "name": "Top Care Maternity Home CLINIC HCII"
        },
        {
          "id": "ao01qm8XEGE",
          "name": "Kakukunyu Clinic"
        },
        {
          "id": "yfEXqa4COUS",
          "name": "Kingdom Life Health Centre CLINIC"
        },
        {
          "id": "qADX1ITocto",
          "name": "Kisimba Mission CLINIC"
        },
        {
          "id": "cxWQMqqkkAF",
          "name": "Kisimba Muslim HC II"
        },
        {
          "id": "HkriBSaojwZ",
          "name": "Kiyindi Medical Clinic"
        },
        {
          "id": "CqBb08p28JJ",
          "name": "Makindu HC III"
        },
        {
          "id": "BIiLhy7eYLl",
          "name": "Makonge HC III"
        },
        {
          "id": "raKmghARRwv",
          "name": "Buwundo HC II"
        },
        {
          "id": "H6KWp7l4pOe",
          "name": "Kizigo HC II"
        },
        {
          "id": "PwgfpQbQXTQ",
          "name": "Najjembe HC III"
        },
        {
          "id": "maCVupfrN6c",
          "name": "Bubiro HC II"
        },
        {
          "id": "k7sazN1EJ0o",
          "name": "DDungi HC II"
        },
        {
          "id": "Eu7L6ZmhA4R",
          "name": "Kikwayi HC II"
        },
        {
          "id": "z7Cf9DgHZWX",
          "name": "Namulesa HC II"
        },
        {
          "id": "qdW8M8091gP",
          "name": "Ngogwe HC III"
        },
        {
          "id": "SYUCRivGRom",
          "name": "Al-hijira HC III"
        },
        {
          "id": "rK2BWivCFxl",
          "name": "Ngoma Medical Clinic HC II"
        },
        {
          "id": "aX1tLCw6glR",
          "name": "Bugungu HC II"
        },
        {
          "id": "Jk4Ws9uWKqb",
          "name": "Bugungu YO Prison HC II"
        },
        {
          "id": "zfecZ4rmn1B",
          "name": "Bugungu YP Prison HC II"
        },
        {
          "id": "ksuEOzTzI0q",
          "name": "Bujagali HC II"
        },
        {
          "id": "RVSKVTxRcnF",
          "name": "Bukaya HC II"
        },
        {
          "id": "XQwQJoJ40dG",
          "name": "Canan HC II"
        },
        {
          "id": "yM6f1QC8fPA",
          "name": "Lugazi II HC II"
        },
        {
          "id": "DQ0Pj4O6Bkq",
          "name": "Nile Breweries Company Clinic  HC III"
        },
        {
          "id": "OiuyBahpmGI",
          "name": "Njeru T.C HC III"
        },
        {
          "id": "GTpX3fXgyQK",
          "name": "Nytil Southern Range Nyanza Clinic"
        },
        {
          "id": "tDnc44nKr26",
          "name": "St. Francis Health Care Services HC III"
        },
        {
          "id": "hYLNzVryIZJ",
          "name": "Ebenezer HC II"
        },
        {
          "id": "aFxXtxwMU7f",
          "name": "Nkokonjeru  HOSPITAL"
        },
        {
          "id": "aRZTDpzTYep",
          "name": "Nkokonjeru Gvt HC II"
        },
        {
          "id": "imFgSY3OUvZ",
          "name": "Buwagajjo HC III"
        },
        {
          "id": "WfEOwsEOMbc",
          "name": "Buziika HC II"
        },
        {
          "id": "L1xyzHN8G9z",
          "name": "Christ The King HC II"
        },
        {
          "id": "Dil7ITnaKlq",
          "name": "Kabizzi -Bugoba HC II"
        },
        {
          "id": "HtDqqOuCfA3",
          "name": "Kabizzi HC  II"
        },
        {
          "id": "Mlh3jcaJw9n",
          "name": "St. Francis Nyenga HOSPITAL"
        },
        {
          "id": "zhFVYJiWUOS",
          "name": "Tongolo HC II"
        },
        {
          "id": "Ijowd7CA6q6",
          "name": "KBMK Medical Clinic HCII"
        },
        {
          "id": "bWOR5fD8vdv",
          "name": "Kavule HC II"
        },
        {
          "id": "iJCZZ9rUZYm",
          "name": "Ssenyi HC II"
        },
        {
          "id": "TYC4xgc3ieF",
          "name": "Ssi HC III"
        },
        {
          "id": "oUk4orT6V2h",
          "name": "Dr. Nabala\'S Clinic"
        },
        {
          "id": "VCZvyMMtuNQ",
          "name": "KKonko HC II"
        },
        {
          "id": "fznvuJ4HgYn",
          "name": "Kalagala HC II GOVT"
        },
        {
          "id": "KbedMCCLOcZ",
          "name": "Kidron Health Center HCII"
        },
        {
          "id": "HXGBuRUKJP9",
          "name": "Kirimutu Clinic"
        },
        {
          "id": "ORS7Injeacl",
          "name": "M/S Mbeyiza Maternity CLINIC"
        },
        {
          "id": "RLxqwqtSrRv",
          "name": "Naminya Community Outreach Home CLINIC"
        },
        {
          "id": "joubGoymaJC",
          "name": "Naminya HC II"
        },
        {
          "id": "Cl3rE7BPg2L",
          "name": "Naminya Maternity Home And Clinic"
        },
        {
          "id": "hQWqHwozRFe",
          "name": "St Eliza Health Center HCII"
        },
        {
          "id": "an1CZjQ65h7",
          "name": "Wakisi HC III"
        },
        {
          "id": "HW6fE9RYBjI",
          "name": "Bigasa HC III"
        },
        {
          "id": "zxCz0mvkspt",
          "name": "Busagula HC II"
        },
        {
          "id": "dRVlZtrIfcM",
          "name": "Kingangazzi HC II"
        },
        {
          "id": "R9ujLreag0E",
          "name": "Buke Medical Centre HC III"
        },
        {
          "id": "obXcd9wL3Pf",
          "name": "Bukomansimbi Medical Center HC III"
        },
        {
          "id": "VCIAE5Bta3H",
          "name": "St. Mary\'S Maternity Home HC III"
        },
        {
          "id": "agQsp6hFdZa",
          "name": "St. Mechtilda Kitaasa HC III"
        },
        {
          "id": "gBVBGJ2p9b3",
          "name": "Butenga HC IV"
        },
        {
          "id": "wHboGxWIhq0",
          "name": "Butenga Medical Center HC III"
        },
        {
          "id": "nVTvYNeCRkf",
          "name": "Buwenda (Butenga) HC II"
        },
        {
          "id": "VOB2uTrIMvs",
          "name": "Eva Domiciliary Clinic HC II"
        },
        {
          "id": "ajaZHwTza6B",
          "name": "Kabigi Muslim HC II"
        },
        {
          "id": "A7BAVMsywaQ",
          "name": "Kawoko Muslim HC III"
        },
        {
          "id": "NCu3t93AIns",
          "name": "Luyitayita HC II"
        },
        {
          "id": "RlulmyGd2XY",
          "name": "Buyoga HC III"
        },
        {
          "id": "hkoHNZAApbg",
          "name": "Kagoggo HC II"
        },
        {
          "id": "HZqbQQwaQhP",
          "name": "Kambi Clinic HC II"
        },
        {
          "id": "ogD8wXFjIUD",
          "name": "Kisojjo HC II GOVT"
        },
        {
          "id": "JWY3IEK3xHs",
          "name": "Legacy Medical Center HCII"
        },
        {
          "id": "RWEo3tF0K1h",
          "name": "Mirambi HC III"
        },
        {
          "id": "UjBIv3oF9PN",
          "name": "Mwebaza Domiciliary Clinic HCII"
        },
        {
          "id": "aULXp2H2LGI",
          "name": "Kitanda HC III"
        },
        {
          "id": "GUlztXJ4MRg",
          "name": "Makuukulu HC III"
        },
        {
          "id": "jKFy8N9yJrQ",
          "name": "St. Jude Domiciliary Clinic HCII"
        },
        {
          "id": "aP3vXWciBlE",
          "name": "Kibugga HC II"
        },
        {
          "id": "qYIh49gM216",
          "name": "Kyabadaza HC III"
        },
        {
          "id": "laPnCXSZ0Oo",
          "name": "Bulo HC III"
        },
        {
          "id": "v6KdyypBQBB",
          "name": "Bulo clinic HC II"
        },
        {
          "id": "F3r8Fr87Bnx",
          "name": "MARIA ASSUMPTA HC III"
        },
        {
          "id": "U5TE1ItzA0v",
          "name": "Sanyu Domicilliary HC II"
        },
        {
          "id": "xuCJ8NBomxP",
          "name": "Gombe HOSPITAL"
        },
        {
          "id": "oglrek2bw9A",
          "name": "Ntolomwe HC II"
        },
        {
          "id": "E1TxjBqAvWB",
          "name": "Butambala Epi Centre HC III GOVT"
        },
        {
          "id": "BE5LiYZjBvM",
          "name": "Kabasanda HC II"
        },
        {
          "id": "aDGztjb0S7j",
          "name": "Kalamba Community HC II"
        },
        {
          "id": "jSTIbH5xzbW",
          "name": "Kilokola HC II"
        },
        {
          "id": "O4sKsWoxekL",
          "name": "Kitimba HC III"
        },
        {
          "id": "J5ZGPIn0Opf",
          "name": "Nsozibirye HC II"
        },
        {
          "id": "gxpqLAdym0H",
          "name": "Savio Medical Centre HC II"
        },
        {
          "id": "lxS0qhIRkQv",
          "name": "Butaaka HC II"
        },
        {
          "id": "yVfysxEDePw",
          "name": "Kawonawo HC II"
        },
        {
          "id": "gr1CRYxuJmr",
          "name": "Kibibi Nursing Home HC III"
        },
        {
          "id": "FI11C32LNK8",
          "name": "Kiziiko HC II"
        },
        {
          "id": "a93zYnWO0cT",
          "name": "Bugobango HC II"
        },
        {
          "id": "kMZGxkg0VmA",
          "name": "Butende HC II GOVT"
        },
        {
          "id": "gt954ahlttH",
          "name": "Kidawalime Nursing Home HC II"
        },
        {
          "id": "lH80zq7z1Hg",
          "name": "Ngando HC III"
        },
        {
          "id": "ieEt6Fjrt54",
          "name": "Baptist Mission Clinic - NR"
        },
        {
          "id": "alEP292p59v",
          "name": "Bugabo HC II - NR"
        },
        {
          "id": "afzGq7RpOTl",
          "name": "Busamuzi HC III"
        },
        {
          "id": "AIl1p6YgqxA",
          "name": "Buwooya HC II"
        },
        {
          "id": "OOEbAPdUMTg",
          "name": "Lingira (Ywam) HC II "
        },
        {
          "id": "aVeE8JrylXn",
          "name": "Bugaya HC III ( Buvuma )"
        },
        {
          "id": "WezVXLK01Ry",
          "name": "Nkata HC II"
        },
        {
          "id": "ewMHVf7tjwq",
          "name": "Buvuma HC IV"
        },
        {
          "id": "G6GmMEAZA9r",
          "name": "Bweema HC III"
        },
        {
          "id": "bBNSvGxVrSq",
          "name": "Lwajje HC II"
        },
        {
          "id": "LDngLVQbqNI",
          "name": "Namatale HC II"
        },
        {
          "id": "aZjBV5O7kkf",
          "name": "Lubya HC II"
        },
        {
          "id": "Eg7Efn5rKSj",
          "name": "Namiti HC II"
        }
      ]
    },
    {
      "dimension": "pe",
      "items": [
        {
          "id": "201802",
          "name": "February 2018"
        },
        {
          "id": "201801",
          "name": "January 2018"
        },
        {
          "id": "201701",
          "name": "January 2017"
        },
        {
          "id": "201702",
          "name": "February 2017"
        },
        {
          "id": "201703",
          "name": "March 2017"
        },
        {
          "id": "201704",
          "name": "April 2017"
        },
        {
          "id": "201705",
          "name": "May 2017"
        },
        {
          "id": "201706",
          "name": "June 2017"
        },
        {
          "id": "201707",
          "name": "July 2017"
        },
        {
          "id": "201708",
          "name": "August 2017"
        },
        {
          "id": "201709",
          "name": "September 2017"
        },
        {
          "id": "201710",
          "name": "October 2017"
        },
        {
          "id": "201711",
          "name": "November 2017"
        },
        {
          "id": "201712",
          "name": "December 2017"
        },
        {
          "id": "201612",
          "name": "December 2016"
        },
        {
          "id": "201611",
          "name": "November 2016"
        },
        {
          "id": "201610",
          "name": "October 2016"
        },
        {
          "id": "201609",
          "name": "September 2016"
        },
        {
          "id": "201608",
          "name": "August 2016"
        },
        {
          "id": "201607",
          "name": "July 2016"
        },
        {
          "id": "201606",
          "name": "June 2016"
        },
        {
          "id": "201605",
          "name": "May 2016"
        },
        {
          "id": "201604",
          "name": "April 2016"
        },
        {
          "id": "201603",
          "name": "March 2016"
        }
      ]
    }
  ],
  "filters": null,
  "showHierarchy": true,
  "legendDisplayStyle": "FILL",
  "url": "https://hmis1.health.go.ug/hmis2",
  "el": "reportTable1"
}]';

?>


<?php

$d=$this->dhis2_model->update_dimension($json_monthly);

print_r($d);

?>
