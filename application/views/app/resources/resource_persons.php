<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">



    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title); ?>s&nbsp;</span>
                </div>
                <div class="actions">

<!--                    --><?php //echo anchor($this->page_level.'transfer',' <i class="fa fa-exchange"></i> View Transactions','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                    <thead>
                    <tr>

                        <th> Full Names </th>
                        <th> Title </th>



                        <th> Area </th>

                        <th> Phone </th>

                        <th> Email </th>


                        <th>Date</th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    foreach($this->db->select()->from('resource_persons a')->order_by('a.id','desc')->get()->result() as $fl): ?>

                        <tr>


                            <td>

                                <?php echo $fl->full_names ?>

                            </td>
                            <td>
                                <?php
                                echo $fl->title
                                ?></td>
                            <td> <?php echo $fl->area  ?> </td>




                            <td>

                                <?php

                                echo  $fl->phone
                                ?>
                            </td>

                            <td>

                                <?php

                                echo  $fl->email
                                ?>
                            </td>


                            <td><?php echo date('d-m-Y H:i',$fl->created_on) ?></td>


                        </tr>
                        <?php  endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->