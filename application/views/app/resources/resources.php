<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"      rel="stylesheet" type="text/css"/>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= base_url() ?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />


<!-- END THEME GLOBAL STYLES -->
<!-- END HEAD -->
        <div class="panel panel-flat">

            <?= form_open(); ?>

            <!--<div class="panel-heading" style="padding: 0px 20px 0px;">

                <div class="form-inline">

                    <div class="caption font-dark form-group" style="    padding: 19px 0;">
                        <i class="icon-home font-dark"></i>
                        <span class="caption-subject bold uppercase"><?php //echo humanize($title) ?></span>
                    </div>

                    <div class="pull-right margin-top-10">
                        <div class="tools form-group">
                            <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                            <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                            <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="panel-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <span> </span>

                        <?= $this->custom_library->role_exist('create resource') ? anchor($this->page_level . 'resources/new', '<i class="fa fa-plus"></i> Add New', 'class="btn btn-xs btn-primary"') : ''; ?>

                    </div>

                    <table class="table datatable-ajax" id="<?= $title ?>">
                        <thead>
                        <tr>
                            <!--
                            <td colspan="10">
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php //echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                            <span class="input-group-addon">From </span>
                                            <input type="text" class="form-control table-group-from-input input-sm" name="from" value="<?php //echo set_value('from') ?>">
                                            <span class="input-group-addon">to </span>
                                            <input type="text" class="form-control table-group-to-input input-sm" name="to" value="<?php //echo set_value('to')?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select  class="table-group-resource-input form-control input-inline input-small input-sm">
                                                <option value="">Resource Type...</option>
                                                <?php //foreach($this->db->select('id,title')->from('resource_types')->get()->result() as $sc): ?>
                                                    <option value="<?php //echo $sc->id ?>"><?php echo humanize($sc->title) ?></option>
                                                <?php //endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <button class="btn btn-sm green table-group-action-submit">
                                            <i class="fa fa-sliders"></i> Apply</button>

                                    </div>
                                </div>
                            </td>
                        -->

                            <td colspan="10">
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                            <input name="date_range" readonly value=""  type="text" class="form-control daterange-basi daterange-predefined form-filter input-x" required="required"
                                                   data-popup="tooltip" title="" data-placement="top" data-original-title="Request Date"
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group ">
                                            <span class="input-group-addon"><i class="icon-sphere"></i></span>
                                            <select name="status" class="form-control" style="width: 150px">
                                                <option value="" selected>Status</option>
                                                <option value="published">Published</option>
                                                <option value="unpublished">Unpublished</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group " >
                                            <span class="input-group-addon"><i class="icon-snowflake"></i></span>
                                            <select name="resource_type" class="select form-filter">
                                                <option value="" selected>Resource Type</option>
                                                <?php foreach ($this->db->select()->from('resource_types')->get()->result() as $cat) { ?>
                                                    <option value="<?php echo $cat->id ?>" <?php echo set_select('resource_type', $cat->id, isset($id) ? $row->resource_type == $cat->id ? true : '' : ''); ?> ><?php echo $cat->title ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn bg-orange-300 filter-submit"><i
                                                    class="icon-equalizer2 position-left"></i>Filter
                                        </button>
                                    </div>
                                </div>
                            </td>

                        </tr>

                        <tr>

                            <th> # </th>
                            <th> Title </th>
                            <th> Resource Type </th>
                            <th> Date </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
            <?= form_close() ?>
        </div>
        <!-- End: life time stats -->


<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
        type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<?php
$this->load->view('app/' . $this->page_level2 .'ajax_'.$title);
?>