<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Browse resources</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

                    <div class="row col-md-9">

                        <?php if(strlen($this->uri->segment(4))>0){ ?>

                        <div class="col-md-12">

                            <div class="content-group">

                                    <?php

                                    $id= $this->uri->segment(4)/date('Y');


                                    $rt=$this->db->select('id,title')->from('resource_types')->where('id',$id)->get()->row();

                                    $res= $this->db->select('id,title,attachment')->from('resources')->where(array('resource_type'=>$id,'status'=>'published'))->get()->result();
                                    ?>

                                    <h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> <?php echo humanize($rt->title) ?></h6>

                                    <div class="list-group no-border">

                                        <?php foreach($res as $r): ?>

                                            <?php  echo ( strlen($r->attachment) > 0 ? '<a class="list-group-item" href="'.base_url($r->attachment).'" target="_blank"><i class="icon-file-text2"></i> '.ucfirst($r->title).'</a>' : anchor($this->page_level.$this->page_level2.'view/'.$r->id*date('Y'),'<i class="icon-file-text2"></i> '.ucfirst($r->title), 'class="list-group-item"')) ?>

                                        <?php endforeach; ?>

                                    </div>
                            </div>

                        </div>

                        <?php }else{ ?>

                            <div class="row">



                            <?php   $no = 0;
                                    foreach( $this->db->select('id,title')->from('resource_types')->get()->result() as $rt ):
                                        if($no != 0 && ($no % 2) == 0 ) echo '</div><div class="row">';

                                            $no++;

                                        ?>

                                <div class="col-md-6">

                                    <div class="content-group">

                                        <?php
                                        $res = $this->db->select('id,title,attachment')->from('resources')->where(array('resource_type'=>$rt->id,'status'=>'published'))->order_by('id','desc')->limit(10)->get()->result();
                                        ?>

                                        <h6 class="text-semibold heading-divided"><i class="icon-folder6 position-left"></i> <?php echo humanize($rt->title) ?></h6>

                                        <div class="list-group no-border">

                                            <?php foreach($res as $r): ?>

                                                <?php  echo ( strlen($r->attachment) > 0 ? '<a class="list-group-item" href="'.base_url($r->attachment).'" target="_blank"><i class="icon-file-text2"></i> '.ucfirst($r->title).'</a>' : anchor($this->page_level.$this->page_level2.'view/'.$r->id*date('Y'),'<i class="icon-file-text2"></i> '.ucfirst($r->title), 'class="list-group-item"')) ?>

                                            <?php endforeach; ?>


                                        <?php

                                            $count_res= $this->db->from('resources')->where(array('resource_type'=> $rt->id ,'status'=>'published'))->count_all_results();
                                            echo '<a href="resource_type/'.$rt->id*date('Y').'" class="list-group-item"><i class="icon-arrow-right22"></i> Show all resources ('.$count_res.') </a>' //anchor($this->page_level.$this->page_level2.'resource_type/'.$rt->id*date('Y'),'View all '.$count_res.' <i class="fa fa-arrow-right"></i>','class="font-text font-yellow-gold"');

                                            //'.$this->page_level.$this->page_level2.'

                                            ?>

                                        </div>
                                    </div>

                                </div>



                            <?php endforeach; ?>

                            </div>

                        <?php } ?>

                    </div>

                    <div class="col-md-3" >
                        <!--        SEARCH WIDGET START-->

                        <div class="widget widget-categories">
                            <h2>Categories</h2>
                            <ul>

                                <?php $all_res=$this->db->from('resources')->where(array('status'=>'published'))->count_all_results() ?>
                                <li><?php echo anchor($this->page_level.$this->page_level2.'preview_resources','All Categories&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:xx-small;" class="badge">'.$all_res.'  </span>')?> </li>
                                <?php foreach($this->db->select()->from('resource_types')->get()->result() as $sc): ?>
                                    <li  class="<?php echo isset($id) ? $sc->id == $id?'active':'': '' ?>">
                                        <?php
                                        $pubs_count= $this->db->where('resource_type',$sc->id)->from('resources')->where(array('status'=>'published'))->count_all_results();
                                        ?>
                                        <?php echo anchor($this->page_level.$this->page_level2.'resource_type/'.$sc->id*date('Y'),$sc->title.' &nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:xx-small;" class="badge"> '.$pubs_count.' </span>') ?>
                                    </li>
                                <?php endforeach; ?>
                                <li class="<?php echo $this->uri->segment(3)=='resource_persons'?'active':'' ?>"> <?php echo anchor($this->page_level.'resources/resource_persons','Resource Persons') ?></li>

                            </ul>
                        </div>

                        <div class="widget widget-new-arrival"  >
                            <h2>Most Popular</h2>
                            <ul>

                                <?php
                                $events = $this->db->select()->from('resources')->where(array('status'=>'published'))->order_by('id','desc')->limit(10)->get()->result();
                                $data=array(
                                    'alert'=>'info',
                                    'message'=>'No resources found...',
                                    'hide'=>1

                                );

                                count($events)==0?'<li>'.$this->load->view('alert',$data).'</li>':''; ?>

                                <?php foreach($events as $ev): ?>

                                    <li>
                                        <h6><?php echo ucfirst($ev->title); ?></h6>
                                    </li>
                                <?php  endforeach; ?>

                            </ul>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- /directory -->

