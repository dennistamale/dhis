<?php if(isset($this->session->id)){ ?>


    <?php



    $user_id=isset($user_id)?$user_id:$this->session->id;



    $this->db->select('a.*,c.country')->from('arrival_departure a');
    $this->db->join('country c','c.a2_iso=a.country_mission','left');
    $this->db->where(array('a.dependant_id'=>$user_id));
    $t=$this->db->get()->result();

    ?>

    <?php

    $status_list = array(
        array("info" => "Submitted"),
        array("warning" => "Printing"),
        array("default" => "Issued"),
        array("success" => "Picked"),
        array("danger" => "Error")
    );

    ?>


    <!-- BEGIN PAGE CONTENT-->
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="panel panel-flat">
                <div class="panel-heading hidden">
                    <div class="panel-title">


                        <div class="caption font-dark hidden-print">


                            <?php echo form_open('','class="form-inline" ')   ?>
                            <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New user','class="btn btn-sm btn-primary"'); ?>


                            <?php echo form_close(); ?>

                        </div>
                        <div class="tools"> </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table   <?= $this->session->user_type==7?'datatable-button-init-basic':'table-lg'; ?>" id="sample_1">

                        <!--                    Date[of application], Request ID, Type[of application], Applicant[fullname], Passport No, Status[Submitted | Vetting | Issued | Picked], Actions [View | Edit | Delete | Vet | Return | Pick]-->

                        <thead>
                        <tr>
                            <th width="2">#</th>
                            <th> Date </th>

                            <th>  Dependant </th>
                            <th>  Designation </th>
                            <th> DIN# </th>
                            <th> Country </th>
<!--                            <th style="width: 8px;">Status</th>-->
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php


                        $no=1;
                        foreach($t as $app): ?>
                            <tr>
                                <td><?= $no; ?></td>


                                <td> <?= trending_date_time($app->created_on) ?> </td>

                                <td>

                                    <?= anchor($this->page_level.'users/edit/'.$app->user_id*date('Y'),$app->first_name.' '.$app->last_name) ?>



                                </td>

                                <td> <?= $app->designation ?> </td>

                                <!--                            <th>  Applicant </th>-->
                                <td>  <?= $app->request_code ?> </td>

                                <td>


                                    <?= $app->country ?>

                                    <?php
//
//                                    if($app->status=='submitted'){
//                                        $key=0;
//                                    }elseif($app->status=='printing'){
//                                        $key=1;
//                                    }elseif($app->status=='issued'){
//                                        $key=2;
//                                    }elseif($app->status=='picked'){
//
//                                        $key=3;
//                                    }else{
//                                        $key=4;
//                                    }
//                                    $status = $status_list[$key];
                                    ?>



<!--                                    <span class="label label---><?//= (key($status)) ?><!--">--><?//=(current($status)) ?><!--</span>-->


                                </td>
                                <td style="width: 80px;">


                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <?php echo anchor($this->page_level.'arrival_departure/view/'.$app->id*date('Y'),'  <i class=" icon-eye"></i> View') ?>
                                                </li>
<!--                                                <li>-->
<!--                                                    --><?php //echo anchor('pdf_export/'.$app->request_page.'/'.str_rot13($app->request_code),'  <i class="icon-file-pdf"></i> Download Application','target="_blank"') ?>
<!--                                                </li>-->
<!--                                                <li --><?//= $this->session->id==7?'hidden':''  ?><!-->
<!--                                                    --><?php //echo anchor($this->page_level.'requests/edit/'.$app->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    --><?php //echo anchor($this->page_level.'arrival_departure/add_dependant/'.$app->id*date('Y'),'  <i class="icon-user-plus"></i> Add Dependant') ?>
<!--                                                </li>-->

                                            </ul>
                                        </li>
                                    </ul>




                                </td>
                            </tr>
                            <?php $no++; endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->



<? } ?>