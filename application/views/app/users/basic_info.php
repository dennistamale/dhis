
<div class="panel panel-flat">
    <div class="panel-heading ">
        <h5 class="panel-title">Account Info </h5>
        <div class="heading-elements">
            <ul class="icons-list">

                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">


<?php echo form_open($this->page_level.$this->page_level2.'edit/'.$prof->id*date('Y')) ?>
<div class="form-group">
    <label class="control-label">Username </label>
    <input type="text" readonly placeholder="<?php echo $prof->username; ?>" class="form-control"/>
</div>
<div class="form-group">
    <label class="control-label">First Name</label><?php echo form_error('first_name','<label style="color:red;">','</label>') ?>
    <input type="text" name="first_name" placeholder=" <?php echo ucwords($prof->first_name); ?>" value="<?php echo ucwords($prof->first_name); ?>" class="form-control"/>
</div>
<div class="form-group">
    <label class="control-label">Last Name</label><?php echo form_error('last_name','<label style="color:red;">','</label>') ?>
    <input type="text" name="last_name" placeholder=" <?php echo ucwords($prof->last_name); ?>" value="<?php echo ucwords($prof->last_name); ?>" class="form-control"/>
</div>
<div class="form-group">
    <label class="control-label">Email </label><?php echo form_error('email','<label style="color:red;">','</label>') ?>
    <input type="email" name="email" placeholder="Add your email" value="<?php echo $prof->email ?>" class="form-control"/>
</div>
<div class="form-group">
    <label class="control-label">Phone </label> <?php echo form_error('phone','<label style="color:red;">','</label>') ?>
    <input type="text" name="phone" placeholder="Add Your Mobile Phone" value="<?php echo $prof->phone; ?>" class="form-control"/>
</div>

<!--                                This is the for the branch Select-->

<div class="form-group">
    <label class="control-label">Role </label> <?php echo form_error('role','<label style="color:red;">','</label>') ?>
    <div class="form-group">
        <?php $rol=$this->db->select('title')->from('user_type')->where('id',$prof->user_type)->get()->row(); ?>
        <select class="form-control" name="role">
            <option value="<?php echo $prof->user_type ?>" <?php echo set_select('role', $prof->user_type, TRUE); ?> ><?php echo  $rol->title ?></option>
            <?php foreach($this->db->select('id,title')->from('user_type')->order_by('id','asc')->get()->result() as $role): ?>
                <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id); ?> ><?php echo $role->title ?></option>
            <?php  endforeach; ?>
        </select>
    </div>
</div>



<div class="margiv-top-10">
    <hr/>
    <button  class="btn btn-success" type="submit"> Update Changes </button>
    <button type="reset" class="btn default">
        Cancel </button>
</div>

<?php echo form_close(); ?>

    </div>
</div>

