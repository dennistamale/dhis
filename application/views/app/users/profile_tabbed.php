<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/user_profile_tabbed.js"></script>


<?php $data['prof']=$prof=$this->db->select()->from('users')->where('id',$id)->get()->row(); ?>


<!-- Detached sidebar -->
<div class="sidebar-detached">
    <div class="sidebar sidebar-default sidebar-separate">
        <div class="sidebar-content">

            <!-- User details -->
            <div class="content-group">
                <div class="panel-body bg-indigo-400 border-radius-top text-center" style="background-image: url(<?= strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>); background-size: contain;">
                    <div class="content-group-sm">
                        <h6 class="text-semibold no-margin-bottom">
                            <?= ucwords($prof->first_name.' '.$prof->last_name); ?>
                        </h6>

                        <span class="display-block">


                            <?php


                            $user_type=$this->db->select('title')->from('user_type')->where(array('id'=>$prof->user_type))->get()->row();

                            echo !empty($user_type)?$user_type->title:'';

                            ?>

                        </span>
                    </div>

                    <a href="#" class="display-inline-block content-group-sm">
                        <img src="<?= strlen($prof->photo)>0?base_url().$prof->photo:base_url().'assets/profile_placeholder.png' ?>" class="img-circle img-responsive" alt="" style="width: 110px; height: 110px;">
                    </a>

                    <ul class="list-inline list-inline-condensed no-margin-bottom">
                        <li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-google-drive"></i></a></li>
                        <li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-twitter"></i></a></li>
                        <li><a href="#" class="btn bg-indigo btn-rounded btn-icon"><i class="icon-github"></i></a></li>
                    </ul>
                </div>

                <div class="panel no-border-top no-border-radius-top">
                    <ul class="navigation">

                        <li class="active <?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>">
                            <a href="#basic_info" data-toggle="tab"> <i class="icon-files-empty"></i>  Account Info</a>
                        </li>
                        <?php if($prof->user_type==7){ ?>


                            <li class="<?php echo $subtitle=='diplomat'?'active':''; ?>">
                                <a href="#diplomat" data-toggle="tab"> <i class="icon-files-empty"></i>  Diplomat Profile</a>
                            </li>

                            <li class="<?php echo $subtitle=='applications'?'active':''; ?>">
                                <a href="#applications" data-toggle="tab"> <i class="icon-files-empty"></i>  Applications</a>
                            </li>
                            <li class="<?php echo $subtitle=='dependants'?'active':''; ?>">
                                <a href="#dependants" data-toggle="tab"> <i class="icon-files-empty"></i>  Dependants</a>
                            </li>

                        <?php } ?>


                        <li class="navigation-header">Others</li>

                        <li class="<?php echo $subtitle=='change_avatar'?'active':''; ?>">
                            <a href="#change_avatar" data-toggle="tab"> <i class="icon-files-empty"></i>  Change Avatar</a>
                        </li>
                        <li <?php echo $subtitle=='change_password'?'active':''; ?>>
                            <a href="#change_password" data-toggle="tab"> <i class="icon-files-empty"></i>  Change Password</a>
                        </li>



                        <li>
                        <?= anchor($this->page_level.'logout','<i class="icon-switch2"></i> Log out') ?>
                        </li>

                    </ul>
                </div>
            </div>
            <!-- /user details -->



        </div>
    </div>
</div>
<!-- /detached sidebar -->


<!-- Detached content -->
<div class="container-detached">
    <div class="content-detached">

        <!-- Tab content -->
        <div class="tab-content">

            <div class="tab-pane fade in active <?php echo $subtitle=='edit'||$subtitle=='info'?'active':''; ?>" id="basic_info">

                <?php $this->load->view('app/users/basic_info',$data); ?>

            </div>

            <!-- END PERSONAL INFO TAB -->

            <?php if($prof->user_type==7){ ?>


                <div class="tab-pane fade <?php echo $subtitle=='diplomat'?'active':''; ?>" id="diplomat">
                    <?php $this->load->view('app/users/diplomat',$data); ?>
                </div>

                <div class="tab-pane fade <?php echo $subtitle=='applications'?'active':''; ?>" id="applications">
                    <?php $this->load->view('app/users/applications',$data); ?>
                </div>

                <div class="tab-pane fade <?php echo $subtitle=='dependants'?'active':''; ?>" id="dependants">
                    <?php $this->load->view('app/users/dependants',$data); ?>
                </div>
            <?php } ?>



            <!-- END CHANGE AVATAR TAB --> <!-- CHANGE AVATAR TAB -->
            <div class="tab-pane fade <?php echo $subtitle=='change_avatar'?'active':''; ?>" id="change_avatar">
                <?php $this->load->view('app/users/change_avatar',$data); ?>
            </div>
            <!-- END CHANGE AVATAR TAB -->
            <!-- CHANGE PASSWORD TAB -->
            <div class="tab-pane fade <?php echo $subtitle=='change_password'?'active':''; ?>" id="change_password">

                <?php $this->load->view('app/users/change_password',$data); ?>

            </div>
            <!-- END CHANGE PASSWORD TAB -->





        </div>
        <!-- /tab content -->

    </div>
</div>
<!-- /detached content -->