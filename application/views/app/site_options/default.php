<?php  $them_color=$this->site_options->title('site_color_code'); ?>

<style>

/* ================================= COLOR STYLE SHEET ====================================== */
a,
a:hover, a:focus, a.active,
.typo-dark h1 a:hover, .typo-dark h1 a:focus, .typo-dark h1 a.active,
.typo-dark h2 a:hover, .typo-dark h2 a:focus, .typo-dark h2 a.active,
.typo-dark h3 a:hover, .typo-dark h3 a:focus, .typo-dark h3 a.active, 
.typo-dark h4 a:hover, .typo-dark h4 a:focus, .typo-dark h4 a.active, 
.typo-dark h5 a:hover, .typo-dark h5 a:focus, .typo-dark h5 a.active, 
.typo-dark h6 a:hover, .typo-dark h6 a:focus, .typo-dark h6 a.active,

.typo-light h1 a:hover, .typo-light h1 a:focus, .typo-light h1 a.active,
.typo-light h2 a:hover, .typo-light h2 a:focus, .typo-light h2 a.active,
.typo-light h3 a:hover, .typo-light h3 a:focus, .typo-light h3 a.active, 
.typo-light h4 a:hover, .typo-light h4 a:focus, .typo-light h4 a.active, 
.typo-light h5 a:hover, .typo-light h5 a:focus, .typo-light h5 a.active, 
.typo-light h6 a:hover, .typo-light h6 a:focus, .typo-light h6 a.active
{
	color: <?php echo $them_color; ?>;
}
.btn,.btn.dark:hover{
	background-color: <?php echo $them_color; ?>;
}





/* Color */
.color{
	color: <?php echo $them_color; ?>;
}

/* Background Color */
.bg-color{
	background-color: <?php echo $them_color; ?> !important;
}
/* Bg Gradient */
.bg-gradient{
	background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(68, 138, 255, 0.8) 0%, rgba(35, 35, 35, 0.8) 100%) repeat scroll 0 0;
}
/* Overlay */
.overlay.bg-color:after{
	background-color: <?php echo $them_color; ?>;
}
/* Background Video */
.video-controls a{
	background-color: <?php echo $them_color; ?>;
}
/* Title */
.title-container.color *,
.title-container .dot-separator.color{
	color: <?php echo $them_color; ?>;
}

.title-container.color .title-wrap .separator.line-separator,
.title-container.color .title-wrap .separator.line-separator:after{
	background-color: <?php echo $them_color; ?>;
}
.title-bg-line > .title a{
   background-color: <?php echo $them_color; ?>;
}
.title-bg-line{
	border-color: <?php echo $them_color; ?>;
}
/*==================== 
	Bootstrap 
====================== */
/* Accordion */
.panel-group .panel{
	border-left-color: <?php echo $them_color; ?>;
}
/* Tab */
.nav-tabs > li > a:hover,.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
	color: <?php echo $them_color; ?>;
	border-bottom-color: <?php echo $them_color; ?>;
}
/* Progress */
.progress-bar{
	background-color: <?php echo $them_color; ?>;
}
/* List Types */
.list-icon li:hover:before{
	color: <?php echo $them_color; ?>;
}
/* Pagination */
.pagination > li > a.active, .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover,
.pager li > a:focus, .pager li > a:hover{
	background-color: <?php echo $them_color; ?>;
}

/* Back to top */
.cd-top:hover, .cd-top:focus {
	background-color: <?php echo $them_color; ?>;
}

/* Tooltip */
.tooltip-color .tooltip-inner{
	background-color: <?php echo $them_color; ?>;
}
.tooltip-color .tooltip.top .tooltip-arrow{
	border-top-color:<?php echo $them_color; ?>;
}

.tooltip-color .tooltip.bottom .tooltip-arrow{
	border-bottom-color:<?php echo $them_color; ?>;
}

.tooltip-color .tooltip.left .tooltip-arrow{
	border-left-color:<?php echo $them_color; ?>;
}

.tooltip-color .tooltip.right .tooltip-arrow{
	border-right-color:<?php echo $them_color; ?>;
}

/* Content box */
.content-box i{
	color:<?php echo $them_color; ?>;
}
.content-box.icon-box .icon-wrap i{
	background-color: <?php echo $them_color; ?>;
}
/* Cat */
.cat{
	background-color: <?php echo $them_color; ?>;
}



/* Team */
.color-border .member-wrap{
	border-color: <?php echo $them_color; ?>;
}
.team-list .member-detail-wrap .position {
	color: <?php echo $them_color; ?>;
}

/*==================== 
	Course 
====================== */
.course-banner-wrap > h6 {
	background-color: <?php echo $them_color; ?>;
}
.course-wrapper h5 span{
	color: <?php echo $them_color; ?>;
}

/*==================== 
	Timeline 
====================== */
.universh-timeline-img{
	background-color: <?php echo $them_color; ?>;
}
/*==================== 
	Events 
====================== */
.events-meta li i, .event-widget p i{
	color: <?php echo $them_color; ?>;
}
/* Testimonial */
.quote-footer span{
	color: <?php echo $them_color; ?>;
}
.quote-wrap.color > blockquote{
	background-color: <?php echo $them_color; ?>;
}
/* Page Header */
.page-header .breadcrumb li, .page-header .breadcrumb li a:hover, .page-header .breadcrumb li a:focus{
	color: <?php echo $them_color; ?>;
}
.page-header .title::after,
.page-header .title::before{
	background-color: <?php echo $them_color; ?>;
}
/* Related Block */
.related-content a{
	background-color: <?php echo $them_color; ?>;
}
/*==================== 
	Widgets 
====================== */
.widget-title span{
	background-color: #e77817;
}
.widget li a:hover, .widget li a:focus, .widget li a.active,
.widgets-dark.typo-light .widget li a:hover, .widgets-dark.typo-light .widget li a:focus, .widgets-dark.typo-light .widget li a.active{
	color: #e77817;
}
/* Tag Widget */
.tag-widget li a:hover, .tag-widget li a:focus, .tag-widget li a.active{
	background-color: <?php echo $them_color; ?>;
	border-color: <?php echo $them_color; ?>;
}
.widgets-dark.typo-light .tag-widget li a:hover, .widgets-dark.typo-light .tag-widget li a:focus, .widgets-dark.typo-light .tag-widget li a.active{
	color: #fff;
}
/* Go widget */
.go-widget li a:hover .badge, .accordion-widget .panel-default > .panel-heading a:hover .badge{
	background-color: <?php echo $them_color; ?>;
}
.widgets-dark.typo-light .widget .accordion-widget .panel-default > .panel-heading:hover .badge{
	background-color: <?php echo $them_color; ?>;
}
.widgets-dark.typo-light .widget.no-box .accordion-widget .panel-default > .panel-heading:hover .badge{
	background-color: <?php echo $them_color; ?>;
}
/* Thumbanil Widget */
.thumbnail-widget span.color{
	color: #e77817;
}
/*==================== 
	Owl Carousel
====================== */
.owl-theme .owl-nav > div:hover {
	background-color:<?php echo $them_color; ?>;
}
.owl-theme .owl-dots .owl-dot:hover span, .owl-theme .owl-dots .owl-dot.active span{
	background-color:<?php echo $them_color; ?>;
}


/* Gallery */
.accordion.gallery-accordion .panel-title a.collapsed{
	background:rgba(33, 150, 243, 0.9);
}
.isotope-filters .nav-pills li a.active, .isotope-filters .nav-pills li a:hover, .isotope-filters .nav-pills li a:focus{
	background-color:<?php echo $them_color; ?>;
}
/* News */
.news-cat{
	background-color:<?php echo $them_color; ?>;
}

/* Shop */
.product-label, .product-details .option-btn, .shop-meta li a:hover, .shop-meta li a:focus{
	background-color:<?php echo $them_color; ?>;
}
.liked-members a:hover,.liked-members a:focus{
	background-color:<?php echo $them_color; ?>;
}


/* Footer */
.footer-copyright nav ul li a:hover, .footer-copyright nav ul li a:focus{
	color: #e77817;
}

/*==================== 
	Header  color
====================== */
#header nav ul.nav-main li.dropdown.open .dropdown-toggle,
#header nav ul.nav-main li.active a,
#header nav ul.nav-main li > a:hover,
#header nav ul.nav-main li.dropdown:hover a,
#header.flat-menu nav ul.nav-main li.active > a {
	background-color: <?php echo $them_color; ?>;
}

#header nav ul.nav-main ul.dropdown-menu {
	border-color: <?php echo $them_color; ?>;
}

#header nav ul.nav-main li.dropdown:hover ul.dropdown-menu li > a:hover {
	background: <?php echo $them_color; ?> ;
}
#header .tip{
	background: <?php echo $them_color; ?>;
}
#header .tip:before{
	border-right-color: <?php echo $them_color; ?>;
}
@media (min-width: 992px) {
	#header.single-menu nav ul.nav-main li.active > a {
		border-top: 5px solid <?php echo $them_color; ?>;
	}

	#header.darken-top-border {
		border-color: <?php echo $them_color; ?>;
	}

	#header.colored .header-top {
		background-color: <?php echo $them_color; ?>;
	}

	#header.colored .header-top ul.nav-top li a:hover {
		background-color: <?php echo $them_color; ?>;
	}
	
	/*#header.flat-menu nav ul.nav-main li.active > a {
		color: <?php echo $them_color; ?>;
	}*/

	#header.flat-menu nav ul.nav-main li > a:hover,
	#header.flat-menu nav ul.nav-main li.dropdown:hover a {
		background-color: <?php echo $them_color; ?>;
	}

	#header nav.mega-menu ul.nav-main li.mega-menu-item ul.dropdown-menu {
		border-top-color: <?php echo $them_color; ?>;
	}
}




</style>