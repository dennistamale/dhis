<script>
    var ComponentsNoUiSliders = function() {

        var demo7 = function() {
            var softSlider = document.getElementById('demo7');

            noUiSlider.create(softSlider, {
                start: 50,
                range: {
                    min: 0,
                    max: 100
                },
                pips: {
                    mode: 'values',
                    values: [0,20,40,60, 80,100],
                    density: 4
                }
            });

            softSlider.noUiSlider.on('change', function ( values, handle ) {
//                if ( values[handle] < 20 ) {
//                    softSlider.noUiSlider.set(20);
//                } else if ( values[handle] > 80 ) {
//                    softSlider.noUiSlider.set(80);
//                }

                $("input[name=status]").val(values[handle]);
              //      alert(values[handle]);
            });
        }

        return {
            //main function to initiate the module
            init: function() {

                demo7();

            }

        };

    }();

    jQuery(document).ready(function() {
        ComponentsNoUiSliders.init();
    });
</script>