
<?php $pub=$this->db->select()->from('publications')->where('id',$id)->get()->row(); ?>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase">  Recommendation </span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>


            <div class="blog-comments">
                <h3 class="sbold blog-comments-title"> <?php echo ucfirst($pub->title) ?></h3>
                <?php echo form_open_multipart() ?>
                                        <div class="form-group">
                                            <input type="text" name="name" value="<?php echo set_value('name') ?>" placeholder="Name" class="form-control c-square">
                                            <?php echo form_error('name','<label class="text-danger">','</label>'); ?>
                                        </div>
<!--                                        <div class="form-group">-->
<!--                                            <input type="text" placeholder="Your Email" class="form-control c-square"> </div>-->
<!--                                        <div class="form-group">-->
<!--                                            <input type="text" placeholder="Your Website" class="form-control c-square"> </div>-->
                <div class="form-group">
                    <?php echo form_error('comment','<label class="text-danger">','</label>'); ?>
                    <textarea rows="5" name="comment" value=" <?php echo set_value('comment') ?>"  placeholder="Write Description here ..." class="form-control c-square "></textarea>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                    <button type="submit" name="save" value="save" class="btn blue uppercase btn-md  sbold" style="width: 49%;"><i class="fa fa-save"></i> Submit</button>
                    <button type="submit" name="save" value="save_and_new" class="btn blue uppercase btn-md  pull-right sbold" style="width: 49%;"><i class="fa fa-refresh"></i> Save and add New</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
                <div class="media">
                    <div class="media-body">
                        <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                    </div>
                </div>

<!---->
<!---->
<!--                    --><?php //$comments= $this->db->select('a.*,b.first_name,b.last_name,b.photo')->from('pub_evaluation_comments a')->join('users b','a.created_by=b.id')->where(array('publication_id'=>$id))->get()->result(); ?>
<!--                    <h3 class="sbold blog-comments-title">Comments(--><?php //echo count($comments); ?><!--)</h3>-->
<!--                    <div class="c-comment-list">-->
<!--                        --><?php //if(count($comments)==0){ ?>
<!--                            <div class="media">-->
<!--                                <div class="media-body ">-->
<!---->
<!--                                    --><?php
//
//                                    $data=array('alert'=>'info',
//                                        'message'=>'No recommendation yet',
//                                        'hide'=>0
//                                    );
//
//                                    $this->load->view('alert',$data) ?>
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!--                        --><?php //} ?>
<!---->
<!---->
<!--                        --><?php //foreach($comments as $com): ?>
<!--                            <div class="media">-->
<!---->
<!---->
<!--                                <div class="media-body ">-->
<!--                                    <h4 class="media-heading">-->
<!--                                        <a href="#" style="font-weight:bold;">--><?php //echo ucfirst($com->name); ?><!--</a>-->
<!---->
<!--                                        <span class="c-date" style="font-size: smaller;"><i class="fa fa-user"></i> --><?php //echo humanize($com->first_name.' '.$com->last_name) ?><!-- on --><?php //echo  trending_date($com->created_on) ?><!--</span>-->
<!--                                    </h4>-->
<!--                                    --><?php
//
//                                    echo ucfirst($com->comment);
//
//                                    ?>
<!---->
<!--                                    --><?php
//
//                                   $kpi=$this->db->from('recommendation_kpi')->where('recommendation_id',$com->id)->count_all_results();
//
//                                    echo anchor($this->page_level.$this->page_level2.'add_kpi/'.$com->id*date('Y'),'<i class="fa fa-plus"></i> Add KPI ('. $kpi.')','class="pull-right btn btn-sm green"') ?>
<!---->
<!---->
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!--                        --><?php //endforeach; ?>
<!---->
<!---->
<!---->
<!--                    </div>-->


            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>
