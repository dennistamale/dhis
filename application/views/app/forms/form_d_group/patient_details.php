<h6 class="form-wizard-title text-semibold">
    <span class="form-wizard-count">1</span>
    Personal info
<!--    <small class="display-block">Tell us a bit about yourself</small>-->
</h6>




<div class="form-horizontal">

    <?php $this->load->view('ajax/location_filter_js') ?>


    <div class="form-group">
        <legend class="text-bold">Form Info</legend>

        <label class="control-label col-lg-1"><span class="pull-right">Diagnosis  </span> </label>
        <div class="col-lg-2">
            <select name="diagnosis" class="form-control select" required  <?= $subtitle=='view'?'disabled':''; ?> >

                <option value="" selected>Diagnosis</option>
                <option value="malaria" <?= set_select('diagnosis','malaria') ?>>Malaria</option>
                <option value="pneumonia" <?= set_select('diagnosis','pneumonia') ?>>Pneumonia </option>
                <option value="diarrhoea" <?= set_select('diagnosis','diarrhoea') ?>>Diarrhoea </option>

            </select>
            <?php echo form_error('diagnosis','<span class="text-danger">','</span>') ?>
        </div>


        <label class="control-label col-lg-2"><span class="pull-right">Respondent Name  </span> </label>

        <div class="col-lg-3">

            <input class="form-control" name="respondent_name" value="<?= set_value('respondent_name') ?>">


            <?php echo form_error('respondent_name','<span class="text-danger">','</span>') ?>
        </div>

        <label class="control-label col-lg-2"><span class="pull-right">Designation  </span> </label>
        <div class="col-lg-2">

            <input class="form-control" name="designation" value="<?= set_value('designation') ?>">
            <?php echo form_error('designation','<span class="text-danger">','</span>') ?>
        </div>

    </div>



</div>

