<h6 class="form-wizard-title text-semibold">
    <span class="form-wizard-count">2</span>
    Data for Completeness
<!--    <small class="display-block">Tell us a bit about yourself</small>-->
</h6>


<div class="col-md-5">

<div class="form-horizontal border-right">

    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Serial No  </span> </label>
        <div class="col-lg-7">
            <label class="radio-inline">
                <input type="radio" name="serial_no" class="styled" value="yes"  <?= set_checkbox('serial_no', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="serial_no" class="styled" value="no"  <?= set_checkbox('serial_no', 'no'); ?>>
                No
            </label>
        </div>

    </div>
    <div class="form-group">


        <label class="control-label col-lg-4"><span class="pull-right">Name of Patient  </span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="name_of_patient" class="styled" value="yes"  <?= set_checkbox('name_of_patient', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">

                <input type="radio" name="name_of_patient" class="styled" value="no"  <?= set_checkbox('name_of_patient', 'no'); ?>>
                No
            </label>
        </div>

    </div>
    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Resident / Village </span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="residence" class="styled" value= "yes"  <?= set_checkbox('residence', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="residence" class="styled" value="no"   <?= set_checkbox('residence', 'no'); ?>>
                No
            </label>
        </div>

    </div>
    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Age </span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="age" class="styled" value="yes"   <?= set_checkbox('age', 'Yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="age" class="styled" value="no"   <?= set_checkbox('age', 'no'); ?>>
                No
            </label>
        </div>

    </div>

    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Sex </span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="sex" class="styled"  <?= set_checkbox('sex', 'Yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="sex" class="styled"  <?= set_checkbox('sex', 'no'); ?>>
                No
            </label>
        </div>


    </div>


    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Next of Kin </span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="next_of_kin" class="styled" value="yes"  <?= set_checkbox('next_of_kin', 'Yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="next_of_kin" class="styled" value="no"   <?= set_checkbox('next_of_kin', 'no'); ?>>
                No
            </label>
        </div>


    </div>



    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Weight</span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="weight" class="styled" value="yes"  <?= set_checkbox('weight', 'Yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="weight" class="styled" value="no"   <?= set_checkbox('weight', 'no'); ?>>
                No
            </label>
        </div>


    </div>



    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Fever</span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="fever" class="styled" value="yes"  <?= set_checkbox('fever', 'Yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="fever" class="styled" value="no"  <?= set_checkbox('fever', 'no'); ?>>
                No
            </label>
        </div>


    </div>



    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Fever</span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="fever" class="styled" value="yes"  <?= set_checkbox('fever', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="fever" class="styled" value="no"   <?= set_checkbox('fever', 'no'); ?>>
                No
            </label>
        </div>


    </div>



    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">TESTS DONE
                <span style="font-size: smaller;">(BS or RDT)</span></span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="tests_done" class="styled" value="yes" <?= set_checkbox('tests_done', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="tests_done" class="styled" value="no"  <?= set_checkbox('tests_done', 'no'); ?>>
                No
            </label>

            <label class="radio-inline">
                <input type="radio" name="tests_done" class="styled" value="na"  <?= set_checkbox('tests_done', 'na'); ?>>
                N/A
            </label>
        </div>


    </div>




    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">RESULTS
                <small>(POS/NEG/NA)</small></span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="tests_done" class="styled" value="yes" <?= set_checkbox('tests_done', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="tests_done" class="styled" value="no"  <?= set_checkbox('tests_done', 'no'); ?>>
                No
            </label>

            <label class="radio-inline">
                <input type="radio" name="tests_done" class="styled" value="na"  <?= set_checkbox('tests_done', 'na'); ?>>
                N/A
            </label>
        </div>


    </div>




    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Diagnosis</span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="diagnosis" class="styled" value="yes" <?= set_checkbox('diagnosis', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="diagnosis" class="styled" value="no"  <?= set_checkbox('diagnosis', 'no'); ?>>
                No
            </label>


        </div>


    </div>



    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Referred-In</span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="referred_in" class="styled" value="yes" <?= set_checkbox('referred_in', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="referred_in" class="styled" value="no"  <?= set_checkbox('referred_in', 'no'); ?>>
                No
            </label>


        </div>


    </div>



    <div class="form-group">

        <label class="control-label col-lg-4"><span class="pull-right">Referred-Out</span> </label>
        <div class="col-lg-8">
            <label class="radio-inline">
                <input type="radio" name="referred_out" class="styled" value="yes" <?= set_checkbox('referred_out', 'yes'); ?>>
               Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="referred_out" class="styled" value="no"  <?= set_checkbox('referred_out', 'no'); ?>>
                No
            </label>


        </div>


    </div>



</div>

</div>

<div class="col-md-7">

    <div class="form-horizonta">
        <?php $this->load->view('app/forms/form_d_group/hmis_accuracy'); ?>
    </div>

</div>