
<!-- Theme JS files -->

<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/wizards/form_wizard/form.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/forms/wizards/form_wizard/form_wizard.min.js"></script>



<script type="text/javascript" src="<?= base_url() ?>assets/js/pages/wizard_form.js"></script>


<!-- Submit form with AJAX -->
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?= humanize($subtitle) ?> Review for Patient Records</h6>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <form class="form-ajax" action="<?= base_url('ajax_api/receive_form')?>" method="post">


        <fieldset class="step" id="ajax-step1">
            <?php $this->load->view('app/forms/form_d_group/patient_details'); ?>
        </fieldset>


        <fieldset class="step" id="ajax-step2">
            <?php $this->load->view('app/forms/form_d_group/data_for_completeness'); ?>
        </fieldset>




        <div class="form-wizard-actions">
            <button class="btn btn-default" id="ajax-back" type="reset">Back</button>
            <button class="btn btn-info" id="ajax-next" type="submit">Next</button>
        </div>

    </form>

    <div id="ajax-data"></div>
</div>
<!-- /submit form with AJAX -->
