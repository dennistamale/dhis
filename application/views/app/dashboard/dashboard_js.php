<script src="<?= base_url() ?>assets/highcharts/highcharts.js"></script>
<!--<script src="https://code.highcharts.com/highcharts-3d.js"></script>-->
<script src="<?php echo base_url() ?>assets/highcharts/highcharts-3d.js"></script>
<script src="<?= base_url() ?>assets/highcharts/exporting.js"></script>
<!--<script src="--><? //= base_url() ?><!--assets/highcharts/themes/dark-green.js"></script>-->

<?php

$status_list = array(
    array("#52c036" => "Pending"),
    array("#3c63e2" => "Closed"),
    array("#7c32b5" => "On Hold"),
    array("#6bffe2" => "Fraud"),
    array("#EC407A" => "Fraud"),
    array("#43A047" => "Fraud"),
    array("#7CB342" => "Fraud"),
    array("#C0CA33" => "Fraud"),
    array("#512DA8" => "Fraud"),
    array("#303F9F" => "Fraud"),
    array("#1976D2" => "Fraud"),
);

?>


<script type="text/javascript">

    $(document).ready(function () {


        <?php

        if($title == 'dashboard' || $subtitle == 'weekly_summaries'){

        $indicators = $this->dhis2_model->indicators('weekly');

        $no = 0;
        foreach ($indicators as $wd){

        $status = $status_list[intval($no)];

        $no++;

        $period_array = $this->dashboard_model->period($wd->indicator, $wd->name, 'weekly');


        $period_items = json_encode($period_array->period);
        $data = json_encode($period_array->data);
        $last_value = end($period_array->data);
        $last_week = end($period_array->period);
        $name = $period_array->name;


        ?>

        $('#last_value_<?= $wd->indicator ?>').html('<?= $last_value ?>');
        $('#week_<?= $wd->indicator ?>').html('<?= $last_week ?>');


        $('#<?= $wd->indicator ?>').highcharts({

            colors: ["<?= key($status) ?>"],
            chart: {
                zoomType: 'x',
                type: 'area'
            },

            title: {
                text: '  <?= $wd->name ?> Reports',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: <?= $period_items ?>

            },
            yAxis: {
                title: {
                    text: 'Cases'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '<?= key($status) ?>'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 50,
                floating: true,
                borderWidth: 0
            },
            series: [
                {
                    name: '<?= $name ?>',
                    data: <?= $data ?>
                }
            ]
        });

        <?php  }  ?>


        <?php }elseif ($subtitle == 'monthly_summaries'){

        $indicators = $this->dhis2_model->indicators('monthly');

        $no = 0;
        foreach ($indicators as $wd){

        $status = $status_list[intval($no)];

        $no++;

        $period_array = $this->dashboard_model->period($wd->indicator, $wd->name, 'monthly');


        $period_items = json_encode($period_array->period);
        $data = json_encode($period_array->data);
        $last_value = end($period_array->data);
        $last_value_name = end($period_array->period);
        $name = $period_array->name;


        ?>

        $('#month_last_value_<?= $wd->indicator ?>').html('<?= $last_value ?>');
        $('#month_name_<?= $wd->indicator ?>').html('<?= $last_value_name ?>');


        $('#month_<?= $wd->indicator ?>').highcharts({

            colors: ["<?= key($status) ?>"],
            chart: {
                zoomType: 'x',
                type: 'area'
            },

            title: {
                text: '  <?= $wd->name ?> Reports',
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20
            },
            xAxis: {
                categories: <?= $period_items ?>

            },
            yAxis: {
                title: {
                    text: 'Cases'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '<?= key($status) ?>'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 50,
                floating: true,
                borderWidth: 0
            },
            series: [
                {
                    name: '<?= $name ?>',
                    data: <?= $data ?>
                }
            ]
        });

        <?php  }

        }
        ?>

    });
</script>


<!--<script src="--><?php //echo base_url() ?><!--assets/horizontal_bar/build/js/jquery.min.js"></script>-->
<script src="<?= base_url() ?>assets/horizontal_bar/build/js/jquery.horizBarChart.min.js"></script>

<script>
    $(document).ready(function () {
        $('.chart').horizBarChart({
            selector: '.bar',
            speed: 1000
        });
    });
</script>

