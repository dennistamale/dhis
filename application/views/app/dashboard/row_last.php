
<?php

$apps = array(
    array("info" => "Submitted"),
    array("warning" => "Printing"),
    array("default" => "Issued"),
    array("success" => "Picked"),
    array("danger" => "Error")
);

?>
<div class="row">
    <div  class="hidden col-lg-5">
        <!-- Scrollable table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Latest Applications</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>


            <div class="table-responsive pre-scrollabl">
                <table class="table  text-nowrap">
                    <thead>
                    <tr>

                        <th> Date </th>
                        <th> Request ID </th>
                        <th>  Type </th>
                        <th style="width: 8px;">Status</th>

                    </tr>
                    </thead>
                    <tbody>


                    <?php


                    $no=1;
                    if($this->dashboard_model->latest_approvals()!=false){

                    foreach($this->dashboard_model->latest_applications() as $app): ?>
                        <tr style="margin-bottom: 1px;">


                            <td> <?= trending_date($app->created_on) ?> </td>

                            <td>
                                <?= anchor($this->page_level.'requests/view/'.$app->id*date('Y'),$app->request_code) ?>
                            </td>

                            <td> <?= humanize($app->title) ?> </td>

                            <td>


                                <?php

                                if($app->status=='submitted'){
                                    $key=0;
                                }elseif($app->status=='printing'){
                                    $key=1;
                                }elseif($app->status=='issued'){
                                    $key=2;
                                }elseif($app->status=='picked'){

                                    $key=3;
                                }else{
                                    $key=4;
                                }
                                $status = $apps[$key];
                                ?>

                                <span  class="label label-<?= (key($status)) ?>"><?=(current($status)) ?></span>

                            </td>

                        </tr>
                        <?php $no++; endforeach; } ?>
                    </tbody>
                </table>
            </div>

            <div class="panel-footer">
                <div class="btn-arrow-link pull-right">
                    <?= anchor($this->page_level.'applications','See All Records <i class="icon-arrow-right16"></i>') ?>

                </div>
            </div>

        </div>
        <!-- /scrollable table -->
    </div>

    <div class="hidden col-lg-4">
        <!-- Scrollable table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Latest Approvals</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>


            <div class="table-responsive pre-scrollabl">
                <table class="table">
                    <thead>
                    <tr>

                        <th>Date</th>
                        <th>Diplomat</th>
                        <th>Designation</th>
                        <th>Reference#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;

                    if($this->dashboard_model->latest_approvals()!=false){

                        foreach ($this->dashboard_model->latest_approvals() as $r): ?>
                            <tr>

                                <td style="white-space: nowrap;"><?= trending_date($r->created_on) ?></td>
                                <td><?= anchor($this->page_level . 'users/edit/' . $r->user_id * date('Y'), humanize($r->first_name)) ?></td>
                                <td><?= $r->designation ?></td>
                                <td><?= anchor($this->page_level . 'arrival_departure/view/' . $r->id * date('Y'), strlen($r->reference_code) > 0 ? $r->reference_code : 'N/A') ?></td>
                            </tr>
                            <?php $no++; endforeach;
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="btn-arrow-link pull-right">
                    <?= anchor($this->page_level.'arrival_departure','See All Records <i class="icon-arrow-right16"></i>') ?>

                </div>
            </div>
        </div>
        <!-- /scrollable table -->
    </div>

    <div class="col-lg-3">
        <!-- Scrollable table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Latest Activities</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive pre-scrollabl" style="height: 349px;">
                <table class="table">
                    <thead>
                    <tr>

<!--                        <th>Type</th>-->
                        <th>Activity</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->dashboard_model->recent_activities() as $tl ): ?>
                    <tr>

<!--                        <td>--><?//= humanize($tl->transaction_type) ?><!--</td>-->
                        <td><a href="#"> <?= humanize($tl->first_name.' '.$tl->last_name) ?></a>  <?php echo $tl->details ?> <?= $tl->target ?>
                        <span class="pull-right text-muted">
                            <?= trending_date($tl->created_on) ?>
                        </span>
                        </td>
                    </tr>
                    <?php $no++; endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <div class="pull-right">
                    <?= anchor($this->page_level.'logs','See All Records <i class="icon-arrow-right16"></i> ') ?>

                </div>
            </div>
        </div>
        <!-- /scrollable table -->
    </div>

</div>
