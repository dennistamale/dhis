
<?php
$fdate;
 $ldate=isset($last_day)?date($last_day):date('Y-m-d');
 ?>


<?php

$status_list = array(
    array("success" => "Pending"),
    array("info" => "Closed"),
    array("danger" => "On Hold"),
    array("warning" => "Fraud"),
    array("primary" => "Fraud"),
    array("pink" => "Fraud"),
    array("violet" => "Fraud")
);

?>

<div class="row">
    <div class="col-md-7">
        <!-- Marketing campaigns -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Indicators from <?php echo date("d F Y",strtotime($fdate))  ?> <?php echo isset($last_day)? ' - '.date("d F Y",strtotime($last_day)):''; ?></h6>

                <div class="heading-elements">

                </div>
            </div>


            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                    <tr>
                        <th>Indicator</th>
                        <th class="col-md-2">People</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($indicators as $in){

                        $status = $status_list[rand(0, 6)];
                        ?>
                    <tr>
                        <td>
                            <div class="media-left media-middle">
                                <a href="#" class="btn border-<?= key($status) ?> text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom">


                                    <?= strtoupper($in->code) ?>


                                </a>
                            </div>


                            <div class="media-left">
                                <div class=""><a href="#" class="text-default text-semibold"><?= $in->indicator ?></a></div>
                                <div class="text-muted text-size-small hidden">
                                    <span class="status-mark border-blue position-left"></span>

                                </div>
                            </div>
                        </td>

                        <?php

                        $value=$this->db->select_sum('indicator_value')->from('facility_report')->where(
                            array(
                                'indicator'=>$in->code,
                                'created_on >='=>strtotime($fdate),
                                'created_on <='=>strtotime($ldate)
                            )
                        )->get()->row();



                        ?>

                        <td><h6 class="text-semibold"><?= number_format($value->indicator_value) ?></h6></td>


                    </tr>

                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /marketing campaigns -->

    </div>

    <div class="col-md-5">

        <!-- Daily sales -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Top Facilities</h6>
                <div class="heading-elements hidden">
                    <span class="heading-text">Balance: <span class="text-bold text-danger-600 position-right">$4,378</span></span>
                    <ul class="icons-list">
                        <li class="dropdown text-muted">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                                <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                                <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>



            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                    <tr>
                        <th>Facility</th>
                        <th>District</th>
                        <th>Registered</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php  $top_facility=$this->db->select('sum(a.indicator_value) as indicator_total,a.facility,b.name,b.state,b.level')->from('facility_report a')
                        ->join('health_facility b','a.facility=b.facility_code','left outer')
                        ->group_by('a.facility')
                        ->order_by('sum(a.indicator_value)','desc')
                        ->limit(10)
                        ->get()->result();



                    ?>

                    <?php foreach ($top_facility as $in){
                        $status = $status_list[rand(0, 6)];

                        ?>

                    <tr>
                        <td>
                            <div class="media-left media-middle">
                                <a href="#" class="btn bg-<?= key($status) ?>-600 btn-rounded btn-icon btn-xs">
                                    <span class="letter-icon"></span>
                                </a>
                            </div>
<!--                            '<span class="label label-'.(key($status)).'">'.(current($status)).'</span>',-->
                            <div class="media-body">
                                <div class="media-heading">
                                    <a href="#" class="letter-icon-title"><?= $in->name ?></a>
                                </div>

                                <div class="text-muted text-size-small"><i class="icon-checkmark3 text-size-mini position-left"></i> <?= $in->level ?></div>
                            </div>
                        </td>
                        <td>
                            <span class="text-muted text-size-small"><?= $in->state ?></span>
                        </td>
                        <td>
                            <h6 class="text-semibold no-margin"><?= number_format($in->indicator_total) ?></h6>
                        </td>
                    </tr>

                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /daily sales -->
    </div>


</div>
