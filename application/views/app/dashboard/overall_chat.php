
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="panel panel-flat">

            <div class="panel-heading hidden">
                <h6 class="panel-title">Registered Statistics</h6>
            </div>
            <div class="panel-body">
                <div id="container" style="min-width: 310px; height: 90px; margin: 0 auto"></div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>



<div class="row">
    <div class="col-md-5 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="panel panel-flat">

            <div class="panel-body">
                <div id="current_week" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-7 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="panel panel-flat">


            <div class="panel-body">
                <div id="3d_pie" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>






<?php $this->load->view('app/dashboard/dashboard_js') ?>