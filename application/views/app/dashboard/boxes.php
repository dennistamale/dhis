
<!-- Quick stats boxes -->
<div class="row">
    <div class="col-md-6">

        <div class="row">

            <div class="col-lg-6">

                <!-- Members online -->
                <div class="panel bg-blue-400 blue-replace">
                    <div class="panel-body">
                        <!--                        <div class="heading-elements">-->
                        <!--                            <span class="heading-text badge bg-teal-800">+53,6%</span>-->
                        <!--                        </div>-->

                        <h3 class="no-margin"><?= $this->dashboard_model->count_hfs(); ?></h3>
                        Facilities

                    </div>


                        <div id="server-load2"></div>

                </div>
                <!-- /members online -->

            </div>

            <div class="col-lg-6">

                <!-- Members online -->
                <div class="panel bg-purple-400">
                    <div class="panel-body">
                        <!--                        <div class="heading-elements">-->
                        <!--                            <span class="heading-text badge bg-teal-800">+53,6%</span>-->
                        <!--                        </div>-->

                        <h3 class="no-margin"><?= $this->dashboard_model->count_districts(); ?></h3>
                        Districts

                    </div>

                    <div id="today-revenue2"></div>
                </div>
                <!-- /members online -->

            </div>

            <div class="col-lg-6">

                <!-- Current server load -->
                <div class="panel bg-orange-400">
                    <div class="panel-body">
                        <h3 class="no-margin"><?= $this->dashboard_model->count_activities() ?></h3>
                        Activities

                    </div>

                    <div id="server-load"></div>
                </div>
                <!-- /current server load -->

            </div>

            <div class="col-lg-6">

                <!-- Today's revenue -->
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="reload"></a></li>
                            </ul>
                        </div>

                        <h3 class="no-margin"><?= $this->dashboard_model->count_assessments() ?></h3>
                        Assessments

                    </div>

                    <div id="today-revenue"></div>
                </div>
                <!-- /today's revenue -->

            </div>


        </div>

    </div>


    <div class="col-md-6">

        <?php $missions =$this->dashboard_model->mission(); ?>
        <!-- Dashboard content -->
        <style>


            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                padding: 12px 12px !important;
                line-height: 1.5384616;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
        </style>
        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Activities</h6>

                    </div>

                    <div class="table-responsive   pre-scrollabl">
                        <table class="table text-nowrap">
                            <thead>
                            <tr>
                                <th>Region</th>
                                <th>Planned</th>
                                <th>Done</th>
                                <th>Beneficiaries</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php



                            foreach ($this->model->get_project_regions() as $r) { ?>

                                <tr>
                                    <td><?= $r->region ?></td>
                                    <td>34</td>
                                    <td>23</td>
                                    <td>24</td>
                                </tr>


                            <?php  }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /marketing campaigns -->

            </div>




        </div>
        <!-- /dashboard content -->


    </div>

</div>
<!-- /quick stats boxes --

