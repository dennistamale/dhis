
<?php

$status_list = array(
    array("#52c036" => "Pending"),
    array("#3c63e2" => "Closed"),
    array("#7c32b5" => "On Hold"),
    array("#6bffe2" => "Fraud"),
    array("#EC407A" => "Fraud"),
    array("#FFA726" => "Fraud"),
);

?>


<?php $indicators=$this->dhis2_model->indicators('weekly'); ?>

<!-- Dashboard content -->
<style>


    .tabl > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 12px 12px !important;
        line-height: 1.5384616;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>
<div class="row">
    <div class="col-lg-12">

        <!-- Marketing campaigns -->
        <div class="panel panel-flat">

            <div class="panel-heading hidden">
                <h5 class="panel-title">Indicators</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-lg text-nowrap">
                    <tbody>
                    <?php
                    $no=0;
                    foreach ($indicators as $wd){

                        $status = $status_list[intval($no)];
                        $no++;
                        ?>
                    <tr>
                        <td class="col-md-2 col-sm-2 col-xs-2 col-lg-2">

                            <div class="media-left">
                                <h2 class="text-semibold no-margin"><span style="font-size: 130px; color: <?= key($status) ?>;"  id="last_value_<?= $wd->indicator ?>"></span> </h2>
                                <ul class="list-inline list-inline-condensed no-margin">

                                    <li>
                                        <span class="text-muted" style="font-size: 18px !important;"><?= $wd->name ?></span>
                                        <br/>
                                        <span class="text-muted" id="week_<?= $wd->indicator ?>" style="font-size: 18px !important;"><?= $wd->name ?></span>
                                    </li>
                                </ul>
                            </div>
                        </td>

                        <td class="col-md-10  col-sm-10 col-xs-10 col-lg-10">

                            <div id="<?= $wd->indicator ?>"  style="height: 250px; margin: 0 auto"></div>

                        </td>

                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>


        </div>
        <!-- /marketing campaigns -->



    </div>
</div>
<!-- /dashboard content -->

<?php //$this->load->view('app/dashboard/dashboard_js') ?>