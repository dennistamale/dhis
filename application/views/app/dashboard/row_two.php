
<!-- Dashboard content -->
<style>


    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 12px 12px !important;
        line-height: 1.5384616;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
</style>
<div class="row">
    <div class="col-lg-8">

        <!-- Marketing campaigns -->
        <div class="panel panel-flat">


            <div class="table-responsive">
                <table class="table table-lg text-nowrap table-bordered">
                    <tbody>
                    <tr>
                        <td class="col-md-6">

                            <div class="row">
                                <div class="col-md-12" style="border-bottom: solid thin silver;">
                                    <div class="media-left">
                                        <h2 class="text-semibold no-margin text-danger"><span style="font-size: 100px;" ><?= $total_ben=$this->dashboard_model->total_beneficiaries(); ?> </span></h2>
                                        <h3 style="text-align: center !important; ">
                                            <span class="text-muted" >People Reached</span>
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="border-right: solid thin silver;">
                                    <div class="media-left">
                                        <h2 class="text-semibold no-margin text-danger"><span style="font-size: smaller;"># Districts Covered</span></h2>
                                        <h3 class="text-semibold no-margin text-danger"><span style="font-size: 40px;" ><?= $this->dashboard_model->total_activity_report_districts(); ?> <i style="font-size: 40px;     top: -5px;"  class="icon-location4"></i></span></h3>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media-left">
                                        <h2 class="text-semibold no-margin text-danger"><span  style="font-size: smaller;"># Facilities Covered</span></h2>
                                        <h3 class="text-semibold no-margin text-danger"><span style="font-size: 40px;" ><?= $this->dashboard_model->total_activity_report_hfs(); ?> <i style="font-size: 40px;     top: -8px;"  class="icon-home9"></i></span> </h3>

                                    </div>
                                </div>
                            </div>


                        </td>

                        <td class="col-md-6">

                            <?php $male=$this->dashboard_model->total_beneficiaries('male'); ?>
                            <?php $female=$this->dashboard_model->total_beneficiaries('female'); ?>

                            <h3 class="text-center">People Reached</h3>

                            <div class="row text-center">
                                <div class="col-md-6">
                                    <i class="fa fa-male text-blue-400  text-block" style="font-size: 135px;"></i>
                                    <h3>Male <span class="text-small"><?= round(($male/$total_ben)*100,2) ?>%</span></h3>
                                    <h2><?= number_format($male); ?></h2>
                                </div>
                                <div class="col-md-6">
                                    <i class="fa fa-female text-pink-400  text-block" style="font-size: 135px;"></i>
                                    <h3>Female <span class="text-small"><?=  round(($female/$total_ben)*100,2) ?>%</span></h3>
                                    <h2><?= number_format($female); ?></h2>
                                </div>
                            </div>



                        </td>


                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
        <!-- /marketing campaigns -->



    </div>

    <div  class="col-lg-4">


        <!-- Progress counters -->
        <div class="row">
            <div class="col-md-6">

                <!-- Available hours -->
                <div class="panel text-center">
                    <div class="panel-body">

                        <!-- Progress counter -->
                        <div class="content-group-sm svg-center position-relative" id="hours-available-progress"></div>
                        <!-- /progress counter -->


                        <!-- Bars -->
                        <div class="hours-available-bars" id="hours-available-bars"></div>
                        <!-- /bars -->

                    </div>
                </div>
                <!-- /available hours -->

            </div>

            <div class="col-md-6">

                <!-- Productivity goal -->
                <div class="panel text-center">
                    <div class="panel-body">

                        <!-- Progress counter -->
                        <div class="content-group-sm svg-center position-relative" id="indicators"></div>
                        <!-- /progress counter -->

                        <!-- Bars -->
                        <div id="goal-bars"></div>
                        <!-- /bars -->

                    </div>
                </div>
                <!-- /productivity goal -->

            </div>
        </div>
        <!-- /progress counters -->



    </div>


</div>
<!-- /dashboard content -->
