
<?php

$status_list = array(
    array("#52c036" => "Pending"),
    array("#3c63e2" => "Closed"),
    array("#7c32b5" => "On Hold"),
    array("#6bffe2" => "Fraud"),
    array("#EC407A" => "Fraud"),
    array("#43A047" => "Fraud"),
    array("#7CB342" => "Fraud"),
    array("#C0CA33" => "Fraud"),
    array("#512DA8" => "Fraud"),
    array("#303F9F" => "Fraud"),
    array("#1976D2" => "Fraud"),
);

?>


<?php $periodic_dimensions=$this->dhis2_model->indicators('monthly'); ?>

<div class="row">
    <div class="col-lg-12">

        <!-- Marketing campaigns -->
        <div class="panel panel-flat">

            <div class="panel-heading hidden">
                <h5 class="panel-title">Indicators</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-lg text-nowrap">
                    <tbody>
                    <?php
                    $no=0;
                    foreach ($periodic_dimensions as $wd){

                        $status = $status_list[intval($no)];
                        $no++;
                        ?>
                    <tr>
                        <td class="col-md-2 col-sm-2 col-xs-2 col-lg-2">

                            <div class="media-left">
                                <h2 class="text-semibold no-margin"><span style="font-size: 90px; color: <?= key($status) ?>;"  id="month_last_value_<?= $wd->indicator ?>"></span> </h2>
                                <ul class="list-inline list-inline-condensed no-margin">

                                    <li>
                                        <span class="text-muted" style="font-size: 18px !important;"><?= $wd->name ?></span>
                                        <br/>
                                        <span class="text-muted" id="month_name_<?= $wd->indicator ?>" style="font-size: 18px !important;"><?= $wd->name ?></span>
                                    </li>
                                </ul>
                            </div>
                        </td>

                        <td class="col-md-10  col-sm-10 col-xs-10 col-lg-10">

                            <div id="month_<?= $wd->indicator ?>"  style="height: 250px; margin: 0 auto"></div>

                        </td>

                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>


        </div>
        <!-- /marketing campaigns -->



    </div>
</div>
