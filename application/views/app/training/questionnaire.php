<style>
    .container {
        margin-top: 1%;
    }

    /*Wizard*/
    .list-group-item.success,
    .list-group-item.success:hover,
    .list-group-item.success:focus {
        background-color: #7aba7b;
        border-color: #7aba7b;
        color: #ffffff;
    }

    .list-group-item.success > h4 {
        color: #ffffff;
    }

    .list-group-item.error,
    .list-group-item.error:hover,
    .list-group-item.error:focus {
        background-color: #d59392;
        border-color: #d59392;
        color: #ffffff;
    }

    .list-group-item.error > h4 {
        color: #ffffff;
    }

</style>

<div class="row">
        <div class="col-md-2">
            <div class="list-group wizard-menu">
                <a href="#" class="list-group-item active step-1-menu" data-for=".step-1">
                    <h4 class="list-group-item-heading">1. Load from DIV</h4>
<!--                    <p class="list-group-item-text">Load your data from a div.</p>-->
                </a>
                <a href="#" class="list-group-item step-2-menu" data-for=".step-2">
                    <h4 class="list-group-item-heading">2. Load from DIV</h4>
<!--                    <p class="list-group-item-text">Load your data from a div.</p>-->
                </a>
                <a href="#" class="list-group-item step-3-menu" data-for=".step-3">
                    <h4 class="list-group-item-heading">3. from DIV TEst</h4>
<!--                    <p class="list-group-item-text">Load your data from a div.</p>-->
                </a>
                <a href="#" class="list-group-item step-4-menu" data-for=".step-4">
                    <h4 class="list-group-item-heading">4. AJAX Load</h4>
<!--                    <p class="list-group-item-text">AJAX-load your data. (Attribute: data-load)</p>-->
                </a>
            </div>
        </div>
        <div class="col-md-10">
            <!--                Load content in-->
            <div class="well wizard-content">

            </div>
            <!--                Content to load-->
            <div class="hide">
                <div class="step-1">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- add attribute data-action="" and data-method="" with path to file and form-method to submit form -->
                            <form method="post" action="#" data-action="index.php">
                                <div class="form-group">
                                    <label for="inputEx1">E-Mail</label>
                                    <input id="inputEx1" type="text" class="form-control" placeholder="E-Mail" required>
                                </div>
                                <hr>
                                <div class="pull-right wizard-nav">
                                    <button type="submit" class="btn btn-primary wizard-next" data-current-step="1">Next step</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="step-2">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- add attribute data-action="" and data-method="" with path to file and form-method to submit form -->
                            <form method="post" action="#" data-action="path/to/action.php" data-method="POST">
                                <div class="form-group">
                                    <label for="inputEx1">Username</label>
                                    <input id="inputEx1" type="text" class="form-control" placeholder="E-Mail" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputEx2">Password</label>
                                    <input id="inputEx2" type="password" class="form-control" placeholder="Password" required>
                                </div>
                                <hr>
                                <div class="pull-right wizard-nav">
                                    <!-- data-current-step is for going back and forward in wizard -->
                                    <a class="btn btn-default wizard-prev" data-current-step="2">Previous step</a>
                                    <button type="submit" class="btn btn-success wizard-fin" data-current-step="2">Finish</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="step-3">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- add attribute data-action="" and data-method="" with path to file and form-method to submit form -->
                            <form method="post" action="#" data-action="path/to/action.php" data-method="POST">
                                <div class="form-group">
                                    <label for="inputEx1">Username3</label>
                                    <input id="inputEx1" type="text" class="form-control" placeholder="E-Mail" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputEx2">Password3</label>
                                    <input id="inputEx2" type="password" class="form-control" placeholder="Password" required>
                                </div>
                                <hr>
                                <div class="pull-right wizard-nav">
                                    <!-- data-current-step is for going back and forward in wizard -->
                                    <a class="btn btn-default wizard-prev" data-current-step="2">Previous step</a>
                                    <button type="submit" class="btn btn-success wizard-fin" data-current-step="2">Finish</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="step-4" data-load="#"></div>
            </div>
        </div>
    </div>


<?php $this->load->view('app/training/questionnaire_js') ?>



<div class="top_header">
    <span class="top_main">Create New Survey</span><br>
    <span class="top_sub">Create a subtopic of your survey</span>
    <hr>
</div>

<div id="survey" class="addsurveyform">
    <form action="javascript:submitSurvey()">
        <?$this->load->view('backend/notes');?>

        <label>Write survey title<span class="small">Main topic of your survey</span></label>
        <input type="text" name="title" id="title" style="width:300px;"/>

        <div class="spacer"></div>

        <label>Choose template<span class="small">Select from contrib templates</span></label>
        <select name="template" id="template" style="width:300px;">
            <?foreach($templates AS $template):?>
                <option value="<?=$template?>"><?=$template?></option>
            <?endforeach;?>
        </select>

        <div class="spacer"></div>

        <div id="add_qstns" style="display:none;">
            <label>Questionnaires<span class="small">Create your survey questions</span></label>

            <div class="spacer"></div>

            <div class="qstn_box">
                <div class="qstn-left">
                    <ul class="qstns">
                        <li class="active">1. <span id="qstn1">Question 1</span></li>
                    </ul>
                </div>
                <div class="qstn-right">
                    <div class="qstn-right-inner">
                        <p class="qstn-head-title"><b>Question 1</b><br>Create your first question</p>

                        <label style="width: 110px;">Question Type<span class="small" style="width: 110px;">Select question type</span></label>
                        <select id="qtype" name="qtype" style="width: 150px;">
                            <option value="single">single</option>
                            <option value="dropdown">dropdown</option>
                            <option value="multi">multi</option>
                            <option value="bigbox">bigbox</option>
                            <option value="smallbox">smallbox</option>
                            <option value="pagebreak">pagebreak</option>
                            <option value="info">info</option>
                        </select>
                        <img src="http://survey.contrib.com/images/icons/help_16x16.gif" id="qtip_qtype_btn">

                        <label style="width: 110px;">This question is<span class="small" style="width: 110px;">Optional or Required</span></label>
                        <select name="validation" id="validation" style="width: 150px;">
                            <option value="optional">Optional</option>
                            <option value="required">Required</option>
                        </select>
                        <img src="http://survey.contrib.com/images/icons/help_16x16.gif" id="qtip_validatn_btn">

                        <div class="spacer"></div>

                        <label style="width: 110px;">Question Title<span class="small">Type your question</span></label>
                        <input type="text" name="qtitle" id="qtitle" style="width: 465px;"/>

                        <div class="spacer"></div>

                        <label style="margin-right:20px;width: 275px;">Responses:<span class="small" style="width:275px">Single/Dropdown/Multi: each option on a new line</span></label>

                        <label style="width: 275px;">Sample Responses:<span class="small" style="width:275px">Sample responses for 'What fruit do you like most?'</span></label>

                        <div class="spacer"></div>
                        <textarea name="responses" id="responses" style="width: 280px;height: 90px;margin-right:20px"></textarea>

                        <textarea  style="width: 275px;height: 90px;" disabled>a. Apple
b. Mango
c. Banana</textarea>

                        <div class="spacer"></div>

                    </div>
                </div>
            </div>

            <div class="spacer"></div>
        </div>
        <br><br>
        <input type="hidden" id="sid" value="">
        <button type="submit" >Submit</button>

    </form>
</div>

