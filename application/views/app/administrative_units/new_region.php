
<?php
if (isset($id)) {

    $r = $this->db->select('a.*')->from('project_regions a')->where(array('a.id' => $id))->get()->row();

    if (!empty($r)) {
        $id = $r->id;
        $selected_districts=explode(',',$r->districts);

    }

    $data['id']=$id;

}
?>




<div class="row">
    <div class="col-md-12">


        <!-- Horizontal form options -->


        <!-- Basic layout-->
        <?= form_open_multipart($this->page_level . $this->page_level2 . 'new_region/' . (isset($id) ? '/' . $id * date('Y') : ''), array('class' => 'form-horizontal')) ?>

        <div class="panel panel-white">
            <div class="panel-heading">

                <h5 class="panel-title"><?= humanize($subtitle) ?>


                </h5>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li>
                            <?= $this->uri->segment(3) == 'view_region' ? anchor($this->page_level . $this->page_level2 . 'edit_region/' . $id * date('Y'), '<i class="icon-pencil6"></i>', 'title="Edit&nbsp;Details" data-popup="tooltip"') : ''; ?>
                        </li>


                        <li><a data-action="collapse"></a></li>
                        <!--                        <li><a data-action="reload"></a></li>-->
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">


                <input name="id" hidden value="<?= isset($id) ? $id : '' ?>">

                <fieldset class="content-group">

                        <div class="form-group">


                            <label class="control-label col-lg-3"><span class="pull-right">Region</span></label>

                            <div class="col-lg-9">

                                <input <?= $subtitle == 'view_region' ? 'readonly' : ''; ?> required type="text"
                                                                                     name="region"
                                                                                     value="<?= set_value('region', isset($id) ? $r->region : '') ?>"
                                                                                     class="form-control <?= $subtitle == 'view_region' ? 'no-border' : ''; ?> "
                                                                                     placeholder="Region">
                                <?php echo form_error('region', '<span class="text-danger">', '</span>') ?>

                            </div>


                        </div>




                        <div class="form-group">

                            <!--                in_array('a', $myArray)-->

                            <?php $district=$this->db->select('state,title')->from('state')->where('country','UG')->get()->result() ?>

                            <label class="control-label col-lg-3"><span class="pull-right">Select Districts</span></label>
                            <div class="col-lg-9">
                                <select  multiple="multiple"  class="select" name="districts[]" required  <?= $subtitle=='view_region'?'disabled':''; ?> >
                                    <option value="">Village</option>
                                    <?php foreach ($district as $d){ ?>
                                        <option value="<?= $d->title ?>" <?= set_select('districts[]', $d->title, isset($id)?(in_array($d->title, $selected_districts)?true:''):'') ?>><?= $d->title ?></option>
                                    <?php } ?>

                                </select>
                                <?php echo form_error('districts[]','<span class="text-danger">','</span>') ?>
                            </div>
                        </div>


</fieldset>


                <div class="text-right <?= $subtitle == 'view_region'? 'hidden' : ''; ?>">
                    <button type="reset" class="btn btn-default">Cancel <i class="icon-blocked position-right"></i>
                    </button>
                    <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- /basic layout -->


        <!-- /vertical form options -->


    </div>

</div>

<?php $this->load->view('app/'.$this->page_level2.'regions') ?>

