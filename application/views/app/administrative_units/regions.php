<?php $regions=$this->db->select('a.*')->from('project_regions a')->get()->result(); ?>


<!-- Basic datatable -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= humanize($title) ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><?= anchor($this->page_level.$this->page_level2.'new_region','<i class="fa fa-plus-square"></i> New','class="label label-success"') ?></li>
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table datatable-button-init-basic">
        <thead>
        <tr>
            <th>Region</th>


            <th>Districts</th>
            <th class="text-center" style="width: 3px;">Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($regions as $i){


            $selected_districts=explode(',',$i->districts);
            $pick_list='';
            foreach ($selected_districts as $p){

                $pick_list .=' <label class="label bg-slate">'.$p.'</label> ';
            }

            ?>

            <tr>
                <td>

                    <a class="letter-icon-title" data-popup="tooltip" title="<?= $i->region ?>" data-placement="right" ><?= strtoupper($i->region) ?></a>

                </td>




                <td><?= $pick_list ?></td>




                <td class="text-center">
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <?= anchor($this->page_level.$this->page_level2.'view_region/'.$i->id*date('Y'),'<i class="icon-eye"></i> View','') ?>
                                </li>
                                <li>
                                    <?= anchor($this->page_level.$this->page_level2.'edit_region/'.$i->id*date('Y'),'<i class="icon-pencil7"></i> Edit','') ?>
                                </li>

                                <li>
                                    <?= anchor($this->page_level.$this->page_level2.'edit_region/'.$i->id*date('Y'),'<i class="icon-pen-plus"></i> Add District','') ?>
                                </li>

                                <li>
                                    <?= anchor($this->page_level.$this->page_level2.'delete_region/'.$i->id*date('Y'),'<i class="icon-bin"></i> Delete','') ?>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>

        <?php } ?>
        </tbody>
    </table>
</div>
<!-- /basic datatable -->
