<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_workplan extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(array(

            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'level' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created_on' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'updated_on' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'null' => TRUE,
            )
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('workplan_level',true);

        $this->dbforge->add_field(array(

            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'parent_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'type_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 30,
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
            ),
            'level' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'description' => array(
                'type' => 'TEXT'
            ),
            'created_on' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'updated_on' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'updated_by' => array(
                'type' => 'INT',
                'null' => TRUE,
            )
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('code_key');
        $this->dbforge->add_key('parent_id_key');
        $this->dbforge->create_table('workplan',true);

    }

    public function down()
    {
        $this->dbforge->drop_table('workplan_level');
        $this->dbforge->drop_table('workplan');

    }
}