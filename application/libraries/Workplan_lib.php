<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php

class workplan_lib
{
    private $ci;
    private $table = 'workplan';
    private $table_level = 'workplan_level';


    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
    }

    public function test()
    {
        echo 'dennis';
    }

    function get_name($id = null)
    {

        if (isset($id)) {
            $result = $this->ci->db->select('id,title')->from($this->table)->where(array('id' => $id))->get()->row();

            if (isset($result)) {

                return $result->name;


            } else {

                return false;
            }

        } else {
            return false;
        }
    }

    function get_villages_under_selection($id)
    {

        /*Array (

        [0] => Array ( [id] => 3 [name] => Eastern )
        [1] => Array ( [id] => 64 [name] => Mbale )
        [2] => Array ( [id] => 956 [name] => Busiu )
        [3] => Array ( [id] => 6105 [name] => Buwalasi )
        [4] => Array ( [id] => 27141 [name] => Namunyu )

        )*/


        $path = $this->get_path($id);

        $path_no = isset($path);

        $villages = array();
        switch ($path_no) {

            case 5://return this Village

                array_push($villages, $id);

                break;

            case 4://Villages
                foreach ($this->get_children($id) as $v) {
                    array_push($villages, $v->id);
                }
                break;

            case 3://Parish
                foreach ($this->get_children($id) as $p) {
                    foreach ($this->get_children($p->id) as $v) {
                        array_push($villages, $v->id);
                    }
                }
                break;

            case 2://sub county
                foreach ($this->get_children($id) as $p) {
                    foreach ($this->get_children($p->id) as $v) {

                        foreach ($this->get_children($v->id) as $k) {
                            array_push($villages, $k->id);
                        }
                    }
                }
                break;

            case 1://District
                foreach ($this->get_children($id) as $p) {
                    foreach ($this->get_children($p->id) as $v) {

                        foreach ($this->get_children($v->id) as $k) {

                            foreach ($this->get_children($k->id) as $p) {
                                array_push($villages, $p->id);
                            }
                        }
                    }
                }
                break;
            case 0://Region
                foreach ($this->get_children($id) as $p) {
                    foreach ($this->get_children($p->id) as $v) {

                        foreach ($this->get_children($v->id) as $k) {

                            foreach ($this->get_children($k->id) as $p) {
                                foreach ($this->get_children($p->id) as $t) {
                                    array_push($villages, $t->id);
                                }
                            }
                        }
                    }
                }
                break;
        }

        return $villages;

    }

    function get_path($id)
    {


        $result = $this->ci->db->select('id,parent_id,type_id,title')->from($this->table)->where(array('id' => $id))->get()->row();


        $path = array();

        if (isset($result)) {
            if ($result->parent_id != '') {

                $pat = array(array('id' => $result->id, 'name' => $result->title));

                $path = array_merge($this->get_path($result->parent_id), $pat);

            }
        }

        return $path;

    }

    function get_children($id = null)
    {
        if (isset($id)) {

            $result = $this->ci->db
                ->select('a.id,a.parent_id,a.code,a.title,a.level,b.name')
                ->from($this->table . ' a')
                ->join('workplan_level b', 'a.level=b.level')
                ->where(array('a.parent_id' => $id))
                ->order_by('id','desc')
                ->get()->result();

            if (count($result) > 0) {

                return $result;


            } else {

                return array();
            }
        } else {
            return false;
        }


    }

    function get_single_item($id = null)
    {

        $output = false;

        if (isset($id)) {
            $r = $this->ci->db->select()->from($this->table)->where(array('id' => $id))->get()->row();
            $output = isset($r) ? $r : false;

        }


        return $output;

    }

    function count_children($id)
    {

        return $this->ci->db->where('parent_id', $id)->from($this->table)->count_all_results();
    }

    function get_tree_type($id = null)
    {
        if (isset($id)) {
            $loc = $this->ci->db->select('b.title')->from($this->table . ' a')->join($this->table_level . ' b', 'a.type_id=b.id')->where('a.id', $id)->get()->row();
            return $loc->title;
        } else {
            return false;
        }

    }


    function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {


            if ($element->parent_id == $parentId) {

                $elements=$this->get_children($element->id);

                print_r($elements);

                $children = $this->buildTree($elements, $element->id);



                if ($children) {

                    $element['children'] = $children;

                }

                $branch[] = $element;
            }
        }

         return ($branch);
    }

//$tree = buildTree($rows);


    function get_level($level)
    {

        return $this->ci->db->select()->from($this->table)->where('level', $level)->get()->result();

    }

    function get_workplan_level($level)
    {
        return $this->ci->db->select()->from('workplan_level')->where('level', $level)->get()->row();
    }

}

?>
