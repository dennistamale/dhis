<?php

class survey_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }




    public function survey_code()
    {


        $token = code(10);

        while (1) {
            if ($this->db->where(array('survey_code' => strtoupper($token)))->from('survey')->count_all_results() == 0) {
                break;
            } else {
                $token = code(10);
            }

        }

        return strtoupper($token);
    }


    function get_surveys($id = null)
    {

             $this->db
                ->select()
                ->from('survey');
            $output = isset($id) ? $this->db->where(array('id' => $id))->get()->row() : $this->db->get()->result();

        return $output;

    }



    function get_survey_questions($survey_id = null,$id=null)
    {

             $this->db
                ->select()
                ->from('survey_questions')->where('survey_id',$survey_id);

            $output = isset($id) ? $this->db->where(array('id' => $id))->get()->row() : $this->db->get()->result();

        return $output;

    }





    function get_survey_options($id = null)
    {

             $this->db
                ->select()
                ->from('survey_options');
            $output = isset($id) ? $this->db->where(array('id' => $id))->get()->row() : $this->db->get()->result();

        return $output;
    }


    function manage_survey_questions($survey_id, $form_code, $array_values)
    {

        $insert = array();
        $update = array();

        foreach ($array_values['id'] as $key => $id_value) {

            $values = array(
                'survey_code' => $form_code,
                'survey_id' => $survey_id,
                'question' => $name=$array_values['question'][$key],
                'question_type' =>$group= $array_values['question_type'][$key],
                'required' => $array_values['required'][$key],
                'placeholder' => $array_values['placeholder'][$key]
            );

            if(strlen($name)>0 && strlen($group)>0) {

                if (strlen($id_value) == 0) {
                    $values['created_on'] = time();
                    $values['created_by'] = $this->session->id;
                    array_push($insert, $values);

                } else {

                    $values['id'] = $id_value;
                    $values['updated_on'] = time();
                    $values['updated_by'] = $this->session->id;

                    array_push($update, $values);
                }
            }


        }

        $table = 'survey_questions';
        count($insert) > 0 ? $this->db->insert_batch($table, $insert) : '';
        count($update) > 0 ? $this->db->update_batch($table, $update, 'id') : '';

    }


    function create_form($survey_id,$main_class=' form-control '){


        $this->db
            ->select('a.*,b.title as type')->from('survey_questions a')
            ->join('survey_options b','a.question_type=b.id')
            ->where(array('survey_id'=>$survey_id));

        $questions=$this->db->get()->result();

        $form_options = array();
        foreach ($questions as $r){

            $type=$r->type;

            switch ($type) {

                default:case 'text':

                    $field=array(
                        'id' => 'name',
                        'placeholder' => $r->placeholder,
                        'name' => $r->id,
                        'type' => 'text',
                        'label' => $r->question.' : ',
                        'class' => $main_class
                    );

                    array_push($form_options,$field);

                    break;

                case 'email':

                    $field=array(
                        'id' => 'email',
                        'placeholder' => $r->placeholder,
                        'name' => $r->id,
                        'type' => 'email',
                        'label' => $r->question.' : ',
                        'class' => $main_class
                    );

                    array_push($form_options,$field);

                    break;

                case 'password':

                    $field=array(
                        'id' => 'password',
                        'placeholder' => $r->placeholder,
                        'name' => $r->id,
                        'type' => 'password',
                        'label' => $r->question.' : ',
                        'class' => $main_class
                    );

                    array_push($form_options,$field);

                    break;

                case 'contact':

                    $field=array(
                        'id' => 'contact',
                        'placeholder' => $r->placeholder,
                        'name' => $r->id,
                        'type' => 'text',
                        'label' => $r->question.' : ',
                        'class' => $main_class
                    );

                    array_push($form_options,$field);

                    break;


                case 'hidden':

                    $field=array(
                        'id' => 'contact',
                        'placeholder' => $r->placeholder,
                        'name' => $r->id,
                        'type' => 'hidden',
                        'label' => $r->question.' : ',
                        'class' => $main_class
                    );

                    array_push($form_options,$field);

                    break;


                case 'select':

                    $options=[];
                  foreach ($this->get_question_options($r->id) as $op){

                      $options[$op->question_option]=$op->option_label;
                  }


                    $field=array(
                        'id' => 'shirts',
                        'placeholder' => $r->placeholder,
                        'label' => $r->question.' : ',
                        'name' => $r->id,
                        'type' => 'select',
                        'selected' => 'large',
                        'class' => 'styled',
                        'options' => $options
                    );

                    array_push($form_options,$field);

                    break;





                case 'radio':


                    $options=[];
                    foreach ($this->get_question_options($r->id) as $op){
                        $options[]=array(
                            'id' => $op->id,
                            'value' => $op->question_option,
                            'label' => $op->option_label,
                            'name' => $op->question_id,
                        );
                    }

                    $field=array(
                        'id' => 'gender',
                        'label' => $r->question.' : ',
                        'name' => $r->id,
                        'type' => 'radio',
                        'class' => 'styled',
                        'options' => $options
                    );

                    array_push($form_options,$field);

                    break;


                case 'checkbox':



                    $options=[];
                    foreach ($this->get_question_options($r->id) as $op){
                        $options[]=array(
                            'id' => $op->id,
                            'value' => $op->question_option,
                            'label' => $op->option_label,
                            'name' => $op->question_id,
                        );
                    }




                    $field=array(
                        'id' => 'gender',
                        'label' => $r->question.' : ',
                        'name' => $r->id,
                        'type' => 'checkbox',
                        'class' => 'styled',
                        'options' => $options
                    );

                    array_push($form_options,$field);

                    break;


                case 'textarea':

                    $field=array(
                        'id' => 'address',
                        'type' => 'textarea',
                        'placeholder' => $r->placeholder,
                        'label' => $r->question.' : ',
                        'name' => $r->id,
                        'class' => $main_class
                    );

                    array_push($form_options,$field);

                    break;


                case 'submit':

                    $field=array(
                        'id' => 'submit',
                        'type' => 'submit',
                        'label' => $r->question.' : ',
                        'name' => $r->id,
                        'class' => 'btn btn-success'

                    );

                    array_push($form_options,$field);

                    break;


                    }

            }
        return $form_options;

    }


    function get_question_options($question_id){

       $options=$this->db
            ->select()
            ->from('survey_question_options')
            ->where(array('question_id'=>$question_id))
            ->get()->result();


       return $options;

    }

    function create_form2(){
        $main_class = ' form-control ';
       return $form_options = array(
            array(/* Name */
                'id' => 'name',
                'placeholder' => 'Please Enter Name',
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name: ',
                'class' => $main_class
            ),
            array(/* Email */
                'id' => 'email',
                'placeholder' => 'Please Enter Email',
                'name' => 'email',
                'type' => 'email',
                'label' => 'Email Id: ',
                'class' => $main_class
            ),
            array(/* Password */
                'id' => 'password',
                'placeholder' => 'Please Enter Password',
                'name' => 'password',
                'type' => 'password',
                'label' => 'Password: ',
                'class' => $main_class
            ),
            array(/* contact */
                'id' => 'contact',
                'placeholder' => 'Please Enter Contact',
                'name' => 'contact',
                'type' => 'text',
                'label' => 'Contact: ',
                'class' => $main_class
            ),


            array(/* Gender */
                'id' => 'shirts',
                'label' => 'Shirts : ',
                'name' => 'shirts',
                'type' => 'select',
                'selected' => 'large',
                'class' => 'styled',
                'options' => array(
                    'small' => 'Small Shirt',
                    'med' => 'Medium Shirt',
                    'large' => 'Large Shirt',
                    'xlarge' => 'Extra Large Shirt',
                )
            ),

            array(/* Gender */
                'id' => 'gender',
                'label' => 'Gender : ',
                'name' => 'Gender',
                'type' => 'radio',
                'class' => 'styled',
                'options' => array(
                    array(
                        'id' => 'gender',
                        'value' => 'male',
                        'label' => 'Male',
                        'name' => 'gender',
                    ),
                    array(
                        'id' => 'gender',
                        'value' => 'female',
                        'label' => 'Female',
                        'name' => 'gender',
                    )
                )
            ),
            array(/* Address */
                'id' => 'address',
                'type' => 'textarea',
                'placeholder' => 'Address',
                'label' => ' Address',
                'name' => 'address',
                'class' => $main_class
            ),
            array(/* SUBMIT */
                'id' => 'submit',
                'type' => 'submit',
                'label' => 'Submit',
                'name' => 'submit',
                'class' => 'btn btn-success'

            )
        );
    }



    function save_surveyjs($count,$survey_code,$values){


       $db_survey=$this->db->where(array('survey_code'=>$survey_code))->from('survey')->countt_all_result();


//        $db_survey==0?


       return $output=array(
            'registration'=>array(
                'statusMessage'=>'Survey Saved Successfully',
                'status'=>'success'
            )
        );

    }


    function get_survey_answer($id){

       return $this->db->select()->from('survey_answer')->where('id',$id)->get()->row();

    }



}