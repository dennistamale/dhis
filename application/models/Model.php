<?php

class model extends CI_Model
{

    private $site_name = '';
    private $site_email = '';
    private $siteurl = '';


    function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->site_name = $this->site_options->title('site_name');
        $this->site_email = $this->site_options->title('contact_email');
        $this->siteurl = $this->site_options->title('siteurl');
    }


    function report_dashboard($week, $year, $indcator)
    {

        $this->db->where(array(
            'week_no' => $week,
            'report_year' => $year,
            'indicator' => $indcator,
        ));
        $fr = $this->db->select_sum('indicator_value')->from('facility_report')->get()->row();

        return isset($fr->indicator_value) ? $fr->indicator_value : 0;

    }

    function get_user_details($user_id)
    {

        $output = false;

        $user_details = $this->db->select()
            ->from('users a')
            ->where(array('a.id' => $user_id))->get()->row();
        if (!empty($user_details)) {

            return $user_details;
        }

        return $output;

    }

    function get_nationality($country)
    {

        $n = $this->db->select('nationality')->from('country')->where('a2_iso', $country)->get()->row();

        return !empty($n) ? $n->nationality : 'N/A';
    }


    function get_district($country = 'UG', $district = null)
    {

        $this->db->select('state,title')->from('state');
        $this->db->where(array('country' => $country));
        $result = isset($district) ? $this->db->where('state', $district)->get()->row() : $this->db->get()->result();
        return $result;

    }


    function register_user($values)
    {

        $values['created_on'] = time();
        $values['created_by'] = isset($this->session->id) ? $this->session->id : 0;

        if ($this->db->insert('users', $values)) {

            $fname = $values['first_name'];

            $username = $values['username'];


            $email_msg = "Dear $fname ,\n \r Your account has been created. Your username is $username
To login, go to " . anchor('login') . ". Remember you can change your password once logged.\r\n";

            $this->custom_library->sendHTMLEmail2($values['email'], $this->site_name, $email_msg);


            //Creating A log in the database
            $account = $this->db->select('id')->from('users')->where(array('username' => $values['username'], 'phone' => $values['phone']))->get()->row();
            $user_id = $account->id;

            $this->custom_library->add_logs($user_id, 'account_creation', $values['first_name'] . ' ' . $values['last_name'], 'has created an account for user ');


            return array(
                'user_id' => $user_id,
                'registered' => 0
            );


        }
    }

    function get_country_name($country = null)
    {

        $output = 'N/A';

        if (isset($country)) {

            $country = $this->db->select()->from('country')->where(array('a2_iso' => $country))->get()->row();

            $output = isset($country) ? $country->country : 'N/A';
        }
        return $output;

    }

    function country_a3_iso($country = null)
    {

        $output = 'N/A';

        if (isset($country)) {

            $country = $this->db->select()->from('country')->where(array('a2_iso' => $country))->get()->row();

            $output = isset($country) ? $country->a3_iso : 'N/A';
        }
        return $output;

    }


//this is the method for changing the password
    function reset_password($id)
    {
        $return = false;
        $user = $this->db->select()->from('users')->where(array('id' => $id))->get()->row();

        if (isset($user)) {
            $year = date('Y', $user->dob);
            $last_name = strtolower($user->last_name);

            if ($pp = $this->create_password($year, $last_name)) {
                $this->db->where(array('id' => $id))->update('users', array('password' => $pp, 'verified' => 0));

                $return = $year . $last_name;
            }
        }

        return $return;
    }


    function create_password($year = null, $last_name = null)
    {

        if (isset($year) && isset($last_name)) {
            return hashValue($year . $last_name);
            // return $year.$last_name;
        } else {
            return false;
        }
    }


    //checking wether the account exists
    public function account_exists($username, $password)
    {
        $status = 0;

        $results = $this->db->select('a.*,b.title')
            ->from('users a')
            ->join('user_type b', 'a.user_type=b.id')
            ->where(array('username' => $username, 'password' => hashValue($password)))
            ->get()->row();

        $count=isset($results)?1:0;

        $this->login_attempts($username, $count);

        return $results;

    }


    //this is session is assigned to the user after signing up an account , it is a one time function
    function session_assign($u, $p)
    {
        $results = $this->account_exists($u, $p);
        if (isset($results->username)) {
            //$this->db->insert('wallet',array('user'=>$results->id,'start_amount'=>0,'current_amount'=>0,'created_on'=>time(),'created_by'=>$results->id));

            $session_data = array(
                'id' => $results->id,
                'username' => $results->username,
                'first_name' => $results->first_name,
                'last_name' => $results->last_name,
                'photo' => $results->photo,
                'email' => $results->email,
                'user_type' => $results->user_type,
                'sub_type' => $results->sub_type,
                'phone' => $results->phone

            );

            $signin = $this->session->set_userdata($session_data);
            $this->get_permissions($this->session->user_type);
            $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));


            // header('Refresh: 3; url=' . base_url('index.php/myportal'));
//            $signin == true ? redirect('/login/', 'location') : $this->login();
            // redirect('/login/', 'location');


        } else {
            redirect('/login/', 'location');
//            $this->login();
        }


    }


    function get_permissions($user_role)
    {


        $per = $this->db->select('a.perm_id,b.perm_group,b.title')
            ->from('role_perm a')->join('permissions b', 'a.perm_id=b.id')
            ->where(array('role_id' => $user_role))
            ->get()->result();

        $user_role = array();
        foreach ($per as $p) {
            $f = array(
                'role' => $p->perm_id,
                'title' => strtolower(trim($p->title)),
                'group' => strtolower(trim($p->perm_group))
            );
            array_push($user_role, $f);
        }
        //  print_r($user_role);

        count($user_role) > 0 ? $this->session->set_userdata(array('permission' => $user_role)) : '';

        //print_r($this->session->perm);
    }

    ///function for checking the login attempts

    function login_attempts($username, $status)
    {
        $user = $this->db
            ->select('id,status,login_attempts,first_name,last_name')
            ->from('users')->where(array('username' => $username))
            ->get()->row();

        if (isset($user)) {
            if ($user->status != 2) {

                if ($status == 0) {


                    if (isset($user) && $user->login_attempts < 4) {

                        $qry1 = "UPDATE `users` SET `login_attempts` = login_attempts+1 WHERE `username` = '$username'";
                        $this->db->query($qry1);

                    } elseif (isset($user) && $user->login_attempts >= 4) {


                        $time = time();
                        $qry1 = "UPDATE `users` SET `login_attempts` = login_attempts+1 ,`status`=2,updated_on=$time WHERE `username` = '$username'";
                        $this->db->query($qry1);
                        $this->add_logs('System', 'account_modification', $user->first_name . ' ' . $user->last_name, '  has blocked user');


                        $dat = array(
                            'alert' => 'danger',
                            'message' => '<span style="color: red; font-size: larger;">Your account has been Blocked from accessing the System <b>Please contact administrator !!!!</b></span>'
                        );

                        $this->load->view('alert', $dat);

                    }

                } else {
                    $qry_reset = "UPDATE `users` SET `login_attempts` = 0 WHERE `username` = '$username'";
                    $this->db->query($qry_reset);
                }
            } else {
                $dat = array(
                    'alert' => 'danger',
                    'message' => '<span style="color: red; font-size: larger;">Your account has been Blocked from accessing the System <b>Please contact administrator !!!!</b></span>'
                );

                $this->load->view('alert', $dat);
            }
        }


    }


    function remove_role($id)
    {


        if ($this->remove_permissions($id)) {

            $this->db->where('id', $id);
            if ($this->db->delete('user_type')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    function remove_permissions($id)
    {

        $this->db->where('role_id', $id);
        if ($this->db->delete('role_perm')) {
            return true;
        } else {
            return false;
        }
    }


    public function plan_code()
    {


        $token = code(10);

        while (1) {
            if ($this->db->where(array('code' => $token))->from('workplan')->count_all_results() == 0) {
                break;
            } else {
                $token = code(10);
            }

        }

        return strtoupper($token);
    }


    public function indicator_code()
    {


        $token = code(10, true);

        while (1) {
            if ($this->db->where(array('indicator_code' => $token))->from('workplan_indicator_titles')->count_all_results() == 0) {
                break;
            } else {
                $token = code(10, true);
            }

        }

        return $token;
    }


    public function activity_report_code()
    {

        $token = code(10, true);

        while (1) {
            if ($this->db->where(array('form_code' => $token))->from('activity_report')->count_all_results() == 0) {
                break;
            } else {
                $token = code(10, true);
            }

        }

        return $token;
    }


    function get_indicators_title($workplan_id = null)
    {

        $output = false;

        if (isset($workplan_id)) {

            $output = $this->db->select()->from('workplan_indicator_titles')->where(array('workplan_id' => $workplan_id))->get()->result();

        }

        return $output;

    }


    function get_indicators($indicator_code = null)
    {

        $output = false;

        if (isset($indicator_code)) {

            $output = $this->db->select()->from('workplan_indicators')->where(array('indicator_code' => $indicator_code))->get()->result();

        }

        return $output;

    }


    function get_activity_status($workplan_id = null)
    {

        $output = false;

        $this->db->select()->from('workplan_activity_status');
        if (isset($workplan_id)&&strlen($workplan_id)>0) {


            $this->db->where(array('workplan_id' => $workplan_id));


        }

        $output = $this->db->get()->result();

        return $output;

    }


    function get_activity_updates($activity_id)
    {
        $output = false;

        if (isset($activity_id)) {

            $output = $this->db->select()
                ->from('workplan_activity_status_updates')
                ->where(array('activity_id' => $activity_id))
                ->order_by('id', 'desc')
                ->get()->result();

        }

        return $output;

    }

    function get_update_status($id = null)
    {

        $this->db->select()->from('update_status');

        $result = isset($id) ? $this->db->where('id', $id)->get()->row() : $this->db->get()->result();


        return $result;


    }




    function get_geo_level($id = null)
    {

        $this->db->select()->from('geo_level');

        $result = isset($id) ? $this->db->where('id', $id)->get()->row() : $this->db->get()->result();


        return $result;


    }



    function get_facility_by_id($id = null)
    {
        $output = false;
        if (isset($id)) {

            $facility = $this->db->select()->from('health_facilities')->where('id', $id)->get()->row();
            $output = isset($facility) ? $facility : false;

        }
        return $output;
    }


    function get_activity_report_id($id = null, $column = 'id')
    {
        $output = false;
        if (isset($id)) {

            $facility = $this->db->select()->from('activity_report')->where($column, $id)->get()->row();
            $output = isset($facility) ? $facility : false;

        }
        return $output;
    }

    function activity_report_facilitators($activity_report_id, $form_code, $array_values)
    {

        $insert = array();
        $update = array();

        foreach ($array_values['id'] as $key => $id_value) {

            $values = array(
                'form_code' => $form_code,
                'activity_report_id' => $activity_report_id,
                'name' => $name = $array_values['name'][$key],
                'group' => $group = $array_values['group'][$key],
                'designation' => $array_values['designation'][$key],
                'note' => $array_values['note'][$key]
            );

            if (strlen($name) > 0 && strlen($group) > 0) {

                if (strlen($id_value) == 0) {
                    $values['created_on'] = time();
                    $values['created_by'] = $this->session->id;
                    array_push($insert, $values);

                } else {

                    $values['id'] = $id_value;
                    $values['updated_on'] = time();
                    $values['updated_by'] = $this->session->id;

                    array_push($update, $values);
                }
            }


        }

        $table = 'activity_report_facilitators';
        count($insert) > 0 ? $this->db->insert_batch($table, $insert) : '';
        count($update) > 0 ? $this->db->update_batch($table, $update, 'id') : '';

    }

    function activity_report_beneficiaries($activity_report_id, $form_code, $array_values)
    {

        $insert = array();
        $update = array();

        foreach ($array_values['id'] as $key => $id_value) {

            $values = array(
                'form_code' => $form_code,
                'activity_report_id' => $activity_report_id,
                'ben_type' => $ben_type = $array_values['ben_type'][$key],
                'age_group' => $age_group = $array_values['age_group'][$key],
                'male' => $array_values['male'][$key],
                'female' => $array_values['female'][$key],
                'combined' => $array_values['combined'][$key]
            );


            if (strlen($ben_type) > 0 && strlen($age_group) > 0) {


                if (strlen($id_value) == 0) {
                    $values['created_on'] = time();
                    $values['created_by'] = $this->session->id;
                    array_push($insert, $values);

                } else {

                    $values['id'] = $id_value;
                    $values['updated_on'] = time();
                    $values['updated_by'] = $this->session->id;

                    array_push($update, $values);
                }
            }

        }

        $table = 'activity_report_beneficiaries';
        count($insert) > 0 ? $this->db->insert_batch($table, $insert) : '';
        count($update) > 0 ? $this->db->update_batch($table, $update, 'id') : '';

    }

    function activity_report_distributions($activity_report_id, $form_code, $array_values)
    {

        $insert = array();
        $update = array();

        foreach ($array_values['id'] as $key => $id_value) {

            $values = array(
                'form_code' => $form_code,
                'activity_report_id' => $activity_report_id,
                'commodity' => $commodity = $array_values['commodity'][$key],
                'quantity' => $quantity = $array_values['quantity'][$key],
                'unit' => $array_values['unit'][$key]
            );

            if (strlen($commodity) > 0 && strlen($quantity) > 0) {

                if (strlen($id_value) == 0) {
                    $values['created_on'] = time();
                    $values['created_by'] = $this->session->id;
                    array_push($insert, $values);

                } else {

                    $values['id'] = $id_value;
                    $values['updated_on'] = time();
                    $values['updated_by'] = $this->session->id;

                    array_push($update, $values);
                }
            }

        }

        $table = 'activity_report_distributions';
        count($insert) > 0 ? $this->db->insert_batch($table, $insert) : '';
        count($update) > 0 ? $this->db->update_batch($table, $update, 'id') : '';

    }

    function activity_report_challenges($activity_report_id, $form_code, $array_values)
    {

        $insert = array();
        $update = array();

        foreach ($array_values['id'] as $key => $id_value) {

            $values = array(
                'form_code' => $form_code,
                'activity_report_id' => $activity_report_id,
                'challenges' => $challenges = $array_values['challenges'][$key],
                'how_solved' => $how_solved = $array_values['how_solved'][$key],
                'referral' => $array_values['referral'][$key]
            );

            if (strlen($challenges) > 0 && strlen($how_solved) > 0) {

                if (strlen($id_value) == 0) {
                    $values['created_on'] = time();
                    $values['created_by'] = $this->session->id;
                    array_push($insert, $values);

                } else {

                    $values['id'] = $id_value;
                    $values['updated_on'] = time();
                    $values['updated_by'] = $this->session->id;

                    array_push($update, $values);
                }

            }
        }

        $table = 'activity_report_challenges';
        count($insert) > 0 ? $this->db->insert_batch($table, $insert) : '';
        count($update) > 0 ? $this->db->update_batch($table, $update, 'id') : '';

    }


    function activity_report_components($table = null, $id = null)
    {
        $output = false;
        if (isset($id) && isset($table)) {

            $result = $this->db->select()->from($table)->where('activity_report_id', $id)->get()->result();
            $output = count($result) > 0 ? $result : false;

        }
        return $output;
    }


    function activity_summary_details($district = null, $year = null)
    {

        $result = array(
            'male' => 0,
            'female' => 0,
            'combined' => 0,
            'total' => 0
        );

        if (isset($district) && isset($year)) {

            $activity_report = $this->db->select('id')->from('activity_report')->where(array('district' => $district, 'activity_year'))->get()->result();

            $m = 0;
            $f = 0;
            $c = 0;

            foreach ($activity_report as $ac) {


                $male = $this->db->select_sum('male')->where(array('activity_report_id' => $ac->id))->get('activity_report_beneficiaries')->row();
                $female = $this->db->select_sum('female')->where(array('activity_report_id' => $ac->id))->get('activity_report_beneficiaries')->row();
                $combined = $this->db->select_sum('combined')->where(array('activity_report_id' => $ac->id))->get('activity_report_beneficiaries')->row();


                $m += $male->male;
                $f += $female->female;
                $c += $combined->combined;


            }


            $result = array(
                'male' => $m,
                'female' => $f,
                'combined' => $c,
                'total' => $m + $f + $c
            );


        }


        return json_decode(json_encode($result, JSON_PRETTY_PRINT));

    }



    function get_distinct_location($type)
    {

        $loc = $this->db->select("distinct($type) as location")->from('health_facilities')->get()->result();

        return $loc;

    }

    function get_resources($id = null, $field = 'id')
    {
        $this->db->select()->from('resources');
        if (isset($id) && strlen($id) > 0) {
            $output = $this->db->where(array($field => $id))->get()->row();
        } else {
            $output = $this->db->get()->result();
        }
        return $output;
    }

    function get_activity_type($id = null, $field = 'id')
    {
        $this->db->select()->from('settings_activity_type');
        if (isset($id) && strlen($id) > 0) {
            $output = $this->db->where(array($field => $id))->get()->row();
        } else {
            $output = $this->db->get()->result();
        }
        return $output;
    }


    function get_facilities()
    {

        return $this->db->select()->from('health_facilities')->get()->result();
    }


    function get_project_regions($project_id = null)
    {
        $this->db->select()->from('project_regions');

        if (isset($project_id)) {
            $this->db->where('id', $project_id);
            return $this->dd->get()->row();
        } else {

            return $this->db->get()->result();
        }
    }


}
