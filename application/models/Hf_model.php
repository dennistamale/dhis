<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 17/01/2018
 * Time: 17:03||2000 & 10000
 */

class hf_model extends CI_Model
{


    function __construct()
    {
        parent::__construct();
    }


    function get_project_regions($id=null)
    {

        $this->db->select()->from('project_regions');
        $result = isset($id) ? $this->db->where('id', $id)->get()->row() : $this->db->get()->result();
        return $result;

    }



    function get_hf_districts()
    {

        $regions = $this->hf_model->get_project_regions();

        $districts = array();

        foreach ($regions as $r) {

            $exp_district = explode(',', $r->districts);

            foreach ($exp_district as $x) {
                array_push($districts, $x);
            }


        }


        return $districts;

    }


    function get_district($country = 'UG', $district = null)
    {

        $this->db->select('state,title')->from('state');
        $this->db->where(array('country' => $country));
        $result = isset($district) ? $this->db->where('state', $district)->get()->row() : $this->db->get()->result();
        return $result;

    }


    function get_county($district = null)
    {

        $output = false;

        if (isset($district)) {
            $this->db->select('distinct(county)')->from('health_facilities');
            $this->db->where(array('district' => strtolower($district)));
            $output = $this->db->get()->result();
        }

        return $output;
    }


    function get_sub_county($county = null)
    {

        $output = false;

        if (isset($county)) {
            $this->db->select('distinct(sub_county)')->from('health_facilities');
            $this->db->where(array('county' => strtoupper($county)));
            $output = $this->db->get()->result();
        }

        return $output;
    }


    function get_parish($sub_county = null)
    {

        $output = false;

        if (isset($sub_county)) {
            $this->db->select('distinct(parish)')->from('health_facilities');
            $this->db->where(array('sub_county' => strtoupper($sub_county)));
            $output = $this->db->get()->result();
        }

        return $output;
    }

    function get_health_facilities($parish = null)
    {
        $output = false;

        if (isset($parish)) {
            $this->db->select()->from('health_facilities');
            $this->db->where(array('parish' => strtoupper($parish)));
            $output = $this->db->get()->result();
        }

        return $output;
    }


    function get_district_health_facilities($parish = null)
    {
        $output = false;

        if (isset($parish)) {
            $this->db->select()->from('health_facilities');
            $this->db->where(array('district' => strtoupper($parish)));
            $output = $this->db->get()->result();
        }

        return $output;
    }


    function get_single_hf($id = null)
    {
        $output = false;

        if (isset($id)) {
            $this->db->select()->from('health_facilities');
            $this->db->where(array('id' => $id));
            $output = $this->db->get()->row();
        }

        return $output;
    }


}