<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 15/09/2017
 * Time: 17:07
 */

class Dashboard_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();


    }

    function count_hfs()
    {
        $hfs = $this->db->from('health_facilities')->count_all_results();
        return $hfs;
    }

    function count_districts()
    {
        $hfs = $this->db->from('health_facilities')->group_by('district')->count_all_results();
        return $hfs;
    }

    function count_assessments()
    {
        $surveys = $this->db->from('survey')->count_all_results();
        return $surveys;
    }

    function count_activities()
    {
        $surveys = $this->db->from('activity_report')->count_all_results();
        return $surveys;
    }

    function recent_activities()
    {// this gets data from logs
        return $this->db->select('a.*,b.first_name,b.last_name')->from('logs a')->join('users b', 'a.created_by=b.id', 'left')->limit(7)->order_by('id', 'desc')->get()->result();

    }


    function latest_approvals()
    {
        return array();
    }

    function latest_applications()
    {
        return array();
    }

    function mission()
    {
        return array();
    }

    function application_types()
    {
        return array();
    }

    ////////////////this section encodes data for the activity Report////////////////////


    function total_beneficiaries($type=null)
    {

        $number=0;

        if (isset($type)) {
            $row = $this->db->select_sum($type)->from('activity_report_beneficiaries')->get()->row();
            $number= $type == 'male' ? $row->male : $row->female;
        } else {

            $female = $this->db->select_sum('female')->from('activity_report_beneficiaries')->get()->row();
            $male = $this->db->select_sum('male')->from('activity_report_beneficiaries')->get()->row();
            $number= $female->female + $male->male;
        }

        return $number;
    }



    function total_activity_report_districts(){

        $count=$this->db->from('activity_report')->group_by('district')->count_all_results();
        return $count;
    }

    function total_activity_report_hfs(){

        $count=$this->db->from('activity_report')->group_by('health_facility')->count_all_results();
        return $count;
    }


    /////////////////////this section encodes data from dhis2//////////////////////

    function period($indicator,$indicator_name,$period_type='weekly'){

        $p=array();
        $data=array();

        $table='report_malaria';


        if($this->input->post()){

            $this->load->model('dhis2_model');
           // print_r($this->input->post());

            $level = $_REQUEST['geo_level'];
            $org_unit = $_REQUEST['geo_level_items'];


            $d = $this->dhis2_model->get_malaria_data($period_type, $level, $org_unit);


            if ($d['alert'] != 'error') {
                $table='report_malaria_filter';
            }

        }



       $this->db->select('distinct(period) as period')->from($table);

        $period_type == 'weekly' ? $this->db->like('period', 'w') : $this->db->not_like('period', 'w');
        $this->db->order_by('SUBSTRING(`period`, 1,4) ASC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) ASC');
        $period_type=='weekly'?$this->db->limit(26,26):'';
        $period=$this->db->get()->result();




        foreach ($period as $pr ):

            $period_value=$pr->period;
        $indicator_value=$this->indicator_value($period_value,$indicator);

        array_push($p,$period_value);
        array_push($data,$indicator_value);

        endforeach;

        return json_decode(json_encode(array(
            'name'=>$indicator_name,
            'period'=>$p,
            'data'=>$data
        )));

    }


    function indicator_value($period_name=null,$indicator=null){
        $this->load->model('dhis2_model');

//        $value= rand(0,1000);


       $r= $this->input->post()?$this->dhis2_model->indicator_summary($period_name,$indicator, 'filtered'):$this->dhis2_model->indicator_summary($period_name,$indicator);

       $value = isset($r)?round($r,1):0;

        return $value;
    }

   

    function get_activity_updates($status=null)
    {
        $output = false;

      $this->db->select('a.id,a.status,b.name,count(a.status) as status_count')
                ->from('workplan_activity_status_updates a')
                ->join('update_status b','a.status=b.id')
                ->group_by('a.status');
    

                isset($status)?$this->db->where('a.status',$status):'';

                $output = $this->db->get()->result();

        return json_decode(json_encode($output));

    }


    function count_indicators(){
        $this->db->from('workplan_indicator_titles');
        $this->db->group_by('workplan_id');
        return $this->db->count_all_results();

    }




}