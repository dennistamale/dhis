<?php
/**
 * Created by PhpStorm.
 * User: DennisTamale
 * Date: 17/01/2018
 * Time: 17:03||2000 & 10000
 */

class Dhis2_model extends CI_Model
{


    function __construct()
    {
        parent::__construct();
    }


    function indicators($period = 'weekly')
    {

        switch ($period) {
            case 'weekly' :

                $u[0]['indicator'] = 'total_malaria_cases';
                $u[0]['name'] = 'Total Malaria Cases';

                $u[1]['indicator'] = 'total_deaths_cases';
                $u[1]['name'] = 'Total Deaths Cases';

                $u[2]['indicator'] = 'testing_rate';
                $u[2]['name'] = 'Testing Rate(%)';

                $u[3]['indicator'] = 'positive_rate';
                $u[3]['name'] = 'Test Positive Rate(%)';

                $u[4]['indicator'] = 'negative_treated_rate';
                $u[4]['name'] = '% Test Negative Treated';

                $u[5]['indicator'] = 'tt_positive_treated';//total positive to “confirmed malaria cases”
                $u[5]['name'] = 'Confirmed Malaria Cases';


                break;
            /*

            Malaria in Pregnancy
            UNDER 5 MALARIA CASES,
            Total malaria cases
            POSITIVE(CONFIRMED CASES)
            TEsted cases
            total_deaths_cases
             *
             */
            case 'monthly' :

                $u[0]['indicator'] = 'tested_cases';
                $u[0]['name'] = 'Tested Cases';//Tested cases

                $u[1]['indicator'] = 'monthly_total_malaria_cases';
                $u[1]['name'] = 'Total Malaria Cases';

                $u[2]['indicator'] = 'malaria_in_Pregnancy';
                $u[2]['name'] = 'Total Malaria in Pregnancy';

                $u[3]['indicator'] = 'under_5_malaria_cases';
                $u[3]['name'] = 'Under 5 Malaria Cases';//UNDER 5 MALARIA CASES

                $u[4]['indicator'] = 'total_tested_above_5months';
                $u[4]['name'] = 'Total Tested Above 5months';

                $u[5]['indicator'] = 'total_positive_confirmed_cases';
                $u[5]['name'] = 'Confirmed Cases';//POSITIVE(CONFIRMED CASES)

                $u[6]['indicator'] = 'monthly_total_deaths_cases';
                $u[6]['name'] = 'Total Deaths Cases';

                $u[8]['indicator'] = 'monthly_positive_rate';
                $u[8]['name'] = 'Test Positive Rate(%)';

                $u[9]['indicator'] = 'monthly_negative_treated_rate';
                $u[9]['name'] = '% Test Negative Treated';

                break;

        }
        return json_decode(json_encode($u));
    }

    function indicator_summary($period, $indicator,$filtered=null)
    {

//        print_r($period);
//        echo '<br/>';
//        print_r($indicator);
//        exit;

        $output = '';
        $weekly_tested_code = 'KPmTI3TGwZw';//KPmTI3TGwZw.uW3d2AqT4Aq//DataElements
        $weekly_treated_code = 'ZgPtploqq0L';//ZgPtploqq0L.RVczviqd1Vv//DataElements
        $weekly_cases = 'fclvwNhzu7d';//fclvwNhzu7d.gGhClrV5odI/DataElements
        $weekly_deaths = 'YXIu5CW9LPR';//YXIu5CW9LPR.gGhClrV5odI//DataElements

        //this is the section for monthly
        $monthly_total_cases = 'Q62QyMDzm3h';//Q62QyMDzm3h.ZIk9srZ7KTh
        $monthly_total_cases2 = 'vPkn90NTWgc';//vPkn90NTWgc.mo1vyeED2OC

        $monthly_total_deaths = 'J4wj2B8rV6P';//J4wj2B8rV6P.ZIk9srZ7KTh

        $monthly_malaria_in_Pregnancy = 'adF5g61P6YH';//adF5g61P6YH.ZIk9srZ7KTh

        $under_5_malaria_cases = 'Q62QyMDzm3h';//Q62QyMDzm3h.ZIk9srZ7KTh

        $monthly_tested_above_5months = 'vPkn90NTWgc';//vPkn90NTWgc.mo1vyeED2OC

        $monthly_positive_confirmed_cases = 'STF7P24RAqC';//STF7P24RAqC.Li89EZS6Jss
        $monthly_positive_confirmed_cases2 = 'fOZ2QUjRSB7';//fOZ2QUjRSB7.Li89EZS6Jss

        $monthly_malaria_microscopy1 = 'STF7P24RAqC';//STF7P24RAqC.snPZPBW4ZW0
        $monthly_malaria_microscopy2 = 'fOZ2QUjRSB7';//fOZ2QUjRSB7.snPZPBW4ZW0

        switch ($indicator) {


            case 'total_malaria_cases':
                $o = $this->total_malaria_cases($weekly_cases, $period,$filtered);
                $output = $o->value;
                break;

            case 'total_deaths_cases':
                $o = $this->total_deaths_cases($weekly_deaths, $period,$filtered);
                $output = $o->value;
                break;

            case 'positive_rate':


                $tt_tested = $this->tt_tested($weekly_tested_code, $period,$filtered);

                $tt_positive = $this->tt_positive($weekly_tested_code, $period,$filtered);

                $testing_rate = $tt_tested->value > 0 ? ($tt_positive->value / $tt_tested->value) * 100 : 0;


                $output = round($testing_rate, 2);

                break;

            case 'negative_treated_rate':
                //Total negative
                $tt_tested = $this->tt_tested($weekly_tested_code, $period,$filtered);
                $tt_positive = $this->tt_positive($weekly_tested_code, $period,$filtered);
                $negative = $tt_tested->value - $tt_positive->value;

                //total Negative Treated

                $o = $this->tt_negative_treated($weekly_treated_code, $period,$filtered);
                $negative_treated = $o->value;


                $negative_treated = $negative > 0 ? ($negative_treated / $negative) * 100 : 0;

                $output = round($negative_treated, 2);

                break;

            case 'tt_positive_treated':
                $o = $this->tt_positive_treated($weekly_treated_code, $period,$filtered);
                $output = $o->value;
                break;

            case 'testing_rate':
                $tt_tested = $this->tt_tested($weekly_tested_code, $period,$filtered);


                $tt_suspected = $this->tt_suspected($weekly_tested_code, $period,$filtered);


                $testing_rate = $tt_suspected->value > 0 ? ($tt_tested->value / $tt_suspected->value) * 100 : '';

                $output = round($testing_rate, 2);

                break;


            //below is the monthly section

            case 'monthly_total_malaria_cases':
                $o = $this->total_malaria_cases($monthly_total_cases, $period,$filtered);
                $o2 = $this->total_malaria_cases($monthly_total_cases2, $period,$filtered);
                $output = $o->value + $o2->value;
                break;

            case 'malaria_in_Pregnancy':
                $o = $this->malaria_in_Pregnancy($monthly_malaria_in_Pregnancy, $period,$filtered);
                $output = $o->value;
                break;


            case 'total_positive_confirmed_cases':
                $o = $this->total_positive_confirmed_cases($monthly_positive_confirmed_cases, $period,$filtered);
                $o2 = $this->total_positive_confirmed_cases($monthly_positive_confirmed_cases2, $period,$filtered);
                $output = $o->value + $o2->value;
                break;

            case 'tested_cases'://Lab Malaria Microscopy
                $o = $this->malaria_microscopy($monthly_malaria_microscopy1, $period,$filtered);
                $o2 = $this->malaria_microscopy($monthly_malaria_microscopy2, $period,$filtered);
                $output = $o->value + $o2->value;
                break;

            case 'monthly_total_deaths_cases':
                $o = $this->total_deaths_cases($monthly_total_deaths, $period,$filtered);
                $output = $o->value;
                break;

            case 'under_5_malaria_cases':
                $o = $this->under_5_malaria_cases($under_5_malaria_cases, $period,$filtered);

                $output = $o->value;
                break;

            case 'total_tested_above_5months':
                $o = $this->total_tested_above_5months($monthly_tested_above_5months, $period,$filtered); //vPkn90NTWgc.btCIMFNK3gf
                $output = $o->value;
                break;


            case 'monthly_positive_rate':

                $tt_tested_confirmed1 = $this->total_positive_confirmed_cases($monthly_positive_confirmed_cases, $period,$filtered);
                $tt_tested_confirmed2 = $this->total_positive_confirmed_cases($monthly_positive_confirmed_cases2, $period,$filtered);
                $tt_tested_confirmed = $tt_tested_confirmed1->value + $tt_tested_confirmed2->value;

                $tested_cases1 = $this->total_malaria_cases($monthly_total_cases, $period,$filtered);
                $tested_cases2 = $this->total_malaria_cases($monthly_total_cases2, $period,$filtered);
                $tested_cases = $tested_cases1->value + $tested_cases2->value;


                $testing_rate = $tested_cases > 0 ? ($tt_tested_confirmed / $tested_cases) * 100 : 0;


                $output = round($testing_rate, 2);

                break;

            case 'monthly_negative_treated_rate':
                //Total negative
                $tt_tested = $this->tt_tested($weekly_tested_code, $period,$filtered);
                $tt_positive = $this->tt_positive($weekly_tested_code, $period,$filtered);
                $negative = $tt_tested->value - $tt_positive->value;

                //total Negative Treated

                $o = $this->tt_negative_treated($weekly_treated_code, $period,$filtered);
                $negative_treated = $o->value;


                $negative_treated = $negative > 0 ? ($negative_treated / $negative) * 100 : 0;

                $output = round($negative_treated, 2);

                break;

            case 'monthly_tt_positive_treated':
                $o = $this->tt_positive_treated($weekly_treated_code, $period,$filtered);
                //$output = $o->value;
                break;


        }

        return $output;


    }

    function total_malaria_cases($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));

//
//        $this->db->group_start();
//
//        $this->db->or_where(
//            array(
//                'categoryOptionCombo' => 't8Jv78lnSbU',// WEP Microscopy Tested Cases
//                'categoryOptionCombo' => 'kcXY5cWAOsB',//WEP RDT Tested Cases
//            )
//        );
//        $this->db->group_end();

        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');


        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }

    function total_positive_confirmed_cases($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


        $this->db->group_start();

        $this->db->or_where(
            array(
                'categoryOptionCombo' => 'Li89EZS6Jss',// Positve confirmed cases STF7P24RAqC.Li89EZS6Jss
                'categoryOptionCombo' => 'PT9vhPUpxc4',//Positve confirmed cases STF7P24RAqC.PT9vhPUpxc4
            )
        );
        $this->db->group_end();



        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function malaria_in_Pregnancy($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));

//
//        $this->db->group_start();
//
//        $this->db->or_where(
//            array(
//                'categoryOptionCombo' => 't8Jv78lnSbU',// WEP Microscopy Tested Cases
//                'categoryOptionCombo' => 'kcXY5cWAOsB',//WEP RDT Tested Cases
//            )
//        );
//        $this->db->group_end();

        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function malaria_microscopy($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


        $this->db->group_start();

        $this->db->or_where(
            array(
                'categoryOptionCombo' => 'bSUziAZFlqN',// STF7P24RAqC.bSUziAZFlqN Microscopy Tested Cases
                'categoryOptionCombo' => 'snPZPBW4ZW0',//STF7P24RAqC.snPZPBW4ZW0 RDT Tested Cases
            )
        );
        $this->db->group_end();

        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function under_5_malaria_cases($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


        $this->db->group_start();

        $this->db->or_where(
            array(
                'categoryOptionCombo' => 'ZIk9srZ7KTh',// Q62QyMDzm3h.ZIk9srZ7KTh
                'categoryOptionCombo' => 'N6sVfgBHDhx',//Q62QyMDzm3h.N6sVfgBHDhx
                'categoryOptionCombo' => 'z5Yltji9KtS',//Q62QyMDzm3h.z5Yltji9KtS
                'categoryOptionCombo' => 'gaJzDinonuC',//Q62QyMDzm3h.gaJzDinonuC
            )
        );
        $this->db->group_end();

        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function total_tested_above_5months($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


        $this->db->group_start();

        $this->db->or_where(
            array(
                'categoryOptionCombo' => 'btCIMFNK3gf',// WEP Microscopy Tested CasesvPkn90NTWgc.btCIMFNK3gf
                'categoryOptionCombo' => 'mo1vyeED2OC',//WEP RDT Tested Cases vPkn90NTWgc.mo1vyeED2OC
            )
        );
        $this->db->group_end();

//        return $this->db->get('report_malaria')->row();
        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function total_deaths_cases($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));

//
//        $this->db->group_start();
//
//        $this->db->or_where(
//            array(
//                'categoryOptionCombo' => 't8Jv78lnSbU',// WEP Microscopy Tested Cases
//                'categoryOptionCombo' => 'kcXY5cWAOsB',//WEP RDT Tested Cases
//            )
//        );
//        $this->db->group_end();

        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');
        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function tt_tested($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


        $this->db->group_start();

        $this->db->or_where(
            array(
                'categoryOptionCombo' => 't8Jv78lnSbU',// WEP Microscopy Tested Cases
                'categoryOptionCombo' => 'kcXY5cWAOsB',//WEP RDT Tested Cases
            )
        );
        $this->db->group_end();

        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');

        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function tt_positive($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));

        $this->db->group_start();
        $this->db->or_where(
            array(
                'categoryOptionCombo' => 'urYpFjLrrYa',// WEP Microscopy Positive Cases
                'categoryOptionCombo' => 't8Jv78lnSbU',//WEP Positive Cases 5+ Years
                'categoryOptionCombo' => 'Kp9EiDyxPNp',//WEP Positve Cases Under 5 Years
                'categoryOptionCombo' => 'PfdPCxOoiBj'//WEP RDT Positve Cases
            )
        );
        $this->db->group_end();

        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');
        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function tt_negative_treated($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


        $this->db->where(
            array(
                'categoryOptionCombo' => 'NfJyyb1eBaw',// WEP RDT Negative Cases Treated
            )
        );


        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');
        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function tt_suspected($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));


//        KPmTI3TGwZw.uW3d2AqT4Aq
        $this->db->where(
            array(
                'categoryOptionCombo' => 'uW3d2AqT4Aq',// WEP Suspected Malaria (fever)
            )
        );


        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');
        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function tt_positive_treated($tested_code, $period,$filtered)
    {

        $this->db->select_sum('value')->where(array(
            'dataElement' => $tested_code,
            'period' => $period
        ));

        $this->db->where(
            array(
                'categoryOptionCombo' => 'QOWAmZ7jBno',// WEP RDT Negative Cases Treated
            )
        );

        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');
        return $this->db->get((isset($filtered)?'report_malaria_filter':'report_malaria'))->row();

    }


    function periodic_dimension($type = 'weekly')
    {
        $type = $type == 'weekly' ? 'LAST_52_WEEKS' : 'LAST_12_MONTHS';

        $this->db->select()->from('report_dimension');
        $this->db->where(array('type' => $type));
        if (isset($id)) {

            $this->db->where(array('id' => $id));
            $output = $this->db->get()->row();
        } else {
            $output = $this->db->get()->result();
        }

        return $output;
    }

    function get_weekly_report($period, $dimension_id)
    {

        //$period= $period=='weekly'?'LAST_52_WEEKS':'LAST_12_MONTHS';

        $d = explode('.', $dimension_id);


        $this->db->select_sum('value')->where(
            array(
                'dataElement' => $d[0],
                'categoryOptionCombo' => $d[1],
                'period' => $period
            )
        );

        $this->db->order_by('SUBSTRING(`period`, 1,4) DESC , CONVERT(SUBSTRING(`period`, 6),UNSIGNED INTEGER) DESC');
        $r = $this->db->get('report_malaria')->row();


        return isset($r->value) ? $r->value : 0;

    }

    function update_dimension($json_data)
    {

        $j = json_decode($json_data);


        $columns = $j[0]->columns;
        $rows = $j[0]->rows;
        $period = $rows[0]->items[0]->id;

///creating dimensions
        if ($this->create_dimension($columns, $period)) {
            $dimension = 'Dimension updated';
        } else {
            $dimension = 'error Updating Dimension';
        }
///creating organisation units
        if ($this->create_org_unit($rows, $period)) {
            $org_unit = 'Organisation unit updated';
        } else {
            $org_unit = 'error Updating Organisation unit';
        }


        return array(
            'dimension' => $dimension,
            'org_unit' => $org_unit,
        );


    }

    function create_dimension($column, $period)
    {

        $items = $column[0]->items;
        $dimension = $column[0]->dimension;


        $values = array();

        foreach ($items as $d) {

            $value = array(
                'id' => $d->id,
                'name' => $d->name,
                'dimension' => $dimension,
                'type' => $period,
            );

            array_push($values, $value);

        }

        $this->db->like('type', $period)->delete('report_dimension');

        if ($this->db->insert_batch('report_dimension', $values)) {
            return true;
        } else {
            return false;
        }


    }

    function create_org_unit($rows, $period)
    {


        $org_unit = $rows[1]->items;
        $dimension = $rows[1]->dimension;


        $values = array();
        foreach ($org_unit as $d) {
            $value = array(
                'id' => $d->id,
                'name' => $d->name,
                'period' => $period,
                'dimension' => $dimension
            );

            array_push($values, $value);

        }


        $this->db->like('period', $period)->delete('report_org_unit');

        if ($this->db->insert_batch('report_org_unit', $values)) {
            return true;
        } else {
            return false;
        }

    }

    function create_org_unit_with_level($level, $data)
    {

        $this->db->where('level', $level)->delete('dhis_organisation_unit');


        $values = array();
        foreach ($data as $d) {


            $value = array(
                'id' => $d->id,
                'name' => $d->name,
                'shortName' => $d->shortName,
                'openingDate' => $d->openingDate,
                'parent' => isset($d->parent->id) ? $d->parent->id : '',
                'level' => $level
            );

            array_push($values, $value);

        }


        if ($this->db->insert_batch('dhis_organisation_unit', $values)) {
            return true;
        } else {
            return false;
        }


    }

    function get_resource_values($value = null, $field = null, $type = null)
    {
        $this->db->select()->from('dhis_resource_values');

        isset($type) ? $this->db->where(array('resource_type' => $type, 'status' => 1)) : '';

        if (isset($value) && strlen($value) > 0) {
            $this->db->where(array($field => $value));

            $output = $field == 'id' || $field == 'dhis_id' ? $this->db->get()->row() : $this->db->get()->row();
        } else {
            $output = $this->db->get()->result();
        }
        return $output;
    }

    function get_organisation_unit($value = null, $field = null, $parent = null)
    {
        $this->db->select()->from('dhis_organisation_unit');

        isset($parent) ? $this->db->where(array('parent' => $parent)) : '';

        if (isset($value) && strlen($value) > 0) {
            $this->db->where(array($field => $value));

            $output = $field == 'id' || $field == 'dhis_id' ? $this->db->get()->row() : $this->db->get()->result();
        } else {
            $output = $this->db->get()->result();
        }
        return $output;
    }


    function get_malaria_data($update_type = null, $level = null, $org_unit = null)
    {

        $this->load->helper('dhis2_json');

        $alert = 'error';
        $message = 'All Parameters are required';

        if (isset($update_type) && isset($level) && isset($org_unit)) {

            if ($update_type == 'weekly' || $update_type == 'monthly') {


                /*
    SPecific for the Period
    https://hmis1.health.go.ug/hmis2/api/25/analytics.json?dimension=dx:Q62QyMDzm3h.ZIk9srZ7KTh;Q62QyMDzm3h.N6sVfgBHDhx;Q62QyMDzm3h.z5Yltji9KtS;Q62QyMDzm3h.gaJzDinonuC;Q62QyMDzm3h.jft3ntNlnPJ;Q62QyMDzm3h.c5w8FybAf6t;Q62QyMDzm3h.PS4qYEgqQmx;Q62QyMDzm3h.dHjWd4es7e4;J4wj2B8rV6P.ZIk9srZ7KTh;J4wj2B8rV6P.N6sVfgBHDhx;J4wj2B8rV6P.z5Yltji9KtS;J4wj2B8rV6P.gaJzDinonuC;J4wj2B8rV6P.jft3ntNlnPJ;J4wj2B8rV6P.c5w8FybAf6t;J4wj2B8rV6P.PS4qYEgqQmx;J4wj2B8rV6P.dHjWd4es7e4;adF5g61P6YH.ZIk9srZ7KTh;adF5g61P6YH.N6sVfgBHDhx;adF5g61P6YH.z5Yltji9KtS;adF5g61P6YH.gaJzDinonuC;adF5g61P6YH.jft3ntNlnPJ;adF5g61P6YH.c5w8FybAf6t;adF5g61P6YH.PS4qYEgqQmx;adF5g61P6YH.dHjWd4es7e4;M3l4H6S7nLR.Avvv8JaSwGR;M3l4H6S7nLR.BhwinOOCTmR;M3l4H6S7nLR.JhQChFHlaNM;STF7P24RAqC.snPZPBW4ZW0;STF7P24RAqC.bSUziAZFlqN;STF7P24RAqC.Li89EZS6Jss;STF7P24RAqC.PT9vhPUpxc4;fOZ2QUjRSB7.snPZPBW4ZW0;fOZ2QUjRSB7.bSUziAZFlqN;fOZ2QUjRSB7.Li89EZS6Jss;fOZ2QUjRSB7.PT9vhPUpxc4;KLvfkPOHRin.gGhClrV5odI;Ug4xX8noNlY.gGhClrV5odI;ps7Cj6JKM64.z1I5NwTvSkt;ps7Cj6JKM64.Bh0JlVT2QRL;ps7Cj6JKM64.eqTLHZ8pEIY;ps7Cj6JKM64.nZpDoUKfYRQ;ps7Cj6JKM64.FwsObyc5Dt2;ps7Cj6JKM64.bGq9J9mHODP;ps7Cj6JKM64.OWku4zsdztD;ps7Cj6JKM64.eA3YLuCoJgJ;vlmLOxXIs6Z.BNx5feudBi7;vlmLOxXIs6Z.Gqu7GLDWYxg&dimension=ou:LEVEL-2;VA90IqaI4Ji&dimension=pe:LAST_12_MONTHS&displayProperty=NAME&outputIdScheme=UID
    https://hmis1.health.go.ug/hmis2/api/25/analytics.json?dimension=dx:Q62QyMDzm3h;J4wj2B8rV6P;adF5g61P6YH;M3l4H6S7nLR;STF7P24RAqC;fOZ2QUjRSB7;KLvfkPOHRin;Ug4xX8noNlY;ps7Cj6JKM64;vlmLOxXIs6Z;Gg4H29c3VpF;l5Z1EvnZY3X;aQnDfHYuztX;d2hY0aD0QDx;h6mZvUrazZW;vPkn90NTWgc;PvC0Ev7BQro;Fql8Zd4Pa2d;fclvwNhzu7d;YXIu5CW9LPR;KPmTI3TGwZw;ZgPtploqq0L;CzsKMvQDwMV&dimension=ou:LEVEL-2;VA90IqaI4Ji&dimension=pe:LAST_12_MONTHS&displayProperty=NAME&outputIdScheme=UID
    https://hmis1.health.go.ug/hmis2/api/25/analytics.json?dimension=dx:Q62QyMDzm3h;J4wj2B8rV6P;adF5g61P6YH;M3l4H6S7nLR;STF7P24RAqC;fOZ2QUjRSB7;KLvfkPOHRin;Ug4xX8noNlY;ps7Cj6JKM64;vlmLOxXIs6Z;Gg4H29c3VpF;l5Z1EvnZY3X;aQnDfHYuztX;d2hY0aD0QDx;h6mZvUrazZW;vPkn90NTWgc;PvC0Ev7BQro;Fql8Zd4Pa2d;fclvwNhzu7d;YXIu5CW9LPR;KPmTI3TGwZw;ZgPtploqq0L;CzsKMvQDwMV&dimension=co&dimension=ou:LEVEL-2;VA90IqaI4Ji&dimension=pe:LAST_12_MONTHS&displayProperty=NAME&hierarchyMeta=true&outputIdScheme=UID
                Weekly
    https://hmis1.health.go.ug/hmis2/api/25/analytics.json?dimension=dx:fclvwNhzu7d.gGhClrV5odI;YXIu5CW9LPR.gGhClrV5odI;KPmTI3TGwZw.urYpFjLrrYa;KPmTI3TGwZw.t8Jv78lnSbU;KPmTI3TGwZw.Kp9EiDyxPNp;KPmTI3TGwZw.t4m0M8uKbwF;KPmTI3TGwZw.PfdPCxOoiBj;KPmTI3TGwZw.kcXY5cWAOsB;KPmTI3TGwZw.uW3d2AqT4Aq;ZgPtploqq0L.Mswd99t7UbA;ZgPtploqq0L.spSdFB7CVOu;ZgPtploqq0L.NfJyyb1eBaw;ZgPtploqq0L.QOWAmZ7jBno&dimension=ou:LEVEL-2;akV6429SUqu&dimension=pe:LAST_52_WEEKS&displayProperty=NAME

                 */


                $level = 'LEVEL-' . $level;
                $selected_org_unit =$org_unit; //'VA90IqaI4Ji';

                $period_monthly = 'LAST_12_MONTH';
                $period_weekly = 'LAST_52_WEEKS';


                $url_monthly = '25/analytics.json?dimension=dx:Q62QyMDzm3h.ZIk9srZ7KTh;Q62QyMDzm3h.N6sVfgBHDhx;Q62QyMDzm3h.z5Yltji9KtS;Q62QyMDzm3h.gaJzDinonuC;Q62QyMDzm3h.jft3ntNlnPJ;Q62QyMDzm3h.c5w8FybAf6t;Q62QyMDzm3h.PS4qYEgqQmx;Q62QyMDzm3h.dHjWd4es7e4;J4wj2B8rV6P.ZIk9srZ7KTh;J4wj2B8rV6P.N6sVfgBHDhx;J4wj2B8rV6P.z5Yltji9KtS;J4wj2B8rV6P.gaJzDinonuC;J4wj2B8rV6P.jft3ntNlnPJ;J4wj2B8rV6P.c5w8FybAf6t;J4wj2B8rV6P.PS4qYEgqQmx;J4wj2B8rV6P.dHjWd4es7e4;adF5g61P6YH.ZIk9srZ7KTh;adF5g61P6YH.N6sVfgBHDhx;adF5g61P6YH.z5Yltji9KtS;adF5g61P6YH.gaJzDinonuC;adF5g61P6YH.jft3ntNlnPJ;adF5g61P6YH.c5w8FybAf6t;adF5g61P6YH.PS4qYEgqQmx;adF5g61P6YH.dHjWd4es7e4;M3l4H6S7nLR.Avvv8JaSwGR;M3l4H6S7nLR.BhwinOOCTmR;M3l4H6S7nLR.JhQChFHlaNM;STF7P24RAqC.snPZPBW4ZW0;STF7P24RAqC.bSUziAZFlqN;STF7P24RAqC.Li89EZS6Jss;STF7P24RAqC.PT9vhPUpxc4;fOZ2QUjRSB7.snPZPBW4ZW0;fOZ2QUjRSB7.bSUziAZFlqN;fOZ2QUjRSB7.Li89EZS6Jss;fOZ2QUjRSB7.PT9vhPUpxc4;KLvfkPOHRin.gGhClrV5odI;Ug4xX8noNlY.gGhClrV5odI;ps7Cj6JKM64.z1I5NwTvSkt;ps7Cj6JKM64.Bh0JlVT2QRL;ps7Cj6JKM64.eqTLHZ8pEIY;ps7Cj6JKM64.nZpDoUKfYRQ;ps7Cj6JKM64.FwsObyc5Dt2;ps7Cj6JKM64.bGq9J9mHODP;ps7Cj6JKM64.OWku4zsdztD;ps7Cj6JKM64.eA3YLuCoJgJ;vlmLOxXIs6Z.BNx5feudBi7;vlmLOxXIs6Z.Gqu7GLDWYxg&dimension=ou:' . $level . ';' . $selected_org_unit . '&dimension=pe:' . $period_monthly . 'S&displayProperty=NAME';
                $url_weekly = '25/analytics.json?dimension=dx:fclvwNhzu7d.gGhClrV5odI;YXIu5CW9LPR.gGhClrV5odI;KPmTI3TGwZw.urYpFjLrrYa;KPmTI3TGwZw.t8Jv78lnSbU;KPmTI3TGwZw.Kp9EiDyxPNp;KPmTI3TGwZw.t4m0M8uKbwF;KPmTI3TGwZw.PfdPCxOoiBj;KPmTI3TGwZw.kcXY5cWAOsB;KPmTI3TGwZw.uW3d2AqT4Aq;ZgPtploqq0L.Mswd99t7UbA;ZgPtploqq0L.spSdFB7CVOu;ZgPtploqq0L.NfJyyb1eBaw;ZgPtploqq0L.QOWAmZ7jBno&dimension=ou:' . $level . ';' . $selected_org_unit . '&dimension=pe:' . $period_weekly . '&displayProperty=NAME';


                if ($update_type == 'weekly') {
                    $data_to_update = $url_weekly;
                } elseif ($update_type == 'monthly') {
                    $data_to_update = $url_monthly;
                }




                $json_data = get_dhis2_Array($data_to_update);


//                var_dump($json_data);

               if($json_data!='null') {


                   $j = json_decode($json_data);

//                print_r($j->rows);
//
//                exit;
//

                   if (!isset($j->status)) {


                       /*
                        0-dataElement.categoryOptionCombo

                       1-orgUnit
                       2-period
                       3-value
                        */


//                    $dataValues = $j->dataValues;

                       $dataValues = array();


                       foreach ($j->rows as $r) {

                           $data = explode('.', $r[0]);


                           $value = array(
                               'dataElement' => $data[0],
                               'categoryOptionCombo' => $data[1],
                               'orgUnit' => $r[1],
                               'period' => $r[2],
                               'value' => $r[3],
                           );


                           array_push($dataValues, $value);

                       }


//                    print_r($dataValues);
//
//                    exit;

                       if (count($dataValues) > 0) {

                           if ($update_type == 'weekly') {
                               $this->db->like('period', 'W');//weekly
                           } elseif ($update_type == 'monthly') {
                               $this->db->not_like('period', 'W');//monthly
                           }

                           $this->db->delete('report_malaria_filter');

                           if ($this->db->insert_batch('report_malaria_filter', $dataValues)) {
                               $alert = 'success';
                               $message = 'Date inserted successfully';
                           }


                       } else {
                           $message = 'No data to insert';
                       }
                   } else {
                       $message = $j->status->message;
                   }
               }else{
                   $message = 'Error Connecting....';
               }

            }
        }

        return array(
            'alert' => $alert,
            'message' => $message
        );

    }


}