<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This converts the 1000s to Ks
 * converts Millions to Ms
 *
 * */


if (!function_exists('CI_form_builder')) {

    function CI_form_builder($myForm = null,$type='horizontal')
    {

        $form='';
        if(isset($myForm)) {

            if($type=='vertical'){

                foreach ($myForm as $key => $val) {
                    $form .= '<div class="form-group">';

                    if ($val['type'] == 'textarea') {

                        $form .= '<div class="col-lg-12">';
                        $form .= '<label>' . $val['label'] . '</label>';
                        $form .= form_textarea($val);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    } elseif ($val['type'] == 'hidden') {

                        $form .= '<div class="col-lg-12">';
                        $form .= '<label>' . $val['label'] . '</label>';
                        $form .= form_input($val);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    } elseif ($val['type'] == 'checkbox') {

                        $form .= '<div class="col-lg-12">';
                        $form .= '<label class="display-block">' . $val['label'] . '</label>';
                        foreach ($val['options'] as $checkbox) {
                            $form .= ' <label class="radio-inline">';
                            $form .= form_checkbox($checkbox);
                            $form .= $checkbox['label'];
                            $form .= '</label>';
                        }
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    }elseif ($val['type'] == 'radio') {

                        $form .= '<div class="col-lg-12">';
                        $form .= '<label class="display-block">' . $val['label'] . '</label>';
                        foreach ($val['options'] as $radio) {
                            $form .= ' <label class="radio-inline">';
                            $form .= form_radio($radio);
                            $form .= $radio['label'];
                            $form .= '</label>';
                        }
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    }  elseif ($val['type'] == 'select') {

                        $other_attributes=array('class'=>'select');

                        $form .= '<div class="col-lg-12">';
                        $form .= '<label>' . $val['label'] . '</label>';
                        $form .= form_dropdown($val['name'], $val['options'], $val['selected'],$other_attributes);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    } elseif ($val['type'] == 'submit') {

                        $form .= '<div class="col-lg-12 text-right">';
                        $form .= '<label ></label>';
                        $form .= '<button type="reset" class="btn btn-default" style="margin-right: 5px;">Cancel <i class="icon-blocked position-right"></i></button>';
                        $form .= '<button type="submit" name="' . $val['name'] . '" class="btn btn-success">' . $val['label'] . ' <i class="icon-floppy-disk position-right"></i></button>';
                        $form .= '</div>';

                    } else {


                        $form .= '<div class="col-lg-12">';
                        $form .= '<label>' . $val['label'] . '</label>';
                        $form .= form_input($val);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    }


                    $form .= ' </div>';

                }

            }else {

                foreach ($myForm as $key => $val) {
                    $form .= '<div class="form-group">';

                    if ($val['type'] == 'textarea') {
                        $form .= '<label class="col-lg-4 control-label">' . $val['label'] . '</label>';
                        $form .= '<div class="col-lg-8">';
                        $form .= form_textarea($val);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';
                    } elseif ($val['type'] == 'radio') {
                        $form .= '<label class="col-lg-4 control-label">' . $val['label'] . '</label>';
                        $form .= '<div class="col-lg-8">';
                        foreach ($val['options'] as $radio) {
                            $form .= ' <label class="radio-inline">';
                            $form .= form_radio($radio);
                            $form .= $radio['label'];
                            $form .= '</label>';
                        }
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';
                    } elseif ($val['type'] == 'select') {


                        $other_attributes=array('class'=>'select');

                        $form .= '<label class="col-lg-4 control-label">' . $val['label'] . '</label>';
                        $form .= '<div class="col-lg-8">';
                        $form .= form_dropdown($val['name'], $val['options'], $val['selected'],$other_attributes);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';

                    } elseif ($val['type'] == 'submit') {
                        $form .= '<label class="col-lg-4 control-label"></label>';
                        $form .= '<div class="col-lg-8 text-right">';
                        $form .= '<button type="reset" class="btn btn-default" style="margin-right: 5px;">Cancel <i class="icon-blocked position-right"></i></button>';
                        $form .= '<button type="submit" name="' . $val['name'] . '" class="btn btn-success">' . $val['label'] . ' <i class="icon-floppy-disk position-right"></i></button>';
                        $form .= '</div>';
                    } else {
                        $form .= '<label class="col-lg-4 control-label">' . $val['label'] . '</label>';
                        $form .= '<div class="col-lg-8">';
                        $form .= form_input($val);
                        $form .= form_error($val['name'], '<span class="text-danger">', '</span>');
                        $form .= '</div>';
                    }

                    $form .= ' </div>';

                }
            }


        }else{
            $form .='<span class="text-danger">No form Created...</span>';
        }

        return $form;

    }
}




?>