<?php
/**
 * You Pass the extra url after that base url, The base url is already catered for in the code
 *
 * Also you can echo that data instead of return
 */
if (!function_exists('get_dhis2_Array')) {
    function get_dhis2_Array($url)
    {

    /*
            Example URLS

            $url .= "organisationUnits";
            $url .= "programDataElements"; or programDataElements?page=2


            */

            $CI =& get_instance();
            $CI->load->library('site_options');

            $api_username=$CI->site_options->title('dhis_api_username');
            $api_password=$CI->site_options->title('dhis_api_password');

            


        $base_url = "https://hmis1.health.go.ug/hmis2/api/";

        $post_string_b64 = base64_encode("$api_username:$api_password");
        $auth = 'Authorization: Basic ' . $post_string_b64;




        $ch = curl_init($base_url.$url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array($auth));

        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        $html = curl_exec($ch);

        $pattern = '/\{(.+)\}/';



        preg_match($pattern, $html, $matches);

//        header('Content-Type: application/json');

        return $json = json_encode(json_decode($matches[0], true));

//        return $json['organisationUnits'];
    }
}


if(!function_exists('tt_tested')){
    function tt_tested(){
        

    }
}


?>