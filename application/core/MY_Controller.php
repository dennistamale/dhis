<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";
    public $page_level3 = "";
    public $notification = array();
    public $view_data = array();
    public $site_name = '';
    public $site_email = '';

    function __construct()
    {
        parent::__construct();

        $this->site_name = $this->site_options->title('site_name');
        $this->site_email = $this->site_options->title('contact_email');


        $this->page_level = $this->uri->slash_segment(1);
        $this->page_level2 = $this->uri->slash_segment(2);
        $this->page_level3 = $this->uri->slash_segment(3);
        // $this->load->library('cart');
//            $this->output->enable_profiler(TRUE);

//        $n=30;
//        $this->output->cache($n);
        //$this->page_level2 != 'logout' ? $this->verified() : '';


    }

    function verified()
    {
        if ($this->session->verified == 0 && $this->session->user_type == 5) {

            $page = $this->uri->segment(2);
            $page != 'profile' ? redirect($this->page_level . 'profile/change_password') : '';
        }
    }

    public function isloggedin()
    {
        return strlen($this->session->user_type) > 0 ? true : false;

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('', 'refresh');
        //echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/login>';
    }

    public function multiple_upload($prefered_name = 'image')
    {
        //$this->load->library('upload');
        $number_of_files_uploaded = count($_FILES['userfile']['name']);
        // Faking upload calls to $_FILE
        for ($i = 0; $i < $number_of_files_uploaded; $i++) :
            $_FILES['userfile']['name'] = $_FILES['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $_FILES['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $_FILES['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $_FILES['userfile']['size'][$i];
            $config = array(
                'file_name' => $prefered_name,
                'allowed_types' => 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG',
                'max_size' => 3000,
                'overwrite' => FALSE,

                /* real path to upload folder ALWAYS */
                'upload_path' => './uploads/arrival/'
            );
            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) :
                $error = array('error' => $this->upload->display_errors());
                $error;
            //$this->load->view('upload_form', $error);
            else :
                $final_files_data[] = $this->upload->data();
                // Continue processing the uploaded data
            endif;
        endfor;
    }

    public function reports($type = 'general', $id = null)
    {

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $root = $this->page_level . $this->page_level2;

        switch ($type) {

            default:

                $this->form_validation
                    ->set_rules('date_range', 'Date Range', 'trim|required')
                    ->set_rules('location', 'Location', 'trim');

                if ($this->form_validation->run() == true) {

                    $dates = explode('-', $this->input->post('date_range'));

                    $data['dates'] = $dates;
                    $from = trim($dates[0]);
                    $to = trim($dates[1]);
                    $location = $this->input->post('location');
                    $from = strtotime(date($from));
                    $to = strtotime(date($to));


                } else {
                    $from = strtotime('monday this week');
                    $to = strtotime('sunday this week');

                    $location = '';

                }

                $data['from'] = date('m/d/Y', $from);
                $data['to'] = date('m/d/Y', $to);
                $data['location'] = $location;


                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'general', $data, true);
                break;

            case 'data_per_health_facility':


                $week = $data['week_no'] = isset($_REQUEST["week_no"]) ? $_REQUEST["week_no"] : date('W') - 1;
                $year = $data['year'] = isset($_REQUEST["year"]) ? $_REQUEST["year"] : date('Y');

                $getStartAndEndDate = getStartAndEndDate($week, $year);

                $from = $getStartAndEndDate[0] . ' 00:00:00';
                $to = $getStartAndEndDate[1] . ' 23:59:59';

                $from = strtotime(date($from));
                $to = strtotime(date($to));


                $this->form_validation
                    ->set_rules('week_no', 'Week No', 'trim|required')
                    ->set_rules('year', 'Year', 'trim|required')
                    ->set_rules('facility', 'Facility', 'trim')
                    ->set_rules('location', 'Location', 'trim');

                if ($this->form_validation->run() == true) {


                    // $dates = explode('-', $this->input->post('date_range'));

                    //$data['dates'] = $dates;
                    //$from = trim($dates[0]);
                    //$to = trim($dates[1]);


                    $location = strlen($this->input->post('location')) > 0 ? $this->input->post('location') : 'empty';

                    $facility = strlen($this->input->post('facility')) > 0 ? $this->input->post('facility') : 'empty';


                    if ($this->input->post('export') == 'excel') {

                        redirect("excel_export/data_per_health_facility/$from/$to/$location/$facility");

                    }


                } else {

                    // $from = strtotime('monday this week');
                    // $to = strtotime('sunday this week');

                    $location = 'empty';
                    $facility = 'empty';

                }

                $data['from'] = date('m/d/Y', $from);
                $data['to'] = date('m/d/Y', $to);
                $data['location'] = $location;
                $data['facility'] = $facility;

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'data_per_health_facility', $data, true);
                break;

            case 'edit':


                $data['id'] = $id = $id / date('Y');


                $this->form_validation
                    ->set_rules('week_no', 'Week no', 'trim|required')
                    ->set_rules('report_year', 'Year', 'trim|required')
                    ->set_rules('facility', 'Facility', 'trim|required');

                if ($this->form_validation->run() == true) {


                    foreach ($this->input->post() as $r => $t) {

                        if ($r != 'week_no' || $r != 'report_year' || $r != 'facility') {

                            $where = array(
                                'week_no' => $this->input->post('week_no'),
                                'report_year' => $this->input->post('report_year'),
                                'facility' => $this->input->post('facility'),
                                'indicator' => $r
                            );

                            $values = array('indicator_value' => $t);


                            $this->db->where($where)->update('facility_report', $values);

                        }
                    }


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'Report has been updated successfully'
                    );
                    $data['page_view'] = '';
                    $data['page_view'] .= $this->load->view('alert', $alert, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'edit', $data, true);
                    // $data['page_view'].=$this->load->view('app/'.$this->page_level2.$title,$data,true);


                } else {

                    $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'edit', $data, true);
                }


                break;


            case 'view':


                $data['id'] = $id = $id / date('Y');

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'edit', $data, true);
                break;


            case 'dashboard':
                $data['subtitle'] = 'report_dashboard';

                $this->form_validation->set_rules('filter', 'Filter', 'trim');
                if ($this->form_validation->run() == true) {


                    $data['week_no'] = $week = $this->input->post('week_no');
                    $data['year'] = $year = $this->input->post('year');

                    //calculating the actual week and the year incase the year has 53 weeks
                    $actual_week = $week + 1 != 53 ? $week + 1 : 1;
                    $actual_year = $week + 1 != 53 ? $year : $year + 1;

                    $getStartAndEndDate = getStartAndEndDate($actual_week, $actual_year);

                    //outputing the days to view
                    $data['fdate'] = $getStartAndEndDate[0] . ' 00:00:00';
                    $data['last_day'] = $getStartAndEndDate[1] . ' 23:59:59';
                    $data['subtitle'] = 'Week:' . $week . ' Year:' . $year;


                } else {

                    $data['fdate'] = date('Y-m-01');
                }

                $data['indicators'] = $this->db->select('code,indicator,initial')->from('indicators')->get()->result();
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'dashboard/overall_chat', $data, true);
                break;

        }


        $this->load->view('app/static/main_page', $data);

    }

    public function tickets($type = null, $id = null)
    {

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;

            case 'new_ticket':


                $main_class = ' form-control ';

                $form_options = array(
                    array(/* Name */
                        'id' => 'name',
                        'placeholder' => 'Please Enter Name',
                        'name' => 'name',
                        'type' => 'text',
                        'label' => 'Name: ',
                        'class' => $main_class
                    ),
                    array(/* Email */
                        'id' => 'email',
                        'placeholder' => 'Please Enter Email',
                        'name' => 'email',
                        'type' => 'email',
                        'label' => 'Email Id: ',
                        'class' => $main_class
                    ),
                    array(/* Password */
                        'id' => 'password',
                        'placeholder' => 'Please Enter Password',
                        'name' => 'password',
                        'type' => 'password',
                        'label' => 'Password: ',
                        'class' => $main_class
                    ),
                    array(/* contact */
                        'id' => 'contact',
                        'placeholder' => 'Please Enter Contact',
                        'name' => 'contact',
                        'type' => 'text',
                        'label' => 'Contact: ',
                        'class' => $main_class
                    ),


                    array(/* Gender */
                        'id' => 'shirts',
                        'label' => 'Shirts : ',
                        'name' => 'shirts',
                        'type' => 'select',
                        'selected' => 'large',
                        'class' => 'styled',
                        'options' => array(
                            'small' => 'Small Shirt',
                            'med' => 'Medium Shirt',
                            'large' => 'Large Shirt',
                            'xlarge' => 'Extra Large Shirt',
                        )
                    ),

                    array(/* Gender */
                        'id' => 'gender',
                        'label' => 'Gender : ',
                        'name' => 'Gender',
                        'type' => 'radio',
                        'class' => 'styled',
                        'options' => array(
                            array(
                                'id' => 'gender',
                                'value' => 'male',
                                'label' => 'Male',
                                'name' => 'gender',
                            ),
                            array(
                                'id' => 'gender',
                                'value' => 'female',
                                'label' => 'Female',
                                'name' => 'gender',
                            )
                        )
                    ),
                    array(/* Address */
                        'id' => 'address',
                        'type' => 'textarea',
                        'placeholder' => 'Address',
                        'label' => ' Address',
                        'name' => 'address',
                        'class' => $main_class
                    ),
                    array(/* SUBMIT */
                        'id' => 'submit',
                        'type' => 'submit',
                        'label' => 'Submit',
                        'name' => 'submit',
                        'class' => 'btn btn-success'

                    )
                );

                $data['myForm'] = $form_options;

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);

                break;
        }

        $this->load->view('app/static/main_page', $data);

    }

    public function forms($type = null, $id = null)
    {

        $this->load->model('hf_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';
        $root = $this->page_level . $this->page_level2;


        switch ($type) {


            default:
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'form_d', $data, true);
                break;
        }
        $this->load->view('app/static/main_page', $data);
    }

    public function survey($type = null, $id = null, $answer_id = null)
    {

        $this->load->model('hf_model');
        $this->load->model('survey_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';
        $root = $this->page_level . $this->page_level2;


        switch ($type) {


            default:
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;

            case 'edit':
            case 'view':

                $data['id'] = $id / date('Y');
//                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'questions/create_questions', $data, true);

                $data['page_view'] .= $this->load->view($root . 'new', $data, true);

                break;

            case 'delete':


                $id = $id / date('Y');

                $this->db->where('id', $id)->delete('specimens');

                $message = 'You have successfully Deleted Specimen';

                $alert = array(
                    'alert' => 'success',
                    'message' => $message
                );

                $alert_view = $this->load->view('alert', $alert, true);
                $this->session->set_flashdata('alert', $alert_view);

                //adding Departure
                $this->custom_library->add_logs('', $title, '', 'has Deleted Specimen id:' . $id);

                redirect($root);

                break;

            case 'new':


                strlen($id) == 0 ? $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[survey.title]') : $this->form_validation->set_rules('title', 'Title', 'trim|required');

                $this->form_validation->set_rules('description', 'Description', 'trim');

                if ($this->form_validation->run() == true) {


                    $values = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                    );


                    $id = $this->input->post('id');

                    if (strlen($id) == 0) {

                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;


                        $this->db->insert('survey', $values);
                        $dat['alert'] = 'success';
                        $dat['message'] = 'Survey has been saved successfully';


                        //adding Survey
                        $this->custom_library->add_logs('', $title, '', 'has Created Survey');


                    } else {


                        $dat['alert'] = 'success';
                        $dat['message'] = 'Survey has been updated successfully';
                        $this->db->where(array('id' => $id))->update('survey', $values);

                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;


                        //adding Survey
                        $this->custom_library->add_logs('', $title, '', 'has Updated Survey id:' . $id);

                    }


                    $alert = $this->load->view('alert', $dat, true);
                    $this->session->set_flashdata('alert', $alert);
                    redirect($root);


                } else {

                    $data['page_view'] .= $this->load->view($root . 'new', $data, true);
                }


                break;


            case 'preview_question':
            case 'answer_preview':

                $data['id'] = $id = $id / date('Y');
                isset($answer_id) ? $data['answer_id'] = $answer_id / date('Y') : '';

                $data['page_view'] .= $this->load->view('app/survey/questions/preview_questions', $data, true);

                break;


            case 'survey_responses':
                $data['id'] = $id = $id / date('Y');

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'survey_responses', $data, true);
                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    public function surveys($type = null, $id = null)
    {

        $this->load->model('hf_model');
        $this->load->model('survey_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';
        $root = $this->page_level . $this->page_level2;


        switch ($type) {


            default:
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;

            case 'edit':
            case 'view':

                $data['id'] = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'questions/create_questions', $data, true);

                break;

            case 'delete':


                $id = $id / date('Y');

                $this->db->where('id', $id)->delete('specimens');

                $message = 'You have successfully Deleted Specimen';

                $alert = array(
                    'alert' => 'success',
                    'message' => $message
                );

                $alert_view = $this->load->view('alert', $alert, true);
                $this->session->set_flashdata('alert', $alert_view);

                //adding Departure
                $this->custom_library->add_logs('', $title, '', 'has Deleted Specimen id:' . $id);

                redirect($root);

                break;

            case 'newhh':


                strlen($id) == 0 ? $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[survey.title]') : $this->form_validation->set_rules('title', 'Title', 'trim|required');

                $this->form_validation->set_rules('description', 'Description', 'trim');

                if ($this->form_validation->run() == true) {


                    $values = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                    );


                    $id = $this->input->post('id');

                    if (strlen($id) == 0) {

                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;


                        $this->db->insert('survey', $values);
                        $dat['alert'] = 'success';
                        $dat['message'] = 'Survey has been saved successfully';


                        //adding Survey
                        $this->custom_library->add_logs('', $title, '', 'has Created Survey');


                    } else {


                        $dat['alert'] = 'success';
                        $dat['message'] = 'Survey has been updated successfully';
                        $this->db->where(array('id' => $id))->update('survey', $values);

                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;


                        //adding Survey
                        $this->custom_library->add_logs('', $title, '', 'has Updated Survey id:' . $id);

                    }


                    $alert = $this->load->view('alert', $dat, true);
                    $this->session->set_flashdata('alert', $alert);
                    redirect($root);


                } else {

                    $data['page_view'] = $this->load->view($root . 'new', $data, true);
                }


                break;

            case 'new':

                $this->form_validation
                    ->set_rules('title', 'Title', 'trim|required')
                    ->set_rules('description', 'Description', 'trim')
                    ->set_rules('qn[id][]', 'id', 'trim')
                    ->set_rules('qn[question][]', 'Question', 'trim')
                    ->set_rules('qn[question_type][]', 'Question Type', 'trim')
                    ->set_rules('qn[required][]', 'Required field', 'trim')
                    ->set_rules('qn[placeholder][]', 'Placeholder', 'trim');
                if ($this->form_validation->run() == true) {

                    $values = array(

                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description')
                    );


                    if (strlen($this->input->post('survey_id')) == 0) {

                        $values['survey_code'] = $survey_code = $this->survey_model->survey_code();
                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {

                        $id = $this->input->post('survey_id');
                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;

                    }

                    if (strlen($this->input->post('survey_id')) == 0) {

                        if ($this->db->insert('survey', $values)) {

                            $survey = $this->db->select('id')->from('survey')->where('survey_code', $survey_code)->get()->row();
                            $survey_id = $survey->id;


                            $this->survey_model->manage_survey_questions($survey_id, $survey_code, $this->input->post('qn'));

                        }
                    } else {


                        $this->db->where('id', $id)->update('survey', $values);
                        $survey = $this->db->select('survey_code')->from('survey')->where('id', $id)->get()->row();
                        $survey_code = $survey->code;
                        $survey_id = $id;

                        $this->survey_model->manage_survey_questions($survey_id, $survey_code, $this->input->post('qn'));


                    }


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully Created a Questionnaire'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                    $alert = $this->load->view('alert', $data, true);
                    $this->session->set_flashdata('alert', $alert);

                    redirect($this->page_level . 'surveys');


                }

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'questions/create_questions', $data, true);
                break;


            case 'preview_question':

                $data['id'] = $id = $id / date('Y');
                $form = $this->survey_model->create_form($id);
                $data['myForm'] = CI_form_builder($form, 'vertical');

                $data['page_view'] .= $this->load->view('app/surveys/questions/preview_questions', $data, true);

                break;


            case 'add_question_options':

                $data['id'] = $id = $id / date('Y');
                $this->form_validation
                    ->set_rules('question_option[]', 'Options', 'trim')
                    ->set_rules('option_label[]', 'Label', 'trim');
                if ($this->form_validation->run() == true) {

                    $insert_batch = array();
                    $update_batch = array();
                    $no = 0;

                    $ev = $this->input->post('option_label');
                    foreach ($this->input->post('question_option') as $t) {


                        //choose if to update the or the create a new record
                        $values = array(
                            'question_id' => $id,
                            'question_option' => $t,
                            'option_label' => $ev[$no],

                        );


                        $option_id = $this->input->post('option_id');

                        if (strlen($option_id[$no]) == 0) {

                            $values['created_on'] = time();
                            $values['created_by'] = $this->session->id;


                            strlen($ev[$no]) > 0 ? array_push($insert_batch, $values) : '';

                        } else {


                            $values['id'] = $option_id[$no];
                            $values['updated_on'] = time();
                            $values['updated_by'] = $this->session->id;


                            array_push($update_batch, $values);

                        }


                        $no++;
                    }


                    count($insert_batch) > 0 ? $this->db->insert_batch('survey_question_options', $insert_batch) : '';
                    count($update_batch) > 0 ? $this->db->update_batch('survey_question_options', $update_batch, 'id') : '';


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully Saved Options'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                    //$alert = $this->load->view('alert', $data, true);
                    // $this->session->set_flashdata('alert', $alert);

                    //  redirect($this->page_level . 'surveys');


                }

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'questions/add_question_options', $data, true);
                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    public function workplan_levels($type = null, $id = null)
    {


        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:
            case 'edit':
            case'view':


                $data['id'] = $id = $id / date('Y');

                $type == 'edit' || $type == 'view' ? $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true) : '';

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;

            case 'new':
                if (isset($id)) {
                    $this->form_validation
                        ->set_rules('name', 'Name', 'trim|required')
                        ->set_rules('level', 'Level', 'trim|required');
                } else {
                    $this->form_validation
                        ->set_rules('name', 'Name', 'trim|is_unique[workplan_level.name]|required')
                        ->set_rules('level', 'Level', 'trim|is_unique[workplan_level.level]|required');
                }

                if ($this->form_validation->run() == true) {


                    $id = $this->input->post('id');

                    $values = array(
                        'name' => $this->input->post('name'),
                        'level' => $this->input->post('level')
                    );

                    if (strlen($id) == 0) {

                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {

                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;

                    }


                    strlen($id) == 0 ? $this->db->insert('workplan_level', $values) : $this->db->where('id', $id)->update('workplan_level', $values);


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully saved data'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                } else {


                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                }

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'workplan_levels', $data, true);
                break;


            case 'delete':

                $id = $id / date('Y');

                if ($this->db->where('id', $id)->delete('workplan_level')) {

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully Deleted Indicator'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);


                break;
        }

        $this->load->view('app/static/main_page', $data);

    }

    public function workplan($type = null, $id = null, $level = null)
    {

//        $this->load->model('workplan_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';

        //$data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'navigation', $data, true);

        switch ($type) {

            default:

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'goals', $data, true);
                break;


            case 'view':
            case 'edit':
            case 'add_another':
                $data['id'] = $id = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new_workplan', $data, true);
                break;


            case 'new_workplan':


                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('parent_id', 'parent id', 'trim')
                    ->set_rules('title', 'Title', 'trim|required')
                    ->set_rules('description', 'Description', 'trim|required')
                    ->set_rules('workplan_level', 'Workplan Level', 'trim|required');


                if ($this->form_validation->run() == true) {

                    //choose if to update the or the create a new record

                    $level = $this->input->post('workplan_level');

                    $from = strtotime(date('Y-m-d 00:00:00', strtotime($this->input->post('start_date'))));
                    $to = strtotime(date('Y-m-d 23:59:59', strtotime($this->input->post('end_date'))));

                    $values = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'level' => $level,
                        'assumption' => $this->input->post('assumption'),
                        'start_time' => $from,
                        'end_time' => $to,
                        'parent_id' => $level == 1 ? 1 : $this->input->post('parent_id')
                    );

                    if (strlen($id) == 0) {

                        $values['code'] = $this->model->plan_code();
                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {

                        if ($this->input->post('subtitle') == 'add_another') {


                            $values['level'] = $level + 1;
                            $values['parent_id'] = $this->input->post('id');
                            $values['code'] = $this->model->plan_code();
                            $values['created_on'] = time();
                            $values['created_by'] = $this->session->id;

                        } else {

                            $values['updated_on'] = time();
                            $values['updated_by'] = $this->session->id;
                        }


                    }


                    //decides on either to update or create a new record

                    if ($this->input->post('subtitle') == 'add_another') {
                        $this->db->insert('workplan', $values);
                    } else {
                        strlen($id) == 0 ? $this->db->insert('workplan', $values) : $this->db->where('id', $id / date('Y'))->update('workplan', $values);
                    }


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully saved data'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                    $alert = $this->load->view('alert', $data, true);
                    $this->session->set_flashdata('alert', $alert);

                    redirect($this->page_level . 'workplan');


                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                }


                break;

            case 'view_goal':
                $data['parent'] = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'impact', $data, true);
                break;

            case 'edit_indicator':
                $data['id'] = $id = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'add_indicator', $data, true);
                break;


            case 'add_indicator':

                $data['workplan_id'] = $id / date('Y');//the id in parameter stands for workplan id


                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('workplan_id', 'Workplan id', 'trim')
                    ->set_rules('indicator', 'Indicator Name', 'trim|required')
                    ->set_rules('data_type', 'Data Type', 'trim|required')
                    ->set_rules('interval_title[]', 'Indicator Title', 'trim|required')
                    ->set_rules('estimated_value[]', 'Estimate', 'trim|required')
                    ->set_rules('description', 'Description', 'trim|required');


                if ($this->form_validation->run() == true) {


                    $title_values = array(
                        'workplan_id' => $this->input->post('workplan_id'),
                        'title' => $this->input->post('indicator'),
                        'data_type' => $this->input->post('data_type'),
                        'indicator_code' => $indicator_code = intval($this->model->indicator_code()),
                        'geo_level' => $this->input->post('geo_level'),
                        'description' => $this->input->post('description'),
                        'created_on' => time(),
                        'created_by' => $this->session->id
                    );

                    if ($this->db->insert('workplan_indicator_titles', $title_values)) {

                        $batch_values = array();
                        $no = 0;

//                    print_r($this->input->post('interval_title[]'));
//                    exit;

                        foreach ($this->input->post('interval_title') as $t) {
                            $ev = $this->input->post('estimated_value');

                            //choose if to update the or the create a new record
                            $values = array(
                                'workplan_id' => $this->input->post('workplan_id'),
                                'indicator_code' => $indicator_code,
                                'interval_title' => $t,
                                'estimated_value' => $ev[$no],

                            );

                            $indicator_id = $this->input->post('id');

                            if (strlen($indicator_id) == 0) {

                                $values['created_on'] = time();
                                $values['created_by'] = $this->session->id;

                            } else {


                                $values['id'] = $indicator_id;
                                $values['updated_on'] = time();
                                $values['updated_by'] = $this->session->id;

                            }


                            array_push($batch_values, $values);


                            $no++;
                        }

                        //print_r($batch_values);
//                    exit;

                        //decides on either to update or create a new record

                        strlen($indicator_id) == 0 ? $this->db->insert_batch('workplan_indicators', $batch_values) : $this->db->update_batch('workplan_indicators', $values, 'id');

                        $alert = array(
                            'alert' => 'success',
                            'message' => 'You have successfully saved data'
                        );
                        $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                        $alert = $this->load->view('alert', $data, true);
                        $this->session->set_flashdata('alert', $alert);

                        redirect($this->page_level . 'workplan');


                    }


                } else {
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'add_indicator', $data, true);
                }

                break;


            case 'delete_indicator':

                $indicator_code = $id / date('Y');

                if ($this->db->where('indicator_code', $indicator_code)->delete('workplan_indicators')) {
                    if ($this->db->where('indicator_code', $indicator_code)->delete('workplan_indicator_titles')) {
                        $alert = array(
                            'alert' => 'success',
                            'message' => 'You have successfully Deleted Indicator'
                        );
                        $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                        $alert = $this->load->view('alert', $data, true);
                        $this->session->set_flashdata('alert', $alert);

                        redirect($this->page_level . 'workplan');
                    }


                }


                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    public function routine_activities($type = null, $id = null)
    {


        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:

                //$data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'referrals/referrals', $data, true);
                break;

        }

        $this->load->view('app/static/main_page', $data);

    }

    public function training($type = null, $id = null)
    {


        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'questionnaire', $data, true);
                break;

        }

        $this->load->view('app/static/main_page', $data);

    }

    public function dhis2_reports($type = null, $id = null)
    {

        $this->load->helper('dhis2_json');
        $this->load->model('dhis2_model');
        $this->load->model('dashboard_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:

//                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'malaria_weekly', $data, true);
                break;

            case 'malaria_weekly':

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'malaria_monthly':

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'weekly_summaries':

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'monthly_summaries':

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

        }

        $this->load->view('app/static/main_page', $data);

    }

    public function activity_report($type = null, $id = null)
    {

        $this->load->model('hf_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;


            case 'status_report':
//activities/activities_inline.php
//                $data['id']='';
//                $data['page_view'] .= $this->load->view('app/activities/activities_inline' , $data, true);
                $data['new_title'] = 'activities';
                $data['page_view'] .= $this->load->view('app/activities/activities', $data, true);
                break;

            case 'activity_report_summary':

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'activity_type_summary':

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;


            case 'view':
            case 'edit':

                $data['id'] = $id = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                break;


            case 'new':

                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('district', 'District', 'trim|required')
                    ->set_rules('county', 'County', 'trim|required')
                    ->set_rules('sub_county', 'Sub County', 'trim|required')
                    ->set_rules('parish', 'Parish', 'trim|required')
                    ->set_rules('start_date', 'Start Date', 'trim|required')
                    ->set_rules('end_date', 'End Date', 'trim|required')
                    ->set_rules('health_facility', 'Health Facility', 'trim')
                    ->set_rules('hf_type', 'Health Facility Type', 'trim')
                    ->set_rules('workplan_ref', 'Workplan Ref#', 'trim|required')
                    ->set_rules('activity_name', 'Activity Name', 'trim|required')
                    ->set_rules('activity_type', 'Activity Type', 'trim|required')
                    ->set_rules('objectives', 'Objectives', 'trim|required')
                    ->set_rules('remarks', 'Remarks', 'trim|required')
                    ->set_rules('submitted_by', 'Submitted By', 'trim|required')
                    ->set_rules('submit_date', 'Submit Date', 'trim|required')
                    ->set_rules('cleared_by', 'Cleared By', 'trim')
                    ->set_rules('clear_date', 'Clear Date', 'trim|required')
                    ->set_rules('description', 'Description', 'trim|required')
                    ->set_rules('other_facility', 'Other Facility', 'trim');


                if ($this->form_validation->run() == true) {


                    $values = array(

                        'district' => $this->input->post('district'),
                        'county' => $this->input->post('county'),
                        'sub_county' => $this->input->post('sub_county'),
                        'parish' => $this->input->post('parish'),
                        'start_date' => $start_date = strtotime($this->input->post('start_date')),
                        'end_date' => strtotime($this->input->post('end_date')),
                        'health_facility' => $this->input->post('health_facility'),
                        'hf_type' => $this->input->post('hf_type'),
                        'other_facility' => $this->input->post('other_facility'),
                        'workplan_ref' => $this->input->post('workplan_ref'),
                        'activity_name' => $this->input->post('activity_name'),
                        'activity_type' => $this->input->post('activity_type'),
                        'objectives' => $this->input->post('objectives'),
                        'remarks' => $this->input->post('remarks'),
                        'submitted_by' => $this->input->post('submitted_by'),
                        'submit_date' => strtotime($this->input->post('submit_date')),
                        'cleared_by' => $this->input->post('cleared_by'),
                        'clear_date' => strtotime($this->input->post('clear_date')),
                        'description' => $this->input->post('description'),
                        'activity_year' => date('Y', $start_date)
                    );

                    if (strlen($this->input->post('id')) == 0) {

                        $values['form_code'] = $form_code = $this->model->activity_report_code();
                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {

                        $id = $this->input->post('id');
                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;

                    }


                    if (strlen($this->input->post('id')) == 0) {
                        if ($this->db->insert('activity_report', $values)) {

                            $activity_report = $this->db->select('id')->from('activity_report')->where('form_code', $form_code)->get()->row();
                            $activity_report_id = $activity_report->id;


                            $this->model->activity_report_facilitators($activity_report_id, $form_code, $this->input->post('fac'));
                            $this->model->activity_report_beneficiaries($activity_report_id, $form_code, $this->input->post('ben'));
                            $this->model->activity_report_distributions($activity_report_id, $form_code, $this->input->post('dist'));
                            $this->model->activity_report_challenges($activity_report_id, $form_code, $this->input->post('chal'));


                        }
                    } else {
                        $this->db->where('id', $id)->update('activity_report', $values);
                        $activity_report_id = $id;

                        $this->model->activity_report_facilitators($activity_report_id, $form_code, $this->input->post('fac'));
                        $this->model->activity_report_beneficiaries($activity_report_id, $form_code, $this->input->post('ben'));
                        $this->model->activity_report_distributions($activity_report_id, $form_code, $this->input->post('dist'));
                        $this->model->activity_report_challenges($activity_report_id, $form_code, $this->input->post('chal'));

                    }


                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully saved data'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                    $alert = $this->load->view('alert', $data, true);
                    $this->session->set_flashdata('alert', $alert);

                    redirect($this->page_level . 'activity_report');


                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                }

                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    public function indicators($type = null, $id = null)
    {


        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'indicators', $data, true);
                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    public function activities($type = null, $id = null)
    {


        $this->load->model('hf_model');

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'activities', $data, true);
                break;

            case 'view':
            case 'edit':

                $data['id'] = $id = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'add_activity_status', $data, true);
                break;


            case 'add_activity_status':

                $data['workplan_id'] = $id / date('Y');//the id in parameter stands for workplan id

//                Array ( [id] => [workplan_id] => 53 [activity_name] => hdhfhhf [number] => ertre [geo_level] => 1 [description] => erter [start_date] => 10 January 2018 [end_date] => 2 January 2018 [deliverable] => ertert [budget] => ertert )

                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('workplan_id', 'Workplan id', 'trim')
                    ->set_rules('activity_name', 'Activity Name', 'trim|required')
                    ->set_rules('number', 'Number', 'trim|required')
                    ->set_rules('item_unit', 'Item Unit', 'trim|required')
                    ->set_rules('geo_level', 'Geo level', 'trim|required')
                    ->set_rules('description', 'Description', 'trim|required')
                    ->set_rules('start_date', 'Start Date', 'trim|required')
                    ->set_rules('end_date', 'End Date', 'trim|required')
                    ->set_rules('deliverable', 'Deliverable', 'trim|required')
                    ->set_rules('budget', 'Budget', 'trim|required');


                if ($this->form_validation->run() == true) {


                    $values = array(
                        'workplan_id' => $this->input->post('workplan_id'),
                        'activity_name' => $this->input->post('activity_name'),
                        'number' => $this->input->post('number'),
                        'item_unit' => $this->input->post('item_unit'),
                        'geo_level' => $this->input->post('geo_level'),
                        'start_date' => strtotime($this->input->post('start_date')),
                        'end_date' => strtotime($this->input->post('end_date')),
                        'deliverable' => $this->input->post('deliverable'),
                        'budget' => $this->input->post('budget'),
                        'description' => $this->input->post('description')
                    );

                    if (strlen($this->input->post('id')) == 0) {

                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {


                        $id = $this->input->post('id');
                        $values['code'] = $this->model->plan_code();
                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;

                    }


                    strlen($this->input->post('id')) == 0 ? $this->db->insert('workplan_activity_status', $values) : $this->db->where('id', $id)->update('workplan_activity_status', $values);

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully saved data'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                    $alert = $this->load->view('alert', $data, true);
                    $this->session->set_flashdata('alert', $alert);

                    redirect($this->page_level . 'workplan');


                } else {
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'add_activity_status', $data, true);
                }

                break;


            case 'delete':

                $activity_id = $id / date('Y');

                if ($this->db->where('activity_id', $activity_id)->delete('workplan_activity_status_updates')) {
                    if ($this->db->where('id', $activity_id)->delete('workplan_activity_status')) {
                        $alert = array(
                            'alert' => 'success',
                            'message' => 'You have successfully Deleted Activity'
                        );
                        $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                        $alert = $this->load->view('alert', $data, true);
                        $this->session->set_flashdata('alert', $alert);

                        redirect($this->page_level . 'activities');
                    }


                }


                break;


            case 'update_activity_status':

                $data['id'] = $activity_id = $id / date('Y');


                $this->form_validation
                    ->set_rules('update_id', 'Update ID', 'trim')
                    ->set_rules('status', 'Status', 'trim|required')
                    ->set_rules('update', 'Update', 'trim|required')
                    ->set_rules('image', 'Image', 'trim');

                if ($this->form_validation->run() == true) {


                    $values = array(
                        'status' => $this->input->post('status'),
                        'activity_id' => $activity_id,
                        'update' => $this->input->post('update'),
                        'attachment' => $this->input->post('image'),
                        'status_date' => time(),
                    );


                    if (strlen($this->input->post('update_id')) == 0) {

                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {


                        $id = $this->input->post('update_id');
                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;

                    }


                    strlen($this->input->post('update_id')) == 0 ? $this->db->insert('workplan_activity_status_updates', $values) : $this->db->where('id', $id)->update('workplan_activity_status_updates', $values);

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully saved data'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);

                }


                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'add_activity_status', $data, true);


                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    public function users($type = null, $id = null)
    {

        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type
        );
        $type != 'filter' ? $id = $data['id'] = $id / date('Y') : '';
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data['page_view'] = '';


        switch ($type) {

            case 'new':
                //this is the section for the form validation
                $this->form_validation
//                        ->set_rules('sub_type', 'User Type', 'trim|required')
                    ->set_rules('first_name', 'First Name', 'trim|required')
                    ->set_rules('last_name', 'Last Name', 'trim|required')
                    ->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]')
//                    ->set_rules('zip_code', 'Zip Code', 'trim|required')
                    ->set_rules('country', 'Country', 'trim|required')
                    ->set_rules('city', 'City', 'trim')
                    ->set_rules('phone', 'Phone', 'trim|required|is_unique[users.phone]|max_length[15]')
                    ->set_rules('gender', 'Gender', 'trim|required|exact_length[1]')
                    ->set_rules('access', 'Access', 'trim')
                    ->set_rules('role', 'User Role', 'trim|required')
                    ->set_rules('dob', 'Date of Birth', 'trim|callback_dob_check');
                // ->set_rules('referral', 'Referral', 'trim');


                if ($this->form_validation->run() == true) {

                    $password = code(6);

                    $dob = $this->input->post('dob');


                    $values = array(
                        'first_name' => $fname = $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'username' => $email = $this->input->post('email'),
                        'password' => hashValue($password),
                        'phone' => phone($this->input->post('phone')),
                        'city' => $this->input->post('city'),
                        'country' => $this->input->post('country'),
                        'gender' => $this->input->post('gender'),
                        'user_type' => $this->input->post('role'),
                        'dob' => strtotime($dob),
                        'created_on' => time(),
                        'created_by' => $this->session->userdata('id'),
//                            'sub_type' => strlen($this->input->post('referral')) > 0 ? 2 : 1,
//                            'referral' => strlen($this->input->post('referral')) > 0 ? $this->input->post('referral') : ''

                    );
                    $this->db->insert('users', $values);
                    $data['message'] = 'User has been added successfully';
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);


                    //this Lists the users in the system


                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                    $email_msg = "Dear $fname ,\n \r Your account has been created. Your username is $email: and password is: $password
To login, go to URL. Remember you can change your password once logged.\r\n";
                    $this->custom_library->sendHTMLEmail2($email, $this->site_name, $email_msg);
                    $this->custom_library->add_logs('', 'account_creation', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has created an account for user ');

                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                }

                break;
            case 'edit':

                //this is the section for the form validation
                $this->form_validation
                    ->set_rules('email', 'Email', 'valid_email|trim')
                    ->set_rules('phone', 'Phone', 'trim|max_length[15]|min_length[10]')
                    ->set_rules('first_name', 'First Name', 'trim')
                    ->set_rules('last_name', 'Last Name', 'trim')
                    ->set_rules('role', 'Role', 'trim');

                if ($this->form_validation->run() == false) {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                } else {

                    $this->db->where('id', $id)
                        ->update('users', array(
                            'email' => $this->input->post('email'),
                            'phone' => $this->input->post('phone'),
                            'user_type' => $this->input->post('role'),
                            'first_name' => $fname = $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),


                        ));

                    $data['message'] = 'User account is updated Successfully';
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                    $email = $this->input->post('email');
                    $email_msg = "Dear $fname ,\r\n Your account has been Updated\n\r";
                    $this->custom_library->sendHTMLEmail2($email, $this->site_name, $email_msg);
                    $this->custom_library->add_logs('', 'account_modification', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has updated account for user');

                }
                break;

            case 'tabbed':


                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'profile_tabbed', $data, true);
                break;
            //this is the part for changing avatar
            case 'change_avatar':


                // this is the function which uploads the profile image
                $this->form_validation->set_rules('image', 'Image', 'trim')->set_rules('pa', 'Pas', 'trim');
                //this is the company name

                $cname = 'users_profile';
                //managing of the images
                $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
                $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config['max_size'] = '200';
                $config['max_width'] = '1920';
                $config['max_height'] = '850';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }
                if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                    $values = array(
                        'photo' => $path . $this->upload->file_name,
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')
                    );
                    $this->db->where('id', $id)->update('users', $values);
                    // $this->session->set_userdata(array('photo'=>$path.$this->upload->file_name));
                    $data['message'] = 'Image has been updated Successfully';
                    $data['alert'] = 'success';
                    $data['alert_page'] = $this->load->view('alert', $data, true);

                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account Profile Picture has been Updated";
                    $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name, $email_msg);

                    $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has changed picture for user ');

                } else {
                    $data['error'] = $this->upload->display_errors();
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                }

                break;


            case 'reset_password':
                if ($pp = $this->model->reset_password($id)) {
                    $dat['message'] = 'Password has been reset successfully to ' . $pp;
                    $dat['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $dat, true);

                    $this->custom_library->add_logs('', 'password_reset', '', '  has reset password for user ID:' . $id);
                } else {
                    $dat['message'] = 'Password has been changed failed <br/>';
                    $dat['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $dat, true);
                }
                $data['page_view'] .= $this->load->view($root . 'users', $data, true);
                break;


            //this is the part for changing the password
            case 'change_password':

                $this->form_validation->set_rules('new_pass', 'New Password', 'required|trim|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|trim');
                if ($this->form_validation->run() == false) {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);
                } else {
                    $this->db->where('id', $id)->update('users', array('password' => hashValue($password = $this->input->post('new_pass'))));
                    $data['message'] = 'Password has been changed successfully <br/>';
                    $data['alert'] = 'success';
                    $data['page_view'] .= $this->load->view('alert', $data, true);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_profile', $data, true);

                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account password has been changed. Your new password is $password
If you didn't initiate this password request, contact the Baylor Admin immediately.\r\n

";
                    $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name, $email_msg);
                    $this->custom_library->add_logs('', 'password_change', $fn->first_name . ' ' . $fn->last_name, '  has changed password for user ');
                }

                break;

            //deleting user
            case 'delete':

                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                $email_msg = "Dear $fn->first_name ,\n \r Your account has been Deleted";
                $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name, $email_msg);

                $this->delete($id, 'users');
                $data['message'] = 'Record has been Deleted successfully';
                $data['alert'] = 'success';
                $data['alert_page'] = $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);

                $this->custom_library->add_logs('', 'account_deletion', $fn->first_name . ' ' . $fn->last_name, '  has deleted user ');
                break;

            case 'make_admin':
                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();
                // $id = $data['id'] = $id / date('Y');
                $this->db->where('id', $id)->update('users',
                    array('user_type' => '1',
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')));
                $data['message'] = 'Record has been Updated successfully';
                $data['alert'] = 'success';
                $data['page_view'] = $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);

                $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has made user admin ');
                break;

            case 'ban':

                $this->db->where('id', $id)->update('users',
                    array('status' => '2',
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')));


                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                $email_msg = "Dear $fn->first_name ,\n \r Your account has been blocked from the system";
                $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name . ' Account Creation', $email_msg);
                $data['message'] = 'Record has been Blocked from accessing the System successfully';
                $data['alert'] = 'warning';
                $data['alert_page'] = $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);

                $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has blocked user');
                break;

            case 'unblock':
                $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                $email_msg = "Dear $fn->first_name ,\n \r Your account has been unblocked from the system";

                $this->custom_library->sendHTMLEmail2($fn->email, $this->site_name . ' Account Creation', $email_msg);

                $this->db->where('id', $id)->update('users',
                    array('status' => '0',
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')));
                $data['message'] = 'Record has been unblocked from accessing the System successfully';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);
                //this is where the default page i loaded

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                $this->custom_library->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has unblocked user');
                break;

            case 'filter':

                $this->form_validation
                    ->set_rules('country', 'Country', 'trim')
                    ->set_rules('role', 'Role', 'trim');

                $this->db->select()->from('users');
                //this is selecting for the date
                strlen($this->input->post('country')) > 0 ? $this->db->where(array('country' => $this->input->post('country'))) : '';
                strlen($this->input->post('role')) > 0 ? $this->db->where(array('user_type' => $this->input->post('role'))) : '';

                if ($this->uri->segment(4) == 'blocked') {
                    $this->db->where(array('status' => 2));
                } elseif ($this->uri->segment(4) == 'active') {
                    $this->db->where(array('status !=' => 2));
                }


                $this->db->where(array('id !=' => $this->session->userdata('id')));

                $data['t'] = $this->db->get()->result();
                //this is the loading of the pages
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                break;

            case 'wallets':
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'user_wallets', $data, true);
                break;

            case 'topup':

                break;
            case 'user_statement':

                $data['t'] = $this->db->select()->from('user_statements')->where('user', $id)->get()->result();
                $this->load->view('app/' . $this->page_level2 . 'user_statements', $data);

                break;

            default:

                $this->form_validation->set_rules('user_role', 'User role', 'trim')->set_rules('status', 'Status', 'trim');
                if ($this->form_validation->run() == 'true') {
                    strlen($user_role = $this->input->post('user_role')) > 0 ? $data['user_role'] = $user_role : '';
                    strlen($status = $this->input->post('status')) > 0 ? $data['status'] = $status : '';
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'users', $data, true);
                break;
        }

        $this->load->view('app/static/main_page', $data);

    }

    function delete($id, $table)
    {
        $this->db->where('id', $id);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    function phone_unique($str)
    {

        $dialing_code = $this->input->post('dialing_code');
        $phone = $dialing_code . $str;

        $output = $this->db->where(array('phone' => $phone))->from('users')->count_all_results();


        if (($output > 0)) {
            $this->form_validation->set_message('phone_unique', 'This <strong>%s</strong> is already Registered with us');
            return false;
        } else {
            return true;
        }


    }

    public function resources($type = null, $id = null)
    {

        $data['page_level'] = $this->page_level;
        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';
        $table = 'resources';
        $root = $this->page_level . $this->page_level2;

        //$this->load->view($this->page_level . 'header', $data);

        //$data['id']=$id=isset($id)?$id/date('Y'):null;


        switch ($type) {

            default:

                $data['page_view'] .= $this->load->view($root . $title , $data, true);
                break;

            case 'edit':
            case 'view':
                $data['id'] = $id = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                break;

            case 'new':

                $this->form_validation
                    ->set_rules('resource_type', 'Resource Type', 'trim|required')
                    ->set_rules('title', 'Title', 'trim|required')
                    ->set_rules('notes', 'Description', 'trim|required')
                    ->set_rules('url', 'Sponsor', 'trim')
                    ->set_rules('attachment', 'attachment', 'trim');
                //checking if the form validation is passed

                if ($this->form_validation->run() == true) {

                    $id = $this->input->post('id');
                    $alert_view = '';



                    $values = array(

                        'resource_type' => $this->input->post('resource_type'),
                        'title' => $this->input->post('title'),
                        'content' => $this->input->post('notes'),
                        'url' => $this->input->post('url'),


                    );

                    if(strlen($id) > 0){

                        $values['updated_by'] = $this->session->userdata('id');
                        $values['updated_on'] = time();

                    }else{

                        $values['created_by'] = $this->session->userdata('id');
                        $values['created_on'] = time();

                    }

                    $cname = 'authors/' . $this->session->userdata('username');
                    //managing of the images
                    $path = $config['upload_path'] = './uploads/' . $this->page_level2 . underscore($cname) . '/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|xls|xlsx|doc|docx|pdf';
                    // $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '20000';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '1000';
                    $this->upload->initialize($config);

                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }

                    if ($this->upload->do_upload('attachment')) {

                        $values['attachment'] = $path . $this->upload->file_name ;

                    } else{

                        $alert = array(
                            'alert' => 'info',
                            'message' => 'No Attachment File Uploaded ' . $this->upload->display_errors()
                        );
                        $alert_view .= $this->load->view('alert', $alert, true);

                    }

                    if(strlen($id) > 0){
                        $this->db->where('id',$id)->update('resources', $values);
                        $alert = array(
                            'alert' => 'success',
                            'message' => 'Resources has been updated successfully'

                        );
                        $this->custom_library->add_logs('','resource_add','', ' updated resource ');
                    }else {
                        $this->db->insert('resources', $values);
                        $alert = array(
                            'alert' => 'success',
                            'message' => 'Resources has been added successfully'

                        );
                        $this->custom_library->add_logs('','resource_add','', ' added resource ');
                    }

                    $alert_view .= $this->load->view('alert', $alert, true);
                    $this->session->set_flashdata('alert', $alert_view);

                    redirect($root);

                }
                else {

                    $data['page_view'] = $this->load->view($root . 'new', $data, true);
                }

                // $this->load->view($root . 'countries', $data);
                break;

            /*
            case 'edit_resource':

                $data['id']=$id;


                $this->form_validation
                    ->set_rules('resource_type', 'Resource Type', 'trim|required')
                    ->set_rules('title', 'Title', 'trim|required')
                    ->set_rules('notes', 'Description', 'trim|required')
                    ->set_rules('url', 'Sponsor', 'trim')
                    ->set_rules('attachment', 'attachment', 'trim');
                //checking if the form validation is passed

                $cname = 'authors/' . $this->input->post('author');
                //managing of the images
                $path = $config['upload_path'] = './uploads/' . $this->page_level2 . underscore($cname) . '/';
                $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|xls|xlsx|doc|docx|pdf';
                // $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config['max_size'] = '20000';
                $config['max_width'] = '1920';
                $config['max_height'] = '1000';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }

                if ($this->form_validation->run() == true) {

                    if(strlen($this->input->post('attachment'))>0) {

                        $attachment = $this->upload->do_upload('attachment') == true ? $path . $this->upload->file_name : '';
                    }else{
                        $attachment = $this->input->post('current');
                    }

                    if (strlen($this->upload->display_errors())) {

                        $data['message'] = $this->upload->display_errors();

                        $data['alert'] = 'warning';
                        $date['hide'] = '';
                        $this->load->view('alert', $data);
                    }

                    $values = array(

                        'resource_type' => $this->input->post('resource_type'),
                        'title' => $this->input->post('title'),
                        'content' => $this->input->post('notes'),
                        'attachment' => $attachment,
                        'updated_by' => $this->session->userdata('id'),
                        'updated_on' => time()

                    );
                    $this->db->where('id',$id)->update('resources', $values);
                    $data['message'] = 'Resources has been Updated successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);

                    $this->load->view($root . 'view_resource', $data);
                    $this->custom_library->add_logs('','Resources_edit',$this->input->post('title'), 'has edited resource ');

                    redirect($root);


                }
                else {

                    $data['error'] = $this->upload->display_errors();
                    $this->load->view($root . $type, $data);
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $this->page_level2, $data, true);
                }



                // $this->load->view($root . 'countries', $data);

                break;

            case 'view_resource':

                $this->form_validation->set_rules('comment','Comment','trim|required');

                if($this->form_validation->run()==true){

                    $values=array(
                        'publication_id'=>$id,
                        'comment'=>$this->input->post('comment'),
                        'created_on'=>time(),
                        'created_by'=>$this->session->userdata('id')

                    );
                    $this->db->insert('pub_evaluation_comments',$values);
                    $data=array(
                        'alert'=>'success',
                        'message'=>'Comment has been posted successfully'
                    );
                    $data['id']=$id;
                    $this->load->view('alert',$data);
                }

                $this->load->view($root . $type, $data);
                break;

            */
            case 'delete_resource':

                $data['id'] = $id = $id / date('Y');

                $t = $this->db->select('title')->from('resources')->where('id',$id)->get()->row();
                $title = $t->title;

                $alert = array();
                if ($this->delete($id, 'resources')) {
                    $alert = array(
                        'alert' => 'success',
                        'message' => 'Record has been Deleted successfully'
                    );

                    $this->custom_library->add_logs('','Resources_delete',$title, 'has deleted resource ');
                }
                else {
                    $alert = array(
                        'alert' => 'danger',
                        'message' => 'An error occurred while performing action'
                    );
                }

                $alert_view = $this->load->view('alert', $alert, true);
                $this->session->set_flashdata('alert', $alert_view);

                redirect($root.'resources');

                break;

            case 'publish':

                $id = $id/date('Y');

                $values['status']='published';
                $values['updated_on']=time();
                $values['updated_by']=$this->session->userdata('id');

                $this->db->where('id',$id)->update('resources',$values);


                $alert=array(
                    'alert'=>'success',
                    'message'=>'Resource has been published'
                );

                $this->custom_library->add_logs('','resource_approve', '' ,' published resource id ('.$id.') ');

                $alert_view = $this->load->view('alert', $alert, true);
                $this->session->set_flashdata('alert', $alert_view);

                redirect($root.'resources');

                break;

            case 'unpublish':

                $id = $id/date('Y');

                $values['status']='unpublished';
                $values['updated_on']=time();
                $values['updated_by']=$this->session->userdata('id');

                $this->db->where('id',$id)->update('resources',$values);


                $alert=array(
                    'alert'=>'success',
                    'message'=>'Resource has been unpublished'
                );

                $this->custom_library->add_logs('','resource_approve', '' ,' unpublished resource id ('.$id.') ');

                $alert_view = $this->load->view('alert', $alert, true);
                $this->session->set_flashdata('alert', $alert_view);

                redirect($root.'resources');

                break;

            case 'preview_resources':
                $data['page_view'] .= $this->load->view($root . $type , $data, true);
                break;

            case 'resource_type':
                /*
                $this->db->select('a.*,d.title as resource_title,d.title as res_type')->from('resources a');
                $this->db->join('resource_types d', 'd.id=a.resource_type');
                $this->db->where(array('a.status' => 'published'));
                $this->db->where('a.resource_type', $id);
                $data['resources'] = $this->db->order_by('a.id', 'desc')->get()->result();
                */

                // $this->load->view($root . $type, $data);
                $data['page_view'] .= $this->load->view($root . 'preview_resources', $data, true);
                break;

            case 'resource_persons':

                $data['page_view'] .= $this->load->view($root . $type, $data, true);
                break;


        }

        $this->load->view('app/static/main_page', $data);

    }//resources

    public function settings($type = null, $id = null)
    {
        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;

        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level
        );

        $this->load->view($page_level . 'static/header', $data);

        switch ($type) {
            //////////////////////// This is the part for the countries ///////////////////////////////////////
            case 'countries':
                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            case 'new_country':
                $this->form_validation
                    ->set_rules('country', 'Country', 'trim|is_unique[selected_countries.a2_iso]');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {
                    $c = $this->db->select()->from('country')->where('a2_iso', $this->input->post('country'))->get()->row();
//                    country, a2_iso, a3_un, num_un, dialing_code, created_by, created_on, a2_iso, id
                    if (isset($c->country)) {
                        $values = array(
                            'country' => $c->country,
                            'a2_iso' => $c->a2_iso,
                            'a3_un' => $c->a3_iso,
                            'num_un' => $c->num_un,
                            'dialing_code' => $c->dialing_code,
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );
                        $this->db->insert('selected_countries', $values);

                        $data['message'] = 'Record has been unblocked from accessing the System successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                    } else {
                        $data['message'] = 'An error occurred during selection of the countries ' . anchor($this->page_level . $this->page_level2 . 'new_country', ' <i class="fa fa-refresh"></i> Try again', 'class="btn green-jungle btn-sm"');
                        $data['alert'] = 'warning';
                        $this->load->view('alert', $data);
                    }
                    $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                } else {


                    $this->load->view('app/' . $this->page_level2 . 'new_country', $data);
                }

                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            //blocking the country
            case 'ban_country':
                if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '0'))) {
                    // $this->db->where('country', $id)->update('branch', array('country_status' => '0'));
                    $data['message'] = 'Country has Been blocked and its Child Branches';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                }


                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            //this is the function for unblocking
            case 'unblock_country':
                if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '1'))) {
                    //$this->db->where('country', $id)->update('branch', array('country_status' => '1'));
                    $data['message'] = 'Country has unblocked and its Child Branches';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                }
                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;
            //deleting_country
            case 'delete_country':
                $this->db->where(array('a2_iso' => $id))->delete('selected_countries');
                $data['message'] = 'Country has Deleted successfully';
                $data['alert'] = 'success';
                $this->load->view('alert', $data);
                $this->load->view('app/' . $this->page_level2 . 'countries', $data);
                break;

            //////////// this is the end of the part for the countries//////////////////////

            case 'regions':

                $this->load->view('app/' . $this->page_level2 . 'regions', $data);
                break;

            case 'resource_types':

                $this->load->view($root . $type, $data);
                break;

            case 'new_resource_type':
                $this->form_validation->set_rules('resource_type', 'Resource Type', 'trim|is_unique[resource_types.title]');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'title' => $this->input->post('resource_type'),
                        'created_by' => $this->session->userdata('id'),
                        'created_on' => time()

                    );
                    $this->db->insert('resource_types', $values);
                    $data['message'] = 'Resource type has been added';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'resource_types', $data);

                    $this->custom_library->add_logs('','resource_types',$this->input->post('resource_type'), 'has added resource type');


                }
                else {


                    $this->load->view($root . 'new_resource_type', $data);
                }

                // $this->load->view($root . 'countries', $data);
                break;

            case 'edit_resource_type':

                $data['id']=$id=$id/date('Y');
                $this->form_validation->set_rules('resource_type', 'Resource Type', 'trim');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'title' => $this->input->post('resource_type'),
                        'updated_by' => $this->session->userdata('id'),
                        'updated_on' => time()

                    );
                    $this->db->where('id',$id)->update('resource_types', $values);
                    $data['message'] = 'Resource type has been edited';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'resource_types', $data);

                    $this->custom_library->add_logs('','resource_types',$this->input->post('resource_type'), 'has edited resource type');


                }
                else {


                    $this->load->view($root . $type, $data);
                }

                // $this->load->view($root . 'countries', $data);
                break;

            case 'delete_resource_type':

                $data['id'] = $id = $id / date('Y');

                $t=$this->db->select('title')->from('resource_types')->where('id',$id)->get()->row();
                $title=$t->title;
                if ($this->delete($id, 'resource_types')) {
                    $data = array(
                        'alert' => 'success',
                        'message' => 'Record has been Deleted successfully'
                    );
                    $this->custom_library->add_logs('','resource_types',$title,' has deleted Publication ');
                }
                else {
                    $data = array(
                        'alert' => 'danger',
                        'message' => 'An error occurred while performing action'
                    );
                }
                $this->load->view('alert', $data);

                $this->load->view($root.'resource_types', $data);

                break;

        }

        $this->load->view($page_level . 'static/footer', $data);

    }

    /**
     * This is where all  the message applies
     * sent message
     * received message
     * inbox
     * templates
     **/
    public function messaging($type = null, $id = null, $table = null)
    {


        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $this->page_level

        );

        $root = $this->page_level . $this->page_level2;

        switch ($type) {
            default:
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_inbox', $data, true);
                break;
            case 'sent':

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_sent', $data, true);
                break;
            case 'compose':
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_compose', $data, true);
                break;
            case 'read':
                $id = $id / date('Y');
                $data['table'] = $table == 'inbox' ? 'inbox' : 'outbox';
                $data['id'] = $id;
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . 'message_read', $data, true);
                break;

        }

        $this->load->view('app/static/main_page', $data);

    }

    public function site_options($type = null, $id = null)
    {

        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $this->page_level

        );
        $root = $this->page_level . $this->page_level2;


        $this->load->view('app/' . 'static/header', $data);

        $data['id'] = $id = $id / date('Y');
        switch ($type) {

            default:

                $this->load->view('app/' . $this->page_level2 . $title, $data);

                break;
            case 'options':

                break;
            case 'edit':


                $data['op'] = $op = $this->db->select()->from('site_options')->where(array('id' => $id))->get()->row();


                $this->form_validation->set_rules('option_value', 'Value', 'trim');

                if ($op->option_name == 'site_logo') {
                    // this is the function which uploads the profile image

                    $this->form_validation->set_rules('image', 'Image', 'trim');
                    //this is the company name

                    //managing of the images
                    $path = $config['upload_path'] = './uploads/site_logo/';
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
                    $config['max_size'] = '1000';
                    $config['max_width'] = '1000';
                    $config['max_height'] = '1000';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }


                }

                if ($this->form_validation->run() == true || $this->upload->do_upload('image') == true) {

                    //echo $op->option_name.' success';
                    if ($op->option_name == 'site_logo') {

                        $value = $this->upload->do_upload('image') == true ? $path . $this->upload->file_name : $op->option_value;
                        $data['error'] = $this->upload->display_errors();

                    } else {
                        $value = $this->input->post('option_value');
                    }


                    if ($this->db->where('id', $id)->update('site_options', array('option_value' => $value))) {

                        $data = array(
                            'alert' => 'success',
                            'message' => 'You have successfully updated site option'
                        );
                        $this->load->view('alert', $data);
                        $this->load->view('app/' . $this->page_level2 . $title, $data);
                    } else {
                        $data = array(
                            'alert' => 'error',
                            'message' => 'an error occurred while updating the the site option'
                        );
                        $this->load->view('alert', $data);
                        $this->load->view('app/' . $this->page_level2 . $type, $data);
                    }


                } else {


                    $data['error'] = $this->upload->display_errors();
                    $this->load->view('app/' . $this->page_level2 . $type, $data);
                }
                break;


        }


        $this->load->view('app/static/footer', $data);

    }

    function logs($type = 'all', $start = 0)
    {
        $this->isloggedin() ? '' : $this->logout();
//        if ($this->custom_libray->role_exist('audit logs', 'group')) {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $title = $this->uri->segment(2),
            'subtitle' => $type,
            'page_level' => $page_level

        );
        $this->load->view('app/' . 'static/header', $data);

        if ($type == 'realtime_monitoring') {
            $this->load->view('app/' . $this->page_level2 . $type, $data);
            //adding Departure
            $this->custom_library->add_logs('', $title, '', 'has listed realtime monitoring Audits');
        } elseif ($type == 'stored_json') {
            $this->load->view($root . $type, $data);
        } else {

            ///////////////////////// this is the begining of the pagination
            $data['number'] = $number = $this->db->count_all('logs');
            $config['uri_segment'] = 4;
            $config['display_pages'] = FALSE;
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['base_url'] = base_url('index.php/' . $this->page_level . '/logs/' . $type);
            $config['total_rows'] = $number;
            $data['per_page'] = $per_page = $config['per_page'] = 15;
            $this->pagination->initialize($config);
            $data['pg'] = $this->pagination->create_links();
            /////////////////////////this is the end of the paginition//////////////////////////

            $this->db->select('a.*,b.first_name,b.last_name')->from('logs a')->join('users b', 'a.created_by=b.id', 'left');
            $type == 'all' ? '' : $this->db->where('transaction_type', $type);
            //// this is when the filter is applied//////////////////
            $this->form_validation
                ->set_rules('from', 'From', 'trim')
                ->set_rules('to', 'To', 'trim')
                ->set_rules('search', 'Search', 'trim');
            if ($this->form_validation->run() == true) {

                $dates = split_date($_REQUEST["date_range"], '~');

                $from = strtotime($dates['date1'] . ' 00:00:00');
                $to = strtotime($dates['date1'] . ' 23:59:59');
                strlen($this->input->post('from')) > 0 ? $this->db->where(array('a.created_on >=' => $from, 'a.created_on <=' => $to)) : '';
                strlen($this->input->post('search')) > 0 ? $this->db->like(array('a.details' => $this->input->post('search')))->or_like(array('a.target' => $this->input->post('search'))) : '';

                strlen($this->input->post('transaction_type')) > 0 ? $this->db->where(array('a.transaction_type' => $this->input->post('transaction_type'))) : '';
            }
            ///////////////this is the end of the filter//////////////
            $rows = $data['tlogs'] = $this->db->order_by('a.id', 'desc')->limit($per_page, $start)->get()->result();
            $data['rows'] = count($rows);
            $this->load->view('app/' . 'logs/logs', $data);

            $this->custom_library->add_logs('', $title, '', 'has listed System Audit Logs');

        }
        $this->load->view('app/' . 'static/footer', $data);
//        } else {
//            redirect($this->page_level);
//        }

    }

    function pass_check($str)
    {
        $pass = $this->db->select('password')->from('users')->where(array('username' => $this->session->userdata('username'), 'password' => hashValue($str)))->get()->row();
        if (!isset($pass->password)) {
            $this->form_validation->set_message('pass_check', ' The %s is incorrect Please type a Correct Password ');
            return false;
        } else {
            return true;
        }
    }

    function profile($type = null)
    {
        $data['title'] = $this->uri->segment(2);
        $data['subtitle'] = $type == null ? 'user_profile' : $type;
        $data['page_view'] = '';

        //$this->load->view('app/static/header', $data);
        if ($type == null) {
            $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);
        } elseif ($type == 'info') {
            $this->form_validation->set_rules('email', 'Email', 'valid_email|trim')
                ->set_rules('phone', 'Phone', 'trim|max_length[13]|min_length[10]')
                ->set_rules('password', 'Current password', 'trim|callback_pass_check')
                ->set_rules('first_name', 'First Name', 'trim')
                ->set_rules('last_name', 'last Name', 'trim');
            if ($this->form_validation->run() == false) {

                $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);
            } else {

                $values = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email')
                );

                $this->db->where('id', $this->session->userdata('id'))->update('users', $values);

                $this->session->set_userdata($values);
                $data['message'] = 'You have successfully Updated your Profile';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);
                $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);

            }
        } elseif ($type == 'change_password') {


            $this->form_validation->set_rules('current_password', 'Current password', 'trim|callback_pass_check')->set_rules('new_pass', 'New Password', 'required|trim|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|trim');
            if ($this->form_validation->run() == false) {

                $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);
            } else {
                $this->db->where('id', $this->session->userdata('id'))->update('users', array('password' => hashValue($this->input->post('new_pass'))));
                $data['message'] = 'You have successfully Changed your password <br/>';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);
                $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);
            }
        } elseif ($type == 'change_avatar') {

            // this is the function which uploads the profile image
            $this->form_validation->set_rules('image', 'Image', 'trim')->set_rules('pa', 'Pas', 'trim');
            //this is the company name

            $cname = $this->session->first_name . ' ' . $this->session->first_name;
            //managing of the images
            $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
            $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
            $config['max_size'] = '200';
            $config['max_width'] = '1920';
            $config['max_height'] = '850';
            $this->upload->initialize($config);


            if (!is_dir($path)) //create the folder if it's not already exists
            {
                mkdir($path, 0777, TRUE);
            }
            if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                $values = array(
                    'photo' => $path . $this->upload->file_name,
                    'updated_on' => time(),
                    'updated_by' => $this->session->userdata('id')
                );
                $this->db->where('id', $this->session->userdata('id'))->update('users', $values);
                $this->session->set_userdata(array('photo' => $path . $this->upload->file_name));
                $data['message'] = 'Image has been updated Successfully';
                $data['alert'] = 'success';
                $data['page_view'] .= $this->load->view('alert', $data, true);

            } else {
                $data['error'] = $this->upload->display_errors();
                $data['page_view'] .= $this->load->view('app/profile/user_profile', $data, true);

            }

        }
        // $this->load->view('app/static/footer');

        $this->load->view('app/static/main_page', $data);
    }

    public function import($type = null)
    {

        $page_level = $this->page_level;
        $root = $page_level . $this->page_level2;
        $data = array(
            'title' => $this->uri->segment(2),
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );
        $this->load->helper('download');
        $this->load->view($page_level . 'static/header', $data);
        switch ($type) {
            case 'resource_persons':
                // this is the function which uploads the profile image
                $this->form_validation->set_rules('file_upload', 'File', 'xss_clean|trim')->set_rules('pa', 'Pas', 'xss_clean|trim');
                //this is the company name


                //managing of the images
                $path = $config['upload_path'] = './uploads/imports/' . $type . '/';
                $config['allowed_types'] = 'xls|xlsx|csv|XLS|XLSX|CSV';
                $config['max_size'] = '200';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }
                if ($this->form_validation->run() == true && $this->upload->do_upload('file_upload') == true) {

                    $values = array(
                        'path' => $path . $this->upload->file_name,
                        'file_name' => $this->upload->file_name,
                        'file_type' => $this->upload->file_type,
                        'file_ext' => $this->upload->file_ext,
                        'file_size' => $this->upload->file_size,
                        'original_name' => $this->upload->orig_name,
                        'created_on' => time(),
                        'created_by' => $this->session->userdata('id')
                    );
                    $this->db->insert('file_uploads', $values);

                    //ignore_user_abort(true); set_time_limit(0);
                    ///////////// this is the begining of the extraction of the files///////////////////
                    $excel = $this->extract_excel_rows($path . $this->upload->file_name);

                    $data['message'] = $this->upload->file_name . ' File is being uploaded Please Wait ';
                    $no = 0;
                    if (strlen(trim($excel[2]['A'])) == 0 || strlen(trim($excel[2]['B'])) == 0 || strlen(trim($excel[2]['D'])) == 0 || strlen(trim($excel[2]['E'])) == 0) {
                        $data['alert'] = 'danger';
                        $data['hide'] = '1';
                        $data['message'] = 'The file you are Uploading is not corresponding with the required file try again !!!
                        <br/> ' . anchor($root . 'resource_persons', 'Upload New');
                        $this->load->view('alert', $data);

                    } else {


                        foreach ($excel as $col => $row) {
                            if ($col > 1) {

                                if ($row['A'] != '' && $row['B'] != '' && $row['C'] != '' && $row['D'] != '' && $row['E'] != '') {

                                    $phone = phone2($row['D']);

                                    $r = $this->db->select('id')->from('resource_persons')->where(array('phone' => $phone,
                                        'email' => $row['E']))->get()->row();

                                    $trans = array(

                                        'full_names' => $row['A'],
                                        'title' => $row['B'],
                                        'area' => $row['C'],
                                        'phone' => $phone,
                                        'email' => $row['E'],
                                        'created_on' => time(),
                                        'created_by' => $this->session->userdata('id'),

                                    );

                                    if (isset($r)) {
                                        $this->db->insert('resource_persons', $trans);
                                    } else {
                                        $this->db->where('id', $r->id)->update('resource_persons', $trans);
                                    }

                                    $data['message'] = 'Data has been extracted Successfully ' . anchor($root . 'resource_persons', 'Upload New');
                                    $data['alert'] = 'success';
                                    $data['hide'] = '1';
                                    // $this->load->view('alert', $data);


                                } else {
                                    $data['message'] = 'Please Fill in all the required Fields in the Excel Sheet  !!! ' . anchor($root . 'resource_persons', 'Upload New');
                                    $data['alert'] = 'danger';
                                    $data['hide'] = '1';
                                    $this->load->view('alert', $data);
                                }
                                $no++;
                            }

                        }
                    }


                    ///message for the successfully upload and extraction of the file
                    $data['message'] = '1.File has been Uploaded Successfully';
                    $data['alert'] = 'success';
                    $data['hide'] = '1';
                    $this->load->view('alert', $data);

                    $this->load->view('app/' . $this->page_level2 . 'file_uploads', $data);

                } else {
                    $data['error'] = $this->upload->display_errors();
                    $this->load->view('app/' . $this->page_level2 . $type, $data);

                }


                break;
            default:

                break;

        }

        $this->load->view($page_level . 'static/footer', $data);
    }

    function extract_excel_rows($file_and_dir)
    {
        $this->load->view('php-excel-reader/Classes/PHPExcel');
        $objPHPExcel = PHPExcel_IOFactory::load($file_and_dir);
        $objPHPExcel->setActiveSheetIndex(0);
        $rows = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        return $rows;
    }

    function dob_check($str)
    {


        $then_year = date('Y', strtotime($str));
        $years = date('Y') - $then_year;

        if (($years <= 12) || ($years >= 70)) {
            $this->form_validation->set_message('dob_check', $years > 70 ?
                '<strong>' . $years . '</strong> Yrs is over age, Years Must be 70 and Below '
                :
                '<strong>' . $years . '</strong> Yrs is Under  Age, Years Must be 12 and Above');
            return false;
        } else {
            return true;
        }

    }

    function checking_current_date($str)
    {
        if ($str >= (date('Y-m-d'))) {
            $this->form_validation->set_message('checking_current_date',
                'The %s cannot be greater than Today');
            return false;
        } else {
            return true;
        }

    }

    //This is the profile function
    function code_check($str)
    {
        $code = $this->db->where(array('user_id' => $this->session->userdata('id'), 'code' => $str))->from('account_verification')->select('code')->get()->row();
        if (isset($code->code)) {
            return true;
        } else {
            $this->form_validation->set_message('code_check', 'The Verification Code you Provided is Wrong');
            return false;
        }

    }

    function permissions($type = null, $id = null)
    {

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_level'] = $this->page_level;

        $data['page_view'] = '';


        switch ($type) {

            default;
            case 'permissions';
                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . $title, $data, true);
                break;

            case 'new':

                isset($id) ? $this->form_validation->set_rules('permission', 'Permission', 'required|trim') : $this->form_validation->set_rules('permission', 'Permission', 'required|trim|is_unique[permissions.title]');

                $this->form_validation->set_rules('group', 'Group', 'required|trim')
                    ->set_rules('description', 'Description', 'trim');
                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'title' => $this->input->post('permission'),
                        'perm_group' => $this->input->post('group'),
                        'perm_desc' => $this->input->post('description'),

                    );


                    $user_id = $this->session->id;

                    if (!isset($id)) {
                        $values['created_on'] = time();
                        $values['created_by'] = $user_id;

                        if ($this->db->insert('permissions', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully added a permission';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }


                    } else {
                        $values['updated_on'] = time();
                        $values['updated_by'] = $user_id;


                        if ($this->db->where('id', $id / date('Y'))->update('permissions', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully Updated a permission';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }

                    }


                } else {
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $title, $data, true);

                break;
            case 'new_role':

                isset($id) ? $this->form_validation->set_rules('role', 'Role', 'required|trim') : $this->form_validation->set_rules('role', 'Role', 'required|trim|is_unique[user_type.title]');

                //checking if the form validation is passed
                if ($this->form_validation->run() == true) {

                    $values = array(
                        'title' => $this->input->post('role')

                    );


                    $user_id = $this->session->id;

                    if (!isset($id)) {
                        $values['created_on'] = time();
                        $values['created_by'] = $user_id;

                        if ($this->db->insert('user_type', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully added a Role';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }


                    } else {
                        $values['updated_on'] = time();
                        $values['updated_by'] = $user_id;


                        if ($this->db->where('id', $id / date('Y'))->update('user_type', $values)) {

                            $data['alert'] = 'success';
                            $data['message'] = 'You have successfully Updated a Role';

                            $data['page_view'] .= $this->load->view('alert', $data, true);
                        }

                    }


                } else {

                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'roles', $data, true);

                break;

            case 'edit_role':
                $data['id'] = $id = $id / date('Y');
                $data['perm'] = $this->db->select()->from('user_type')->where(array('id' => $id))->get()->row();

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new_role', $data, true);
                break;

            case 'remove_role':

                $id = $id / date('Y');

                if ($this->model->remove_role($id)) {

                    $data['alert'] = 'success';
                    $data['message'] = 'Role has been removed successfully';


                    $data['page_view'] .= $this->load->view('alert', $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'roles', $data, true);
                break;

            case 'edit':
                $data['id'] = $id = $id / date('Y');
                $data['perm'] = $this->db->select()->from('permissions')->where(array('id' => $id))->get()->row();

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new', $data, true);
                break;

            case 'roles':
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'remove_permission':

                $id = $id / date('Y');

                if ($this->delete($id, 'role_perm')) {

                    $data['alert'] = 'success';
                    $data['message'] = 'Permission has been removed successfully';


                    $data['page_view'] .= $this->load->view('alert', $data, true);
                }
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'roles', $data, true);
                break;

            case 'role_perm':

                $data['id'] = $id = $id / date('Y');


                $this->form_validation->set_rules('permissions[]', 'Permission', 'trim');

                if ($this->form_validation->run() == true) {

                    //print_r($this->input->post('permissions'));

                    $values = array();
                    foreach ($this->input->post('permissions') as $perm_id) {

                        $select_perm = array(
                            'role_id' => $id,
                            'perm_id' => $perm_id
                        );

                        $res = $this->db->where($select_perm)->from('role_perm')->count_all_results();

                        $res == 0 ? array_push($values, $select_perm) : '';

                    }


                    count($values) > 0 ? $this->db->insert_batch('role_perm', $values) : '';


                    $data['id'] = $id;
                    $data['alert'] = 'success';
                    $data['message'] = 'Roles are added successfully';


                    $data['page_view'] .= $this->load->view('alert', $data, true);

                } else {

                }

                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'perm_group':

                $data['perm_group'] = humanize(str_rot13($id));

                $data['page_view'] = $this->load->view('app/' . $this->page_level2 . $type, $data, true);
                break;

            case 'delete':

                $id = $id / date('Y');

                if ($this->db->where('perm_id', $id)->delete('role_perm')) {
                    if ($this->db->where('id', $id)->delete('permissions')) {


                        $data = array(
                            'alert' => 'success',
                            'message' => 'Permission has been deleted successfully'
                        );
                        $alert = $this->load->view('alert', $data, true);
                        $this->session->set_flashdata('alert', $alert);
                        redirect($this->page_level . 'permissions/perm_group/' . $this->uri->segment(5));
                    }
                }

                break;


        }


        //$this->load->view('static/footer_table', $data);
        $data['page_view'] = $this->load->view('app/static/main_page', $data);
    }

    public function facilities($type = null, $id = null)
    {

        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $root = 'app/' . $this->page_level2;

        switch ($type) {
            default:
                $data['page_view'] = $this->load->view($root . $title, $data, true);
                break;
            case 'edit':
                $data['id'] = $id = $id / date('Y');

                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('facility_name', 'Facility Name', 'trim|required')
                    ->set_rules('owner', 'Owner', 'trim|required')
                    ->set_rules('authority', 'Authority', 'trim|required')
                    ->set_rules('level', 'Level', 'trim|required')
                    ->set_rules('district', 'District', 'trim|required')
                    ->set_rules('county', 'County', 'trim|required')
                    ->set_rules('sub_county', 'Sub County', 'trim|required')
                    ->set_rules('parish', 'Parish', 'trim|required');

                if ($this->form_validation->run() == true) {

                    $values = array(
                        'health_unit' => $this->input->post('facility_name'),
                        'owner' => $this->input->post('owner'),
                        'authority' => $this->input->post('authority'),
                        'level' => $this->input->post('level'),
                        'district' => $this->input->post('district'),
                        'county' => $this->input->post('county'),
                        'sub_county' => $this->input->post('sub_county'),
                        'parish' => $this->input->post('parish'),
//                        'updated_by' => $this->session->id,
//                        'updated_on' => time()
                    );

                    $this->db->where('id', $id)->update('health_facilities', $values);

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully updated the facility'
                    );
                    $data['page_view'] = '';
                    $data['page_view'] .= $this->load->view('alert', $alert, true);
                    $data['page_view'] .= $this->load->view($root . $title, $data, true);


                } else {

                    $data['page_view'] = $this->load->view($root . 'new', $data, true);
                }


                break;
            case 'view':
                $data['id'] = $id = $id / date('Y');

                $data['page_view'] = $this->load->view($root . 'new', $data, true);
                break;
            case 'new':

                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('facility_name', 'Facility Name', 'trim|required')
                    ->set_rules('country', 'Country', 'trim|required')
                    ->set_rules('village', 'Village', 'trim|required')
                    ->set_rules('type', 'Type', 'trim|required')
                    ->set_rules('incharge', 'In Charge', 'trim|required')
                    ->set_rules('phone_incharge', 'Phone In charge', 'trim|required')
                    ->set_rules('reg_officer', 'Reg Officer', 'trim|required')
                    ->set_rules('reg_officer_phone', 'Reg Officer Phone', 'trim|required');

                if ($this->form_validation->run() == true) {

                    $values = array(
                        'name' => $this->input->post('facility_name'),
                        'village' => $this->input->post('village'),
                        'type' => $this->input->post('type'),
                        'inCharge' => $this->input->post('incharge'),
                        'phoneInCharge' => $this->input->post('phone_incharge'),
                        'regOfficer' => $this->input->post('reg_officer'),
                        'phoneRegOfficer' => $this->input->post('reg_officer_phone'),
                        'created_by' => $this->session->id,
                        'created_on' => time()
                    );

                    $this->db->insert('healthfacilities', $values);

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully added a facility'
                    );
                    $data['page_view'] = '';
                    $data['page_view'] .= $this->load->view('alert', $alert, true);
                    $data['page_view'] .= $this->load->view($root . $title, $data, true);


                } else {

                    $data['page_view'] = $this->load->view($root . 'new', $data, true);
                }


                break;

            case 'activate':
            case 'deactivate':
                $data['id'] = $id = $id / date('Y');

                $status = $type == 'activate' ? 1 : 2;
                $this->db->where('id', $id)->update('health_facility', array('status' => $status));

                $alert = array(
                    'alert' => 'success',
                    'message' => 'You have successfully ' . $type . 'd the facility'
                );
                $data['page_view'] = '';
                $data['page_view'] .= $this->load->view('alert', $alert, true);
                $data['page_view'] .= $this->load->view($root . $title, $data, true);
                break;

            case 'view_contact':
                $data['id'] = $id = $id / date('Y');

                $data['page_view'] = $this->load->view($root . 'add_contacts', $data, true);
                break;
            case 'add_contacts':
                $data['id'] = $id = $id / date('Y');

                $this->form_validation
                    ->set_rules('id', 'id', 'trim|required')
                    ->set_rules('first_names[][first_name]', 'First Name', 'trim')
                    ->set_rules('last_names[][last_name]', 'Last Name', 'trim')
                    ->set_rules('contacts[][contact]', 'Contact', 'trim');

                if ($this->form_validation->run() == true) {

                    // print_r($this->input->post('first_names'));

                    $no = 0;

                    $id = $this->input->post('id');
                    $batch_values = array();
                    $batch_update_values = array();
                    foreach ($this->input->post('first_names') as $r) {

                        $first_names = $this->input->post('first_names');
                        $last_names = $this->input->post('last_names');
                        $contacts = $this->input->post('contacts');


                        if ($first_names[$no]['first_name'] != '' && $last_names[$no]['last_name'] != '' && $contacts[$no]['contact']) {

                            $values = array(

                                'first_name' => $first_names[$no]['first_name'],
                                'last_name' => $last_names[$no]['last_name'],
                                'contact' => phone($contacts[$no]['contact']),
                                'facility_id' => $id,
                                'created_on' => time(),
                                'created_by' => $this->session->id

                            );


                            $exist = $this->db->where(array(
                                    'contact' => phone($contacts[$no]['contact']),
                                    //'facility_id'=>$id
                                )
                            )->from('facility_contact')->count_all_results();

//                               $exist==0?array_push($batch_values,$values):array_push($batch_update_values,$values);
                            $exist == 0 ? array_push($batch_values, $values) : '';

                        }


                        $no++;
                    }


                    count($batch_values) > 0 ? $this->db->insert_batch('facility_contact', $batch_values) : '';

                    count($batch_update_values) > 0 ? $this->db->update_batch('facility_contact', $batch_update_values, 'contact') : '';

                    $rows = $this->db->affected_rows();

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully submitted ' . $rows . ' contacts to a facility'
                    );
                    $data['page_view'] = '';
                    $data['page_view'] .= $this->load->view('alert', $alert, true);
                    $data['page_view'] .= $this->load->view($root . $title, $data, true);


                } else {

                    $data['page_view'] = $this->load->view($root . $type, $data, true);
                }


                break;
            case 'remove_contact':

                $id = $id / date('Y');


                if ($this->db->where(array('id' => $id))->delete('facility_contact')) {
                    redirect($this->page_level . $this->page_level2 . 'view/' . $this->uri->segment(5));

                }

                break;
        }


        $this->load->view('app/static/main_page', $data);

    }

    public function administrative_units($type = null, $id = null)
    {


        $data['title'] = $title = $this->uri->segment(2);
        $data['subtitle'] = $type;
        $data['page_view'] = '';


        switch ($type) {


            default:
            case 'regions':
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'regions', $data, true);
                break;


            case 'new_region':


                $this->form_validation
                    ->set_rules('id', 'id', 'trim')
                    ->set_rules('region', 'Region', 'trim')
                    ->set_rules('districts[]', 'Districts', 'trim|required');


                if ($this->form_validation->run() == true) {


                    $values = array(
                        'region' => $this->input->post('region'),
                        'districts' => implode(',', $this->input->post('districts[]')),
                    );

                    if (strlen($this->input->post('id')) == 0) {

                        $values['created_on'] = time();
                        $values['created_by'] = $this->session->id;

                    } else {

                        $id = $this->input->post('id');
                        $values['updated_on'] = time();
                        $values['updated_by'] = $this->session->id;

                    }


                    strlen($this->input->post('id')) == 0 ? $this->db->insert('project_regions', $values) : $this->db->where('id', $id)->update('project_regions', $values);

                    $alert = array(
                        'alert' => 'success',
                        'message' => 'You have successfully saved data'
                    );
                    $data['page_view'] .= $g = $this->load->view('alert', $alert, true);


                    $alert = $this->load->view('alert', $data, true);
                    $this->session->set_flashdata('alert', $alert);

                    redirect($this->page_level . $this->page_level2 . 'regions');


                } else {
                    $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new_region', $data, true);
                }

                break;

            case 'edit_region':
            case 'view_region':
                $data['id'] = $id = $id / date('Y');
                $data['page_view'] .= $this->load->view('app/' . $this->page_level2 . 'new_region', $data, true);
                break;


        }

        $this->load->view('app/static/main_page', $data);

    }

    function w()
    {

        //$workplan = $this->workplan_lib->get_children(1);
//print_r($workplan);

        //$path = $this->workplan_lib->get_path(53);

       // print_r($path);


//        strtotime()


    }

    function update_dimension($period = 'weekly')
    {

        $this->load->helper('dhis2_json');
        $this->load->model('dhis2_model');

        $this->load->view('app/dhis2_reports/dimension_update/' . $period);

        $data['update_type'] = $period;
        $this->load->view('app/dhis2_reports/malaria_json', $data);
    }

    function update_dimension_filter($period = 'weekly')
    {


        $this->load->model('dhis2_model');
        $level=2;
        $org_unit='akV6429SUqu';
       $d= $this->dhis2_model->get_malaria_data($period,$level,$org_unit);

       print_r($d);


    }

    function update_org_units($level = 1)
    {

        $this->load->helper('dhis2_json');
        $this->load->model('dhis2_model');

        $org_unit_url = "organisationUnits.json?pageSize=20000&fields=id,code,name,shortName,openingDate,parent&filter=level:eq:$level";



        $json_data = get_dhis2_Array($org_unit_url);
        $j = json_decode($json_data);


        $dataValues = $j->organisationUnits;

//        print_r($dataValues);

        $this->dhis2_model->create_org_unit_with_level($level,$dataValues);


    }

    function missing_hfs()
    {

        $sql = "select a.name,a.id,a.parent from  org_unit a  where a.level=5 and a.name not in( select health_unit_fullname from health_facilities ) order by a.parent;";

        echo '<table border="1"  cellspacing="0">
        <thead><tr><th>Name</th><th>id</th><th>Parent</th><th>Subcounty</th><th>District</th><th>Region</th><th>data-type</th></tr></thead>
        <tbody';

        foreach ($this->db->query($sql)->result() as $r):


            $subcounty = $this->get_location($r->parent);
            $district = $this->get_location($subcounty->parent);
            $region = $this->get_location($district->parent);


            echo "<tr><td>$r->name</td><td>$r->id</td><td>$r->parent</td><td>$subcounty->name</td><td>$district->name</td><td>$region->name</td><td>dhis</td></tr>";
           foreach ($mapd_data=$this->get_mapd_hfs($subcounty->name,$district->name) as $m) {

               echo "<tr><td>$m->health_unit_fullname</td><td></td><td></td><td>$m->sub_county</td><td>$m->district</td><td>$m->region</td><td>mapd</td></tr>";
           }

        endforeach;
        echo '</tbody></table>';

    }

    function get_location($parent)
    {

        $location = $this->db->select('name,parent')->from('org_unit')->where(array('id' => $parent))->get()->row();

        return $location;

    }

    function get_mapd_hfs($subcounty,$district)
    {

        $subcounty=strtoupper(trim(str_replace('Subcounty','',$subcounty)));
        $district=strtoupper(trim(str_replace('District','',$district)));

        $location = $this->db->select()->from('health_facilities')->where(array('sub_county' => $subcounty,'district' => $district))->get()->result();

        return $location;

    }

    public function error()
    {

        $this->load->view('404');
    }

}
